import logging, os, re
from DeviceVal import *
from Serial import *
from TestCase import *

Logger = logging.getLogger('DeviceVal_Logger.Test_Cases')

### Test Cases
###

class Tests:

	TestList = [
		('led_tests', 'LED tests'),
	]

	TestList = [
		('led_tests', 'LED tests'),
	]
		
	@Test_Case
	def led_tests():
		# disable comm/gps leds
		Serial_Cmd('AT$APP PARAM 1024,20,8,8', 'OK') 
		rc = Operator_Instruction('Comm and GPS LEDs should be off')
		# set blink pattern = num satellites acquired
		sats = int(re.search('^Sats.*:\s*(.*)', Serial_Cmd('ATIG', '^Sats')).group(1))
		Serial_Cmd('AT$APP PARAM 1024,51,8,8', 'OK')  
		rc = Operator_Instruction('Repeating blink pattern [%d] for acquired satellites' % (sats))
		# ?? turning off leds
		# 1024,51,40,40
		# led blinks twice, pause, 4 blinks
		
	# peg action 146 ACT_HIBERNATE_ALT
		
Run_Tests(Tests)