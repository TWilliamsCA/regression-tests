import json, logging, re
import DeviceVal
import TestCase
from TestCase import *
from Serial import *

Logger = logging.getLogger('DeviceVal_Logger.Test_Cases')

########################################
### Params
###

class Params:

	Name = 'Params'

	TestList = [
		('param_771_retry_sched', 'Inbound message retries'),
	]

	TestList = [
		('param_771_retry', 'Inbound message retries'),
	]

	
	@Test_Case
	@Debug
	def param_771_retry_sched():
		# set 5 short retries.  before 6 enable connection.  verify msg received.
	
Run_Tests(Params)
