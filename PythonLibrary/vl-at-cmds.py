import json, logging, re
import DeviceVal
import TestCase
from TestCase import *
from Serial import *

Logger = logging.getLogger('DeviceVal_Logger.Test_Cases')

########################################
### Params
###

class AT_Cmds:

	Name = 'AT Commands'

	TestList = [
		('cmd_+++', 'Hayes - terminate PPP/SLIP session'),
		('cmd_at&c', 'Carrier Detect settings'),
		('cmd_at&d', 'Date Terminal Ready settings'),
		('cmd_at+ipr', 'Hayes - Query host port baud rate'),
		('cmd_at+ipr_q', 'Hayes - Query host port baud rate'),
		('cmd_at+ipr=q', 'Hayes - Query available host port baud rates'),
		('cmd_at#factory', 'Return modem to factory settings'),
		('cmd_at$app_acc', 'Query accumulators'),
		('cmd_at$app_acc_set', 'Set an accumulator'),
		('cmd_at$app_adc', 'Query ADCs'),
		('cmd_at$app_addr', 'Query current ip address'),
		('cmd_at$app_addr_inbound', 'Set inbound ip address'),
		('cmd_at$app_bt', 'Query Bluetooth device status'),
		('cmd_at$app_clear_emasks', 'Clear all environment masks'),
		('cmd_at$app_clear_events', 'Clear all events'),
		('cmd_at$app_comm', 'Query comm status'),
		('cmd_at$app_comm_connect', 'Query comm connection'),
		('cmd_at$app_comm_index', 'Set the active comm index'),
		('cmd_at$app_comm_index_q', 'Query the active comm index'),
		('cmd_at$app_comm_status', 'Query detailed comm status'),
		('cmd_at$app_default', 'Default the modem settings'),
		('cmd_at$app_default_all', 'Default all LMU settings'),
		('cmd_at$app_default_config', 'Default all LMU configuration settings'),
		('cmd_at$app_default_emasks', 'Default all LMU environment masks'),
		('cmd_at$app_default_events', 'Default all LMU events'),
		('cmd_at$app_default_sregs', 'Default all LMU S-register settings'),
		('cmd_at$app_default_tparams', 'Default all LMU trigger params'),
		('cmd_at$app_dial', 'Set a dial packet string'),
		('cmd_at$app_dial_q', 'Query active dial packet strings'),
		('cmd_at$app_dns', 'Perform DNS lookup for URL'),
		('cmd_at$app_dns_q', 'Query DNS servers info'),
		('cmd_at$app_dns_inbound', 'Perform DNS lookup on Inbound URL'),
		('cmd_at$app_dns_maint', 'Perform DNS lookup on Maintenance URL'),
		('cmd_at$app_dns_server', 'Set DNS server ip address'),
		('cmd_at$app_esn', 'Query serial number'),
		('cmd_at$app_flags', 'Query flag states'),
		('cmd_at$app_gprs_context', 'Set APN values for GPRS network'),
		('cmd_at$app_gprs_context_index', 'Set active GPRS context index'),
		('cmd_at$app_gprs_context_q', 'Query APN values for GPRS network'),
		('cmd_at$app_gps', 'Query GPS location'),
		('cmd_at$app_gps_pos', 'Override GPS position reading'),
		('cmd_at$app_gps_speed', 'Override GPS speed reading'),
		('cmd_at$app_gps_status', 'Query GPS status'),
		('cmd_at$app_gz_erase', 'Erase geo-zone memory space'),
		('cmd_at$app_gz_read', 'Read geo-zone points'),
		('cmd_at$app_gz_write', 'Write a geo-zone point'),
		('cmd_at$app_inbound', 'Set inbound ip address and port'),
		('cmd_at$app_inbound_q', 'Query inbound settings'),
		('cmd_at$app_input', 'Query input states'),
		('cmd_at$app_log', 'Query detailed log status'),
		('cmd_at$app_log_batch', 'Set log mode to SNF'),
		('cmd_at$app_log_status', 'Query detailed log status'),
		('cmd_at$app_log_snf', 'Set log mode to Batch'),
		('cmd_at$app_mobid', 'Set the mobile id type'),
		('cmd_at$app_mobid_min', 'Set the mobile id MIN value'),
		('cmd_at$app_mobid_user', 'Set the mobile id User value'),
		('cmd_at$app_mobid', 'Set the mobile id type'),
		('cmd_at$app_mobid_q', 'Query the mobile id type'),
		('cmd_at$app_modem_activate', 'Reset a CDMA modem'),
		('cmd_at$app_modem_cmd', 'Pass AT cmd to modem'),
		('cmd_at$app_modem_iota', 'Reset a CDMA modem for Sprint device'),
		('cmd_at$app_modem_min', 'Program phone number for CDMA modem'),
		('cmd_at$app_modem_min_q', 'Query phone number for CDMA modem'),
		('cmd_at$app_modem_update', 'Reset a CDMA modem'),
		('cmd_at$app_msg', 'Force unit to send user msg'),
		('cmd_at$app_output', 'Control an output'),
		('cmd_at$app_output_q', 'Query output states'),
		('cmd_at$app_param', 'Set a parameter value'),
		('cmd_at$app_param_q', 'Query a parameter value'),
		('cmd_at$app_param_begin', 'Enter batch mode for programming params'),
		('cmd_at$app_param_end', 'Exit batch mode for programming params'),
		('cmd_at$app_password', 'Set the LMU password'),
		('cmd_at$app_peg_action', 'Perform a PEG action'),
		('cmd_at$app_peg_commsel', 'Set active comm index and inbound settings'),
		('cmd_at$app_peg_logrpt', 'Force unit to log event report'),
		('cmd_at$app_peg_sleep', 'Force unit to sleep'),
		('cmd_at$app_peg_sndalrt', 'Force unit to send alert report'),
		('cmd_at$app_peg_sndid', 'Force unit to send id report'),
		('cmd_at$app_peg_sndrpt', 'Force unit to send event report'),
		('cmd_at$app_peg_sndsms', 'Force unit to send event report using SMS'),
		('cmd_at$app_peg_sndtaip', 'Force unit to send TAIP format event report'),
		('cmd_at$app_peg_snfrpt', 'Force unit to send event report'),
		('cmd_at$app_peg_speed', 'Simulate speed threshold crossing'),
		('cmd_at$app_peg_sunrpt', 'Force unit to send unacked event report'),
		('cmd_at$app_pin', 'Set the SIM pin'),
		('cmd_at$app_pin_q', 'Query the SIM pin'),
		('cmd_at$app_ppp', 'Set network credentials '),
		('cmd_at$app_ppp_q', 'Query network credentials '),
		('cmd_at$app_s', 'Set an S-register value'),
		('cmd_at$app_s_q', 'Query an S-register value'),
		('cmd_at$app_serial', 'Configure LMU for MDT mode'),
		('cmd_at$app_sendto', 'Force unit to send id report to specific address'),
		('cmd_at$app_sms', 'Send text message via SMS'),
		('cmd_at$app_state_q', 'Query the PEG states'),
		('cmd_at$app_taip_radio', 'Configure LMU to send TAIP msg'),
		('cmd_at$app_time_q', 'Query current time'),
		('cmd_at$app_time_daylight', 'Set the local daylight savings time mode'),
		('cmd_at$app_time_offset', 'Set the local time offset from GMT'),
		('cmd_at$app_timers', 'Query timer states'),
		('cmd_at$app_unit', 'Query unit status'),
		('cmd_at$app_ver', 'Query version info'),
		('cmd_at$app_zones', 'Query detailed zone states'),
		('cmd_at$pw', 'Enter AT cmd password'),
		('cmd_atd', 'Hayes - dial commands'),
		('cmd_ate0', 'Hayes - disable echo'),
		('cmd_ate1', 'Hayes - enable echo'),
		('cmd_ati0', 'Query firmware version info'),
		('cmd_ati1', 'Query radio info'),
		('cmd_ati2', 'Query radio technology info'),
		('cmd_ati3', 'Query comm info'),
		('cmd_ati4', 'Query GPS status'),
		('cmd_ati5', 'Query comm status'),
		('cmd_atib', 'Query battery status'),
		('cmd_atic', 'Query detailed comm status'),
		('cmd_atig', 'Query detailed GPS status'),
		('cmd_atij', 'Query Google Maps URL'),
		('cmd_atil', 'Query detailed log status'),
		('cmd_atl', 'Hayes - modem speaker volume'),
		('cmd_atl', 'Hayes - modem speaker control'),
		('cmd_atq', 'Hayes - quiet mode'),
		('cmd_atv0', 'Hayes - enable verbose response format'),
		('cmd_atv1', 'Hayes - enable terse response format'),
		('cmd_atx', 'Hayes - call progress result code'),
		('cmd_atz', 'Hayes - reset Hayes settings'),
	]

	TestList = [
		('cmd_atz', 'Reset Hayes settings'),
	]

	
	@Test_Case
	@Debug
	def cmd_at&c():
		# has no effect
		Serial_Cmd('AT&C', '^OK')
	
	@Test_Case
	@Debug
	def cmd_+++():
		# enter a PPP session
		Serial_Cmd('+++', '^OK')
		# verify session terminated
	
	@Test_Case
	@Debug
	def cmd_at&d():
		# do not disconnect data session if dtr drops
		Serial_Cmd('AT&D1', '^OK')
		# disconnect data session if dtr drops
		Serial_Cmd('AT&D2', '^OK')
	
	@Test_Case
	@Debug
	def cmd_at+ipr():
		Serial_Cmd('AT+IPR=<baud rate>', '^OK')
		# uh .. if you change probably can't talk to lmu ...
	
	@Test_Case
	@Debug
	def cmd_at+ipr_q():
		Serial_Cmd('AT+IPR?', '^OK')
		# verify baud rate against another cmd
	
	@Test_Case
	@Debug
	def cmd_at+ipr=q():
		Serial_Cmd('AT+IPR=?', '^OK')
		# verify a couple rates available

	@Test_Case
	@Debug
	def cmd_at#factory():
		Serial_Cmd('AT#FACTORY', '^OK')
		# check with pete how to verify.  resets modem only?

	@Test_Case
	@Debug
	def cmd_at$app_acc():
		Serial_Cmd('AT$APP ACC?', '^OK')
		# set some different accum values.  verify them.

	@Test_Case
	@Debug
	def cmd_at$app_acc_set():
		Serial_Cmd('AT$APP ACC SET <acc> <value>', '^OK')
		# set some different accum values.  verify them.

	@Test_Case
	@Debug
	def cmd_at$app_adc():
		Serial_Cmd('AT$APP ADC?', '^OK')
		# verify voltages from assembly info. use wide range

	@Test_Case
	@Debug
	def cmd_at$app_addr():
		Serial_Cmd('AT$APP ADDR?', '^OK')
		# set ip addr. verify it.

	@Test_Case
	@Debug
	def cmd_at$app_addr_inbound():
		Serial_Cmd('AT$APP ADDR INBOUND <ip addr>', '^OK')		# set inbound ip addr. use it.

	@Test_Case
	@Debug
	def cmd_at$app_bt():
		Serial_Cmd('AT$APP BT?', '^OK')
		# verify against expected data

	@Test_Case
	@Debug
	def cmd_at$app_clear_emasks():
		# set up a few emasks
		Serial_Cmd('AT$APP CLEAR EMASKS', '^OK')
		# verify they are cleared??  talk to pete.

	@Test_Case
	@Debug
	def cmd_at$app_clear_events():
		# set up a few events
		Serial_Cmd('AT$APP CLEAR EVENTS', '^OK')
		# verify events are gone

	@Test_Case
	@Debug
	def cmd_at$app_comm():
		Serial_Cmd('AT$APP COMM? ', '^OK')
		# verify when comm inactive, active

	@Test_Case
	@Debug
	def cmd_at$app_comm_connect():
		Serial_Cmd('AT$APP COMM CONNECT? ', '^OK')
		# verify when comm inactive, active

	@Test_Case
	@Debug
	def cmd_at$app_comm_index():
		Serial_Cmd('AT$APP COMM INDEX <index> ', '^OK')
		Serial_Cmd('AT$APP COMM INDEX? ', '^OK')
		# use the comm index

	@Test_Case
	@Debug
	def cmd_at$app_comm_index_q():
		Serial_Cmd('AT$APP COMM INDEX <index>', '^OK')
		Serial_Cmd('AT$APP COMM INDEX? ', '^OK')
		# set the index, read it.

	@Test_Case
	@Debug
	def cmd_at$app_comm_status():
		Serial_Cmd('AT$APP COMM STATUS?', '^OK')
		# verify all fields filled in, didn't change.

	@Test_Case
	@Debug
	def cmd_at$app_default():
		Serial_Cmd('AT$APP DEFAULT <type>', '^OK')
		# talk to pete how to verify.

	@Test_Case
	@Debug
	def cmd_at$app_default_all():
		Serial_Cmd('AT$APP DEFAULT ALL', '^OK')
		# talk to pete how to verify.

	@Test_Case
	@Debug
	def cmd_at$app_default_config():
		Serial_Cmd('AT$APP DEFAULT CONFIG', '^OK')
		# talk to pete how to verify.

	@Test_Case
	@Debug
	def cmd_at$app_default_emasks():
		Serial_Cmd('AT$APP DEFAULT EMASKS', '^OK')
		# talk to pete how to verify.

	@Test_Case
	@Debug
	def cmd_at$app_default_events():
		Serial_Cmd('AT$APP DEFAULT EVENTS', '^OK')
		# talk to pete how to verify.

	@Test_Case
	@Debug
	def cmd_at$app_default_sregs():
		Serial_Cmd('AT$APP DEFAULT SREGS', '^OK')
		# talk to pete how to verify.

	@Test_Case
	@Debug
	def cmd_at$app_default_tparams():
		Serial_Cmd('AT$APP DEFAULT TPARAMS', '^OK')
		# talk to pete how to verify.

	@Test_Case
	@Debug
	def cmd_at$app_dial():
		Serial_Cmd('AT$APP DIAL <indx> <dial string>', '^OK')
		Serial_Cmd('AT$APP DIAL? ', '^OK')
		# use the dial string

	@Test_Case
	@Debug
	def cmd_at$app_dial_q():
		Serial_Cmd('AT$APP DIAL <indx> <dial string>', '^OK')
		Serial_Cmd('AT$APP DIAL <indx> <dial string>', '^OK')
		Serial_Cmd('AT$APP DIAL? ', '^OK')
		# verify dial strings

	@Test_Case
	@Debug
	def cmd_at$app_dns():
		# setup dns server
		Serial_Cmd('AT$APP DNS <url>', '^OK')
		# lookup a url

	@Test_Case
	@Debug
	def cmd_at$app_dns_q():
		# set up dns servers
		Serial_Cmd('AT$APP DNS?', '^OK')
		# verify dns server info

	@Test_Case
	@Debug
	def cmd_at$app_dns_inbound():
		# set up dns inbound url
		Serial_Cmd('AT$APP DNS INBOUND', '^OK')
		# verify dns lookup

	@Test_Case
	@Debug
	def cmd_at$app_dns_maint():
		# set up dns maint url
		Serial_Cmd('AT$APP DNS MAINT', '^OK')
		# verify dns lookup

	@Test_Case
	@Debug
	def cmd_at$app_dns_server():
		Serial_Cmd('AT$APP DNS SERVER <ip addr>', '^OK')
		# verify addr set and use it.

	@Test_Case
	@Debug
	def cmd_at$app_esn():
		Serial_Cmd('AT$APP ESN? ', '^OK')
		# verify esn

	@Test_Case
	@Debug
	def cmd_at$app_flags():
		Serial_Cmd('AT$APP FLAGS? ', '^OK')
		# set some peg flags.  verify them.

	@Test_Case
	@Debug
	def cmd_at$app_gprs_context():
		Serial_Cmd('AT$APP GPRS CONTEXT <index> <context> ', '^OK')	
		Serial_Cmd('AT$APP GPRS CONTEXT? ', '^OK')
		# verify context info.  use the context.

	@Test_Case
	@Debug
	def cmd_at$app_gprs_context_index():
		Serial_Cmd('AT$APP GPRS CONTEXT <index> <context> ', '^OK')	
		Serial_Cmd('AT$APP GPRS CONTEXT INDEX <index>', '^OK')
		# verify context info.  use the context.

	@Test_Case
	@Debug
	def cmd_at$app_gprs_context_q():
		Serial_Cmd('AT$APP GPRS CONTEXT <index> <context> ', '^OK')	
		Serial_Cmd('AT$APP GPRS CONTEXT?', '^OK')
		# verify context info.

	@Test_Case
	@Debug
	def cmd_at$app_gps():
		Serial_Cmd('AT$APP GPS?', '^OK')
		# set gps location.  verify it.

	@Test_Case
	@Debug
	def cmd_at$app_gps_pos():
		Serial_Cmd('AT$APP GPS POS <lat> <long>', '^OK')
		Serial_Cmd('ATIG', '^OK')
		# set gps location.  verify it.

	@Test_Case
	@Debug
	def cmd_at$app_gps_speed():
		Serial_Cmd('AT$APP GPS SPEED <speed>', '^OK')
		Serial_Cmd('ATIG', '^OK')
		# set gps location.  verify it.

	@Test_Case
	@Debug
	def cmd_at$app_gps_status():
		Serial_Cmd('AT$APP GPS STATUS?', '^OK')
		# verify gps from canned value?

	@Test_Case
	@Debug
	def cmd_at$app_gz_erase():
		# load some gz values
		Serial_Cmd('AT$APP GZ ERASE', '^OK')
		# verify gz values erased

	@Test_Case
	@Debug
	def cmd_at$app_gz_read():
		# load some gz values
		Serial_Cmd('AT$APP GZ WRITE <index> <type> <id> <range> <lat> <long>', '^OK')
		Serial_Cmd('AT$APP GZ READ <index> <count>', '^OK')
		# write a zero record, then add a few more points
		Serial_Cmd('AT$APP GZ WRITE <index> <type> <id> <range> <lat> <long>', '^OK')
		# verify gz values displayed.  use the values.

	@Test_Case
	@Debug
	def cmd_at$app_gz_write():
		# load some gz values
		Serial_Cmd('AT$APP GZ WRITE <index> <type> <id> <range> <lat> <long>', '^OK')
		# verify gz values displayed

	@Test_Case
	@Debug
	def cmd_at$app_inbound():
		Serial_Cmd('AT$APP INBOUND <ip addr>:<port>', '^OK')
		Serial_Cmd('AT$APP INBOUND <msg format>', '^OK')
		Serial_Cmd('AT$APP INBOUND? ', '^OK')
		# set inbound ip:port. use it.
		# test msg format

	@Test_Case
	@Debug
	def cmd_at$app_inbound_q():
		Serial_Cmd('AT$APP ADDR INBOUND <ip addr>', '^OK')
		Serial_Cmd('AT$APP INBOUND <ip addr>:<port>', '^OK')
		Serial_Cmd('AT$APP INBOUND <msg format>', '^OK')
		Serial_Cmd('AT$APP INBOUND <index> ADDR <value>', '^OK')
		Serial_Cmd('AT$APP INBOUND <index> URL <value>', '^OK')
		Serial_Cmd('AT$APP INBOUND INDEX <index>', '^OK')
		Serial_Cmd('AT$APP INBOUND? ', '^OK')
		# set some inbound values values.  verify them.
		# * is active index

	@Test_Case
	@Debug
	def cmd_at$app_inbound_index():
		Serial_Cmd('AT$APP INBOUND <index> ADDR <value>', '^OK')
		Serial_Cmd('AT$APP INBOUND INDEX <index>', '^OK')
		# verify index is used for outbound.

	@Test_Case
	@Debug
	def cmd_at$app_input():
		Serial_Cmd('AT$APP INPUT?', '^OK')
		# set some input state, bias values.  verify them.

	@Test_Case
	@Debug
	def cmd_at$app_log():
		Serial_Cmd('AT$APP LOG?', '^OK')
		# fill log.  verify pct
		# change log params. verify.

	@Test_Case
	@Debug
	def cmd_at$app_log_status():
		Serial_Cmd('AT$APP LOG STATUS? ', '^OK')
		# fill log.  verify some values.

	@Test_Case
	@Debug
	def cmd_at$app_log_snf():
		Serial_Cmd('AT$APP LOG BATCH', '^OK')
		# verify log mode changed by status cmd

	@Test_Case
	@Debug
	def cmd_at$app_log_snf():
		Serial_Cmd('AT$APP LOG SNF', '^OK')
		# verify log mode changed by status cmd

	@Test_Case
	@Debug
	def cmd_at$app_mobid():
		Serial_Cmd('AT$APP MOBID <type>', '^OK')
		Serial_Cmd('AT$APP MOBID?', '^OK')
		# verify mobile id type being used.  talk to pete.

	@Test_Case
	@Debug
	def cmd_at$app_mobid_min():
		Serial_Cmd('AT$APP MOBID MIN <value>', '^OK')
		Serial_Cmd('AT$APP MOBID MIN', '^OK')
		Serial_Cmd('AT$APP MOBID?', '^OK')
		# verify mobile min being used

	@Test_Case
	@Debug
	def cmd_at$app_mobid_user():
		Serial_Cmd('AT$APP MOBID USER <value>', '^OK')
		Serial_Cmd('AT$APP MOBID USER', '^OK')
		Serial_Cmd('AT$APP MOBID?', '^OK')
		# verify mobile user being used

	@Test_Case
	@Debug
	def cmd_at$app_mobid_q():
		Serial_Cmd('AT$APP MOBID <type>', '^OK')
		Serial_Cmd('AT$APP MOBID?', '^OK')
		# verify log mode changed by status cmd

	@Test_Case
	@Debug
	def cmd_at$app_modem_activate():
		# CDMA only.  operator must support activation thru PRL dial string
		Serial_Cmd('AT$APP MODEM ACTIVATE', '^OK')
		# ask Pete about verify

	@Test_Case
	@Debug
	def cmd_at$app_modem_cmd():
		Serial_Cmd('AT$APP MODEM CMD <cmd>', '^OK')
		Serial_Cmd('AT$APP MODEM CMD <cmd>', '^OK')
		Serial_Cmd('AT', 'OK')
		# at+gmm.  try a few

	@Test_Case
	@Debug
	def cmd_at$app_modem_iota():
		# CDMA Sprint only. activation thru IOTA session.
		Serial_Cmd('AT$APP MODEM IOTA', '^OK')
		# ask Pete about verify

	@Test_Case
	@Debug
	def cmd_at$app_modem_min():
		# CDMA only. program phone number for 1xrtt modem.
		Serial_Cmd('AT$APP MODEM MIN <phone #> <SPC> <MSID>', '^OK')
		# see at cmd set wiki doc.  talk to pete.

	@Test_Case
	@Debug
	def cmd_at$app_modem_min_q():
		# CDMA only.
		Serial_Cmd('AT$APP MODEM MIN <phone #> <SPC> <MSID>', '^OK')
		Serial_Cmd('AT$APP MODEM MIN?', '^OK')

	@Test_Case
	@Debug
	def cmd_at$app_modem_update():
		# CDMA only.  operator must support activation thru PRL dial string
		Serial_Cmd('AT$APP MODEM UPDATE', '^OK')
		# ask Pete about verify

	@Test_Case
	@Debug
	def cmd_at$app_msg():
		Serial_Cmd('AT$APP MSG <byte> <disposition>', '^OK')
		# verify msg sent.  try each disposition.  

	@Test_Case
	@Debug
	def cmd_at$app_output():
		Serial_Cmd('AT$APP OUTPUT <output> <state>', '^OK')
		Serial_Cmd('AT$APP OUTPUT?', '^OK')
		# set some output states.  verify them.

	@Test_Case
	@Debug
	def cmd_at$app_output_q():
		Serial_Cmd('AT$APP OUTPUT <output> <state>', '^OK')
		Serial_Cmd('AT$APP OUTPUT?', '^OK')
		# set some output states.  verify them.

	@Test_Case
	@Debug
	def cmd_at$app_param():
		Serial_Cmd('AT$APP PARAM <something ..>', '^OK')
		Serial_Cmd('AT$APP PARAM? <something ..>', '^OK')
		# ask pete if some generic test that makes sense

	@Test_Case
	@Debug
	def cmd_at$app_param_q():
		Serial_Cmd('AT$APP PARAM <something ..>', '^OK')
		Serial_Cmd('AT$APP PARAM? <something ..>', '^OK')
		# ask pete if some generic test that makes sense

	@Test_Case
	@Debug
	def cmd_at$app_param_begin():
		Serial_Cmd('AT$APP PARAM BEGIN', '^OK')
		Serial_Cmd('AT$APP PARAM <something ..>', '^OK')
		Serial_Cmd('AT$APP PARAM? <something ..>', '^OK')
		Serial_Cmd('AT$APP PARAM END', '^OK')
		Serial_Cmd('AT$APP PARAM? <something ..>', '^OK')
		# verify that param doesn't take effect until end.  does param? do it?

	@Test_Case
	@Debug
	def cmd_at$app_param_end():
		Serial_Cmd('AT$APP PARAM BEGIN', '^OK')
		Serial_Cmd('AT$APP PARAM <something ..>', '^OK')
		Serial_Cmd('AT$APP PARAM? <something ..>', '^OK')
		Serial_Cmd('AT$APP PARAM END', '^OK')
		Serial_Cmd('AT$APP PARAM? <something ..>', '^OK')
		# verify that param doesn't take effect until end.  does param? do it?

	@Test_Case
	@Debug
	def cmd_at$app_password():
		Serial_Cmd('AT$APP PASSWORD <dd.dd.dd.dd>', '^OK')
		# used to authenticate incoming msgs? talk to pete ..

	@Test_Case
	@Debug
	def cmd_at$app_peg_action():
		Serial_Cmd('AT$APP PEG ACTION <action> <param>', '^OK')
		# verify action happened
		# peg actions tested separately

	@Test_Case
	@Debug
	def cmd_at$app_peg_commsel():
		# The lower nibble (4 bits) defines the Inbound Index, the upper nibble defines the Comm Index
		Serial_Cmd('AT$APP PEG COMMSEL <index/index>', '^OK')
		# verify the indexes are being used

	@Test_Case
	@Debug
	def cmd_at$app_peg_logrpt():
		Serial_Cmd('AT$APP PEG LOGRPT <event code>', '^OK')
		# verify report is sent in log mode

	@Test_Case
	@Debug
	def cmd_at$app_peg_sleep():
		# set timer
		Serial_Cmd('AT$APP PEG SLEEP <timer index>', '^OK')
		# verify sleep

	@Test_Case
	@Debug
	def cmd_at$app_peg_sndalrt():
		Serial_Cmd('AT$APP PEG SNDALRT <event code>', '^OK')
		# verify alert msgs sent.  ask pete what should be sent.

	@Test_Case
	@Debug
	def cmd_at$app_peg_sndid():
		Serial_Cmd('AT$APP PEG SNDID <source port>', '^OK')
		# verify id report sent.  check for each source port.  ask pete what/how to test.

	@Test_Case
	@Debug
	def cmd_at$app_peg_sndsms():
		Serial_Cmd('AT$APP PEG SNDSMS <event code>', '^OK')
		# verify event report sent to SMS inbound addr

	@Test_Case
	@Debug
	def cmd_at$app_peg_sndrpt():
		Serial_Cmd('AT$APP PEG SNDRPT <event code>', '^OK')
		# verify report trigger w that event code

	@Test_Case
	@Debug
	def cmd_at$app_peg_sndtaip():
		Serial_Cmd('AT$APP PEG SNDTAIP <event code>', '^OK')
		# verify event report sent in taip format

	@Test_Case
	@Debug
	def cmd_at$app_peg_snfrpt():
		Serial_Cmd('AT$APP PEG SNFRPT <event code>', '^OK')
		# verify report trigger w that event code

	@Test_Case
	@Debug
	def cmd_at$app_peg_speed():
		# simulates speed for Speed Above/Below triggers
		Serial_Cmd('AT$APP PEG SPEED <speed>', '^OK')
		# verify can cause speed trigger to fire

	@Test_Case
	@Debug
	def cmd_at$app_peg_sunrpt():
		Serial_Cmd('AT$APP PEG SUNRPT <event code>', '^OK')
		# verify report send and is not acked

	@Test_Case
	@Debug
	def cmd_at$app_pin():
		Serial_Cmd('AT$APP PIN', '^OK')
		Serial_Cmd('AT$APP PIN?', '^OK')
		# use the sim pin??

	@Test_Case
	@Debug
	def cmd_at$app_pin_q():
		Serial_Cmd('AT$APP PIN', '^OK')
		Serial_Cmd('AT$APP PIN?', '^OK')
		# verify sim pin

	@Test_Case
	@Debug
	def cmd_at$app_ppp():
		Serial_Cmd('AT$APP PPP <index> USER <username>', '^OK')
		Serial_Cmd('AT$APP PPP <index> PASSWORD <username>', '^OK')
		Serial_Cmd('AT$APP PPP?', '^OK')
		# use the credentials to access network

	@Test_Case
	@Debug
	def cmd_at$app_ppp_q():
		Serial_Cmd('AT$APP PPP <index> USER <username>', '^OK')
		Serial_Cmd('AT$APP PPP <index> PASSWORD <username>', '^OK')
		Serial_Cmd('AT$APP PPP?', '^OK')
		# verify credentials

	@Test_Case
	@Debug
	def cmd_at$app_s():
		Serial_Cmd('AT$APP S<reg>=<value>', '^OK')
		Serial_Cmd('AT$APP S<reg>?', '^OK')
		# verify s-regs being set. s-regs tested elsewhere

	@Test_Case
	@Debug
	def cmd_at$app_s_q():
		Serial_Cmd('AT$APP S<reg>=<value>', '^OK')
		Serial_Cmd('AT$APP S<reg>?', '^OK')
		# verify s-regs being set. s-regs tested elsewhere

	@Test_Case
	@Debug
	def cmd_at$app_serial():
		Serial_Cmd('AT$APP SERIAL <mode>', '^OK')
		# check with mdt team about how to test

	@Test_Case
	@Debug
	def cmd_at$app_sendto():
		Serial_Cmd('AT$APP SENDTO <ipaddr:port> <format>', '^OK')
		# test each format.  check with pet about what to test.

	@Test_Case
	@Debug
	def cmd_at$app_sms():
		Serial_Cmd('AT$APP SMS <dest> <msg>', '^OK')
		# verify msg received at dest

	@Test_Case
	@Debug
	def cmd_at$app_state_q():
		Serial_Cmd('AT$APP STATE?', '^OK')
		# verify msg received at dest

	@Test_Case
	@Debug
	def cmd_at$app_taip_radio():
		Serial_Cmd('AT$APP TAIP RADIO', '^OK')
		Serial_Cmd('ATRESET', '')
		# talk pete how to test

	@Test_Case
	@Debug
	def cmd_at$app_time_q():
		Serial_Cmd('AT$APP TIME?', '^OK')
		# verify gmt time against external
		# verify local time by setting tz
		# verify time for diff available sources??

	@Test_Case
	@Debug
	def cmd_at$app_time_daylight():
		Serial_Cmd('AT$APP TIME DAYLIGHT 0', '^OK')
		# check local time disabled
		Serial_Cmd('AT$APP TIME DAYLIGHT 1', '^OK')
		# check local time enabled

	@Test_Case
	@Debug
	def cmd_at$app_time_offset():
		Serial_Cmd('AT$APP TIME?', '^OK')
		Serial_Cmd('AT$APP TIME OFFSET <offset>', '^OK')
		Serial_Cmd('AT$APP TIME?', '^OK')
		# check local time changed by offset

	@Test_Case
	@Debug
	def cmd_at$app_timers():
		Serial_Cmd('AT$APP TIMERS?', '^OK')
		# set some timers.  verify them.

	@Test_Case
	@Debug
	def cmd_at$app_unit():
		Serial_Cmd('AT$APP UNIT? ', '^OK')
		# check if format changed, all fields fill in.

	@Test_Case
	@Debug
	def cmd_at$app_ver():
		Serial_Cmd('AT$APP VER? ', '^OK')
		# check if format changed, all fields fill in.

	@Test_Case
	@Debug
	def cmd_at$app_zones():
		Serial_Cmd('AT$APP ZONES? ', '^OK')
		# setup some zones.  verify them.
	
	@Test_Case
	@Debug
	def cmd_at$pw():
		# set up a password
		Serial_Cmd('AT$PW <pwd>', '^OK')
		Serial_Cmd('AT', '^OK')
	
	@Test_Case
	@Debug
	def cmd_atd():
		Serial_Cmd('ATD0', '^OK')
		Serial_Cmd('ATDP0', '^OK')
		Serial_Cmd('ATDT0', '^OK')
		# check with Pete ...
		# verify invalid at cmd
	
	@Test_Case
	@Debug
	def cmd_ate0():
		Serial_Cmd('ATE0', '^OK')
		# verify cmds no longer echo in passthru mode
	
	@Test_Case
	@Debug
	def cmd_ate1():
		Serial_Cmd('ATE1', '^OK')
		# verify cmds echo in passthru mode
	
	@Test_Case
	@Debug
	def cmd_ati0():
		Serial_Cmd('ATIO', '^OK')
		# check if format changed, all fields fill in.
	
	@Test_Case
	@Debug
	def cmd_ati1():
		Serial_Cmd('ATI1', '^OK')
		# check if format changed, all fields fill in.
	
	@Test_Case
	@Debug
	def cmd_ati2():
		Serial_Cmd('ATI2', '^OK')
		# check if format changed, all fields fill in.
	
	@Test_Case
	@Debug
	def cmd_ati3():
		Serial_Cmd('ATI3', '^OK')
		# check if format changed, all fields fill in.
	
	@Test_Case
	@Debug
	def cmd_ati4():
		Serial_Cmd('ATI4', '^OK')
		# check if format changed, all fields fill in.
	
	@Test_Case
	@Debug
	def cmd_ati5():
		Serial_Cmd('ATI5', '^ERROR')
		# not supported, at least on 3030
	
	@Test_Case
	@Debug
	def cmd_atib():
		Serial_Cmd('ATIB', '^OK')
		# supported on 700, 1200 series devices.
		# check if format changed, all fields filled in.
	
	@Test_Case
	@Debug
	def cmd_atic():
		Serial_Cmd('ATIC', '^OK')
		# check if format changed, all fields filled in.
	
	@Test_Case
	@Debug
	def cmd_atic():
		Serial_Cmd('ATIG', '^OK')
		# verify gps against canned value??
	
	@Test_Case
	@Debug
	def cmd_atij():
		Serial_Cmd('ATIJ', '^OK')
		# how is this used by device?

	@Test_Case
	@Debug
	def cmd_atil():
		Serial_Cmd('ATIL', '^OK')
		# fill log.  verify some values.

	@Test_Case
	@Debug
	def cmd_atm():
		# Hayes cmd not supported
		Serial_Cmd('ATM0', '^OK')
		Serial_Cmd('ATM9', '^OK')

	@Test_Case
	@Debug
	def cmd_atl():
		# Hayes cmd not supported
		Serial_Cmd('ATL0', '^OK')
		Serial_Cmd('ATL9', '^OK')

	@Test_Case
	@Debug
	def cmd_atq():
		# Hayes cmds not supported
		Serial_Cmd('ATQ0', '^OK')
		Serial_Cmd('ATQ1', '^OK')

	@Test_Case
	@Debug
	def cmd_atv0():
		Serial_Cmd('ATV0', '^OK')
		# verify only numeric codes returned in passthru

	@Test_Case
	@Debug
	def cmd_atv0():
		Serial_Cmd('ATV1', '^OK')
		# verify text msgs returned in passthru

	@Test_Case
	@Debug
	def cmd_atx():
		# Hayes cmd not supported
		Serial_Cmd('ATX0', '^OK')
		Serial_Cmd('ATX9', '^OK')

	@Test_Case
	@Debug
	def cmd_atz():
		# has no effect
		Serial_Cmd('ATZ', '^OK')
	
Run_Tests(AT_Cmds)
