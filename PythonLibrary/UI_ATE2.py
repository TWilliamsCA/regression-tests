import clr
clr.AddReference('PresentationFramework')
clr.AddReference('PresentationCore')
clr.AddReference('WindowsBase')
clr.AddReference('System.Windows.Presentation')
import atexit, ConfigParser, datetime, logging, os, re, socket, signal, SocketServer, subprocess, threading, time, wpf, pickle
from datetime import datetime
from System import Action, Console
from System.Windows import Application, MessageBox, Window, Media
from System.Windows.Documents import Paragraph, Run
from System.Windows.Media.Effects import BlurBitmapEffect
from System.Windows.Threading import Dispatcher, DispatcherExtensions
from threading import Timer
import ctypes, System
from Version import __version__

clr.AddReference('System')
from System.Collections.ObjectModel import *
from System.ComponentModel import *
from pyevent import *

##--------------------------------------------
## Constants
##--------------------------------------------
conFail = ' FAIL ' * 25
conPass = ' PASS ' * 25

LOG_DIR = 'C:/CalAmpAte2/logs/'
Dir_ATE_DB = 'c:/CalAmpAte/db'
Dir_Last_Label = 'c:\\CalAmpAte2\\BarTender\\Last_Label\\*'
Dir_Scan_Label = 'c:\\Commander\\Scan'

gValue = ""
bClearLog = True
bJP1Log = False
bRun = False
gScriptSecCnt = 0
gSubp = None
bNoPower = False

gModel = gPart = gCRAF = gScript = gCust = ''
gScriptPath = ''
gConfParser = ConfigParser.ConfigParser()

gparms_file = 'c:/CalAmpAte2/DB/parms.txt'
gcookie_file = 'c:/CalAmpAte2/DB/cookie.txt'

class NotifyPropertyChangedBase(INotifyPropertyChanged):
	PropertyChanged = None
	def __init__(self):
		(self.PropertyChanged, self._propertyChangedCaller) = make_event()

	def add_PropertyChanged(self, value):
		self.PropertyChanged += value

	def remove_PropertyChanged(self, value):
		self.PropertyChanged -= value

	def OnPropertyChanged(self, propertyName):
		self._propertyChangedCaller(self, PropertyChangedEventArgs(propertyName))

class PartRec(NotifyPropertyChangedBase):
	@property
	def Model(self):
		return self._Model

	@Model.setter
	def Model(self, value):
		self._Model = value
		# self.OnPropertyChanged("Model")

	@property
	def Part(self):
		return self._Part

	@Part.setter
	def Part(self, value):
		self._Part = value
		# self.OnPropertyChanged("Part")

	@property
	def Customer(self):
		return self._Customer

	@Customer.setter
	def Customer(self, value):
		self._Customer = value
		#  self.OnPropertyChanged("Customer")

	@property
	def Script(self):
		return self._Script

	@Script.setter
	def Script(self, value):
		self._Script = value
		# self.OnPropertyChanged("Script")

class StepRec(NotifyPropertyChangedBase):
	@property
	def IsSelected(self):
		return self._IsSelected

	@IsSelected.setter
	def IsSelected(self, value):
		self._IsSelected = value
		self.OnPropertyChanged("IsSelected")

	@property
	def Type(self):
		return self._Type

	@Type.setter
	def Type(self, value):
		self._Type = value
		# self.OnPropertyChanged("Type")

	@property
	def Lib(self):
		return self._Lib

	@Lib.setter
	def Lib(self, value):
		self._Lib = value
		# self.OnPropertyChanged("Lib")

	@property
	def Name(self):
		return self._Name

	@Name.setter
	def Name(self, value):
		self._Name = value
		# self.OnPropertyChanged("Name")

class MyWindow(Window):

	gWorkstationName = os.environ['COMPUTERNAME']
	gUsername = os.environ['USERNAME']

	def __init__(self):
		wpf.LoadComponent(self, 'ATE2.xaml')
		self.ATE_Status.FontSize = 36
		self.UpdateStatusInfo(" Validation is fun!")
		self.ATE_Log.FontSize = 12
		self.stepData = ObservableCollection[StepRec]()
		self.ATE_Steps2.ItemsSource = self.stepData
		self.data = ObservableCollection[PartRec]()
		self.ATE_Parts2.ItemsSource = self.data
		self.Get_Parts(self)

	def Cancel_OnClick(self, sender, e):
		global bRun, gSubp, bNoPower
		if bRun == False: return
		bRun = False
		dt = time.asctime()
		w.UpdateStatusFail(" Script Canceled...")
		w.UpdateLog('*'*80, color = Media.Brushes.Red, fontSize = 18)
		w.UpdateLog('Script Canceled - %s' % (dt), color = Media.Brushes.Red, fontSize = 18)
		w.UpdateLog('*'*80, color = Media.Brushes.Red, fontSize = 18)
		w.btnStart.IsEnabled = True
		# send a SIGBREAK to script process
		ctypes.windll.kernel32.GenerateConsoleCtrlEvent(1, gSubp.pid)

	def Clear_Checked(self, sender, e):
		global bClearLog
		bClearLog = True

	def Clear_Unchecked(self, sender, e):
		global bClearLog
		bClearLog = False
		
	def Get_Parts(w, self):
		f = open('C:/CalAmpAte2/DB/parts.conf', 'r')
		for rec in f:
			newPartRec = PartRec()
			flds = rec.strip().split(':')
			newPartRec.Model = flds[0]
			newPartRec.Part = flds[0]
			newPartRec.Customer = flds[0]
			newPartRec.Script = flds[0]
			self.data.Add(newPartRec)

	def JP1_Checked(self, sender, e):
		global bJP1Log
		bJP1Log = True
		w.WriteParms()

	def JP1_Unchecked(self, sender, e):
		global bJP1Log
		bJP1Log = False
		w.WriteParms()
			
	def NoTouch_OnClick(self, sender, e):
		if w.ATE_Screen.BitmapEffect != None:
			w.ATE_Screen.BitmapEffect = None
			return
		myBlurEffect = BlurBitmapEffect()
		myBlurEffect.Radius = 50
		w.ATE_Screen.BitmapEffect = myBlurEffect
				
	def NoPower_Checked(self, sender, e):
		global bNoPower
		bNoPower = True
		w.WriteParms()

	def NoPower_Unchecked(self, sender, e):
		global bNoPower
		bNoPower = False
		w.WriteParms()

	def ProcessScript(w, self):
		global gScriptPath
		self.stepData.Clear()
		f = open(gScriptPath, 'r')
		skip = False
		for line in f:
			if re.search('### resume ###', line):
				skip = False
			if re.search('### skip ###', line) or skip == True:
				skip = True
				continue
			m = re.match('^(Local)_(Test|Step)_(\w*)', line)
			if not m:
				m = re.match('^(.*?)\.(Test|Step)_(\w*)', line)
			if m:
				if re.match('^def', m.group(1)): continue
				if re.match('^Start$|^End$|^Fail$', m.group(3)): continue
				newStepRec = StepRec()
				newStepRec.IsSelected = 'True'
				newStepRec.Type = m.group(2)
				lib = re.sub('^\s*', '', m.group(1))
				newStepRec.Lib = lib if lib != '' else 'Script'
				newStepRec.Name = m.group(3)
				self.stepData.Add(newStepRec)
		f.close()

	def Reprint_OnClick(self, send, e):
		global Dir_Last_Label, Dir_Scan_Label
		os.system('copy /Y %s %s' % (Dir_Last_Label, Dir_Scan_Label))
		w.UpdateStatusInfo(" Label reprinted")

	def SelectPart_OnClick(self, sender, e):
		global gModel, gPart, gCust, gScript, bNoPower, gScriptPath
		gModel = w.ATE_Parts2.SelectedItem.Model
		gPart = w.ATE_Parts2.SelectedItem.Part
		gCust = w.ATE_Parts2.SelectedItem.Customer
		gScript = w.ATE_Parts2.SelectedItem.Script
		w.txtModel.Text = 'Model: ' + gModel
		w.txtPN.Text = 'PN: ' + gPart
		w.txtCust.Text = 'Cust: ' + gCust
		w.txtScript.Text = 'Script: ' + gScript
		gScriptPath = 'c:/CalAmpAte2/models/' + gModel + '/scripts/' + gScript + '.py'
		gConfParser.read('c:/CalAmpAte2/models/' + gModel + '/configs/' + gPart + '.ini')
		craf_appId = gConfParser.get('CRAF', 'LMU App ID')
		craf_fwVer = gConfParser.get('CRAF', 'LMU FW Ver')
		craf_custAppVer = gConfParser.get('CRAF', 'Customer App Ver')
		assy_ProcType = gConfParser.get('ASSEMBLY', 'Processor Type')
		
		if re.match('STM32', assy_ProcType):
			w.JP1.IsEnabled = True
		else:
			w.JP1.IsEnabled = False
			w.JP1.IsChecked = False
			
		w.txtFW.Text = 'FW: ' + craf_appId + '/' + craf_fwVer
		w.txtCSV.Text = 'CSV: ' + craf_custAppVer
		if gModel == '':
			w.UpdateStatusInfo(" Welcome Back ...")
		else:
			w.ProcessScript(self)
			w.UpdateStatusInfo(" You have chosen wisely ...")
			
			# Set for First Time Run
			msg = ["0"]
			# ATE2.Cookie_Set(msg)
			f = open(gcookie_file, 'wb')
			pickle.dump(msg,f)
			f.close()
			
		w.ATE_Log.Text = ""
		w.ATE_Tabs.SelectedIndex = 0
		w.WriteParms()

	def SelectTests_All_OnClick(self, sender, e):
		for rec in w.ATE_Steps2.ItemsSource:
			rec.IsSelected = 'True'

	def SelectTests_ClearAll_OnClick(self, sender, e):
		for rec in w.ATE_Steps2.ItemsSource:
			rec.IsSelected = 'False'
			
	def SelectTests_ClearTests_OnClick(self, sender, e):
		for rec in w.ATE_Steps2.ItemsSource:
			if rec.Type == 'Test':
				rec.IsSelected = 'False'

	def SelectTests_SetDefaults_OnClick(self, sender, e):
		return
		
	def Start_OnClick(self, sender, e):
		global bRun, gSubp, gModel, gScript, gPart
		if gModel == '':
			w.UpdateStatusUser(' Make selection from Parts menu ...')
			return
		self.UpdateStatusRunning(" Running Script ...")
		if bClearLog == True:
			self.ATE_Log.Text = ""
		## start logging
		root = logging.getLogger()
		if root.handlers:
			for handler in root.handlers:
				root.removeHandler(handler)
		dtStr = re.sub('-', '', str(datetime.now().date()))
		logging.basicConfig(filename='%sWS%s_%s_%s-%s-ui' % (LOG_DIR, Ws, Loc, gPart, dtStr), level=logging.DEBUG, format='%(asctime)s : %(message)s')
		## print header
		dt = time.asctime()
		# get model version
		f = open('C:/CalampATE2/Models/' + gModel + '/version.py', 'r')
		rec = f.readline().strip()
		f.close()
		modelVersion = re.search('.*\'(.*)\'', rec).group(1)
		w.UpdateLog('*'*80, color = Media.Brushes.Purple, fontSize = 18)
		w.UpdateLog("Script Started [%s] - %s" % (gScript, dt), color = Media.Brushes.Purple, fontSize = 18)
		w.UpdateLog('Workstation : %s, Username : %s' % (self.gWorkstationName, self.gUsername))
		w.UpdateLog('Model: %s, Lib Version: %s, Model Version: %s' % ( gModel, __version__, modelVersion))
		w.UpdateLog('*'*80, color = Media.Brushes.Purple, fontSize = 18)
		## check syntax of script
		scriptPath = 'c:/CalAmpAte2/models/' + gModel + '/scripts/' + gScript + '.py'
		out = subprocess.Popen('python -m py_compile ' + scriptPath, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		rc = out.wait()
		scriptErr = False
		for line in out.stderr.readlines():
			scriptErr = True
			line = line.decode('utf-8').strip('\r\n')
			w.UpdateLog(line)
		if scriptErr:
			self.UpdateStatusFail('Script Syntax Errors')
			return
		# export list of Test/Steps active
		f = open('C:/CalAmpAte2/DB/tests.txt', 'w')
		for rec in w.ATE_Steps2.ItemsSource:
			f.write(rec.Type + '_' + rec.Name + ' ' + rec.IsSelected + '\n')
		f.close()
		w.Title = 'Validator 1.0 (Version %s) (%s -- Last run: %s)' % (__version__, os.environ['COMPUTERNAME'], time.asctime())
		###############
		## run script
		###############
		bRun = True
		w.btnStart.IsEnabled = False
		w.UpdateScriptTimer(init=True)
		print('STARTING ' * 15)
		filename='%s%s-%s-detail' % (LOG_DIR, gPart, datetime.now().date())
		print(filename)
		p = subprocess.Popen('cmd /c C:/Python34/python ' + scriptPath, shell=False, creationflags=512)
		print('*** Running - pid = %d' % (p.pid))
		gSubp = p

	def TimerScriptCnt_Fired(w):
		DispatcherExtensions.BeginInvoke(dispatcher, Action(w.UpdateScriptTimer))

	def UpdateLog__Common(w, text, color, fontSize):
		if re.search('^Log', text):
			text = re.sub('^Log:*', '', text)
		if re.search('^Info', text):
			text = re.sub('^Info:*', '', text)
		r = Run()
		r.Text = ' ' + text + "\n"
		r.Foreground = color
		r.FontSize = fontSize
		w.ATE_Log.Inlines.Add(r)
		w.ATE_Log_Scroll.UpdateLayout()
		w.ATE_Log_Scroll.ScrollToBottom()
		logging.debug(r.Text.strip())
		
	def UpdateLog(w, text, color=Media.Brushes.Black, fontSize = 12):
		w.UpdateLog__Common(text, color, fontSize)

	def UpdateLog_Info(w, text, color=Media.Brushes.Green, fontSize = 12):
		w.UpdateLog__Common(text, color, fontSize)

	def UpdateLog_Info_Big(w, text, color=Media.Brushes.Green, fontSize = 28):
		w.UpdateLog__Common(text, color, fontSize)
		
	def UpdateLog_Error(w, text, color=Media.Brushes.Crimson, fontSize = 12):
		text = re.sub('^LogError:', '', text)
		w.UpdateLog__Common(text, color, fontSize)

	def UpdateLog_Step(w):
		global gValue
		gValue = re.sub('^Log', '', gValue)
		w.UpdateLog(gValue, color=Media.Brushes.DarkGoldenrod, fontSize = 16)

	def UpdateLog_SubStep(w):
		global gValue
		gValue = re.sub('^LogSubStep:', '', gValue)
		w.UpdateLog('   ' + gValue, color=Media.Brushes.Blue, fontSize = 16)

	def UpdateLog_Test(w):
		global gValue
		gValue = re.sub('^LogTest:\s*', '', gValue)
		w.UpdateLog(gValue, color=Media.Brushes.Green, fontSize = 16)

	def UpdateLog_Warning(w, text, color=Media.Brushes.Orange, fontSize = 12):
		w.UpdateLog__Common(text, color, fontSize)
		
	def UpdateResult(w):
		global bRun, gValue
		if re.search("Pass", gValue):
			w.UpdateStatusPass()
			color = Media.Brushes.Green
		else:
			w.UpdateStatusFail(conFail)
			color = Media.Brushes.Crimson
		w.UpdateLog(gValue, color=color, fontSize = 18)
		dt = time.asctime()
		w.UpdateLog('*'*80, color = Media.Brushes.Purple, fontSize = 18)
		w.UpdateLog("Script Completed - %s" % (dt), color = Media.Brushes.Purple, fontSize = 18)
		w.UpdateLog('*'*80, color = Media.Brushes.Purple, fontSize = 18)
		bRun = False
		## script could kill itself.  try to kill if still alive
		try:
			gSubp.terminate()
		except Exception:
			pass
		w.btnStart.IsEnabled = True

	def UpdateScriptTimer(w, init=False):
		global bRun, gScriptSecCnt
		if init == True:
			gScriptSecCnt = 0
		else:
			gScriptSecCnt += 1
		min = gScriptSecCnt/60
		sec = gScriptSecCnt % 60
		w.ScriptTimer.Content = "%d:%02d" % (min, sec)
		if bRun == True:
			tmrScript = Timer(1.0, w.TimerScriptCnt_Fired)
			tmrScript.start()

	def UpdateStatusFail(w, str):
		w.ATE_Status.Background = Media.Brushes.Crimson
		w.ATE_Status.Text = str

	def UpdateStatusInfo(w, str):
		w.ATE_Status.Background = Media.Brushes.LightBlue
		w.ATE_Status.Text = str

	def UpdateStatusPass(w):
		w.ATE_Status.Background = Media.Brushes.LightGreen
		w.ATE_Status.Text = conPass

	def UpdateStatusRunning(w, str):
		w.ATE_Status.Background = Media.Brushes.Orange
		w.ATE_Status.Text = str

	def UpdateStatusUser(w, str):
		w.ATE_Status.Background = Media.Brushes.Yellow
		w.ATE_Status.Text = str
		
	def Update_UI_Field(w):
		global gValue
		val = re.sub('.*;', '', gValue)
		if re.search('MfgTracking;', gValue):
			w.txtMfgTrack.Text = 'Mfg Tracking: ' + val
		elif re.search('WorkOrder;', gValue):
			w.txtWorkOrder.Text = 'Work Order: ' + val
		elif re.search('SalesOrder;', gValue):
			w.txtSalesOrder.Text = 'Sales Order: ' + val
			
	def WorkStation_Config_Get(w):
		#BKMB - New function for Workstation information
		# location = 2 chars, ws num = 4 chars
		wsCfg = 'c:\\CalAmpATE\\Workstation\wsGroupID.cfg'
		# get workstation info
		f = open(wsCfg, 'r')
		line = f.readline()
		f.close()
		ws, loc = re.search('ID:(\d*).(\d*)', line).group(1,2)
		ws = ws.rjust(4, '0') 
		loc = loc.rjust(2, '0')
		return loc, ws

	def WriteParms(w):
		global gModel, gPart, gCust, bNoPower, bJP1Log
		# use file to send parms to script until passed when script started
		f = open(gparms_file, 'w')
		f.write(gModel + ":" + gPart + ":" + gCust + ":" + gScript + ":" + str(bNoPower) + ":" + __version__ + ":" + str(bJP1Log) + '\n')
		f.close()
		
####################
## Server
####################
class ScriptTCPHandler(SocketServer.StreamRequestHandler):

	def handle(self):
		global gValue
		self.data = self.rfile.readline().strip()
		gValue = self.data
		DispatcherExtensions.BeginInvoke(dispatcher, Action(UIaction))

def Script_Server_Kill(server):
	server.socket.close()

def Script_Server():
	HOST, PORT = "localhost", 9999

	server = SocketServer.TCPServer((HOST, PORT), ScriptTCPHandler)
	if server is None:
		MessageBox.Show("Error - Server did not start")

	atexit.register(Script_Server_Kill, server)
	server.serve_forever()

def UIaction():
	if re.match("Log:", gValue):
		w.UpdateLog(gValue)
	elif re.match("LogError:", gValue):
		w.UpdateLog_Error(gValue)
	elif re.match("LogInfo:", gValue):
		w.UpdateLog_Info(gValue)
	elif re.match("LogKen Says:", gValue):
		w.UpdateLog_Info_Big(gValue)
	elif re.match("LogStep:", gValue):
		w.UpdateLog_Step()
	elif re.match("LogSubStep:", gValue):
		w.UpdateLog_SubStep()
	elif re.match("LogTest:", gValue):
		w.UpdateLog_Test()
	elif re.match("LogWarning:", gValue):
		w.UpdateLog_Warning(gValue)
	elif re.match("Result:", gValue):
		w.UpdateResult()
	elif re.match("SimStart:", gValue):
		w.SimStart()
	elif re.match("UiUpdate", gValue):
		w.Update_UI_Field()
	else:
		w.UpdateStatusFail(" Error : Unexpected UI message")
		w.UpdateLog_Error(gValue)

#########################################################################

if __name__ == '__main__':

	## make sure only one UI running

	## start server to accept msgs from script
	tScript_Server = threading.Thread(target=Script_Server)
	tScript_Server.daemon = "True"
	tScript_Server.start()
	dispatcher = Dispatcher.CurrentDispatcher
	## start app
	w = MyWindow()
	w.Title = 'Validator 1.0 (Version %s)' % __version__
	Ws, Loc = w.WorkStation_Config_Get()
	Application().Run(w)
