import json, logging, re
import DeviceVal
import TestCase
import os
import time
from TestCase import *
from Serial import *

Logger = logging.getLogger('DeviceVal_Logger.Test_Cases')

### Test Cases
###


class BasicTests:

	Name = 'Basic Tests'

	TestList = [
		('accum_gps_off', 'Accumulate time when GPS off'),
		('accumulator_restore_on_wakeup', 'Restore accumulator on wakeup'),
		('autoconnect_on_powerup', 'Auto-connect on power-up'),
		('fill_log_reset', 'Fill log reset'),
		('log_batchmode', 'Log Batchmode'),
		('info_cmds', 'Information cmds'),
		('ota_firmware_download', 'OTA firmware download'),
		('ota_obd_fw_db','OTA FW & BD download'),
		('ota_config_update','OTA Config Update'), #lance try this now <-- 
		('peg_event_report', 'PEG - Event report'),
		('peg_id_report', 'PEG - ID report'),
		('recover_system_time_after_wakeup', 'Recover system time'), 
		('peg_timeouts','Peg timer timeouts'), 
		('peg_sleep_wakeup_timer', 'PEG - Sleep wakeup on timer'),
		('peg_triggers', 'PEG triggers'),
		('sms_param_msg', 'Send SMS param msg'),
		('comm_on_off', 'COMM ON/OFF'),
		('gps_on_off', 'GPS ON/OFF'),
		('sms_unit_request_msgs', 'Send SMS unit request msg'),
		('sms_vehicle_bus_msgs', 'Send SMS vehicle bus msg'),
		('wakeup_on_time_of_day', 'Wakeup on time-of-day'),
		#('auto_apn', 'Auto APN'), #need to fix
		('factory_defaults', 'Factory defaults'),
		
		('system_time_src_3D_GPS', 'System time does not sync to none-3D GPS fix time, S174 bit 4'),
		('system_time_src_reject_CellNetwork','System time does not sync to network time'),
		('system_time_src_reject_Server','System time does not sync to Server time'),
		('system_time_src_reject_None_3D_GPS','System time does not sync to none-3D GPS time'),
		('system_time_src_reject_RTC','System time does not sync to RTC time'),
		('system_time_src_reject_3D_GPS','System time does not sync to 3D-GPS time'),
		('system_time_src_Server','System time syncs to Server time'),
		
		('kevins_test','Kevins Test'),
		('p2560_test','checking only accum'),
		('parameter_acceptance','Checking Bad Parameters'),
		('lmu3200wifi', 'WIFI'),
		('script_loading', 'script loading')
	]

	TestList = [
		#('ota_firmware_download', 'OTA firmware download'),
		#('kevins_test','Kevins Test'),
		#('factory_defaults', 'Factory defaults'),
		#('sms_param_msg', 'Send SMS param msg'),
		#('sms_unit_request_msgs', 'Send SMS unit request msg'),
		# ('system_time_src_3D_GPS', 'System time does not sync to none-3D GPS fix time, S174 bit 4'),
		# ('system_time_src_reject_CellNetwork','System time does not sync to network time'),
		# ('system_time_src_reject_Server','System time does not sync to Server time'),
		# ('system_time_src_reject_None_3D_GPS','System time does not sync to none-3D GPS time'),
		# ('system_time_src_reject_RTC','System time does not sync to RTC time'),
		# ('system_time_src_reject_3D_GPS','System time does not sync to 3D-GPS time'),
		# ('system_time_src_Server','System time syncs to Server time'),
		('accum_gps_off', 'Accumulate time when GPS off'),
		('accumulator_restore_on_wakeup', 'Restore accumulator on wakeup'),
		('autoconnect_on_powerup', 'Auto-connect on power-up'),
		('fill_log_reset', 'Fill log reset'),
		#('log_batchmode', 'Log Batchmode'),
		('info_cmds', 'Information cmds'),
		#('ota_firmware_download', 'OTA firmware download'),
		#('ota_obd_fw_db','OTA FW & BD download'),
		#('ota_config_update','OTA Config Update'), #lance try this now <-- 
		('peg_event_report', 'PEG - Event report'),
		('peg_id_report', 'PEG - ID report'),
		('recover_system_time_after_wakeup', 'Recover system time'), 
		('peg_timeouts','Peg timer timeouts'), 
		('peg_sleep_wakeup_timer', 'PEG - Sleep wakeup on timer'),
		('peg_triggers', 'PEG triggers'),
		#('sms_param_msg', 'Send SMS param msg'),
		('comm_on_off', 'COMM ON/OFF'),
		('gps_on_off', 'GPS ON/OFF'),
		#('sms_unit_request_msgs', 'Send SMS unit request msg'),
		#('sms_vehicle_bus_msgs', 'Send SMS vehicle bus msg'),
		('wakeup_on_time_of_day', 'Wakeup on time-of-day'),
		#('auto_apn', 'Auto APN'), #need to fix
		#('factory_defaults', 'Factory defaults'),
	]
	
	
	@Test_Case
	@Debug
	def lmu3200wifi():
		while True:
			Serial_Cmd('' , 'foofoofooo', rdRetryCnt=10, rdRetryDelay=6, noFail=True) 
			Serial_Cmd('at$app adc?' , '^OK') 
			
			##### remember to change serial.py and delete line 41 which is filter = none after done with testing
	
	@Test_Case
	#@Debug
	def parameter_acceptance():
		DeviceVal.Factory()
		Serial_Cmd('ats125=1', '^OK')
		Logger.debug('+++ load the customer csv')
		cwd = os.getcwd()
		CSVLOC = cwd 
		print(CSVLOC)
		#PLACE CUSTOMER SCRIPT 
		working = DeviceVal.Load_BadCustomer_Script(CSVLOC, '111.222-paramTEST.csv', '111')
		if working == '1':
			print('1 Rec has been passed correctly.')
			#1024,1,6F
		else:
			TC_Fail('CustomerScript Recieved Incorrect Number of Records: [%s]' % (working), 'REC_VALUE')
		
		
		
		
		
		
		#DeviceVal.Factory()
		#Serial_Cmd('', ' 0 Recs', rdRetryCnt=5, filter='PRM: Config Updated')
		#256
		# for n in range(0,4):   #number of index's 
			# Serial_Cmd('AT$APP Param? 256,%d' % (n), '256,%d,0' % (n), rdRetryCnt=5)  
		# for n in range(0,4):   #number of index's 
			# Serial_Cmd('AT$APP Param? 264,%d' % (n), '264,%d,0' % (n), rdRetryCnt=5)  
		# for n in range(0,4):   #number of index's 
			# Serial_Cmd('AT$APP Param? 269,%d' % (n), '269,%d,0' % (n), rdRetryCnt=5)  	
		# for n in range(0,64):   #number of index's 
			# Serial_Cmd('AT$APP Param? 280,%d' % (n), '280,%d,0' % (n), rdRetryCnt=5) 
		# for n in range(0,2):   #number of index's 
			# Serial_Cmd('AT$APP Param? 2319,%d' % (n), '2319,%d,""' % (n), rdRetryCnt=5) 
		# Serial_Cmd('AT$APP Param? 2320,0' , '2320,0,"maint.vehicle-location.com"', rdRetryCnt=5) 
		
		
		
	# @Test_Case
	# @Debug
	# def 786test():
		# pass
		# #reset


				
	@Test_Case
	def accum_gps_off():
		Serial_Cmd('AT$APP PARAM 512,0,11,0,0,0,26,0,0,0', 'OK')  # 11=gps on trigger, 26,0=time into acc 0
		Serial_Cmd('AT$APP PARAM 512,1,12,0,0,0,28,0,0,0', 'OK')  # 12=gps off on trigger, 28,0 time off in acc 0
		Serial_Cmd('AT$APP PEG ACTION 19 0', 'OK') # clear acc 0
		Serial_Cmd('AT$APP GPS ON', 'OK')
		DeviceVal.Sleep(170) # acc should have this approx value
		Serial_Cmd('AT$APP GPS OFF', 'OK')
		acc, value, limitType =(Serial_Cmd('AT$APP ACC?', '^\s*0\s*').split())
		if int(value) < int(3):
			TC_Fail('Accumulator Value less than expected [%s]' % (value), 'ACC_VALUE')
		Logger.debug('Acc [%s] Value [%s]' % (acc, value))
		
	def accum_gps_on():
		Serial_Cmd('AT$APP PARAM 512,0,12,0,0,0,26,0,0,0', 'OK')  # 12=gps on trigger, 26,0=time into acc 0
		Serial_Cmd('AT$APP PARAM 512,1,11,0,0,0,28,0,0,0', 'OK')  # 12=gps on on trigger, 28,0 time off in acc 0
		#DeviceVal.Reset()
		#DeviceVal.Sleep(180)
		Serial_Cmd('AT$APP PEG ACTION 19 0', 'OK',filter='.*') # clear acc 0
		Serial_Cmd('AT$APP GPS ON', 'OK',filter='.*')
		#DeviceVal.Sleep(120) # acc should have this approx value
		#Serial_Cmd('AT$APP GPS OFF', 'OK',filter='.*')
		DeviceVal.Sleep(170)
		acc, value, limitType =(Serial_Cmd('AT$APP ACC?', '^\s*0\s*').split())
		print (value)
		if int(value) < int(5):
			TC_Fail('Accumulator Value less than expected [%s]' % (value), 'ACC_VALUE')
		Logger.debug('Acc [%s] Value [%s]' % (acc, value))
	
		
	@Test_Case
	@Debug
	def accumulator_restore_on_wakeup():
		Serial_Cmd('AT$APP PEG ACTION 26 1', '^OK')  # put time in acc 1
		Serial_Cmd('AT$APP PARAM 1024,7,1,1', '^OK')  # save accs to nvm on sleep
		Serial_Cmd('AT$APP PARAM 265,0,10', '^OK')  # set time to sleep in acc 0
		Serial_Cmd('AT$APP PEG ACTION 29 2', '^OK') # reset acc 2
		Serial_Cmd('AT$APP PEG ACTION 42 16', '^OK') # reset acc 2
		
		for n in range(3,63):
			Serial_Cmd('AT$APP PEG ACTION 18 %d' % (n), '^OK', rdRetryCnt=5)  # incr acc n
		#Serial_Cmd('AT$APP PEG ACTION 22 0', '^OK')  # sleep on acc 0
		Serial_Cmd('AT$APP PEG ACTION 22 0', 'Sleep', filter='.*')
		DeviceVal.Sleep(60)
		Serial_Cmd('', 'Sleep.*sec', rdRetryCnt=30) #15
		Serial_Cmd('', 'VERSION', rdRetryCnt=150)
		DeviceVal.Check_Connected()
		#Serial_Cmd('ATS125=0', '^OK', rdRetryCnt=15,rdRetryDelay=3)
		Serial_Cmd('ATS125=7', '^OK', rdRetryCnt=15)
		Serial_Cmd('AT$APP ACC?', '^OK', rdRetryCnt=10,rdRetryDelay=3, filter='^\d*\s*') 
		
	@Test_Case
	def info_cmds():
	
		class Subtests:
		
			Name = 'Info Tests'

			SubtestList = [
				('ati0', 'Info ATI0'),
				('ati1', 'Info ATI1'),
				('ati2', 'Info ATI2'),
				('ati3', 'Info ATI3'),
				('atic', 'Info ATIC'),
				('atig', 'Info ATIG'),
				('atip', 'Info ATIP'),
			]
			
			SubtestList = [
				('ati0', 'Info ATI0'),
				('ati1', 'Info ATI1'),
				('ati2', 'Info ATI2'),
				('ati3', 'Info ATI3'),
				('atic', 'Info ATIC'),
				('atig', 'Info ATIG'),
			]

			def ati0():
				DeviceVal.Validate_Info('ATI0', 30)
			
			def ati1():
				DeviceVal.Validate_Info('ATI1', 30)

			def ati2():
				DeviceVal.Validate_Info('ATI2', 1)
				#radioTech = re.search('.*:\s*(.*)', Serial_Cmd('ATIC', 'Radio Access')).group(1)
				#print ("Vinod")
				#print (radioTech)
				#if re.search('UMTS|GSM', radioTech):
				#	Note('Radio technology being mapped to HSPA')
				#	radioTech = 'HSPA'
				#radioTech = 'CDMA' #cdma #etc #gsm #hspa #lte
				
				radioTech = '[CDMA|GSM|HSPA|LTE]' #cdma #etc #gsm #hspa #lte
				Serial_Cmd('ATI2', radioTech, errCode='INFO_ATI2_RADIOTECH')
				
			def ati3():
				DeviceVal.Validate_Info('ATI3', 1)
				inpBits = re.search('INP:\s*(.*) ', Serial_Cmd('ATI3', 'INP:')).group(1)[::-1] # inp bits reversed
				Note('Reversed bits = %s' % (inpBits))
				# verify each INP bit matches line in Input? cmd
				input = Serial_Cmd('AT$APP INPUT?', 'OK', retBuf=True, filter='.*')
				print ("vinod")
				print (input)
				cnt = 0
				for bit in inpBits:
					found = False
					for line in input:
						m = re.search('INPUT-%s:.*State=(.*)' % (cnt), line)
						if m:
							print('Bit [%d] Value [%s] State [%s]' % (cnt, bit, m.group(1)))
							if bit != m.group(1):
								TC_Fail('ATI3 INP bit mismatch with Input?', 'INFO_ATI3_INPBIT')
							else:
								found = True
								break
					if not found:
						TC_Fail('Input not found in Input?', 'INFO_ATI3_INPBIT')
					cnt = cnt + 1
					
			def atic():
				DeviceVal.Validate_Info('ATIC', 1)
				atic = Serial_Cmd('ATIC', '^OK', retBuf=True, filter='.*')
				cnx = Serial_Match('^Connection.*:\s*(.*)', atic)
				print('Connection = %s' % (cnx))
				if cnx != 'Yes':
					TC_Fail('No connection', 'INFO_ATIC_NOCONN')
				rssi = int(Serial_Match('^RSSI.*:\s*([-\d]*)', atic))
				print('RSSI = %d' % (rssi))
				if rssi < -100 or rssi > -50: 	
					TC_Fail('RSSI out of range', 'INFO_ATIC_RSSI')

			def atig():
				DeviceVal.Check_GPS_Connected()
				DeviceVal.Validate_Info('ATIG', 1)
				atig = Serial_Cmd('ATIG', '^OK', retBuf=True, filter='.*')
				sats = int(Serial_Match('^Sats.*:\s*(.*)', atig))
				print('Sats = %d' % (sats))
				if sats < 4:
					TC_Fail('Not enough satellites acquired', 'INFO_ATIG_SATS')
				Add_TC_Detail('SATS', '%d' % (sats), '4', '99')
				hdop = float(Serial_Match('^HDOP.*:\s*(.*)', atig))
				print('HDOP = %d' % (hdop))
				if hdop > 5:
					TC_Fail('HDOP value [%d] too high' % (hdop), 'INFO_ATIG_HDOP')
				gpsTime = Serial_Match('^GPS Time.*:\s*(.*)', atig)
				# validate gps time within 5 min of gmt network service time
				print('GPS Time = %s' % (gpsTime))
				
			def atip():
				DeviceVal.Validate_Info('ATIP', 1)
				
		Run_Subtests(Subtests)
		
	@Test_Case
	def auto_apn():     #APN LIST TEST 
		# use apn list file
		DeviceVal.Load_APN_List()
		Serial_Cmd('ATS155=0', 'OK')
		Serial_Cmd('AT$APP PARAM 2306,0,"', 'OK')
		Serial_Cmd('AT$APP PARAM 2306,1,"', 'OK')
		Serial_Cmd('AT$APP PARAM? 2306,*', 'OK')
		DeviceVal.Reset()
		DeviceVal.Check_Connected()
		Serial_Cmd('AT$APP PARAM? 2306,0', 'BROADBAND')
		# Serial_Cmd('AT$APP PARAM? 2306,1', 'BROADBAND')
		return
		# auto provision on, 2306 empty, connects and populates blank 2306 slots
		Serial_Cmd('ATS155=0', 'OK')
		Serial_Cmd('AT$APP PARAM 2306,0,"', 'OK')
		Serial_Cmd('AT$APP PARAM 2306,1,"', 'OK')
		Serial_Cmd('AT$APP PARAM? 2306,*', 'OK')
		DeviceVal.Reset()
		DeviceVal.Check_Connected()
		Serial_Cmd('AT$APP PARAM? 2306,0', 'ISP.CINGULAR')
		Serial_Cmd('AT$APP PARAM? 2306,1', 'ISP.CINGULAR')
		# auto provision on, 2306 populated, connects and populates 2306
		Serial_Cmd('ATS155=0', 'OK')
		Serial_Cmd('AT$APP PARAM 2306,0,"', 'OK')
		Serial_Cmd('AT$APP PARAM 2306,1,"ISP.CINGULAR', 'OK')
		Serial_Cmd('AT$APP PARAM? 2306,*', 'OK')
		DeviceVal.Reset()
		DeviceVal.Check_Connected()
		Serial_Cmd('AT$APP PARAM? 2306,0', 'ISP.CINGULAR')
		Serial_Cmd('AT$APP PARAM? 2306,1', 'ISP.CINGULAR')
		# auto provision on, 2306 empty, connects but no populate 2306
		Serial_Cmd('ATS155=1', 'OK')
		Serial_Cmd('AT$APP PARAM 2306,0,"', 'OK')
		Serial_Cmd('AT$APP PARAM 2306,1,"', 'OK')
		Serial_Cmd('AT$APP PARAM? 2306,*', 'OK')
		DeviceVal.Reset()
		DeviceVal.Check_Connected()
		Serial_Cmd('AT$APP PARAM? 2306,0', '0,""')
		Serial_Cmd('AT$APP PARAM? 2306,1', '1,""')
		# use apn list file
		Serial_Cmd('ATS155=0', 'OK')
		Serial_Cmd('AT$APP PARAM 2306,0,"', 'OK')
		Serial_Cmd('AT$APP PARAM 2306,1,"', 'OK')
		Serial_Cmd('AT$APP PARAM? 2306,*', 'OK')
		DeviceVal.Reset()
		DeviceVal.Check_Connected()
		Serial_Cmd('AT$APP PARAM? 2306,0', 'BROADBAND')
		
		
		
		# if ats155=1 and 2306 populated then will use 2306
		
		# for non-cdma devices
		# ats155=0	auto provision on
		# should auto populate from apn list file
		# 
		# ats155=1	auto provision off
		# set 2306
		# should connect using value in 2306
		
		# test that both slots used?
		# test slot 1 with slot 0 wrong value
		
		# cdma/lte
		# leave ats155=0 ...
		# 2306 doesn't get populated, but still get connection

	@Test_Case
	def autoconnect_on_powerup():
		Serial_Cmd('ATS125=7', 'OK')
		Serial_Cmd('AT$APP PEG ACTION 70 1', 'REBOOT')  # restart app
		#DeviceVal.Sleep(60)
		DeviceVal.Check_Connected(False)  #needs  to makesure device already is powered off
		DeviceVal.Sleep(20)
		DeviceVal.Check_Connected()	

	@Test_Case
	def factory_defaults():
		# set factory defaults
		DeviceVal.Factory()
		# dial string
		DeviceVal.Sleep(30)
		Serial_Cmd('AT$APP PARAM? 2316,0', 'OK') # echo current dial string
		Serial_Cmd('AT$app param 2306,0,"broadband"', 'OK', cmdRetryCnt=1, rdRetryCnt=60, rdRetryDelay=2 , retMatch = True)
		#Serial_Cmd('AT$app param 2306,1,"broadband"', 'OK', cmdRetryCnt=1, rdRetryCnt=60, rdRetryDelay=2 , retMatch = True)
		Serial_Cmd('AT$app param 2320,0,"testpuls.calamp.com"', 'OK', cmdRetryCnt=1, rdRetryCnt=60, rdRetryDelay=2 , retMatch = True)
		Serial_Cmd('AT$app param 2319,0,"gpstrax.gieselman.com"', 'OK', cmdRetryCnt=1, rdRetryCnt=60, rdRetryDelay=2 , retMatch = True)
		DeviceVal.Check_Connected()

	@Test_Case
	def ota_config_update():
		#esn = DeviceVal.Get_ESN()
		esn = re.search('.*:\s*(.*)', Serial_Cmd('AT#ESN?', 'ESN:')).group(1)
		DeviceVal.PULS_Config_Set(esn, '241', '101')
		#sets device to pending
		Serial_Cmd('AT$APP PEG ACTION 49 129', 'OK')  # send ID Report now
		DeviceVal.Check_ID_Report_Time()
		
		# file w bad values
		# empty file
		# large file??
		
	@Test_Case
	def ota_firmware_download():
		
		fW, appid = (DeviceVal.Get_Config_Value('RUN', 'Ident')).split('/')
		
		esn = re.search('.*:\s*(.*)', Serial_Cmd('AT#ESN?', 'ESN:')).group(1)
		
		
		jsonObj = DeviceVal.PULS_Device_Info(esn)
	
		dnldRec = jsonObj['firmwareHistorys'][0]
		print('\n\n' + str(dnldRec)  + '\n\n')
		fwNew = dnldRec['newFirmware']['versionNumber']
		fwOld = dnldRec['oldFirmware']['versionNumber']
		ATE2.Log('Updated %s from %s' % (fwNew, fwOld))
		jsonObj = DeviceVal.PULS_Firmware_Info(appid)
		print (fW)
		
		
		
		upgradeBuildTest = DeviceVal.Get_Config_Value('RUN', 'Firmware Upgrade List')
		print (upgradeBuildTest)
		
		upgrade = upgradeBuildTest.split(',')
		print (upgrade)
		
		upgradbuildcount = 0
		while(upgradbuildcount < 1):
			
			ATE2.Log('++ Iteratation %d' % (upgradbuildcount))
			Logger.debug('++ Iteratation %d' % (upgradbuildcount))
			for build in upgrade:
				print ('Requested build to upgrade is ',build)
				DeviceVal.PULS_Firmware_Set(esn, build)
				Serial_Cmd('at$app peg action 49 129', '^OK', cmdRetryCnt=60, cmdRetryDelay=3)
				#time.sleep(10)
				DeviceVal.Sleep(300)
				Serial_Cmd('at$app peg action 49 129', '^OK', cmdRetryCnt=60, cmdRetryDelay=3,filter='.*')
				ati0build = build[0] + '.' + build[1: ]
				print (build, ati0build)
				jsonObj = DeviceVal.PULS_Device_Info(esn)
				devicecompleted = False
				for retry in range(84):
					jsonObj = DeviceVal.PULS_Device_Info(esn)
					status = jsonObj['firmwareHistorys'][0]['status']
					print (status)
							
					if status == 'Completed':
						devicecompleted = True
						break
					DeviceVal.Sleep(5)	
				if not devicecompleted:
					TC_Fail('Download failed','PULS_DNLD-FAIL')
				print ('upgradedone')
				Serial_Cmd('ati0', ati0build, cmdRetryCnt=200, cmdRetryDelay=3, filter='APP:')	
			
			
			
			DeviceVal.Sleep(60)
			Serial_Cmd('at$app peg action 49 129', '^OK', cmdRetryCnt=60, cmdRetryDelay=3)
			upgradbuildcount = upgradbuildcount + 1
	
	

		
		
	@Test_Case
	@Debug
	def peg_comm_shutdown():
		Serial_Cmd('AT$APP PEG ACTION 55 0', '<Comm Shutdown>', rdRetryCnt=5, filter='Trig=')
		DeviceVal.Check_Connected(False)
	
	@Test_Case
	@Debug
	def peg_sleep_wakeup_timer():
		Serial_Cmd('AT$APP PARAM 265,0,10', 'OK')  # set timer 0
		Serial_Cmd('AT$APP PEG ACTION 22 0', 'OK', rdRetryCnt=15)  # sleep, wakeup on 0
		dtStart = datetime.datetime.now()
		Serial_Cmd('', 'VERSION', rdRetryCnt=35) #rdretry count was 15
		dtEnd = datetime.datetime.now()
		dur = DeviceVal.DT_Duration(dtStart, dtEnd)
		Logger.debug('Sleep duration = %s' % (dur))
		if dur < 10 or dur > 25:
			TC_Fail('Sleep duration out-of-range', errCode='PEG_SLEEPDUR')
			
	@Test_Case
	def peg_id_report():
		Serial_Cmd('AT$APP PEG ACTION 49 129', 'OK')  # send ID Report now
		DeviceVal.Check_ID_Report_Time()
	
	@Test_Case
	@Debug
	def peg_event_report():
		for offset in range(3):
			code = 101 + offset
			#Serial_Cmd('AT$APP PEG ACTION 1 %d' % (code), 'Ack0x', rdRetryCnt=10, filter='LMD: Send|LMD: Rcv|Inbnd:')
			Serial_Cmd('AT$APP PEG ACTION 1 %d' % (code), 'Ack', rdRetryCnt=10, filter='.*')
	@Test_Case
	def sms_param_msg():
		DeviceVal.Send_SMS('!S0<payload')	

	@Test_Case
	def sms_serial_msg():
		# just payload
		DeviceVal.Send_SMS('!S0<payload>')	
		# payload with cr/lf
		DeviceVal.Send_SMS('!S1<payload>')	
		
	@Test_Case
	def comm_on_off():
		# comm off
		Serial_Cmd('AT$APP COMM OFF', 'OK')
		DeviceVal.Sleep(20)
		DeviceVal.Check_Connected(False)
		# comm on
		Serial_Cmd('AT$APP COMM ON', 'OK')
		DeviceVal.Sleep(20)
		DeviceVal.Check_Connected()
		
	@Test_Case
	def gps_on_off():
		# gps off
		Serial_Cmd('AT$APP GPS OFF', 'OK')
		DeviceVal.Sleep(20)
		DeviceVal.Check_GPS_Connected(False)
		#Serial_Cmd('ATIG', 'Power\s*:\s*Off', errCode='INFO_ATIG_POWER')
		# gps on
		Serial_Cmd('AT$APP GPS ON', 'OK', errCode='INFO_ATIG_POWER')
		DeviceVal.Sleep(20)
		DeviceVal.Check_GPS_Connected()
		#Serial_Cmd('ATIG', 'Power\s*:\s*On')
		
	@Test_Case
	def recover_system_time_after_wakeup():
		ATE2.Log('GPS Off')
		Serial_Cmd('AT$APP GPS OFF', 'OFF')
		Serial_Cmd('AT$APP PARAM 265,0,5', 'OK')  # set timer
		#Serial_Cmd('AT$APP PEG ACTION 22 0', 'OK')  # sleep on timer
		Serial_Cmd('AT$APP PEG ACTION 22 0', 'Sleep', filter='.*')
		DeviceVal.Sleep(120)
		DeviceVal.Get_GMT_Time()
		DeviceVal.Sleep(20)
		# at$app gps off
		# peg action 22 0    sleep 265,0,<cnt> 
		#	pull time from app[time] debug and compare to compare gmt remote time
		
		# turn off comm, on gps
		# sleep, wakup
		# verify gmt time
		
		# turn off both comm, gps
		# sleep wakup
		# check time
		
	@Test_Case
	def peg_timeouts():
		Serial_Cmd('AT$APP PARAM 512,0,12,0,0,0,13,0,0,0', 'OK')  # start timer when gps off
		Serial_Cmd('AT$APP PARAM 512,1,11,0,0,0,14,0,0,0', 'OK')  # stop timer when gps off
		Serial_Cmd('AT$APP GPS OFF', 'OK')
		Serial_Cmd('AT$APP GPS ON', 'OK')
		
	@Test_Case
	@Debug
	def peg_time_of_day_timer():
		DeviceVal.Get_GMT_Time()
		Serial_Cmd('AT$APP PARAM 267,0,<GMT TIME OFFSET>', '')  # calculate second past gmt midnight to time of test + 2 min
		Serial_Cmd('AT$APP PARAM 265,0,5', 'OK')  # timer 0 to 5 seconds
		Serial_Cmd('AT$APP PARAM 512,0,20,0,0,0,22,0,0,0', 'OK') # (22,0) sleep using timer 0, 20 trigger the peg tmo day trigger
		
		# at$app param 267,0,<calculate gmt offset>    # 
			# calculate second past gmt midnight to time of test + 2 min
			# 265,0,15   # timer 0 to 15 seconds
			# at$app param 512,0,20,0,0,0,22,0,0,0    # (22,0) sleep using timer 0, 20 trigger the peg tmo day trigger
			# check sleep event occured.  checked wakeup event occurred
			
	@Test_Case
	def sleep_on_ignition_off():
		Serial_Cmd('AT$APP PARAM 265,0,10', 'OK') # timer 0 10 seconds
		Serial_Cmd('AT$APP PARAM 512,0,16,0,0,0,22,0,0,0') # (16=ignition off, 22,0 = sleep using val in timer 0)
		
		# at$app 265,0,10    # timer 0 to 10 seconds
		# at$app param 512,0,16,0,0,0,22,0,0,0    (16=ignition off, 22,0 = sleep using val in timer 0)
		# force ignition on/off  ?? how to do this w/o a test fixture
		# verify sleep/wakeup
	
	@Test_Case
	def sleep_on_timer():
		Serial_Cmd('AT$APP PARAM 265,0,10', 'OK') # timer 0 - 10 seconds
		Serial_Cmd('AT$APP PARAM 265,1,5', 'OK') # timer 1 - 5 seconds
		Serial_Cmd('AT$APP PARAM 512,0,18,0,0,0,22,0,0,0', 'OK') # 18=timer tmo trigger, 22,0 = sleep using timer 0
		Serial_Cmd('AT$APP PARAM 512,1,3,0,0,0,13,1,0,0', 'OK') # 3=on powerup, 13=start timer 1
		
		# vinod to redo this 

	@Test_Case
	@Debug
	def sms_param_msg():
		# write request - set timer 0 = 120, accum 0 thresh = 300
		Serial_Cmd('AT$APP PARAM 265,0,0', 'OK') # timer 0
		Serial_Cmd('AT$APP PARAM 266,0,0', 'OK') # accum 0 thresh
		resp = DeviceVal.Send_SMS_Sinch('!P0000000101010900050000000078010A0005000000012C00000000', mRE='!P000000010300000000')
		Serial_Cmd('AT$APP PARAM? 265,0', ',120$') # timer 0
		Serial_Cmd('AT$APP PARAM? 266,0', ',300$')
		
	@Test_Case
	@Debug
	def sms_unit_request_msgs():
		class Subtests:
				
			Name = 'Unit Request Msgs'

			SubtestList = [
				('r0_lmu_status', '!R0 - LMU status'),
				('r1_write_param', '!R1 - write param'),
				('r3_49_129_id_report', '!R3,49,129 - ID Report'),
				('r3_70_0_app_reset', '!R3,70,0 - app reset'),
				('r5_gps_status', '!R5 - GPS status'),
				('r8_firmware_update', '!R8 - firmware update'),
				('ra_adc_values', '!RA - ADC values'),
				('rb_battery_status', '!RB - Battery fuel guage status'),
				('rc_comm_status', '!RC - comm status'),
				('rj_google_maps', '!RJ - Google maps location'),
				('rn_nuvi_status', '!RN - NUVI status'),
				('rp_param', '!RP - read/write a param'),
				('rv_audio_settings', '!RV - audio settings'),
			]
			
			SubtestList = [
				 ('r0_lmu_status', '!R0 - LMU status'),
				('r1_write_param', '!R1 - write param'),
				 ('r3_49_129_id_report', '!R3,49,129 - ID Report'),
				 ('r3_70_0_app_reset', '!R3,70,0 - app reset'),
				  ('r5_gps_status', '!R5 - GPS status'),
				 ('r8_firmware_update', '!R8 - firmware update'),
				 ('ra_adc_values', '!RA - ADC values'),
				 ('rb_battery_status', '!RB - Battery fuel guage status'),
				 ('rc_comm_status', '!RC - comm status'),
				  ('rj_google_maps', '!RJ - Google maps location'),
				 ('rn_nuvi_status', '!RN - NUVI status'),
				 ('rp_param', '!RP - read/write a param'),
				 ('rv_audio_settings', '!RV - audio settings'),
			]
			
			def r0_lmu_status():
				resp = DeviceVal.Send_SMS_Sinch('!r0', mRE='Resp.*LMD', fRE='Resp: APP:|Resp: INB:')
				#resp = DeviceVal.Send_SMS_Sinch('!r0R8585551212', mRE='18585551212 Error Invalid Number')
				# redirect response to diff number
				#resp = DeviceVal.Send_SMS_Sinch('!r0R8585551212', mRE= 'is a landline')
				#resp = DeviceVal.Send_SMS_Sinch('!r0R8585551212', mRE= ['18585551212 Error Invalid Number | is a landline'])
				
				
			def r0_lmu_status_w_pwd():
				# use password to access locked LMU
				ATE2.Serial_Exec_Cmd('AT$APP PARAM 2177,0,"%s"' % ('foo'), 'OK')
				ATE2.Serial_Exec_Cmd('AT$APP PARAM 1024,51,16,16', 'OK')
				DeviceVal.Reset()
				resp = DeviceVal.Send_SMS_Sinch('!r0P<pwd>', mRE='Resp.*LMD', fRE='Resp: APP:|Resp: INB:')
				ATE2.Serial_Exec_Cmd('AT$PW "%s"' % ('foo'), '^OK')
				ATE2.Serial_Exec_Cmd('AT$APP PARAM 1024,51,16,0', 'OK')
			
			def r1_write_param():
				Serial_Cmd('AT$APP PARAM 2306,0,"orbcomm.t-mobile.com', 'OK')
				DeviceVal.Send_SMS_Sinch('!r1,2306,1,badparam.com')
				Serial_Cmd('AT$APP PARAM? 2306,1', 'badparam.com')
				
			def r3_49_129_id_report():
				resp = DeviceVal.Send_SMS_Sinch('!r3,49,129', mRE='MAINT:')
				DeviceVal.Check_ID_Report_Time()
				
			def r3_70_0_app_reset():
				resp = DeviceVal.Send_SMS_Sinch('!r3,70,0', mRE='Rebooting...')
				DeviceVal.Check_Device_Booted()

			def r5_gps_status():
				resp = DeviceVal.Send_SMS_Sinch('!r5', mRE='Resp.*AGC', fRE='Resp: AGC:')

			def r8_firmware_update():
				resp = DeviceVal.Send_SMS_Sinch('!r8')

			def ra_adc_values():
				resp = DeviceVal.Send_SMS_Sinch('!ra', mRE='ADC0')

			def rb_battery_status():
				resp = DeviceVal.Send_SMS_Sinch('!rb', mRE='')

			def rc_comm_status():
				#resp = DeviceVal.Send_SMS_Sinch('!rc', mRE='Resp.*\.\.\.')  #looking for . . . . 
				resp = DeviceVal.Send_SMS_Sinch('!rc', mRE='Resp.*') 
				 
			def rj_google_maps():
				resp = DeviceVal.Send_SMS_Sinch('!rj', mRE='Heading')

			def rn_nuvi_status():
				resp = DeviceVal.Send_SMS_Sinch('!rn', mRE='NUVI')

			def rp_param():
				resp = DeviceVal.Send_SMS_Sinch('!rp,2306,1,paramtest.com')
				resp = DeviceVal.Send_SMS_Sinch('!rp?2306,*', mRE='paramtest.com')

			def rv_audio_settings():
				resp = DeviceVal.Send_SMS_Sinch('!rv', mRE='')
				
		Run_Subtests(Subtests)
		
	@Test_Case
	@Debug
	def sms_vehicle_bus_msgs():
		class Subtests:
				
			Name = 'Vehicle Bus Msgs'

			SubtestList = [
				('v0_vbus_status', '!V0 - vehicle bus status'),
				('vd_dtc_status', '!VD - DTC status'),
				('vv_vehicle_detection_status', '!VV - vehicle detection status'),
			]
			
			SubtestList = [
				('v0_vbus_status', '!V0 - vehicle bus status'),
				('vd_dtc_status', '!VD - DTC status'),
				('vv_vehicle_detection_status', '!VV - vehicle detection status'),
			]		
			
			def v0_vbus_status():
				resp = DeviceVal.Send_SMS_Sinch('!v0', mRE='=VBUS=')
			
			def vd_dtc_status():
				resp = DeviceVal.Send_SMS_Sinch('!vd', mRE='=VBUS DTC=')
			
			def vv_vehicle_detection_status():
				resp = DeviceVal.Send_SMS_Sinch('!vv', mRE='-Veh Disc-')

		Run_Subtests(Subtests)			
			
	@Test_Case
	def wakeup_on_ignition():
		Serial_Cmd('AT$APP PARAM 265,0,0', 'OK') # timer 0 - 0 seconds
		Serial_Cmd('AT$APP PARAM 1029,0,255,1', 'OK') # wakeup from sleep on ignition on
		Serial_Cmd('AT$APP PEG ACTION 22 0', '') # force sleep indefinitely until trigger

		# trigger ignition on
		# check device running
		
	@Test_Case
	def wakeup_on_motion():
		Serial_Cmd('AT$APP PARAM 265,0,0', 'OK') # timer 0 - 0 seconds
		Serial_Cmd('AT$APP PARAM 1029,0,255,1', 'OK') # wakeup from sleep on motion
		Serial_Cmd('AT$APP PEG ACTION 22 0', '') # force sleep indefinitely until trigger
		
		#if(motion != 'No'):						#checks ini 
		#	Serial_Cmd('AT$APP PARAM 265,0,0', 'OK') # timer 0 - 0 seconds
		#	Serial_Cmd('AT$APP PARAM 1029,0,255,1', 'OK') # wakeup from sleep on motion  1029 0-1
		#	Serial_Cmd('AT$APP PEG ACTION 22 0', '') # force sleep indefinitely until trigger
		#else:
		#	ATE2.Log('Motion Detector not enabled.')
		# trigger motion
		# check device running
		
	@Test_Case
	@Debug
	def wakeup_on_time_of_day():
		tod = DeviceVal.Calc_TOD_With_Offset(60)
		print(tod)
		Serial_Cmd('AT$APP PARAM 267,0,%s' % (tod), 'OK')  # current time + delta
		Serial_Cmd('AT$APP PEG ACTION 24 0', 'Sleep until TOD')  # device sleep and wakeup on timer 0
		Serial_Cmd('', 'Sleep.*sec', rdRetryCnt=120)
		Serial_Cmd('', 'VERSION', rdRetryCnt=160)
		DeviceVal.Sleep(20)
		# 267,0,<gps current time + delta>
		# peg 24 0   # cause device to sleep and wakeup on timer 0 
		# check that wakeup occurred
	
		# 1030 - local time zone
		# at$app time?
	
	
	@Test_Case	
	def fill_log_reset():
		#Serial_Cmd('at$app param 2319,0,"gpstraxcloud.gieselman.com"', '^OK')
		# print(Serial_Cmd('at$app param? 769,0', '^OK'))
		Serial_Cmd('at$app comm off', 'COMM_Deactivate')
		Serial_Cmd('at$app clear log', '^OK', rdRetryCnt=10)
		Serial_Cmd('at$app log fill 50', '^OK')  
		#Serial_Cmd('at$app log qfill 50', '^OK')
		DeviceVal.Sleep(20)
		Serial_Cmd('at$app log?', '50 Records', filter='Log Status:', cmdRetryCnt=10)
		DeviceVal.Sleep(20)
		Serial_Cmd('at$app comm on', '^OK')
		DeviceVal.Sleep(20)
		DeviceVal.Check_Connected()
		DeviceVal.Sleep(40)
		Serial_Cmd('at$app log?', '0 Records', filter='Log Status', cmdRetryCnt=15)
	
	
	
	
	@Test_Case
	def log_batchmode():
		Serial_Cmd('at$app comm off', '^OK')
		Serial_Cmd('at$app clear log', '^OK', rdRetryCnt=10)
		Serial_Cmd('at$app peg action 1 100', '^OK')
		Serial_Cmd('at$app peg action 2 200', '^OK')
		Serial_Cmd('at$app peg action 3 300', '^OK')
		Serial_Cmd('at$app log?','3 [Records | Recs]',filter='Log Status',cmdRetryCnt=10) #8bits require Recs 
		Serial_Cmd('at$app param 1024,20,32,32', '^OK') #LMU will use Batch mode on next power up
		Serial_Cmd('at$app log?','3 [Records | Recs]',filter='Log Status',cmdRetryCnt=10)
		#Serial_Cmd('atreset','')
		#DeviceVal.Power_Reset()
		DeviceVal.Reset()
		#Serial_Cmd('at$app comm on', '^OK',rdRetryCnt=20)
		DeviceVal.Check_Connected()
		Serial_Cmd('at$app log?','3 [Records | Recs]',filter='Log Status',cmdRetryCnt=10)
		
		
		
	@Test_Case
	def lmu_manager():
		pass
		# manual test    .. lmu manager be run from cmd line?
		# automation loads script onto device
		# prompt an lmu manager procedure that changes version to 99.99
		# at$app param  to read the changed value
		
	
	@Test_Case
	def sync_time():
		pass
# Added Test Cases:
# 1. Prevent system time being sync'd to GPS time until GPS has a 3D fix ( 1-78, V5.0c Release Notes)
# Procedure:
# - Set s178=16
# - Power cycle the LMU
# - Watch for LMU's system time

# The result should be: device system time syncs to 3D GPS time.  
 
	@Test_Case
	def comm_mode_4():
		pass
# 2. COMM mode 4 added ( 2 radio and 3 logs ) ( 3 logs not supported yet) ( Item 1-80 V5.0c Release Notes)
# 14:55:47.432> at$app param? 1068,*
# 14:55:47.432> 1068,0,4
# 14:55:47.432> 
# 14:55:47.432> OK
# 14:55:48.712> ati0 
# 14:55:48.712> APP:LMU,184 V5.0c (Aug 19 2016 17:55:45) 
# 14:55:48.712> PIC:STM S/N 4832001478 
# 14:55:48.712> GPS:UBLOX-00040007 -7.03 (45969) 
# 14:55:48.712> Radio:u-blox LEON-G100-07.60.00+ 
# 14:55:48.712> Radio1:u-blox LEON-G100-07.60.00+
# 14:55:48.712> 
# 14:55:48.712> OK

	@Test_Case
	@Debug
	def peg_triggers():
		import Triggers

		
	@Test_Case
	def system_time_src_Server():
		Serial_Cmd('AT$APP PARAM 1069,0,23', '^OK') # disable all time sources except server
		DeviceVal.Power_Reset()
		DeviceVal.Sleep(140)
		DeviceVal.Check_Connected()
		Serial_Cmd('AT$APP PEG ACTION 92 0', '^OK')
		Serial_Cmd('atit', '^Source 3', cmdRetryCnt=5, cmdRetryDelay=30, filter='^Source')
		
	
	@Test_Case
	def system_time_src_reject_3D_GPS():
		Serial_Cmd('AT$APP PARAM 1069,0,1', '^OK') # enable 3D-GPS time rejection, param 1069 bit 0 
		DeviceVal.Power_Reset()
		DeviceVal.Sleep(140)
		DeviceVal.Check_Connected()
		m = Serial_Cmd('atit', '^Source [0|1|2|4]', cmdRetryCnt=5, cmdRetryDelay=30, filter='^Source')
		if re.search('Source 0', m):
			TC_Fail('Bad time source', 'TIME_BADSOURCE')
			
	@Test_Case
	def system_time_src_reject_RTC():
		Serial_Cmd('AT$APP PARAM 1069,0,2', '^OK') # enable RTC time rejection, param 1069 bit 1 
		DeviceVal.Power_Reset()
		DeviceVal.Sleep(140)
		DeviceVal.Check_Connected()
		m = Serial_Cmd('atit', '^Source [0|1|2|4]', cmdRetryCnt=5, cmdRetryDelay=30, filter='^Source')
		#DeviceVal.Sleep(10)
		if re.search('Source 1', m):
			TC_Fail('Bad time source', 'TIME_BADSOURCE')		
			
	@Test_Case
	def system_time_src_reject_None_3D_GPS():
		Serial_Cmd('AT$APP PARAM 1069,0,4', '^OK') # enable RTC time rejection, param 1069 bit 2 
		DeviceVal.Power_Reset()
		DeviceVal.Sleep(140)
		DeviceVal.Check_Connected()
		m = Serial_Cmd('atit', '^Source [0|1|2|4]', cmdRetryCnt=5, cmdRetryDelay=30, filter='.*')
		if re.search('Source 2', m):
			TC_Fail('Bad time source', 'TIME_BADSOURCE')
			
	@Test_Case
	def system_time_src_reject_Server():
		Serial_Cmd('AT$APP PARAM 1069,0,8', '^OK') # enable RTC time rejection, param 1069 bit 3 
		DeviceVal.Power_Reset()
		DeviceVal.Sleep(140)
		DeviceVal.Check_Connected()
		Serial_Cmd('AT$APP PEG ACTION 92 0', '^OK')
		DeviceVal.Sleep(10)
		m = Serial_Cmd('atit', '^Source [0|1|2|3|4|5]', cmdRetryCnt=5, cmdRetryDelay=10, filter='^Source')
		if re.search('Source 3', m):
			TC_Fail('Bad time source', 'TIME_BADSOURCE')
		
	
	@Test_Case
	def system_time_src_reject_CellNetwork():
		Serial_Cmd('AT$APP PARAM 1069,0,16', '^OK') # enable network time rejection, param 1069 bit 4 
		DeviceVal.Power_Reset()
		DeviceVal.Sleep(140)
		DeviceVal.Check_Connected()
		m = Serial_Cmd('atit', '^Source [0|2|4|5]', cmdRetryCnt=5, cmdRetryDelay=10, filter='^Source')
		if re.search('Source 4', m):
			TC_Fail('Bad time source', 'TIME_BADSOURCE')
		

	@Test_Case
	def system_time_src_3D_GPS():
		Serial_Cmd('ats174=18', '^OK') # enable 3d gps system time, s174 bit 4,s174 default is 2
		DeviceVal.Reset()
		DeviceVal.Sleep(140)
		DeviceVal.Check_Connected()
		#8bit (source: vs source) 
		# m = Serial_Cmd('atit', '^Source: [0|2|4]', cmdRetryCnt=10, cmdRetryDelay=30, filter='^Source')
		# if re.search('Source: 2', m):
			# TC_Fail('Bad time source', 'TIME_BADSOURCE')
		m = Serial_Cmd('atit', '^Source [0|2|4]', cmdRetryCnt=10, cmdRetryDelay=30, filter='^Source')
		if re.search('Source 2', m):
			TC_Fail('Bad time source', 'TIME_BADSOURCE')

	@Test_Case
	def script_loading():
		esn = re.search('.*:\s*(.*)', Serial_Cmd('AT#ESN?', 'ESN:')).group(1)
		#pcba = re.search('.*:\s*(.*)', Serial_Cmd('AT#PCBA?', 'PCBA:')).group(1)
		param1, param2 = (DeviceVal.Get_Config_Value('RUN', 'Parameter Migration')).split('/')
		module = Serial_Cmd('ATI2', 'GENERIC', filter='.*')
		module = re.search('[^\s]+',module).group(0)
		#module = 'HSPA'#######################################################################################################################
		ati0 = Serial_Cmd('ATI0', 'APP', filter='.*')
		unit = re.search('.*[:](\w{3})',ati0).group(1)
		appID = re.search('.*[,](\S{3})',ati0).group(1)
		fw = re.search('.*[V](\S{4})',ati0).group(1)

		bldFiles = {
		'fw1': 'LMU-%s-%s-%s.bin' %(module,appID,param1), #firmware to upgrade from
		'fw2': 'LMU-%s-%s-%s.bin' %(module,appID,param2), #firmware to upgrade to
		'script':'32bit_Init_50j.csv',
		'script2':'250.253-Hino Jasper APN Transition script.csv'
		}
		bldFiles1 = {
		'fw1': 'TTU-%s-%s-%s.bin' %(module,appID,param1), #firmware to upgrade from
		'fw2': 'TTU-%s-%s-%s.bin' %(module,appID,param2), #firmware to upgrade to
		}
		if unit == 'LMU':
			print('Using LMU')
			mode = bldFiles
		else:
			print('Using TTU')
			mode = bldFiles1
		firmwarePath = os.getcwd() + '\\fw'
		fw = fw.replace(".", "")

		if fw[0] > param1[0]: #proper way to downgrade firmwares ex: 5 > 4
			print('MUST DOWNGRADE PROPERLY\n')
			ATE2.Log('MUST DOWNGRADE PROPERLY')			
			if fw < '50j':
				Serial_Cmd('at#flash-eraseall', 'OK', rdRetryCnt=60, initDelay = 1)
				DeviceVal.Ymodem_File_Transfer('atdnld', firmwarePath, mode['fw1'])
				DeviceVal.Sleep(45)
				Serial_Cmd('AT', 'OK', cmdRetryCnt=60)
				DeviceVal.Factory()
				DeviceVal.Reset()
				print('IF if')
			else:
				DeviceVal.Ymodem_File_Transfer('at#dngrd', firmwarePath, mode['fw1'])
				DeviceVal.Sleep(45)
				Serial_Cmd('AT', 'OK', cmdRetryCnt=60)
				DeviceVal.Factory()
				DeviceVal.Reset()
				print('IF else')
			
			Serial_Cmd('AT', 'OK', cmdRetryCnt=60)
			Serial_Cmd('AT#ESN %s OVERRIDE' % (esn), 'OK', cmdRetryCnt=3)
			Serial_Cmd('at#nvminit', 'OK', rdRetryCnt=60, initDelay = 1)
		
		elif fw == param1: # the case when nothing needs to be done ex: firmwares are the same
			print('elif')
			pass
		else: # the case when upgrade needs to be done or within the same firmware number ex 50c and 50j
			print('Executing else')
			print(fw)
			print(param1)
			param1 = param1.replace(".", "")
			print(param1)
			DeviceVal.Ymodem_File_Transfer('atdnld', firmwarePath, mode['fw1'])
			DeviceVal.Sleep(45)
			Serial_Cmd('AT', 'OK', cmdRetryCnt=60)
			DeviceVal.Factory()
			print('Else Statement executed')
			
		#Serial_Cmd('AT#PCBA %s' % (pcba), 'OK', cmdRetryCnt=3)
		#DeviceVal.Ymodem_File_Transfer('atdscfg', firmwarePath, bldFiles['script'])
		#DeviceVal.Sleep(5)		
		#Serial_Cmd('AT$APP PEG ACTION 39 0', 'OK')
		#kevins_test()
		DeviceVal.Ymodem_File_Transfer('atdnld', firmwarePath, mode['fw2'])
		DeviceVal.Sleep(45)
		Serial_Cmd('AT', 'OK', cmdRetryCnt=60)
		kevins_test()
	
		
	
	@Test_Case
	def kevins_test():
		ati0 = Serial_Cmd('ATI0', 'APP',rdRetryCnt=60, initDelay = 1, filter='.*')
		ati0 = re.search('.*[,](.*)([\s]+)',ati0).group(1)
		appID = ati0[:3]
		fw = ati0[5:9]
		DeviceVal.Sleep(5)
		Serial_Cmd('AT$APP GPS OFF', 'OK')
		fileDump = 'C:\\Users\\labuser\\Desktop\\Tests-1101-2000\\Parameter Migration'
		if not os.path.exists(fileDump):
			os.makedirs(fileDump)
		if(os.getcwd() != fileDump):
			os.chdir('Parameter Migration')
			
		target = open('[%s]param-mismatch[%s].txt' % (appID, fw), 'w+') #file to be written to if parameters are not matching
		target2 = open('[%s]param-error[%s].txt' % (appID, fw), 'w+') # file to be written to if the return is "ERROR"
		with open("32bit_Init_50j.txt") as f:
			for line in f:
				line = line.rstrip()
				param, slot, value = re.search ('^(\d*),(\d*),(.*)', line).group(1, 2, 0)
				buf = Serial_Cmd('AT$APP PARAM? %s,%s' % (param, slot), '^%s' % (param), rdRetryCnt=1, initDelay = 0.1, noFail=True)
				if param == '2691':
					if fw <= '5.0c':
						print('Bug with the parameter------skipping')
						ATE2.Log('Bug with parameter 2691 before version 5.0c. SKIPPING')
						continue
				if buf == '':
					print('no param - [%s] [%s]' % (buf, value))
					ATE2.Log('no param - [%s] [%s]' % (buf, value))
					target2.write('exp = [%s] act = [%s]\n' % (value, buf))
					continue
				if buf == value:
					continue
				else:
					print('mismatch - [%s] [%s]' % (buf, value))
					ATE2.Log('mismatch - [%s] [%s]' % (buf, value))
					target.write('exp = [%s] act = [%s]\n' % (value, buf))
		target.close()
		target2.close()

		
	@Test_Case
	def p2560_test():
		ati0 = Serial_Cmd('ATI0', 'APP', filter='.*')
		ati0 = re.search('.*[,](.*)([\s]+)',ati0).group(1)
		appID = ati0[:3]
		print('*******')
		print(appID)
		fw = ati0[5:9]
		print('************')
		print(fw)
		Serial_Cmd('AT$APP GPS OFF', 'OK')
		
		target = open('[%s]param-mismatch[%s].txt' % (appID, fw), 'w+') #file to be written to if parameters are not matching
		target2 = open('[%s]param-error[%s].txt' % (appID, fw), 'w+') # file to be written to if the return is "ERROR"
		with open("32bit_Init_50h.txt") as f:
			for line in f:
				line = line.rstrip()
				param, slot, value = re.search ('^(\d*),(\d*),(.*)', line).group(1, 2, 0)
				if param == '2560':
					buf = Serial_Cmd('AT$APP PARAM? %s,%s' % (param, slot), '^%s' % (param), rdRetryCnt=1, initDelay = 0.1, noFail=True)
					if buf == '':
						print('no param - [%s] [%s]' % (buf, value))
						ATE2.Log('no param - [%s] [%s]' % (buf, value))
						target2.write('exp = [%s] act = [%s]\n' % (value, buf))
						continue
					if buf == value:
						continue
					else:
						print('mismatch - [%s] [%s]' % (buf, value))
						ATE2.Log('mismatch - [%s] [%s]' % (buf, value))
						target.write('exp = [%s] act = [%s]\n' % (value, buf))
			target.close()
			target2.close()
		
		
	@Test_Case
	@Debug
	def ota_obd_fw_db():
		fW, appid = (DeviceVal.Get_Config_Value('RUN', 'Ident')).split('/')
		esn = re.search('.*:\s*(.*)', Serial_Cmd('AT#ESN?', 'ESN:')).group(1)
		upgradeDB = DeviceVal.Get_Config_Value('RUN', 'OBD Database Upgrade List')
		upgradeOFW = DeviceVal.Get_Config_Value('RUN', 'OBD Firmware Upgrade List')
		devicecompleted = False
		
		##@@@@@@@@@@@@@@@@@@@FIRMWARE@@@@@@@@@@@@@@@@@
		upgradeBuildTest = DeviceVal.Get_Config_Value('RUN', 'OBD Firmware Upgrade List')
		print (upgradeBuildTest)
		upgrade = upgradeBuildTest.split(',')
		for build in upgrade:
			print ('Requested build to upgrade is ',build)
		
			customerApiKey = "Qehfd-gNO8nBZuRgsG_8cb4DPWEtS-5AEwzqNCrv9dqoGr0iGXNM2l3mklqi0aN2" 
			print('Upgrading Firmware to -> ' + upgradeOFW )
			print('************')
			#
			resp = DeviceVal.PULS_OBD_FW_Set(esn,build,customerApiKey)  #set obd firmware
			print('POST DEVICEVAL OBD FW SET')
			Serial_Cmd('at$app peg action 49 129','^OK', cmdRetryCnt=30, cmdRetryDelay=3)
			print('sleeping')
			DeviceVal.Sleep(180)
			a = Serial_Cmd('ati0','APP',filter ='.*')
			b = Serial_Cmd('ativ','FWVer',filter ='.*')
			
			print('aaaaaaaaaaa')
			print(a)
			print(b)
			print('aaaaaaaaaaaa')
			print(resp)
			print('looping')
	
		#Serial_Cmd('at$app peg action 49 129','^OK', cmdRetryCnt=30, cmdRetryDelay=3)
		##DeviceVal.Sleep(300)
		##Serial_Cmd('at$app peg action 49 129','^OK', cmdRetryCnt=30, cmdRetryDelay=3)
		
		print('1^^^^^^^^^^^^')
		jsonObj = DeviceVal.PULS_Device_Info(esn)
		print('2^^^^^^^^^^^^')
		#status = jsonObj['vbusDeviceFiles'][1]['status']
		#status = jsonObj['vbusFileHistories'][0]['status']

		print('3^^^^^^^^^^^^')

		# print('********')
		# print (status)
		# print('********')
							
		##if status == 'Completed':
		##	devicecompleted = True
			
			
		##if not devicecompleted:
		##	TC_Fail('Download of OBD Failed.','PULS_DNLD-FAIL')
		print('END OF TEST')
		print('$$$$$$$$$$$$$$$')
					
					
	
Run_Tests(BasicTests)
