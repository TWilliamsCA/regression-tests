 ###############################################
## Imports
###############################################
import base64, configparser, datetime, dateutil, inspect, logging, os, platform, re, sys, time, urllib3, psycopg2
sys.path.append('C:/CalAmpAte2/libs')
import Activate, ATE2, Coproc, General, iBeacon, LMU, LMU_CSV, Linux, Orbcomm, Password, RF, Serialize
from tkinter import *
import tkinter.simpledialog as tks
import tkinter.messagebox as tkm
import easygui as eg
import sqlite3, ast
from dateutil.parser import parse
#added for TX Power sensor test
import subprocess
from os import system
from subprocess import Popen, PIPE

###############################################
## Module Initializations
###############################################

Logger = logging.getLogger('ATE2_Logger.LMU')

class _UUT(object):
	def __init__(self):
		self.ESN = ''
		self.USERESN = ''
		self.MOBILEID = ''
		self.Data = {}
		self.ATI0 = {}
		self.ATI1 = {}
		self.ATIV = {}		
		self.Password = ''
		self.IsActivated = False

Bat_Data = 'C:/TEMP/BDat'
Dev_Value = 'C:/TEMP/DVal'
Dir_Last_Label = 'c:/CalAmpAte2/BarTender/Last_Label/'
Dir_Scan_Label = 'C:/Commander/Scan/'
Dir_ATE_DB = 'c:/CalAmpAte2/db/'
Pwr_Sensor_Path = 'C:/CALAMPATE2/bin/Power_Sensor/'
PULS_Extra_Vals = ''
Path_ATE1_Results_DB = 'c:/CalAmpAte/TestResults/TestResults.db3'
ResultsSQLiteDbConn = None

###############################################
## LMU
###############################################

def Accessory_Configure_Main_Board():
	# if no Aux2 clear streams
	aux_port = ATE2.Config_Get('ASSEMBLY', 'Aux Port')
	if aux_port == 'No':
		# clear Aux from streams
		ATE2.Serial_Exec_Cmd('AT$APP PARAM 3072,0,255', 'OK');
		ATE2.Serial_Exec_Cmd('AT$APP PARAM 3072,6,255', 'OK');

	# bluetooth
	bt = ATE2.Config_Get('ASSEMBLY', 'Accessory Bluetooth')
	if bt == 'Yes':
		import Bluetooth
		Bluetooth.Bluetooth_Configure()
	# jpod
	jpod = ATE2.Config_Get('ASSEMBLY', 'Accessory JPOD')
	if jpod == 'Yes':
		import JPOD
		JPOD.JPOD_Configure()
	# jpod2
	jpod2 = ATE2.Config_Get('ASSEMBLY', 'Accessory JPOD2')
	if jpod2 == 'Yes':
		import JPOD2
		JPOD2.JPOD2_Configure()
	# vpod2
	vpod2 = ATE2.Config_Get('ASSEMBLY', 'Accessory VPOD2')
	if vpod2 == 'Yes':
		import VPOD2
		VPOD2.VPOD2_Configure()
	# serial rs232
	aux = ATE2.Config_Get('ASSEMBLY', 'Accessory Serial RS232')
	if aux == 'Yes':
		import AuxPort
		AuxPort.Serial_RS232_Configure()
	iridium = ATE2.Config_Get('ASSEMBLY', 'Accessory Iridium')
	if iridium == 'Yes':
		import Iridium
		Iridium.Iridium_Configure()
	# reset device so params take effect
	LMU.Device_Reset()
	# dump info to log
	ATE2.Log_SubStep('Dump stream setup to log')
	ATE2.Serial_Exec_Cmd('AT$APP STREAM?', 'OK')
		
def ADCs_Test():		
	assy_adcList = ATE2.Config_Get('ASSEMBLY', 'ADC List')
	assy_adc4_20list = ATE2.Config_Get('ASSEMBLY', 'ADC 4-20mA Mode List')
	if assy_adcList == '' and assy_adc4_20list == '':
		ATE2.Log('ADCs not configured', logToDetails=True)
		return (1)
		
	# set special voltage for test if requested
	test_volt = ATE2.Config_Get('ASSEMBLY', 'ADC Test Voltage')
	if test_volt != '':
		ATE2.Power_Set_P25V_Voltage(test_volt, logUI=True)
	
	# vRef on if requested
	assy_vrefOutput = ATE2.Config_Get('ASSEMBLY', 'ADC vRef Output')
	if assy_vrefOutput != '':
		ATE2.Log_SubStep('Turn on vRef')
		ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 8 %s' % (assy_vrefOutput), 'OK')

	assy_vOutOutput = ATE2.Config_Get('ASSEMBLY', 'ADC vOut Output')
	if assy_vOutOutput != '':
		ATE2.Log_SubStep('Turn on vRef')
		ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 8 %s' % (assy_vOutOutput), 'OK')		

	# test the list of regular ADCs
	ADCs_Test_List(assy_adcList)
	
	# test the list of 4-20mA mode ADCs
	if assy_adc4_20list != '':
		ATE2.Log('Test ADCs in 4-20mA mode')
		for adc in assy_adc4_20list.split():	# set 4-20mA mode
			ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 8 %d' % (10 + int(adc)), 'OK')
		ADCs_Test_List(assy_adc4_20list, modeText='4-20mA Mode')
		ATE2.Log('Return ADCs to normal mode')
		for adc in assy_adc4_20list.split():	# set normal mode
			ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 9 %d' % (10 + int(adc)), 'OK')
			
	# vRef off if requested
	if assy_vrefOutput != '':
		ATE2.Log_SubStep('Turn off vRef')
		ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 9 %s' % (assy_vrefOutput), 'OK')
		
	# reset voltage if special voltage
	if test_volt != '':
		reg_volt = ATE2.Config_Get('ASSEMBLY', 'Voltage')
		ATE2.Power_Set_P25V_Voltage(reg_volt, logUI=True)
		
def ADCs_Test_List(adcList, modeText=''):
	if modeText != '': modeText = modeText + ' '
	# loop thru adcs
	for adc in adcList.split():
		adcTo = 10
		adcMin, adcMax, units = ATE2.Config_Get('TEST DATA', 'ADC%s' % (adc)).split()
		adcMin = int(adcMin); adcMax = int(adcMax)
		# loop thru retries
		for retry in range(adcTo):
			line = ATE2.Serial_Exec_Cmd('AT#ADC?', 'ADC%s' % (adc), returnMatch=True, cmdRepeatCnt=10)
			val = int(re.match('.*:\s*(\d*)', line).group(1))
			if val >= adcMin and val <= adcMax:
				ATE2.Log('ADC%s value [%d] %s in range [%d, %d]' % (adc, val, units, adcMin, adcMax), logToDetails=True)
				LMU.SQLiteDB_Post_TestResults(ts='', name='ADC%s:' % (adc), result='PASS', tvalue=val, string=units)
				PULS_Add_Entry('ADC%s %svalue [%d] %s in range [%d, %d]' % (adc, modeText, val, units, adcMin, adcMax))
				ATE2.Mfg_Add_Step_Detail(property='ADC%s' % adc, value=val, minRange=adcMin, maxRange=adcMax)
				break
			adcTo -= 1
			if adcTo > 0:
				ATE2.Sleep(1)
			else:
				LMU.SQLiteDB_Post_TestResults(ts='', name='ADC%s:' % (adc), result='FAIL', tvalue=val, string=units)
				ATE2.Mfg_Add_Step_Detail(property='ADC%s' % adc, value=val, minRange=adcMin, maxRange=adcMax)
				ATE2.Step_Fail('ADC%s %svalue [%d] %s out of range [%d, %d]' % (adc, modeText, val, units, adcMin, adcMax), errorCode='LMU_ADC_OUT_OF_RANGE')

def APN_List_Load():
	craf_apnList = ATE2.Config_Get('CRAF', 'APN List')
	Ymodem_File_Transfer('AT$APP FDNLDSER', '.', craf_apnList)
	ATE2.Serial_Exec_Cmd('FPROG 0 15', 'OK')
	PULS_Add_Entry('#APNLIST')
	PULS_Add_Entry('APN List: %s' % (craf_apnList))
	ATE2.Mfg_Add_Step_Detail(property='APN LIST', value=craf_apnList)
				
def Assembly_Configure():
	assembly_assyNum = ATE2.Config_Get('ASSEMBLY', 'Assembly Number')
	fName = assembly_assyNum + '.cmds'
	fPath = ATE2.PathFiles + 'Assembly_Configs\\' + fName
	if not os.path.exists(fPath):
		ATE2.Log('No assembly configuration file', logToDetails=True)
		return
	# assembly config file present - run cmds
	ATE2.Log('Setup assembly configuration [%s]' % (fName), logToDetails=True)
	with open(fPath) as f:
		for cmd in f:
			cmd = cmd.rstrip()
			if cmd == '' or re.search('^#', cmd):
				continue
			if re.search('^%', cmd):
				ATE2.Log(cmd)
				continue
			ATE2.Serial_Exec_Cmd(cmd, 'OK')
	f.close()
	# reset device so params take effect
	LMU.Device_Reset()
	# dump stream info to log
	ATE2.Log_SubStep('Dump stream setup to log')
	ATE2.Serial_Exec_Cmd('AT$APP STREAM?', 'OK')

def Audiovox_Alarm():
	# Need to turn on feature for this part number:
	ATE2.Serial_Exec_Cmd('ATS125=3', 'OK')
	ATE2.Serial_Exec_Cmd('ATS166=1', 'OK')
	# This function will not work with AT#TESTCONFIG on
	ATE2.Power_Turn_Off()
	ATE2.Power_Turn_On(wait=2)
	rc = ATE2.Serial_Exec_Cmd('at#av 1', 'A6 Receive 55',initDelayRead=1.5, noFail=True, cmdRepeatCnt=3)
	if not rc:
		ATE2.Step_Fail('Audiovox Alarm system did not respond', errorCode='LMU_AUDIOVOX_ALARM')
	else:
		ATE2.Log('Audiovox Alarm replied')
	ATE2.Serial_Exec_Cmd('ATS125=0', 'OK', noFail=True)
	#Used to turn the testconfig back on.
	LMU.Device_Reset()
	
def Aux_All_Port_Test(aux_port):
	cmd = 'AT#SERTEST %s' % (aux_port)
	ATE2.Serial_Exec_Cmd(cmd, 'PEND')
	ATE2.Serial_Exec_Cmd(cmd, 'PASS')

def Aux_Port_Test():
	craf_auxPortList = ATE2.Config_Get('ASSEMBLY', 'Aux Port List')
	if craf_auxPortList == '':
		ATE2.Log('No Aux Ports configured', logToDetails=True)
		return (1)
	for rec in craf_auxPortList.split('\n'):
		if rec == '': continue
		auxPort, devPort = rec.split()
		ATE2.Log_SubStep('Test %s Port' % (auxPort))
		ATE2.Serial_Exec_Cmd('AT#SERTEST %s' % (devPort), 'PEND')
		ATE2.Serial_Exec_Cmd('AT#SERTEST %s' % (devPort), 'PASS')
		PULS_Add_Entry('Aux Test %s [port %s]' % (auxPort, devPort))

def Aux_Port_Test_Old():
	ATE2.Log_SubStep('Test AUX Port Old Method')
	ATE2.Serial_Exec_Cmd('AT#AUXPORT?', 'PEND')
	ATE2.Serial_Exec_Cmd('AT#AUXPORT?', 'PASS')
		
def Battery_Charger_Test():
	assy_battery = ATE2.Config_Get('ASSEMBLY', 'Battery')
	if assy_battery != 'Yes':
		ATE2.Log('Battery not configured', logToDetails=True)
		return
	line = ATE2.Serial_Exec_Cmd('AT#BATT?', 'CHARGING|DONE', repeatCnt=5, returnMatch=True)
	state = re.match('.*:\s*(\w*)', line).group(1)
	ATE2.Log('Battery charging state [%s]' % (state))

def Battery_Charger_Ext_Test():
	adc = str(5)	#ADC which measures the battery
	Bat_Charger_Dis = str(5) #Output which disables the charger
	Curr_Bat_chrger_Off = 100 #mA, Current not to exceed when charger off
	Curr_Bat_chrger_On = 350 #mA, Current to exceed when charger is on.
	# gather information from ini
	assy_battery = ATE2.Config_Get('ASSEMBLY', 'Battery')
	adcMin, adcMax, units = ATE2.Config_Get('TEST DATA', 'ADC%s' % (adc)).split()
	#check if battery is present
	if assy_battery != 'Yes':
		ATE2.Log('Battery not configured', logToDetails=True)
		return
	Radio_Com_Off()
	ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 8 %s' %(Bat_Charger_Dis), 'OK')  # disable batt charger
	#if-else statement to make sure the current while device off is below 100
	current_off = 1000 * float(ATE2.Power_Read_Current()) #need to convert mA to A 
	if current_off > Curr_Bat_chrger_Off: 
		ATE2.Log('Current when charger off [%d] Upper Limit [%d]' % (int(current_off), int(Curr_Bat_chrger_Off)), logToDetails=True)
		ATE2.Step_Fail('Charger current above limit - Current [%d] Limit [%d]' %(int(current_off), int(Curr_Bat_chrger_Off)), errorCode='LMU_BATTERY_CHARGER_EXT_TEST_CURRENT_OFF')
	else:
		ATE2.Log('Current when charger off [%d] Upper Limit [%d]' % (int(current_off), int( Curr_Bat_chrger_Off)), logToDetails=True)	
		ATE2.Mfg_Add_Step_Detail(property='BATT CHGR CURR OFF', value=current_off, minRange='', maxRange=Curr_Bat_chrger_Off)
	#Read ADC5
	line = ATE2.Serial_Exec_Cmd('AT#ADC?', 'ADC%s' % (adc), returnMatch=True, cmdRepeatCnt=10)
	val = int(re.match('.*:\s*(\d*)', line).group(1))
	adcMin = int(adcMin) 
	adcMax = int(adcMax)
	if val >= adcMin and val <= adcMax:
		ATE2.Log('ADC%s Battery Charger value [%d] %s in range [%d, %d]' % (adc, val, units, adcMin, adcMax), logToDetails=True)
		PULS_Add_Entry('ADC%s BATT CHGE value [%d] %s in range [%d, %d]' % (adc, val, units, adcMin, adcMax))
		ATE2.Mfg_Add_Step_Detail(property='BATT CHGR ADC%s' % adc, value=val, minRange=adcMin, maxRange=adcMax)
	else:
		ATE2.Step_Fail('ADC%s Battery Charger value [%d] %s in not range [%d, %d]. Try again with a different battery' % (adc, val, units, adcMin, adcMax), errorCode='LMU_BATTERY_CHARGER_EXT_TEST_ADC')
	ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 9 %s' % (Bat_Charger_Dis), 'OK')  # Enable batt charger	
	line = ATE2.Serial_Exec_Cmd('AT#BATT?', 'CHARGING', repeatCnt=5, returnMatch=True)
	state = re.match('.*:\s*(\w*)', line).group(1)
	ATE2.Log('Battery charging state [%s]' % (state))
	current = float(ATE2.Power_Read_Current())
	current_on = current * 1000 # need to convert mA to A
	#if-else statement to make sure the Ccurrent while device on is above 350
	if current_on < Curr_Bat_chrger_On: 
		ATE2.Log('Current when charger on [%d] Lower Limit [%d]' % (int(current_on), int(Curr_Bat_chrger_On)))	
		ATE2.Step_Fail('Charger current below limit - Current [%d] Limit [%d]' % (int(current_on), int(Curr_Bat_chrger_On)), errorCode='LMU_BATTERY_CHARGER_EXT_TEST_CURRENT_ON')
	else:
		ATE2.Log('Current when charger on [%d] Lower Limit [%d]' % (int(current_on), int(Curr_Bat_chrger_On)), logToDetails=True)	
		ATE2.Mfg_Add_Step_Detail(property='BATT CHGR CURR ON', value=current_on, minRange=Curr_Bat_chrger_On, maxRange='')
	
	ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 8 %s' % (Bat_Charger_Dis), 'OK')  # Disable batt charger
	Radio_Com_On()	
	
def Battery_Check_Configured():
	assy_battery = ATE2.Config_Get('ASSEMBLY', 'Battery')
	return 'True' if assy_battery == 'Yes' else 'False'

def Battery_Data_From_User():
	ATE2.Log_SubStep('Get Battery Data From User')
	craf_batData = ATE2.Config_Get('ASSEMBLY', 'Battery Type')
	if craf_batData is None:
		ATE2.Log('Battery Type not provided')
		return
	try:
		f = open(Bat_Data + '.txt', 'r') 
		btData = f.read()
		f.close()
	except:
		btData = ''
	t = ATE2.Operator_Input(instr = 'Enter Battery Date Value', charCnt=0, noFail=False, bClear=False, defVal=btData)
	f = open(Bat_Data + '.txt', 'w')
	f.write(t)
	PULS_Add_Entry('Battery: %s-%s' %(craf_batData,btData))

def Battery_Internal_Test(): 
	ATE2.Log_SubStep('Test the relay and battery voltage')
	adc = ATE2.Config_Get('ASSEMBLY', 'Battery ADC')
	ATE2.Serial_Exec_Cmd('AT#BATT?', 'NONE')
	ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 149 1', 'OK')  # close battery relay
	ATE2.Serial_Exec_Cmd('AT#BATT?', 'CHARGING|DONE', repeatCnt=5)
	ADCs_Test_List('%s' % (adc))
	ATE2.Log_SubStep('Test the internal/external switcher')
	ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 8 7', 'OK')  # disable switcher/run on internal batt
	ATE2.Serial_Exec_Cmd('AT$APP INPUT?', 'INPUT-10.*State=1')
	ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 9 7', 'OK')  # enable switcher/run on external pwr
	ATE2.Serial_Exec_Cmd('AT$APP INPUT?', 'INPUT-10.*State=0')
	ATE2.Log_SubStep('Test the battery charger')
	ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 8 8', 'OK')  # disable batt charger
	ATE2.Serial_Exec_Cmd('AT#BATT?', 'NONE')
	ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 9 8', 'OK')  # enable batt charger
	ATE2.Serial_Exec_Cmd('AT#BATT?', 'CHARGING|DONE')
	ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 149 0', 'OK')  # open battery relay
	ATE2.Serial_Exec_Cmd('AT#BATT?', 'NONE')

def Battery_Low_Voltage_Test():
	assy_battery = ATE2.Config_Get('ASSEMBLY', 'Battery')
	assy_lowVoltInput = ATE2.Config_Get('ASSEMBLY', 'Battery Low Volt Alarm Input')
	line = ATE2.Serial_Exec_Cmd('AT#INP?', '#INP%s' % (assy_lowVoltInput), returnMatch=True)
	val = int(re.match('.*:\s*(\d*)', line).group(1))
	if val == 0:
		return
	elif val > 0 and assy_battery == 'Yes':
		ATE2.Step_Fail('Low voltage alarm with battery configured', errorCode='LMU_BATT_LOW_VOLT_WHEN_BATT')
	else:
		ATE2.Log('Voltage alarm OK when no battery configured')

def Boost_Test():
	Step_Power_3_6_v()
	ATE2.Serial_Exec_Cmd('AT$APP COMM OFF','OK')
	ATE2.Serial_Exec_Cmd('ATIC','Inactive', cmdRepeatCnt=20, cmdRepeatDelay=2, returnMatch=True)
	ATE2.Serial_Exec_Cmd('AT#ADC?','OK')
	rc = ATE2.Serial_Exec_Cmd('AT#INP?','#INP2: 0', cmdRepeatCnt=30, noFail=True)
	if not rc:
		ATE2.Serial_Exec_Cmd('AT#ADC?','OK')
		ATE2.Step_Fail('Boost did not turn off ', errorCode='LMU_BOOST_TURNOFF')
	else:
		ATE2.Log_SubStep('Boost Turned OFF')
	Step_Power_3_1_v(powerOff=False)
	ATE2.Serial_Exec_Cmd('AT#ADC?','OK')
	rc = ATE2.Serial_Exec_Cmd('AT#INP?','#INP2: 1', cmdRepeatCnt=30, noFail=True)
	if not rc:
		ATE2.Serial_Exec_Cmd('AT#ADC?','OK')
		ATE2.Step_Fail('Boost did not turn on', errorCode='LMU_BOOST_TURNON')
	else:
		ATE2.Log_SubStep('Boost Turned ON')	
		ATE2.Serial_Exec_Cmd('AT#TESTCONFIG', 'OK', cmdRepeatCnt=5)
		Step_Power_3_6_v()
		ATE2.Log_SubStep('Check Boost OFF when voltage raised')
		ATE2.Serial_Exec_Cmd('AT#INP?','#INP2: 0', cmdRepeatCnt=30)
		ATE2.Serial_Exec_Cmd('AT$APP COMM ON','OK')
		ATE2.Log_SubStep('Check Boost ON when radio is ON')
		ATE2.Serial_Exec_Cmd('AT#INP?','#INP2: 1', cmdRepeatCnt=30)
	assy_bval = ATE2.Config_Get('ASSEMBLY', 'Boost Voltage')
	if assy_bval == 'Yes':
		Step_Power_4_5v()
		return
	else:
		Step_Power_3_6_v()

def BootDelay_Set(bootDelay=10):
	#Set the rebooting delay
	ATE2.Serial_Exec_Cmd('AT#BOOTDELAY %d' %(bootDelay),'OK',repeatCnt=10, repeatDelay=2, cmdRepeatCnt=2, cmdRepeatDelay=2, returnMatch=True)
		
def Buzzer_Test():
	# ATE2.Operator_Instruction('Listen for the Buzzer to make alternating On - Off tone')
	# turn on buzzer to a 1KHZ alternating tone
	ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 11 0', '')
	rc = int(ATE2.Operator_Instruction('Is the Buzzer making alternating tones?',''))
	if not rc:
		ATE2.Step_Fail('Operator failed Buzzer Output test', errorCode='LMU_BUZZER_OPER_FAIL')
	else:  
		ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 9 0', '')	# turn off buzzer
		PULS_Add_Entry('#Buzzer Pass')

def CDMA_Activation():
	prlList = ['50650','50651','50652','50653','50654','50655','50656','50657']
	ATE2.Serial_Exec_Cmd('AT', 'OK',cmdRepeatCnt=20, cmdRepeatDelay=1 , returnMatch = True)
	Factory_Config_Set()
	ATE2.Serial_Exec_Cmd('ATS125=7', 'OK',cmdRepeatCnt=20, cmdRepeatDelay=1 , returnMatch=True)  #Turns Debug ON
	ATE2.Serial_Exec_Cmd('ATRESET','OK',repeatCnt=20, repeatDelay=3)
	ATE2.Serial_Exec_Cmd(' ','MODEM: Send Cmd: ATE0V1', repeatCnt=20, repeatDelay=2 , returnMatch = True)
	Passthru_Enter()
	ATE2.Serial_Exec_Cmd('ATE1','OK')  # Echo ON
	for rtnCnt in range(2):
		errorFlag = False  
		ATE2.Serial_Exec_Cmd('AT$RTN=000000', 'OK') # Wipe out phone number, prl and starts the OMA process
		for statCnt in range(3):
			line = ATE2.Serial_Exec_Cmd('','UOMASTAT:1,1,0', repeatCnt=60, repeatDelay=3 , returnMatch=True, noFail=True, errorRE=['UOMASTAT:1,2,']) 
			#UOMASTAT:1,0,0 means OMA Pair started, UOMASTAT:1,1,0 means OMA Pair Completed
			if line == '+UOMASTAT:1,2,' or line != '+UOMASTAT:1,1,0':
				errorFlag = True
				break
		if errorFlag == False:
			break
	if errorFlag == True:
		ATE2.Step_Fail('UOMASTAT pair did not complete', errorCode='CDMA_ACT_PAIRNOTCOMPLETE' )
	ATE2.Log_SubStep('OMASTAT Pairing Successful')
	Passthru_Exit(runTestConfig=False)
	ATE2.Serial_Exec_Cmd('ATS125=0', 'OK')   # Turns Debug OFF
	for conCnt in range(2):
		errorFlag = False
		line = ATE2.Serial_Exec_Cmd('ATIC','Connection\s*: Yes', cmdRepeatCnt=60, cmdRepeatDelay=3 , returnMatch=True, noFail=True)
		if line == 'Connection\s*: No':
			errorFlag = True
			ATE2.Serial_Exec_Cmd('ATRESET','OK',cmdRepeatCnt=1, noFail=True)
			break
		if errorFlag == False:
			break
		#Do PRL Validation
	errorFlag = True
	line = ATE2.Serial_Exec_Cmd('AT#CDMA?','#CDMA:', returnMatch=True) 
	prl = re.search('.*,(.*)', line).group(1)
	if prl == '10077':
		ATE2.Step_Fail('PRL 10077 found - not activated properly', errorCode='CDMA_ACT_NOTACTPROP')
		errorFlag = True
	for thisPrl in prlList:
		print(thisPrl)
		if prl == thisPrl:
			errorFlag = False
			break
	if errorFlag == True:
		ATE2.Step_Fail('PRL [%s] not found in the PRL List' % (prl), errorCode='CDMA_ACT_PRLNOTFOUND')
		#Do PULS Check in and Check out 
	Network_Test()
		
def Check_ATI0():
	# Check to see if ATI0 has been filled in before.
	# Assume Radio will be filled in if so.
	try:
		if UUT.ATI0['Radio'] > '': 
			# ATE2.Log_Info('ATI0 information available')
			return (1)
	except:
		# ATE2.Log_Info('ATI0 information not available')
		return (0)

def Check_Radio_Operating_Mode(): 
	# This function will check the radio is operating mode
	assy_procType = ATE2.Config_Get('ASSEMBLY', 'Processor Type')
	craf_fwVer = ATE2.Config_Get('CRAF','LMU FW Ver')
	
	ats125_val = Debug_Value()
	Debug_On(val=7)
	outBuf = ATE2.Serial_Exec_Cmd('AT$APP MODEM CMD "AT+CFUN?"', '\+CFUN: 1', repeatCnt=5, noFail=True)
	for line in outBuf:
		if re.search('\+CFUN: 1', line):
			mline = re.search('.*(\+CFUN: 1.*)',line).group(1)
			ATE2.Log('Operating Mode: [%s]' % (mline), logToDetails=True)
			ATE2.Mfg_Add_Step_Detail(property='RADIO OP MODE', value=mline, minRange='', maxRange='')
			Debug_On(val=ats125_val)
			return 1

	# Can only use the below commands on certain devices.
	# Other devices need to determine the problem on why the unit is in Non-Radio mode
	if (re.match('STM8', assy_procType)) or (re.match('STM32', assy_procType) and craf_fwVer <= '4.0a'):
		ATE2.Step_Fail('Radio Operating Mode is set incorrectly.', errorCode='LMU_CHECK_RADIO_OPERATING_MODE')	

	Logger.debug('Resetting operating mode in Radio')
	for retry in range(3):
		if re.match('STM32', assy_procType) and craf_fwVer <= '4.0a':
			ATE2.Serial_Exec_Cmd('AT$APP MODEM CMD "AT+CFUN=1"', 'OK', repeatCnt=5)
		else:
			ATE2.Serial_Exec_Cmd('AT+CFUN=1', 'OK', repeatCnt=5)
		ATE2.Sleep(5)
		outBuf = ATE2.Serial_Exec_Cmd('AT$APP MODEM CMD "AT+CFUN?"', '\+CFUN: 1', repeatCnt=2, noFail=True)
		for line in outBuf:
			if re.search('\+CFUN: 1', line):
				mline = re.search('.*(\+CFUN: 1.*)',line).group(1)
				ATE2.Log('Operating Mode: [%s]' % (mline), logToDetails=True)
				ATE2.Mfg_Add_Step_Detail(property='RADIO OP MODE', value=mline, minRange='', maxRange='')
				Debug_On(val=ats125_val)
				return 1
	ATE2.Step_Fail('Radio Operating Mode is set incorrectly', errorCode='LMU_CHECK_RADIO_OPERATING_MODE')	
	
def Comm_Get_Radio_Version():
	assy_radioVerList = ATE2.Config_Get('ASSEMBLY', 'Radio Ver List')
	# Need to see if Radio_Version already available
	if assy_radioVerList == None or (assy_radioVerList == ''): return
	if Check_ATI0(): return UUT.ATI0['Radio']
	Info_ATI0(ats125='ON')
	return UUT.ATI0['Radio']
		
def Comm_Test():
	# get radio attributes
	assy_radioType = ATE2.Config_Get('ASSEMBLY', 'Radio Type')
	uut_model =  ATE2.Config_Get('CRAF', 'Model')
	uut_radioVer = Comm_Get_Radio_Version()
	# match radio to test function
	with open (Dir_ATE_DB + 'radio73.txt', 'r') as f:
		for line in f:
			#Ticket #436			
			num_param_radio = len(line.split('||'));
			if num_param_radio == 5:
				radioType, carrier, radioVer, rule, commFunc = line.split('||');
			else:
				ATE2.Step_Fail('Radio file in DB directory does not have the correct # of items: [%d]. \n%s' % (num_param_radio,line), errorCode='LMU_COMM_RADIO_FILE_ERR')
			commFunc = commFunc.rstrip('\n')
			if radioType != assy_radioType and radioType != '*': continue
			# print('^%s^ = ^%s^' % (uut_radioVer, radioVer))
			if uut_radioVer != radioVer and radioVer != '*': continue
			if (re.search(uut_model, rule) == None): continue
			# call the selected Comm test
			ATE2.Log_Info('Test using function [%s]' % (commFunc))
			if commFunc == 'Comm_Test_HSPA_UTEST': Comm_Test_HSPA_UTEST()
			elif commFunc == 'Comm_Test_Ublox_Sprint': Comm_Test_Ublox_Sprint()
			elif commFunc == 'Comm_Test_GPRS_HSPA': Comm_Test_GPRS_HSPA()
			elif commFunc == 'Comm_Test_Cinterion_EHS': Comm_Test_Cinterion_EHS()
			elif commFunc == 'Comm_Test_EHS5_HSPA_POWER_METER_PASSTHRU': Comm_Test_EHS5_HSPA_POWER_METER_PASSTHRU()
			elif commFunc == 'Comm_Test_GPRS_HSPA_Linux': Linux.Comm_Test_GPRS_HSPA()
			elif commFunc == 'Comm_Test_Tx_Power': Linux.Comm_Test_Tx_Power()
			elif commFunc == 'Comm_Test_RSRP': Linux.Comm_Test_RSRP()
			elif commFunc == 'Comm_Test_Telit_HSPA_POWER_METER_PASSTHRU': Comm_Test_Telit_HSPA_POWER_METER_PASSTHRU()
			elif commFunc == 'Comm_Test_Telit_LTE_PWR_MTR': RF.Comm_Test_Telit_LTE_PWR_MTR()
			elif commFunc == 'Comm_Test_MTK_GSM_POWER_METER_PASSTHRU': RF.Comm_Test_MTK_GSM_POWER_METER_PASSTHRU()
			elif commFunc == 'Comm_Test_Ublox_TOBY_PWR_MTR': RF.Comm_Test_Ublox_TOBY_PWR_MTR()
			else:
				ATE2.Step_Fail('Requested Comm test [%s] not found' % (commFunc), errorCode='LMU_COMM_TEST_NOT_FOUND')
			f.close()
			return
		ATE2.Step_Fail('Radio attributes did not match entry in Radio table [%s] [%s] [%s]' % (assy_radioType, uut_radioVer, uut_model), errorCode='LMU_COMM_RADIO_ATTR_NO_MATCH')
	f.close()

def Comm_Test_Cinterion_EHS():
	tmo = 180; tmoCnt = 0; repeatDelay = 3; wait_sxrat = 5
	
	ATE2.Serial_Exec_Cmd('ATS125=7', 'OK')
	ATE2.Serial_Exec_Cmd('ATRESET', 'CREG?', repeatCnt=30, repeatDelay = 1.5, initDelayRead=1, returnMatch=False, exitOnMatch=False, cmdRepeatCnt=1, cmdRepeatDelay=1, noFail=False)
	Passthru_Enter()
	# Will place radio into GSM only mode
	#Wait for radio to start searching.
	ATE2.Sleep(wait_sxrat)
	ATE2.Serial_Exec_Cmd('AT^SXRAT=0,0','OK', repeatCnt=60, initDelayRead=1, returnMatch=True)
	ATE2.Log('Placing radio into GSM Only mode', logToDetails=True)
	
	craf_comm = ATE2.Config_Get('TEST DATA', 'COMM')
	craf_rssiLow, craf_rssiHigh, craf_units = craf_comm.split()
	craf_rssiLow = int(craf_rssiLow)
	craf_rssiHigh = int(craf_rssiHigh)
	ATE2.Log('Comm RSSI range [%d to %d] %s' % (craf_rssiLow, craf_rssiHigh, craf_units), logToDetails=True)
	
	while True:
		line = ATE2.Serial_Exec_Cmd('AT^SMONI','SMONI:|ERROR',repeatCnt=3, initDelayRead=1, returnMatch=True, exitOnMatch=False, cmdRepeatCnt=1, cmdRepeatDelay=3, noFail=False)
		if re.search('ERROR', line):
			ATE2.Sleep(repeatDelay)
			continue
		flds = line.strip().split(',')
		if re.search('SEARCH', flds[2]):
			ATE2.Sleep(repeatDelay)
			tmoCnt += repeatDelay
			continue
		uut_rssi = int(flds[2])
		Logger.debug('RSSI value [%d] in range [%d] [%d]' % (craf_rssiLow, uut_rssi, craf_rssiHigh))
		if craf_rssiHigh >= uut_rssi >= craf_rssiLow:
			LMU.SQLiteDB_Post_TestResults(ts='', name='#COMM', result='PASS', tvalue=uut_rssi, string='')
			ATE2.Mfg_Add_Step_Detail(property='COMM', value=uut_rssi, minRange=craf_rssiLow, maxRange=craf_rssiHigh)
			PULS_Add_Entry('Comm value [%d] %s in range [%d, %d]' % (uut_rssi, craf_units, craf_rssiLow, craf_rssiHigh))
			ATE2.Log_Info('Comm passed with channel [%d] value [%d] %s' % (int(flds[1]), uut_rssi, craf_units), logToDetails=True)
			ATE2.Log('Placing radio into GSM/UMTS mode', logToDetails=True)
			ATE2.Serial_Exec_Cmd('AT^SXRAT=1,2','OK', repeatCnt=60, initDelayRead=1, returnMatch=True)
			Passthru_Exit()
			ATE2.Serial_Exec_Cmd('ATS125=0', 'OK')
			return
		tmoCnt += repeatDelay
		Logger.debug('Timeout number %d out of %d' % (tmoCnt, tmo))
		if tmoCnt >= tmo: break
		ATE2.Sleep(repeatDelay)
	LMU.SQLiteDB_Post_TestResults(ts='', name='#COMM', result='FAIL', tvalue=uut_rssi, string='')
	ATE2.Step_Fail('RSSI [%d] not within range [%d %d]' % (uut_rssi, craf_rssiLow, craf_rssiHigh), errorCode='LMU_COMM_RSSI_RANGE_CINTERION_EHS')

def Comm_Test_EHS5_HSPA_POWER_METER_PASSTHRU():
	# Pwr_Sensor_Path provided as a global variable
	tmo = 10; tmoCnt = 0; repeatDelay = 3
	craf_TxPwrLow, craf_TxPwrHigh, craf_units = ATE2.Config_Get('TEST DATA', 'COMM').split()
	craf_TxPwrLow = float(craf_TxPwrLow)
	craf_TxPwrHigh = float(craf_TxPwrHigh)

	assy_channel = ATE2.Config_Get('ASSEMBLY', 'Radio Channel')
	assy_channel = int(assy_channel)
	pwr_sensor_freq = RF.Calc_ARFCN_Freq(assy_channel)
	Logger.debug('Channel [%d], Frequency [%s]' % (assy_channel,str(pwr_sensor_freq)))
	ATE2.Log('Comm Tx range [%.2f to %.2f] %s' % (craf_TxPwrLow, craf_TxPwrHigh, craf_units), logToDetails=True)

	#< === now read power sensor through Minicircuits dll === >
	# Read All Serial Numbers
	try:
		pipe = subprocess.Popen(Pwr_Sensor_Path + "pwr_cs.exe -ls", stdout=subprocess.PIPE)
		pipe.wait
		SerialNumber = pipe.stdout.read()
		sn = str(int(SerialNumber))
		Logger.debug('Pwr Meter Serial # %d' % (int(SerialNumber)))

		# Read Model Name 
		pipe = subprocess.Popen(Pwr_Sensor_Path + "pwr_cs.exe -sn " + sn + " -m", stdout=subprocess.PIPE)
		pipe.wait
		ModelName = pipe.stdout.read()
		Logger.debug('Pwr Meter Model: %s' % (ModelName))

	except:
		ATE2.Step_Fail('Power Meter not Available.', errorCode = 'EQUIPMENT_ERROR_POWER_METER')
	
	# Enter Test Mode
	uut_radioVer = Comm_Get_Radio_Version()
	
	#Enter Passthru Mode
	ATE2.Serial_Exec_Cmd('ATS125=7', 'OK')
	ATE2.Serial_Exec_Cmd('ATRESET', 'ATS0=0', repeatCnt=30, repeatDelay = 1.5, initDelayRead=1, returnMatch=False, exitOnMatch=False, cmdRepeatCnt=1, cmdRepeatDelay=1, noFail=False)
	ATE2.Serial_Exec_Cmd('ATPASSTHRU', 'OK|CME ERROR')
	# Wait for Radio to take passthru mode.
	ATE2.Sleep(1)
	# Place in Airplane mode so SIM will not get in the way of settings.
	ATE2.Serial_Exec_Cmd('AT+CFUN=4', 'SYSSTART|PBREADY', repeatCnt=20)
	ATE2.Sleep(1)

	try:
		uut_ID1, uut_ID5 = re.match('.*-(\d*).(\d*)/(\d*).(\d*).(\d*)', uut_radioVer).group(1,5)
		if (int(uut_ID1) >= 3) and (int(uut_ID5) > 14):
			#New Method
			ATE2.Serial_Exec_Cmd('AT^SCFG="MEopMode/CT","1","","40","%d"' %(assy_channel),'OK', repeatCnt=2, initDelayRead=1, returnMatch=True)		
		else:
			#Old Method
			ATE2.Serial_Exec_Cmd('AT^SCFG="MEopMode/CT","1","","High",%d' %(assy_channel),'OK', repeatCnt=2, initDelayRead=1, returnMatch=True)
	except:
		# Old method
		line = ATE2.Serial_Exec_Cmd('AT^SCFG="MEopMode/CT","1","","High",%d' %(assy_channel),'OK|ERROR', repeatCnt=2, initDelayRead=1, returnMatch=True)
		if re.search('ERROR', line):
			#New Method
			line = ATE2.Serial_Exec_Cmd('AT^SCFG="MEopMode/CT","1","","40","%d"' %(assy_channel),'OK|ERROR', repeatCnt=2, initDelayRead=1, returnMatch=True)					
			if re.search('ERROR', line):
				#Fail can not use.
				ATE2.Serial_Exec_Cmd('AT+CFUN=1,1', 'SYSSTART|PBREADY', repeatCnt=20)
				ATE2.Step_Fail('Radio command error', errorCode = 'LMU_Comm_Test_EHS5_HSPA_POWER_METER_PASSTHRU_RADIO')	

	
	# Read Power (Set Compensation Frequency First)
	while True:
		try:
			freq = pwr_sensor_freq
			pipe = subprocess.Popen(Pwr_Sensor_Path + "pwr_cs.exe -sn " + sn + " -p " + str(freq), stdout=subprocess.PIPE)
			pipe.wait
			Power = pipe.stdout.read()
			# extract float
			val = []
			for t in Power.split():
				try:
					val.append(float(Power))
				except ValueError:
					pass
			# now remove brackets
			pwr = (", ".join( str(e) for e in val))
			pwr = float(pwr)
			
		except:
			ATE2.Serial_Exec_Cmd('AT+CFUN=1,1', 'SYSSTART|PBREADY', repeatCnt=20)
			ATE2.Sleep(2)
			ATE2.Step_Fail('Power Meter not Available.', errorCode = 'EQUIPMENT_ERROR_POWER_METER')
			
		if craf_TxPwrHigh >= pwr >= craf_TxPwrLow:
			Logger.debug('TX Power value [%.2f] in range [%.2f] [%.2f]' % (pwr, craf_TxPwrLow, craf_TxPwrHigh))
			ATE2.Log_Info('Comm passed with value [%.2f] %s' % (pwr, craf_units), logToDetails=True)
			ATE2.Mfg_Add_Step_Detail(property='COMM', value='%.2f' %pwr, minRange=craf_TxPwrLow, maxRange=craf_TxPwrHigh)
			LMU.PULS_Add_Entry('Comm value [%.2f] %s in range [%.2f, %.2f]' % (pwr, craf_units, craf_TxPwrLow, craf_TxPwrHigh))
			
			ATE2.Serial_Exec_Cmd('AT+CFUN=1,1', 'SYSSTART|PBREADY', repeatCnt=20)
			ATE2.Sleep(2)
			
			# now turn off Tx and return radio to Operational Mode"
			Passthru_Exit()
			ATE2.Sleep(repeatDelay)
			ATE2.Serial_Exec_Cmd('ATS125=0', 'OK')
			return
		Logger.debug('TX Power value [%.2f] not in range [%.2f] [%.2f]' % (pwr, craf_TxPwrLow, craf_TxPwrHigh))
		tmoCnt += repeatDelay
		if tmoCnt >= tmo: break
		ATE2.Sleep(repeatDelay)
	# Need to put back to OM mode
	ATE2.Serial_Exec_Cmd('AT+CFUN=1,1', 'SYSSTART|PBREADY', repeatCnt=20)
	ATE2.Sleep(1)
	Passthru_Exit()
	ATE2.Sleep(repeatDelay)
	ATE2.Serial_Exec_Cmd('ATS125=0', 'OK')
	ATE2.Step_Fail('Power Level not within range - last reported value [%.2f]' % (pwr), errorCode = 'LMU_COMMTEST_TXPOWER_LIMIT_FAILURE')
	
def Comm_Test_GPRS_HSPA():
	tmo = 180; tmoCnt = 0; repeatDelay = 3
	craf_comm = ATE2.Config_Get('TEST DATA', 'COMM')
	craf_rssiLow, craf_rssiHigh, craf_units = craf_comm.split()
	craf_rssiLow = int(craf_rssiLow)
	craf_rssiHigh = int(craf_rssiHigh)
	ATE2.Log('Comm RSSI range [%d to %d] %s' % (craf_rssiLow, craf_rssiHigh, craf_units), logToDetails=True)
	while True:
		line = ATE2.Serial_Exec_Cmd('AT#COMM?','#COMM:', returnMatch=True)
		if re.search('SIM ERROR', line):
			ATE2.Step_Fail('SIM Error during test', errorCode='LMU_COMM_SIM_ERR_GPRS_HSPA')
		uut_rssi = int(re.sub('^.*,', '', line))
		Logger.debug('RSSI value [%d] in range [%d] [%d]' % (craf_rssiLow, uut_rssi, craf_rssiHigh))
		if craf_rssiHigh >= uut_rssi >= craf_rssiLow:
			ATE2.Log_Info('Comm passed with value [%d] %s' % (uut_rssi, craf_units), logToDetails=True)
			LMU.SQLiteDB_Post_TestResults(ts='', name='#COMM', result='PASS', tvalue=uut_rssi, string=craf_units)
			ATE2.Mfg_Add_Step_Detail(property='COMM', value=uut_rssi, minRange=craf_rssiLow, maxRange=craf_rssiHigh)
			PULS_Add_Entry('Comm value [%d] %s in range [%d, %d]' % (uut_rssi, craf_units, craf_rssiLow, craf_rssiHigh))
			return
		tmoCnt += repeatDelay
		if tmoCnt >= tmo: break
		ATE2.Sleep(repeatDelay)
	LMU.SQLiteDB_Post_TestResults(ts='', name='#COMM', result='FAIL', tvalue=uut_rssi, string=craf_units)
	ATE2.Step_Fail('RSSI [%d] not within range [%d %d]' % (uut_rssi, craf_rssiHigh, craf_rssiLow), errorCode='LMU_COMM_RSSI_RANGE_GPRS_HSPA')
	
def Comm_Test_HSPA_UTEST():
	craf_fwVer = ATE2.Config_Get('CRAF','LMU FW Ver')
	assy_comm = ATE2.Config_Get('TEST DATA', 'COMM')
	assy_procType = ATE2.Config_Get('ASSEMBLY', 'Processor Type')
	assy_rssiLow, assy_rssiHigh, assy_units = assy_comm.split()
	ATE2.Log('Comm RSSI range [%s to %s] %s' % (assy_rssiLow, assy_rssiHigh, assy_units), logToDetails=True)
	errFlag = False
	if (re.match('STM8', assy_procType) and craf_fwVer >= '2.6m') or (re.match('STM32', assy_procType) and craf_fwVer >= '3.7e'):
	#if craf_fwVer >= '3.7e' :
		ATE2.Serial_Exec_Cmd('ATI0', 'Radio:u-', cmdRepeatCnt=20, cmdRepeatDelay=3)
		time.sleep(2)
		retryCnt = 10
		for retry in range(retryCnt,0,-1):
			ATE2.Serial_Exec_Cmd('AT#UTEST 150 5000', 'OK', repeatCnt=3, repeatDelay=3)
			time.sleep(2)
			line = ATE2.Serial_Exec_Cmd('AT#UTEST?', '150,5000', cmdRepeatCnt=15, cmdRepeatDelay=2, returnMatch=True)
			uut_rssi = int(re.search('.*,([-\d]*),.*', line).group(1))
			if uut_rssi >= int(assy_rssiLow) and uut_rssi <= int(assy_rssiHigh):
				errFlag = False
				break
			else:
				errFlag = True
	else:
		ATE2.Log('Using Passthru UTEST prior to version 3.7e' , logToDetails=True)
		ATE2.Serial_Exec_Cmd('ATS125=7', 'OK')
		ATE2.Serial_Exec_Cmd('ATRESET', 'ATE0V1', repeatCnt=20, initDelayRead=0.25, repeatDelay=2, returnMatch=False, exitOnMatch=False, cmdRepeatCnt=1, cmdRepeatDelay=1, noFail=False)
		Passthru_Enter()
		time.sleep(8)
		ATE2.Serial_Exec_Cmd('AT+COPS=2','OK')
		ATE2.Serial_Exec_Cmd('AT+UTEST=1','OK')
		time.sleep(2)
		retryCnt = 10
		for retry in range(retryCnt,0,-1):
			line = ATE2.Serial_Exec_Cmd('AT+UTEST=2,150,5000','UTEST:', cmdRepeatCnt=15, returnMatch=True)
			uut_rssi = int(re.search('.*,.*,.*,([-\d]*),.*', line).group(1))
			if uut_rssi >= int(assy_rssiLow) and uut_rssi <= int(assy_rssiHigh):
				errFlag = False
				break
			else:
				if retry == 1:
					errFlag = True
	if errFlag:
		LMU.SQLiteDB_Post_TestResults(ts='', name='#COMM', result='FAIL', tvalue=uut_rssi, string=assy_units)
		ATE2.Step_Fail('RSSI [%s] not within range [%s %s]' % (uut_rssi, assy_rssiLow, assy_rssiHigh), errorCode='LMU_COMM_RSSI_RANGE_HSPA_UTEST')	
	LMU.SQLiteDB_Post_TestResults(ts='', name='#COMM', result='PASS', tvalue=uut_rssi, string=assy_units)
	ATE2.Mfg_Add_Step_Detail(property='COMM', value=uut_rssi, minRange=assy_rssiLow, maxRange=assy_rssiHigh)
	PULS_Add_Entry('Comm value [%d] %s in range [%s, %s]' % (uut_rssi, assy_units, assy_rssiLow, assy_rssiHigh))
	ATE2.Log_Info('Comm passed with value [%d] %s' % (uut_rssi, assy_units), logToDetails=True)
	Passthru_Exit()
	ATE2.Serial_Exec_Cmd('ATS125=0', 'OK')
	ATE2.Log('Power cycle board because UTEST leaves radio in bad state')
	ATE2.Power_Turn_Off()
	ATE2.Power_Turn_On()
	Device_Reset()

def Comm_Test_Telit_HSPA_POWER_METER_PASSTHRU():
	# Pwr_Sensor_Path provided as a global variable
	tmo = 10; tmoCnt = 0; repeatDelay = 3
	craf_TxPwrLow, craf_TxPwrHigh, craf_units = ATE2.Config_Get('TEST DATA', 'COMM').split()
	craf_TxPwrLow = float(craf_TxPwrLow)
	craf_TxPwrHigh = float(craf_TxPwrHigh)

	assy_band = ATE2.Config_Get('ASSEMBLY', 'Radio Band')
	assy_band = int(assy_band)
	assy_channel = ATE2.Config_Get('ASSEMBLY', 'Radio Channel')
	assy_channel = int(assy_channel)
	# Used for Power Level in 2G mode
	assy_txgain =  ATE2.Config_Get('ASSEMBLY', 'Radio Txgain')
	assy_txgain = int(assy_txgain)
	pwr_sensor_freq = ATE2.Config_Get('WORKSTATION', 'Power Sensor Freq')
	pwr_sensor_freq = int(pwr_sensor_freq)
	ATE2.Log('Comm Tx range [%.2f to %.2f] %s' % (craf_TxPwrLow, craf_TxPwrHigh, craf_units), logToDetails=True)
	
	#Enter Passthru Mode
	ATE2.Serial_Exec_Cmd('ATS125=7', 'OK')
	ATE2.Serial_Exec_Cmd('ATRESET', 'ATE0V1', repeatCnt=20, initDelayRead=0.25, repeatDelay=2, returnMatch=False, exitOnMatch=False, cmdRepeatCnt=1, cmdRepeatDelay=1, noFail=False)
	Passthru_Enter()
	
	ATE2.Serial_Exec_Cmd('ATE1', 'OK', cmdRepeatCnt=2, cmdRepeatDelay = 3)
	ATE2.Serial_Exec_Cmd('AT', 'OK', cmdRepeatCnt=2, cmdRepeatDelay = 3)
	#ATE2.Sleep(3)
	line = ATE2.Serial_Exec_Cmd('AT#TESTMODE?', '#TESTMODE:', returnMatch=True)
	flds = line.strip().split(':')
	if int(flds[1]) == 0:
		# Operative Mode - Need to change to Test Mode
		Logger.debug('Radio being placed into Test Mode')
		ATE2.Serial_Exec_Cmd('AT#TESTMODE="TM"', 'PBREADY', repeatCnt=20)
	elif int(flds[1]) == 1:
		# Test Mode - Leave Alone
		Logger.debug('Radio already in Test Mode')
	else:
		ATE2.Step_Fail('Radio returned incorrect Test Mode', errorCode='LMU_COMM_TEST_MODE_ERR')
	# Allow reboot of radio
	ATE2.Sleep(1)
	# set band
	cmd = ('AT#TestMode="setpcsband %s"' %(assy_band))
	ATE2.Serial_Exec_Cmd(cmd, 'OK', cmdRepeatCnt=1, repeatCnt=3, cmdRepeatDelay = 2)	
	#set channel
	cmd = ('AT#TestMode="CH %s"' %(assy_channel))
	ATE2.Serial_Exec_Cmd(cmd, 'OK', cmdRepeatCnt=1, repeatCnt=3, cmdRepeatDelay = 2)
	#enable TX
	cmd = ('AT#TestMode="TCH"')
	ATE2.Serial_Exec_Cmd(cmd, 'OK', cmdRepeatCnt=1, repeatCnt=3, cmdRepeatDelay = 2)
	#set TX gain 
	cmd = ('AT#TestMode="PL %s"' %(assy_txgain))
	ATE2.Serial_Exec_Cmd(cmd, 'OK', port = ATE2.LINUXport, cmdRepeatCnt=1, repeatCnt=3, cmdRepeatDelay = 2)	
	
	#< === now read power sensor through Minicircuits dll === >

	# Read All Serial Numbers
	pipe = subprocess.Popen(Pwr_Sensor_Path + "pwr_cs.exe -ls", stdout=subprocess.PIPE)
	pipe.wait
	SerialNumber = pipe.stdout.read()
	try:
		sn = str(int(SerialNumber))
	except:
		ATE2.Step_Fail('Power Meter not Available.', errorCode = 'EQUIPMENT_ERROR_POWER_METER')
	Logger.debug('Pwr Meter Serial # %d' % (int(SerialNumber)))

	# Read Model Name 
	pipe = subprocess.Popen(Pwr_Sensor_Path + "pwr_cs.exe -sn " + sn + " -m", stdout=subprocess.PIPE)
	pipe.wait
	ModelName = pipe.stdout.read()
	Logger.debug('Pwr Meter Model: %s' % (ModelName))

	# Read Power (Set Compensation Frequency First)
	while True:
		freq = pwr_sensor_freq
		pipe = subprocess.Popen(Pwr_Sensor_Path + "pwr_cs.exe -sn " + sn + " -p " + str(freq), stdout=subprocess.PIPE)
		pipe.wait
		Power = pipe.stdout.read()
		# extract float
		val = []
		for t in Power.split():
			try:
				val.append(float(Power))
			except ValueError:
				pass
		# now remove brackets
		pwr = (", ".join( str(e) for e in val))
		pwr = float(pwr)
		
		if craf_TxPwrHigh >= pwr >= craf_TxPwrLow:
			Logger.debug('TX Power value [%.2f] in range [%.2f] [%.2f]' % (pwr, craf_TxPwrLow, craf_TxPwrHigh))
			ATE2.Log_Info('Comm passed with value [%.2f] %s' % (pwr, craf_units), logToDetails=True)
			ATE2.Mfg_Add_Step_Detail(property='COMM', value='%.2f' %pwr, minRange=craf_TxPwrLow, maxRange=craf_TxPwrHigh)
			LMU.PULS_Add_Entry('Comm value [%.2f] %s in range [%.2f, %.2f]' % (pwr, craf_units, craf_TxPwrLow, craf_TxPwrHigh))
			# now turn off Tx and return radio to Operational Mode"
			ATE2.Serial_Exec_Cmd('AT#TESTMODE="ESC"', 'OK', cmdRepeatCnt=1, repeatCnt=3, cmdRepeatDelay = 2)
			ATE2.Serial_Exec_Cmd('AT#TESTMODE="OM"', 'OK', cmdRepeatCnt=1, repeatCnt=10, cmdRepeatDelay = 2)
			Passthru_Exit()
			ATE2.Sleep(repeatDelay)
			ATE2.Serial_Exec_Cmd('ATS125=0', 'OK')
			return
		Logger.debug('TX Power value [%.2f] not in range [%.2f] [%.2f]' % (pwr, craf_TxPwrLow, craf_TxPwrHigh))
		tmoCnt += repeatDelay
		if tmoCnt >= tmo: break
		ATE2.Sleep(repeatDelay)
	# Need to put back to OM mode
	ATE2.Serial_Exec_Cmd('AT#TESTMODE="OM"', 'OK', repeatCnt=5)
	ATE2.Sleep(1)
	ATE2.Step_Fail('Power Level not within range - last reported value [%.2f]' % (pwr), errorCode = 'LMU_COMMTEST_TXPOWER_LIMIT_FAILURE')

def Comm_Test_Ublox_Sprint():
	ATE2.Serial_Exec_Cmd('ATS125=7', 'OK')
	# Note: Need to wait for the MODEM to respond because Iridium units will have ALTMDM respond first.
	ATE2.Serial_Exec_Cmd('ATRESET', 'MODEM: Rcv Resp: ATE0V1', repeatCnt=20, initDelayRead=0.25, repeatDelay=2, returnMatch=False, exitOnMatch=False, cmdRepeatCnt=1, cmdRepeatDelay=1, noFail=False)
	Passthru_Enter()
	
	tmo = 180; tmoCnt = 0; repeatDelay = 3
	craf_comm = ATE2.Config_Get('TEST DATA', 'COMM')
	craf_rssiLow, craf_rssiHigh, craf_units = craf_comm.split()
	craf_rssiLow = int(craf_rssiLow)
	craf_rssiHigh = int(craf_rssiHigh)
	ATE2.Log('Comm RSSI range [%d to %d] %s' % (craf_rssiLow, craf_rssiHigh, craf_units), logToDetails=True)
	while True:
		line = ATE2.Serial_Exec_Cmd('AT+NETPAR=0','NETPAR:', returnMatch=True)
		flds = line.strip().split(',')
		if re.search('SIM ERROR', line):
			ATE2.Step_Fail('SIM Error during test', errorCode='LMU_COMM_SIM_ERR_UBLOX_SPRINT')
		uut_rssi = int(flds[5])
		Logger.debug('RSSI value [%d] in range [%d] [%d]' % (craf_rssiLow, uut_rssi, craf_rssiHigh))
		if craf_rssiHigh >= uut_rssi >= craf_rssiLow:
			LMU.SQLiteDB_Post_TestResults(ts='', name='#COMM', result='PASS', tvalue=uut_rssi, string='')
			ATE2.Mfg_Add_Step_Detail(property='COMM', value=uut_rssi, minRange=craf_rssiLow, maxRange=craf_rssiHigh)
			PULS_Add_Entry('Comm value [%d] %s in range [%d, %d]' % (uut_rssi, craf_units, craf_rssiLow, craf_rssiHigh))
			ATE2.Log_Info('Comm passed with value [%d] %s' % (uut_rssi, craf_units), logToDetails=True)
			Passthru_Exit()
			ATE2.Serial_Exec_Cmd('ATS125=0', 'OK')
			return
		tmoCnt += repeatDelay
		if tmoCnt >= tmo: break
		ATE2.Sleep(repeatDelay)
	LMU.SQLiteDB_Post_TestResults(ts='', name='#COMM', result='FAIL', tvalue=uut_rssi, string='')
	ATE2.Step_Fail('RSSI [%d] not within range [%d %d]' % (uut_rssi, craf_rssiLow, craf_rssiHigh), errorCode='LMU_COMM_RSSI_RANGE_UBLOX_SPRINT')
	
def Customer_App_Download():
	craf_custAppVer = ATE2.Config_Get('CRAF', 'Customer App Ver')
	if craf_custAppVer == '':
		ATE2.Log('No Customer App', logToDetails=True)
		return (1)
	# check if cust app updated
	match1 = ATE2.Serial_Exec_Cmd('AT$APP PARAM? 1024,1', '^1024', returnMatch=True)
	match2 = ATE2.Serial_Exec_Cmd('AT$APP PARAM? 1024,23', '^1024', returnMatch=True)
	uut_custAppVer = re.split(',', match1)[2] + '.' + re.split(',', match2)[2]
	
	#02.01.11.15 - Update
	craf_pn = ATE2.Config_Get('CRAF', 'Part Number')
	custAppFileSearch = craf_custAppVer + '-' + craf_pn
	
	if craf_custAppVer == uut_custAppVer:
		ATE2.Log('Customer App version up-to-date [%s]' % uut_custAppVer)
	else:
		ATE2.Log('Customer App not up-to-date - CRAF [%s] Board [%s]' % (craf_custAppVer, uut_custAppVer))
		# find cust app file
		for f in os.listdir(ATE2.PathFiles):
			if re.search(custAppFileSearch, f):
				custAppFile = f
				break
		else:
			ATE2.Step_Fail('Customer App file not found [%s]' % (custAppFileSearch), errorCode='LMU_CUST_APP_DNLD_FILE_NOT_FOUND')
		# select method for loading script
		assy_procType = ATE2.Config_Get('ASSEMBLY', 'Processor Type')
		ATE2.Log('Customer App file [%s]' % (custAppFile))
		PULS_Add_Entry('Customer Script: %s' % (custAppFile))
		if re.match('STM8', assy_procType):
			Customer_App_Download_Serial(custAppFile)
		else:
			Custom_App_Download_Ymodem(custAppFile)
		# check the version values
		ATE2.Log_SubStep('Verify Customer App updated')
		match1 = ATE2.Serial_Exec_Cmd('AT$APP PARAM? 1024,1', '^1024', returnMatch=True)
		match2 = ATE2.Serial_Exec_Cmd('AT$APP PARAM? 1024,23', '^1024', returnMatch=True)
		uut_custAppVer = re.split(',', match1)[2] + '.' + re.split(',', match2)[2]
		if craf_custAppVer == uut_custAppVer:
			LMU.SQLiteDB_Post_TestResults(ts='', name='#CUST APP', result='PASS', tvalue=uut_custAppVer, string='')
			ATE2.Log('Customer App version up-to-date [%s]' % uut_custAppVer)
			# check maintenance report interval < 3600
			ATE2.Log_SubStep('Check the maintenance report interval in allowed range')
			ATE2.Serial_Exec_Cmd('AT$APP PARAM? 2322,0', 'OK')
		else:
			LMU.SQLiteDB_Post_TestResults(ts='', name='#CUST APP', result='FAIL', tvalue=uut_custAppVer, string='')
			ATE2.Step_Fail('Customer App version not up-to-date - CRAF [%s] Board [%s]' % (craf_custAppVer, uut_custAppVer), errorCode='LMU_CUST_APP_DNLD_VER_NOT_UTD')

def Customer_App_Download_Serial(custAppFile):
	ATE2.Log_SubStep('Use serial method to load Customer App')
	with open(ATE2.PathFiles + custAppFile) as fp:
		cnt = 0
		for line in fp:
			line = line.rstrip('\r\n')
			if re.match('^param', line): continue
			cnt += 1
			id, index, value = line.split(',')
			cmd = LMU_CSV.Get_Cust_Script_Cmd(int(id), index, value)
			Logger.debug('>> %s' % (cmd))
			ATE2.Serial_Exec_Cmd(cmd, 'OK', initDelayRead=.05)
		ATE2.Log('Records processed [%d]' % (cnt))
	fp.close()

def Custom_App_Download_Ymodem(custAppFile):
	ATE2.Log_SubStep('Use Ymodem to load Customer App')
	ATE2.Log('Downloading file [%s]' % (custAppFile))
	ATE2.Serial_Exec_Cmd('ATDSCFG', 'Waiting', repeatCnt=5, returnMatch=True, exitOnMatch=True)
	custAppPath = ATE2.PathFiles + custAppFile
	port = re.sub('COM', '', ATE2.ATEport)
	cmd = "C:\\CalAmpAte2\\bin\\Ymodem\\YmodemAppVS6 -sy -p%s -le %s %s" % (port, custAppPath, '')
	Logger.debug(cmd)
	rc = os.system(cmd)
	if rc != 0:
		ATE2.Step_Fail('YModem Download failed - see detail log', errorCode='LMU_CUST_APP_DNLD_YMODEM_FAIL')
	# Wait for the CRC line before talking to the unit.
	line = ATE2.Serial_Exec_Cmd('', 'CRC Pass', repeatCnt=10, returnMatch=True, exitOnMatch=True, noFail=True)

	#If FW is 5.0c or higher then information such as Updating Config % will come out of the debug port.
	craf_FW = ATE2.Config_Get('CRAF', 'LMU FW Ver')
	cnt = 0
	if re.search('5.0c', craf_FW):
	# if int(re.match('^(\d*)\.', craf_FW).group(1)) >= 5:	
		while True:
			line = ATE2.Serial_Exec_Cmd('', 'Updating Config', repeatCnt=15, repeatDelay=3, returnMatch=True)
			ATE2.Log(line)
			if re.search('90%', line):
				ATE2.Log('Updating Config 100%')
				break
			if cnt > 12:
				ATE2.Step_Fail('YModem Download failed during waiting for update', errorCode='LMU_CUST_APP_DNLD_YMODEM_FAIL_UPDATE')
			else:
				cnt=+1

	# Unit will take it's time coming back up. Will respond to AT when complete.
	# If this is not done then the download file get's corrupted.
	ATE2.Serial_Exec_Cmd('AT', 'OK', repeatCnt=1, cmdRepeatCnt=60, cmdRepeatDelay=.1, returnMatch=True, exitOnMatch=True)

def Customer_SIM_Install():
	return
			
def Debug_Off():
	ATE2.Serial_Exec_Cmd('ATS125=0', 'OK')

def Debug_On(val=7):
	ATE2.Serial_Exec_Cmd('ATS125=%d' % (val), 'OK')
	
def Debug_Value():
	line = ATE2.Serial_Exec_Cmd('ATS125?', 'OK')
	for buf in line:
		try:
			ats_val = int(buf)
			break
		except:
			continue
	return(ats_val)
	
def Device_Hibernate():
	assy_battery = ATE2.Config_Get('ASSEMBLY', 'Battery')
	if assy_battery != 'Yes':
		ATE2.Log('No battery configured', logToDetails=True)
		return (1)
	assy_hiberMask = ATE2.Config_Get('ASSEMBLY', 'Hibernate Mask')
	if assy_hiberMask == '':
		ATE2.Log('Device has battery but does not hibernate', logToDetails=True)
		return (1)
	# Enable sleep mode
	ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 23 1', 'OK')
	ATE2.Serial_Exec_Cmd('AT#HIBERNATE %s' % (assy_hiberMask), '', cmdRepeatCnt=5)
	Operator_Manual_Instruction('Wait for LEDs to turn off.  Did device hibernate?')
	
def Device_Reset(testcfg = True):
	ATE2.Log_SubStep('Device being reset')
	ATE2.Serial_Exec_Cmd('ATRESET', '', cmdRepeatCnt=1, noFail=True)
	ATE2.Sleep(1.5)
	if testcfg == True:
		ATE2.Serial_Exec_Cmd('AT#TESTCONFIG', 'OK', cmdRepeatCnt=40)
	else:
		ATE2.Serial_Exec_Cmd('AT', '^OK', cmdRepeatCnt=40)

def Device_Hardware_Values():
	try: 
		f = open(Dev_Value + '.txt', 'r')
		defVal = f.read()
		f.close()
	except:
		defVal = ''
	t = ATE2.Operator_Input(instr= 'Enter the PWBA and REV of the board', charCnt=0, noFail=False, bClear=False, defVal=defVal)
	f = open(Dev_Value + '.txt', 'w+')
	f.write(t)
	PULS_Add_Entry('TEST_PWA_REV:%s' %(defVal))

def EHS5_SETTINGS():
	# When updating a Gemalto Radio from REV2 to REV3, these paramaters need to be checked.
	# Need to make sure this is an EHS5 RAdio. Place this test After the INFO because the ^SYSTEM Start needs to have completed.
	if 'Radio' not in UUT.ATI0:
		ATE2.Step_Fail('Place ESH5_SETTINGS funtion after TEST_INFO function.', errorCode='LMU_EHS5_SETTING_FAIL')
	if not re.search ('EHS5', UUT.ATI0['Radio']): 
		ATE2.Log('Non EHS5 Radio')
		return
	
	Debug_On()
	
	line = ATE2.Serial_Exec_Cmd('at$app modem cmd "at+cscs?"', 'CSCS:',  repeatCnt=5, initDelayRead=0.25, repeatDelay=2, returnMatch=False, exitOnMatch=False, cmdRepeatCnt=2,
								cmdRepeatDelay=1, noFail=True)

	#See what line returns, want to place what the CSCS is.
	reval = None
	for tp in line:
		if re.search('CSCS:',tp):
			if re.search('GSM',tp):
				reval = tp
			else:
				revalFail = tp
	
	if reval == None:
		ATE2.Mfg_Add_Step_Detail(property='EHS5_CSCS_TEST', value="FAIL", minRange='', maxRange='')
		ATE2.Step_Fail('CSCS failure return value was %s' % (revalFail), errorCode='LMU_EHS5_SETTING_CSCS')
	ATE2.Log('Pass: CSCS test', logToDetails=True)
	ATE2.Mfg_Add_Step_Detail(property='EHS5_CSCS_TEST', value="PASS", minRange='', maxRange='')
	
	reval = None
	hldfail = None
	hldval = 'JRC-1.56.40.jad","Java Remote Control MIDlet Suite","Cinterion","1.56.40",1'
	hldval1 = 'JRC-1.56.30.jad","Java Remote Control MIDlet Suite","Cinterion","1.56.30",1'
	line = ATE2.Serial_Exec_Cmd('at$app modem cmd "AT^sjam=5"', 'SJAM:', noFail=True)
	for tp in line:
		if re.search(hldval,tp): reval = tp
		if re.search(hldval1,tp): reval = tp
		if re.search('SJAM:',tp): 
			try:
				hldfail = re.match('.*JRC-(\d*.\d*.\d*).*',tp).group(1)
				hldpass = re.match('.*JRC-(\d*.\d*.\d*).*',hldval).group(1)
				hldpass1 = re.match('.*JRC-(\d*.\d*.\d*).*',hldval1).group(1)
			except:
				hldfail = re.match('.*SJAM: (.*)',tp).group(1)
	if reval == None:
		ATE2.Mfg_Add_Step_Detail(property='EHS5_SJAM_TEST', value="FAIL", minRange='', maxRange='')
		ATE2.Step_Fail('SJAM 5 SPEC:[%s or %s] BOARD:[%s]' %(hldpass, hldpass1, hldfail), errorCode='LMU_EHS5_SETTING_SJAM5')
	
	ATE2.Log('Pass: SJAM test', logToDetails=True)
	ATE2.Mfg_Add_Step_Detail(property='EHS5_SJAM_TEST', value="PASS", minRange='', maxRange='')
	
	Debug_Off()
	
def ESN_Request_From_PULS(writeEsn=True):
	global UUT
	ATE2.Log_SubStep('Send mfg record to PULS')
	usrAgent = 'CalAmpATE2/' + ATE2.TestVals.ATEver + '/' + ATE2.TestVals.ModelVersion
	pn = ATE2.Config_Get('CRAF', 'Part Number')
	assy = ATE2.Config_Get('ASSEMBLY', 'Assembly Number') + '-G0000'
	pcba = UUT.Data['PCBA']
	app = re.sub(' \(.*', '', UUT.ATI0['APP'])
	if 'IMEI' in UUT.ATI1:
		imei = UUT.ATI1['IMEI']
	else:
		imei = UUT.ATI1['MEID']
		Logger.debug('Did not find Multimode MEID')
	if 'MEID' in UUT.ATI1:
		meid = UUT.ATI1['MEID']
	else:
		meid = ''
	if imei is None or imei == '':
		try:
			imei = re.search('(.*),.*', meid).group(1)
		except:
			# There are some products without radio's (LMU4530D)
			ATE2.Log('No IMEI or MEID')
			# ATE2.Step_Fail('No IMEI', errorCode='ESN_Request_NOIMEI')
	testId = UUT.Data['TestID']
	body = """
UNITPARTNUMBER: %s
BOARDPARTNUMBER: %s
PCBA: %s
#TEST_ID: %s
APP:%s
MEID: %s
IMEI: %s
%s
	""" % (pn, assy, pcba, testId, app, meid, imei, PULS_Extra_Vals)
	Logger.debug(body)
	http = urllib3.PoolManager()
	url = ATE2.Config_Get('ENV', 'PULS URL')
	headers = {"User-Agent" : "%s" % usrAgent}
	resp = http.urlopen('POST /labview/esn.php', url, headers=headers, body=body)
	status = str(resp.status)
	respTxt = resp.data.decode().rstrip()
	Logger.debug(status)
	Logger.debug(respTxt)
	if status == '201':
		m = re.match('.*? (\d+)', respTxt)
		print(m)
		esn = m.group(1)
		ATE2.Log('PCBA [%s] ESN [%s]' % (pcba, esn))
	else:
		ATE2.Log('Status [%s]' % (status))
		if status == '200':
			ATE2.Step_Fail('PULS request failed [%s]' % respTxt, errorCode='LMU_ESN_PULS_REQ_200_FAIL')
		else:
			ATE2.Step_Fail('PULS request failed - see Detail Log', errorCode='LMU_ESN_PULS_REQ_FAIL')
	# write the ESN to the device
	UUT.ESN = esn
	if writeEsn:
		ATE2.Log_SubStep('Write ESN to board')
		ATE2.Serial_Exec_Cmd('AT#ESN %s OVERRIDE' % (esn), 'OK', cmdRepeatCnt=3)
		ATE2.UUT.Ident1  = 'ESN: ' + UUT.ESN
		esn = ATE2.Serial_Exec_Cmd('AT#ESN?', 'OK', returnMatch=True)

def Factory_Config_Set():
	ATE2.Serial_Exec_Cmd('AT#FACTORY', '^OK|^Factory Reset Complete', repeatDelay=3, repeatCnt=20)
	ATE2.Serial_Exec_Cmd('AT', '^OK', cmdRepeatCnt=40)

def Firmware_Download(craf_procType='', craf_fwVer='', craf_appId='', versionCheckOnly=False, specialCoprocMode=False, versionCheckOnlyNoFail=False):
	if craf_procType == '':
		craf_procType = ATE2.Config_Get('ASSEMBLY', 'Processor Type')
	ATE2.Log('Processor type [%s]' % (craf_procType))
	if craf_procType == 'STM8_128K':
		rc = Firmware_Download_8_Bit(craf_fwVer='', craf_appId='', versionCheckOnly=versionCheckOnly)
	elif craf_procType == 'STM32F10xxExx':
		rc = Firmware_Download_32_Bit(craf_fwVer='', craf_appId='', versionCheckOnly=versionCheckOnly, specialCoprocMode=specialCoprocMode, versionCheckOnlyNoFail=versionCheckOnlyNoFail)
	elif craf_procType == 'STM32F103VEH6':
		rc = Firmware_Download_32_Bit(craf_fwVer='', craf_appId='', versionCheckOnly=versionCheckOnly, specialCoprocMode=specialCoprocMode, versionCheckOnlyNoFail=versionCheckOnlyNoFail)
	elif craf_procType == 'STM32F2_512K':
		rc = Firmware_Download_32_Bit(craf_fwVer='', craf_appId='', versionCheckOnly=versionCheckOnly, specialCoprocMode=specialCoprocMode, versionCheckOnlyNoFail=versionCheckOnlyNoFail)
	elif craf_procType == 'STM32F2_1024K':
		rc = Firmware_Download_32_Bit(craf_fwVer='', craf_appId='', versionCheckOnly=versionCheckOnly, specialCoprocMode=specialCoprocMode, versionCheckOnlyNoFail=versionCheckOnlyNoFail)
	elif craf_procType == 'STM32F205ZF':
		rc = Firmware_Download_32_Bit(craf_fwVer='', craf_appId='', versionCheckOnly=versionCheckOnly, specialCoprocMode=specialCoprocMode, versionCheckOnlyNoFail=versionCheckOnlyNoFail)
	elif craf_procType == 'STM32_High-density_512K':
		rc = Firmware_Download_32_Bit(craf_fwVer='', craf_appId='', versionCheckOnly=versionCheckOnly, specialCoprocMode=specialCoprocMode, versionCheckOnlyNoFail=versionCheckOnlyNoFail)	
	else:
		ATE2.Step_Fail('Unknown processor type [%s] to ATE' % (craf_procType), errorCode='LMU_FW_DNLD_PROC_TYPE_UNKNOWN')
	if versionCheckOnlyNoFail:
		return rc
	if specialCoprocMode:
		return (1)  # no log result to PULS in this mode

def Firmware_Download_8_Bit(craf_fwVer='', craf_appId='', versionCheckOnly=False):
	if craf_fwVer == '':
		craf_fwVer = ATE2.Config_Get('CRAF', 'LMU FW Ver')
	if craf_appId == '':
		craf_appId = ATE2.Config_Get('CRAF', 'LMU App ID')
	assy_radioType = ATE2.Config_Get('ASSEMBLY', 'Radio Type')
	# check if any firmware loaded
	hasFw = False
	needFwUpdate = False
	try:
		line = ATE2.Serial_Exec_Cmd('AT#TESTCONFIG', 'OK', returnMatch=True, noFail=True, cmdRepeatCnt=2)
		if line is not None and re.search('OK', line):
			hasFw = True
	except:
		pass
	if not hasFw:
		ATE2.Log('No firmware found')
		needFwUpdate = True
	else:
		# check if firmware up-to-date
		line = ATE2.Serial_Exec_Cmd('ATI0', 'APP:', returnMatch=True)
		uut_appId, uut_fwVer = re.match('.*,(\d*) V([\w\.]*)', line).group(1,2)
		if craf_fwVer == uut_fwVer and craf_appId == uut_appId:
			ATE2.Log_Info('Firmware up-to-date [%s/%s]' % (uut_appId, uut_fwVer))
			ATE2.Mfg_Add_Step_Detail(property='LMU FW', value='%s/%s' % (uut_appId, uut_fwVer), minRange='', maxRange='')
			return
		else:
			ATE2.Log('Firmware not up-to-date - CRAF [%s/%s] Board [%s/%s]' % (craf_appId, craf_fwVer, uut_appId, uut_fwVer))
			needFwUpdate = True
	# select download method
	Firmware_Download_8_Bit_Stm(assy_radioType, craf_appId, craf_fwVer)
	# check firmware now up-to-date
	line = ATE2.Serial_Exec_Cmd('ATI0', 'APP:', returnMatch=True)
	uut_appId, uut_fwVer = re.match('.*,(\d*) V([\w\.]*)', line).group(1,2)
	if craf_fwVer == uut_fwVer and craf_appId == uut_appId:
		ATE2.Log_Info('Firmware up-to-date [%s/%s]' % (craf_appId, uut_fwVer))
		ATE2.Mfg_Add_Step_Detail(property='LMU FW', value='%s/%s' % (uut_appId, uut_fwVer), minRange='', maxRange='')
		return
	ATE2.Step_Fail('Firmware not up-to-date - CRAF [%s/%s] Board [%s/%s]' % (craf_appId, craf_fwVer, uut_appId, uut_fwVer), errorCode='LMU_FW_DNLD_8_BIT_WRONG_FW')

def Firmware_Download_8_Bit_Stm(assy_radioType, craf_appId, craf_fwVer):
	# send pokes to invalidate the current firmware
	# 12/17/14 this here until Y-Modem file download works
	ATE2.Log_SubStep('Send Poke cmds to put device in boot mode')
	#Check to seek if POKE commands previously set. If so no response from device but will program.
	line = ATE2.Serial_Exec_Cmd('AT$POKE 9998 20578 86', 'OK', returnMatch=True, noFail=True,cmdRepeatCnt=2)
	if line and re.search('OK', line):
		ATE2.Serial_Exec_Cmd('AT$POKE 9998 20578 174', 'OK')
		ATE2.Serial_Exec_Cmd('AT$POKE 9998 32768 0', 'OK')
	elif line is None:
		ATE2.Step_Fail('8 bit firmware failed the POKE commands', errorCode='LMU_FW_DNLD_8_BIT_STM_POKE')
	ATE2.Log_SubStep('Reset power')
	# cycle power to get to boot mode
	ATE2.Power_Turn_Off()
	ATE2.Power_Turn_On()
	# download new firmware
	v =  assy_radioType + '.*' + craf_appId + '.*' + re.sub('\.', '', craf_fwVer)
	ATE2.Log_SubStep('Downloading firmware')
	# get firmware file
	for f in os.listdir(ATE2.PathFiles):
		if re.search(v, f):
			fwFile = f
			break
	else:
		ATE2.Step_Fail('Firmware file not found [%s]' % (v), errorCode='LMU_FW_DNLD_8_BIT_FILE_NOT_FOUND')
	# continue download
	ATE2.Log('Downloading file [%s]' % (fwFile))
	# calculate number of pages of flash
	p = ATE2.PathFiles + fwFile
	pages = int((os.stat(p).st_size/1000)-2)
	assy_procType = ATE2.Config_Get('ASSEMBLY', 'Processor Type')
	port = re.sub('COM', '', ATE2.ATEport)
	# call flashloader
	t1 = '"C:\\CalAmpAte2\\bin\\STMFlash_2.5\\STMFlashLoader.exe" ' + \
		'-p --drp -c --pn %s --br 115200 ' % (port) + \
		'--db 8 --pr 2 --sb 1 --to 10000 -i %s ' % (assy_procType)
		# no need for -- sec ??
		# '-e --sec '
	# t2 = '%s' % (pages)
	# for sec in range(0, pages):
		# t2 += ' %s' % (sec)
	t2 = ''
	t3 = ' -d --a 8000 --fn '
	cmd = t1 + t2 + t3 + p + ' -r --a 8000'
	Logger.debug(cmd)
	rc = os.system(cmd)
	if rc != 0:
		ATE2.Step_Fail('Download failed - see Detail log', errorCode='LMU_FW_DNLD_8_BIT_DNLD_FAIL')
	# reset the device
	Device_Reset()

def Firmware_Download_32_Bit(craf_fwVer='', craf_appId='', versionCheckOnly=False, specialCoprocMode=False, versionCheckOnlyNoFail=False):
	if craf_fwVer == '':
		craf_fwVer = ATE2.Config_Get('CRAF', 'LMU FW Ver')
	if craf_appId == '':
		craf_appId = ATE2.Config_Get('CRAF', 'LMU App ID')
	assy_radioType = ATE2.Config_Get('ASSEMBLY', 'Radio Type')
	# check if any firmware loaded
	hasFw = False
	needFwUpdate = False
	if (ATE2.TestVals.bJP1Log == 'False') or (versionCheckOnly) or (versionCheckOnlyNoFail):
		try:
			line = ATE2.Serial_Exec_Cmd('AT#TESTCONFIG', 'OK|Password Required', cmdRepeatCnt=60, returnMatch=True, noFail=True)
			if line is not None and re.search('OK', line):
				hasFw = True
		except:
			pass
		if line is not None and re.search('Password Required', line):
			ATE2.Step_Fail('Board is password locked', 'LMU_PWD_REQUIRED')
	if not hasFw:
		ATE2.Log('No firmware found')
		needFwUpdate = True
		uut_fwVer = None
	else:
		# check if firmware up-to-date
		line = ATE2.Serial_Exec_Cmd('ATI0', 'APP:', returnMatch=True)
		uut_appId, uut_fwVer = re.match('.*,(\d*) V([\w\.]*)', line).group(1,2)
		if craf_fwVer == uut_fwVer and craf_appId == uut_appId:
			ATE2.Log('Firmware up-to-date [%s/%s]' % (uut_appId, uut_fwVer))
			ATE2.Mfg_Add_Step_Detail(property='LMU FW', value='%s/%s' % (uut_appId, uut_fwVer), minRange='', maxRange='')
			return True
		else:
			ATE2.Log('Firmware not up-to-date - CRAF [%s/%s] Board [%s/%s]' % (craf_appId, craf_fwVer, uut_appId, uut_fwVer))
			needFwUpdate = True
	# version check only
	if versionCheckOnlyNoFail and needFwUpdate:
		return False
	if versionCheckOnly:
		if needFwUpdate:
			ATE2.Step_Fail('Firmware version check failed', errorCode='LMU_FW_DNLD_8_BIT_FW_VER_CHECK')
		else:
			return
	# update fw if needed
	if needFwUpdate:
		# can't use ymodem when proc/coproc downloaded together because board in program mode and won't come back up
		if uut_fwVer is not None and not specialCoprocMode:
			# download using Ymodem if already a build in device
			Firmware_Download_32_Bit_Ymodem(assy_radioType, craf_appId, craf_fwVer)
		else:
			Firmware_Download_32_Bit_Stm(assy_radioType, craf_appId, craf_fwVer, specialCoprocMode)
		# check firmware now up-to-date
		if specialCoprocMode: return	# versions checked after both proc, coproc downloaded
		line = ATE2.Serial_Exec_Cmd('ATI0', 'APP:', returnMatch=True)
		uut_appId, uut_fwVer = re.match('.*,(\d*) V([\w\.]*)', line).group(1,2)

		line = ATE2.Serial_Exec_Cmd('ATI0', 'APP:', returnMatch=True)
		uut_fwVer = re.match('[\w:,]* V([\w\.]*)', line).group(1)
		if craf_fwVer == uut_fwVer:
			ATE2.Log('Firmware up-to-date [%s/%s]' % (uut_appId, uut_fwVer))
			ATE2.Mfg_Add_Step_Detail(property='LMU FW', value='%s/%s' % (uut_appId, uut_fwVer), minRange='', maxRange='')
		else:
			ATE2.Step_Fail('Firmware not up-to-date - CRAF [%s/%s] Board [%s/%s]' % (craf_appId, craf_fwVer, uut_appId, uut_fwVer), errorCode='LMU_FW_DNLD_8_BIT_FW_NOT_UTD')
			
def Firmware_Download_32_Bit_Stm(assy_radioType, craf_appId, craf_fwVer, specialCoprocMode=False):
	ATE2.Power_Turn_Off()
	if not specialCoprocMode:
		bootRelay = ATE2.Config_Get('ENV', 'Boot Relay')
		if bootRelay != '':
			ATE2.Relay_Set(bootRelay)
		else:
			ATE2.Operator_Instruction('Install Jumper to JP1')
	ATE2.Power_Turn_On()
	# download new firmware
	assy_radioType = ATE2.Config_Get('ASSEMBLY', 'Radio Type')
	v =  assy_radioType + '.*' + craf_appId + '.*' + re.sub('\.', '', craf_fwVer)
	Logger.debug('file: %s %s' % (v, ATE2.PathFiles))
	for f in os.listdir(ATE2.PathFiles):
		if re.search(v, f):
			fwFile = f
			break
	else:
		ATE2.Step_Fail('Firmware file not found [%s]' % (v), errorCode='LMU_FW_DNLD_32_BIT_FILE_NOT_FOUND')
	ATE2.Log('Downloading file [%s]' % (fwFile))
	
	#### build the STMFlash cmd
	assy_procType = ATE2.Config_Get('ASSEMBLY', 'Processor Type')
	p = ATE2.PathFiles + fwFile
	port = re.sub('COM', '', ATE2.ATEport)
	##
	if assy_procType == 'STM32F10xxExx':
		t1 = '"C:\CALAMPATE2\\bin\\STMFlash_1.7\STMFlashLoader.exe" ' + \
			'-p --drp -c --pn %s --br 115200 ' % (port) + \
			'--db 8 --pr NONE --sb 1 --to 10000 -i %s ' % (assy_procType) + \
			'-e --sec '
		pages = int((os.stat(p).st_size / 2048) + 1)	# calc pages of flash
		t2 = '%s' % (pages)
		for sec in range(0, pages):
			t2 += ' %s' % (sec)
		t3 = ' -d --a 8000000 --fn '
		cmd = t1 + t2 + t3 + p
	elif assy_procType == 'STM32_High-density_512K':
		t1 = '"C:\CALAMPATE2\\bin\\STMFlash_2.5\STMFlashLoader.exe" ' + \
			'-p --drp -c --pn %s --br 115200 ' % (port) + \
			'--db 8 --pr NONE --sb 1 --to 10000 -i %s ' % (assy_procType) + \
			'-e --sec '
		pages = int((os.stat(p).st_size / 2048) + 1)	# calc pages of flash
		t2 = '%s' % (pages)
		for sec in range(0, pages):
			t2 += ' %s' % (sec)
		t3 = ' -d --a 8000000 --fn '
		cmd = t1 + t2 + t3 + p
	else:
		t1 = '"C:\CALAMPATE2\\bin\\STMFlash_2.5\STMFlashLoader.exe" ' + \
			'-p --drp -c --pn %s --br 115200 ' % (port) + \
			'--db 8 --pr EVEN --sb 1 --to 10000 -i %s ' % (assy_procType)
		t2 = ''
		t3 = ' -e --sec 12 0 1 2 3 4 5 6 7 8 9 10 11 -d --a 8000000 --fn '
		cmd = t1 + t2 + t3 + p
	# run the flash cmd
	Logger.debug(cmd)
	rc = os.system(cmd)
	if rc != 0 and rc != 1:
		ATE2.Step_Fail('Download failed [%d] - see Detail log' % (rc), errorCode='LMU_FW_DNLD_32_BIT_DNLD_FAIL')
	# reset the device
	if not specialCoprocMode:
		ATE2.Log_SubStep('Power cycle the device')
		ATE2.Power_Turn_Off()
		if bootRelay != '':
			ATE2.Relay_Clear(bootRelay)
		else:
			ATE2.Operator_Instruction('Remove Jumper from JP1')			
		ATE2.Power_Turn_On()
		Device_Reset()
	
def Firmware_Download_32_Bit_Ymodem(assy_radioType, craf_appId, craf_fwVer):
	# # download new firmware
	# v =  '.*' + assy_radioType + '.*' + craf_appId + '.*' + re.sub('\.', '', craf_fwVer)
	# ATE2.Log_SubStep('Downloading firmware using Ymodem')
	# # get firmware file
	# for f in os.listdir(ATE2.PathFiles):
		# # print(f, v)
		# if re.search(v, f):
			# fwFile = f
			# break
	# else:
		# ATE2.Step_Fail('Firmware file not found [%s]' % (v), errorCode='LMU_FW_DNLD_32_BIT_FILE_NOT_FOUND')
	FW = 'LMU-HSPA-039-50j.bin'
	# download file
	ATE2.Log_SubStep('Use Ymodem to load data file')
	ATE2.Log('Downloading file [%s]' % (FW))
	ATE2.Serial_Exec_Cmd('ATDNLD', 'Waiting', repeatCnt=5, returnMatch=True, exitOnMatch=True)
	custAppPath = ATE2.PathFiles + FW
	port = re.sub('COM', '', ATE2.ATEport)
	cmd = "C:\\CalAmpAte2\\bin\\Ymodem\\YmodemAppVS6 -sy -p%s -le %s %s" % (port, custAppPath, '')
	Logger.debug(cmd)
	os.system(cmd)
	# Wait for the CRC line before talking to the unit.
	# line = ATE2.Serial_Exec_Cmd('', 'CRC', repeatCnt=30, returnMatch=True, exitOnMatch=True)
	# BKMB - CRC not always coming back... Wait for product to start updating then Press for AT.
	ATE2.Sleep(10)
	# Unit will take it's time coming back up. Will respond to AT when complete.
	# If this is not done then the download file get's corrupted
	# Need to allow plenty of time for Updates to 8 bit processor also.
	ATE2.Serial_Exec_Cmd('AT', 'OK', initDelayRead=2, cmdRepeatCnt=120, repeatCnt=5, repeatDelay=.1, returnMatch=True, exitOnMatch=True)
	# To complete the change need to turn device off / On.
	# When changing APP ID or major updates in functionality of the FW
	# Example is JPOD2 functionality from APP ID 213 to 043.
	#ATE2.Serial_Exec_Cmd('ATS125=0', 'OK', returnMatch=True)
	ATE2.Serial_Exec_Cmd('AT', 'OK', initDelayRead=2, cmdRepeatCnt=120, repeatCnt=5, repeatDelay=.1, returnMatch=True, exitOnMatch=True)
	ATE2.Power_Turn_Off()
	ATE2.Power_Turn_On(wait=3)
	# Make sure communicating.
	ATE2.Serial_Exec_Cmd('AT', 'OK', initDelayRead=2, cmdRepeatCnt=40, cmdRepeatDelay=3, returnMatch=True, exitOnMatch=True)

def Firmware_Download_Coproc():
	# This function downloads products with a seperate coprocessor from the processor.
	# Example is TTU2830
	
	# Check for firmware update
	# reload both processors if either processor needs update
	
	# Check JP1 tick on the front panel.
	if ATE2.TestVals.bJP1Log == 'False':
		ATE2.Log_Step('Check if either processor needs firmware update')
		#Return True if firmware is current, False if firmware !=)
		procUpdated = Firmware_Download(versionCheckOnlyNoFail=True)
		coprocUpdated = Coproc.Firmware_Download(versionCheckOnlyNoFail=True)
	else:
		ATE2.Log_Step('FW Update')
		procUpdated = False
		coprocUpdated = False
		
	if procUpdated and coprocUpdated:
		LMU.PULS_Add_Entry('#FW Pass')
		LMU.PULS_Add_Entry('#FW-COPROC Pass')
	else:		
		factory_relaybox = ATE2.Config_Get('FACTORY', 'Relay Box Present')
		boot_relay_list = ATE2.Config_Get('ENV', 'Boot Relay')
		
		if ((factory_relaybox == '') or (boot_relay_list == '')) and (factory_relaybox.upper() != 'YES'):
			ATE2.Operator_Instruction('Set program switch to ON')
		else:
			#Turn on the Relays
			for relayNum in boot_relay_list.split():
				ATE2.Relay_Clear(relayNum)
				ATE2.Sleep(1.5)
		
		ATE2.Log_SubStep('Force download of Coproc Version 27 due to firmware issue')
		Coproc.Firmware_Download_STM8_SWIM(27)
		Firmware_Download(specialCoprocMode=True)
		Coproc.Firmware_Download(specialCoprocMode=True)
		ATE2.Operator_Instruction('Set program switch to OFF')
		if ((factory_relaybox == '') or (boot_relay_list == '')) and (factory_relaybox.upper() != 'YES'):
			ATE2.Operator_Instruction('Set program switch to OFF')
		else:
			for relayNum in boot_relay_list.split():
				ATE2.Relay_Set(relayNum)
				ATE2.Sleep(1.5)
		ATE2.Power_Turn_Off()
		ATE2.Power_Turn_On()
		# after 4.0a is combined proc/coproc download.  that is taking 35+ seconds, so need to wait
		ATE2.Serial_Exec_Cmd('AT', 'OK', cmdRepeatCnt=60)
		ATE2.Serial_Exec_Cmd('ATI0', 'OK', cmdRepeatCnt=60)
		procUpdated = Firmware_Download(versionCheckOnlyNoFail=True)
		coprocUpdated = Coproc.Firmware_Download(versionCheckOnlyNoFail=True)
		if procUpdated and coprocUpdated:
			LMU.PULS_Add_Entry('#FW Pass')
			LMU.PULS_Add_Entry('#FW-COPROC Pass')
		else:
			ATE2.Step_Fail('FW Update Failed', errorCode='LMU_FIRMWARE_DOWNLOAD_COPROC')
	
def Flash_Test():
	ATE2.Serial_Exec_Cmd('AT#FLASH?', 'FLASH: PASS', initDelayRead=1)

def HWREV_Test():
	outBuf = ATE2.Serial_Exec_Cmd('AT#HWREV?', 'OK', initDelayRead=1)
	for line in outBuf:
		if re.search(':', line):
			Logger.debug('HW Rev line: %s' %(line))
			ATE2.Mfg_Add_Step_Detail(property='HW REV', value=line)

def HWREV_Set_Step():
	outBuf = ATE2.Serial_Exec_Cmd('AT#HWREV?', 'OK', initDelayRead=1)
	for line in outBuf:
		if re.search(':', line):
			hwrev = re.sub('.*: *', '', line)
			if hwrev != '' and hwrev != '0' :
				ATE2.Log('HWREV previously set [%s]' % (hwrev))
				Logger.debug('HW Rev line: %s' %(line))
				ATE2.Mfg_Add_Step_Detail(property='HW REV', value=line)
				return
	
	#Need to set HW Rev 
	assy_hwrev = ATE2.Config_Get('ASSEMBLY', 'HW REV')
	if assy_hwrev == '':
		ATE2.Step_Fail('HW REV in INI file expected to be greater than 0', errorCode='LMU_HWREV_SET_STEP_INI_FAIL')
	outBuf = ATE2.Serial_Exec_Cmd('AT#HWREV %s' %(assy_hwrev.strip()), 'OK', initDelayRead=1)
	outBuf = ATE2.Serial_Exec_Cmd('AT#HWREV?', 'OK', initDelayRead=1)
	for line in outBuf:
		if re.search(':', line):
			hwrev = re.sub('.*: *', '', line)
			if hwrev != '' and hwrev != '0' :
				ATE2.Log('HWREV set [%s]' % (hwrev))
				Logger.debug('HW Rev line: %s' %(line))
				ATE2.Mfg_Add_Step_Detail(property='HW REV', value=line)
			else:
				ATE2.Step_Fail('Failed to set the HW REV of board', errorCode='LMU_HWREV_SET_STEP_FAIL')
			
def iBeacon_Config_Set():
	craf_iBeaconFunc = ATE2.Config_Get('CRAF', 'iBeacon Func')
	if craf_iBeaconFunc == '':
		return
	else:
		func = getattr(iBeacon, craf_iBeaconFunc)
	ATE2.Log('iBeacon function [%s]' % (craf_iBeaconFunc), logToDetails=True)
	func() # run customer func
	PULS_Add_Entry('#iBeacon Pass')
	
def Info_ATI0(ats125 = 'OFF'):
	ATE2.Log_SubStep('Read ATI0 info')
	ats_val = 0
	if ats125 != 'OFF':	
		line = ATE2.Serial_Exec_Cmd('ATS125?', 'OK')
		for buf in line:
			try:
				ats_val = int(buf)
				break
			except:
				continue
	ATE2.Serial_Exec_Cmd('ATS125=0', 'OK')
	assy_radio = ATE2.Config_Get('ASSEMBLY', 'Radio Ver List')
	for retry in range(30,0,-1):
		outBuf = ATE2.Serial_Exec_Cmd('ATI0', 'OK', repeatCnt=5)
		cnt = 0
		for line in outBuf:
			if re.search(':$|:-*$', line):
				if assy_radio != 'None':
					cnt += 1
		if cnt > 0:
			ATE2.Sleep(3)
		else:
			# call optional validator for each ATIO attribute
			retryFlag = False
			for line in outBuf:
				m = re.match('(^.*?):\s*(.*$)', line)
				if m is None: continue
				valFunc = 'Info_ATI0_Val_' + m.group(1)
				try:
					globals()[valFunc]
				except:
					pass
					continue
				ret = globals()[valFunc](m.group(1), m.group(2))
				# see if retry requested
				if re.search('^Retry', ret):
					retryFlag = True
					# ATE2.Log_Warning('Retry requested by field [%s]' % (m.group(1)))
				if re.search('^Fail', ret) or (re.search('^Retry', ret) and retry == 1):
					ATE2.Step_Fail('ATI0 validation failed [%s] [%s]' % (m.group(1), ret), errorCode='LMU_ATI0_VAL_FAIL')
			# all validations pass
			if (not retryFlag):
				for line in outBuf:
					if re.search(':', line):
						ATE2.Log(line)
						m = re.match('(^.*?):\s*(.*$)', line)
						UUT.ATI0[m.group(1)] = m.group(2)
						LMU.SQLiteDB_Post_TestResults(ts='', name=m.group(1), result='', tvalue=m.group(2), string='')
						PULS_Add_Entry(line)
				ATE2.Serial_Exec_Cmd('ATS125=%d' % (ats_val), 'OK')
				return
			# retry requested
			ATE2.Sleep(1)
	ATE2.Log_Warning('Fields w/o value follows:', logToDetails=True)
	for line in outBuf:
		if re.search(':$|:-*$', line):
			ATE2.Log(line, logToDetails=True)
	ATE2.Step_Fail('ATI0 cmd failed with missing field values', errorCode='LMU_ATI0_CMD_FAIL_MISSING_VALS')
	
def Info_ATI0_Val_Radio(field, uut_radioVer):
	# check that a radio exists in Assembly
	assy_radio = ATE2.Config_Get('ASSEMBLY', 'Radio Ver List')
	errSection = 'Assembly'
	if assy_radio == 'None':
		errSection == ''
		return('Pass')
	# check that radio version in Assembly list
	assy_radioVerList = ATE2.Config_Get('ASSEMBLY', 'Radio Ver List')
	craf_radioVerList = ATE2.Config_Get('CRAF', 'Radio Ver List')
	errSection = 'Assembly'
	for ver in assy_radioVerList.split('\n'):
		if ver == uut_radioVer:
			errSection = ''
			break
	# verify radio version in CRAF if specified
	if errSection == '' and craf_radioVerList != '': 
		errSection = 'CRAF'
		for ver in craf_radioVerList.split('\n'):
			if ver == uut_radioVer:
				errSection = ''
				break
	if errSection == '':
		return('Pass')
	else:
		return ('Retry: Radio version not in %s list [%s]' % (errSection, uut_radioVer))

def Info_ATI0_Val_Radio2(field, uut_radioVer):
	# check that radio version in Assembly list
	assy_radioVerList = ATE2.Config_Get('ASSEMBLY', 'Radio2 Ver List')
	craf_radioVerList = assy_radioVerList
	craf_radioVerList = ATE2.Config_Get('CRAF', 'Radio2 Ver List')
	errSection = 'Assembly'
	for ver in assy_radioVerList.split('\n'):
		if ver == uut_radioVer:
			errSection = ''
			break
	# verify radio version in CRAF if specified
	if errSection == '' and craf_radioVerList != '':
		errSection = 'CRAF'
		for ver in craf_radioVerList.split('\n'):
			if ver == uut_radioVer:
				errSection = ''
				break
	if errSection == '':
		return('Pass')
	else:
		return ('Retry: Radio version not in %s list [%s]' % (errSection, uut_radioVer))
		
def Info_ATI1():
	ATE2.Log_SubStep('Read ATI1 info')
	assy_radio = ATE2.Config_Get('ASSEMBLY', 'Radio Ver List')
	reptCnt = 0
	for retry in range(30):
		outBuf = ATE2.Serial_Exec_Cmd('ATI1', 'OK', cmdRepeatCnt=5)
		cnt = 0
		for line in outBuf:
			if re.search(':$', line):
				if re.search('COMM2:', line): continue
				if assy_radio != 'None':
					cnt += 1
		if cnt > 0:
			reptCnt += 1
			ATE2.Sleep(3)
		else:
			for line in outBuf:
				if re.search(':', line):
					ATE2.Log(line)
					m = re.match('(^.*?):\s*(.*$)', line)
					UUT.ATI1[m.group(1)] = m.group(2)
					LMU.SQLiteDB_Post_TestResults(ts='', name=m.group(1), result='', tvalue=m.group(2), string='')
					PULS_Add_Entry(line)
			UUT.ESN = UUT.ATI1['ESN']
			ATE2.UUT.Ident1  = 'ESN: ' + UUT.ESN
			return
	ATE2.Log_Warning('Fields w/o value follows:')
	for line in outBuf:
		if re.search(':$', line):
			ATE2.Log(line)
	ATE2.Step_Fail('ATI1 cmd failed', errorCode='LMU_ATI1_CMD_FAIL')

def Info_ATIC():
	min = re.search('.*: (\d*)', ATE2.Serial_Exec_Cmd('ATIC', 'Phone Number.*: \d*', returnMatch=True, cmdRepeatCnt=10)).group(1)
	PULS_Add_Entry('MIN: %s' % (min))

def Info_ATIP():
	ATE2.Log_SubStep('Read ATIP info')
	craf_fw = ATE2.Config_Get('CRAF', 'LMU FW Ver')
	if craf_fw < '4.0c':
		ATE2.Log_Warning('Command not available before 4.0c')
		return
	buf = ATE2.Serial_Exec_Cmd('ATIP', 'OK')
	str = 'ATIP:'
	for line in buf:
		if re.search(':', line):
			str = str + line + ';'
			ATE2.Log(line)
			m = re.search('Unique ID:\s*(.*)', line)
			if m:
				ATE2.Mfg_Add_Step_Detail(property='UNIQUE_ID PROC', value=m.group(1))
	PULS_Add_Entry(str)
	
def Info_ATIV():
	line = ATE2.Serial_Exec_Cmd('ATIV', 'SlcnID:', returnMatch=True, cmdRepeatCnt=10)
	sid = re.search(':(.*)', line).group(1)
	UUT.Data['UUT_SID'] = sid
	print('setting UUT_SID')
	line = ATE2.Serial_Exec_Cmd('ATIV', 'S/N:', returnMatch=True, cmdRepeatCnt=10)
	sn = re.search(':(.*)', line).group(1)
	UUT.Data['UUT_SN'] = sn
	
def Info_VBus():
	#set up VBUS
	ATE2.Serial_Exec_Cmd('AT$APP PARAM 3072,7,0','OK')
	ATE2.Sleep(2)
	ATE2.Serial_Exec_Cmd('AT$APP PARAM 3072,3,1', 'OK')
	ATE2.Sleep(2)
	ATE2.Serial_Exec_Cmd('ATS178=5', 'OK')
	ATE2.Sleep(2)
	ATE2.Serial_Exec_Cmd('ATRESET', 'OK')	
	
	# get VBus attributes
	reptCnt = 0
	for retry in range(30):
		outBuf = ATE2.Serial_Exec_Cmd('ATIV', 'OK', cmdRepeatCnt=5)
		cnt = 0
		for line in outBuf:
			if re.search('=VBUS=', line): continue
			cnt += 1
		if cnt > 0:
			reptCnt += 1
			ATE2.Sleep(3)
		else:
			for line in outBuf:
				if re.search(':', line):
					ATE2.Log(line)
					m = re.match('(^.*?):\s*(.*$)', line)
					UUT.ATIV[m.group(1)] = m.group(2)
					LMU.SQLiteDB_Post_TestResults(ts='', name=m.group(1), result='', tvalue=m.group(2), string='')
					PULS_Add_Entry(line)
			#UUT.ESN = UUT.ATI1['ESN']
			return
	ATE2.Log_Warning('Fields w/o value follows:')
	for line in outBuf:
		if re.search(':$', line):
			ATE2.Log(line)
	ATE2.Step_Fail('ATIV cmd failed with missing fields', errorCode='LMU_ATIV_CMD_FAIL')

def IO_Check_Bias(checkList, cmdRepeatCnt=5, cmdRepeatDelay=1):
	#Logger.debug('Inputs: %s' % (str(checkList)))
	ioPass = {}
	for cnt in range(cmdRepeatCnt, 0, -1):
		errFlg = 0
		outBuf = ATE2.Serial_Exec_Cmd('AT$APP INP?', 'OK', initDelayRead=1)
		for check in checkList:
			input, expectVal = check
			for line in outBuf:
				if re.search('INPUT-%d:' % (input), line):
					boardVal = int(re.search('Bias=(\d*)', line).group(1))
					if input not in ioPass:
						if boardVal == expectVal:
							ioPass[input] = 1
						else:
							errFlg = 1
							if cnt == 1:
								ATE2.Log_Warning('Input [%d] - Read [%d] Expected [%d]' % (input, boardVal, expectVal))
		if errFlg == 0: return
		ATE2.Sleep(cmdRepeatDelay)
	ATE2.Step_Fail('One or more Inputs did not match expected Bias value [cmds:%d delay:%d] - see Detail Log' % (cmdRepeatCnt, cmdRepeatDelay), errorCode='LMU_IO_CHECK_BIAS_VAL')
	
def IO_Check_ADCs(checkList, cmdRepeatCnt=5, cmdRepeatDelay=1):
	Logger.debug('ADCs: %s' % (str(checkList)))
	ioPass = {}
	for cnt in range(cmdRepeatCnt, 0, -1):
		errFlg = 0
		outBuf = ATE2.Serial_Exec_Cmd('AT#ADC?', 'OK', initDelayRead=1)
		for check in checkList:
			input, expectVal = check
			for line in outBuf:
				if re.search('ADC%d:' % (input), line):
					boardVal = int(re.search('.*:\s*(\d*)', line).group(1))
					oper, limit = re.search('(.)\s*(\d*)', expectVal).group(1,2)
					limit = int(limit)
					if input not in ioPass:
						if oper == '>' and  boardVal >= limit:
							ioPass[input] = 1
						elif oper == '<' and boardVal <= limit:
							ioPass[input] = 1
						else:							
							errFlg = 1
							if cnt == 1:
								ATE2.Log_Warning('ADC [%d] - Read [%d] Expected [%s]' % (input, boardVal, expectVal))
		if errFlg == 0: return
		ATE2.Sleep(cmdRepeatDelay)
	ATE2.Step_Fail('One or more ADCs were not in range [cmds:%d delay:%d] - see Detail Log' % (cmdRepeatCnt, cmdRepeatDelay), errorCode='LMU_IO_ADC-VOLT-RANGE')

def IO_Check_Inputs(checkList, cmdRepeatCnt=8, cmdRepeatDelay=1):
	Logger.debug('Inputs: %s' % (str(checkList)))
	ioPass = {}
	ioFail = []
	for cnt in range(cmdRepeatCnt, 0, -1):
		errFlg = 0
		outBuf = ATE2.Serial_Exec_Cmd('AT#INP?', 'OK', initDelayRead=1, cmdRepeatCnt=10)
		for check in checkList:
			input, expectVal = check
			for line in outBuf:
				if re.search('INP%d:' % (input), line):
					boardVal = int(re.sub('.*: ', '', line))
					if input not in ioPass:
						if boardVal == expectVal:
							ioPass[input] = 1
						else:
							errFlg = 1
							if cnt == 1:
								ATE2.Log_Warning('Input [%d] - Read [%d] Expected [%d]' % (input, boardVal, expectVal))
								ioFail.append(input)
		if errFlg == 0: return
		ATE2.Sleep(cmdRepeatDelay)
	ioFailStr = ', '.join(str(i) for i in ioFail)
	ATE2.Step_Fail('I/O Input [%s] did not match expected State value - see Detail Log' % (ioFailStr), errorCode='LMU_IO_CHECK_INPUT_STATE_VAL')
		
def IO_Delay(delay):
	delay = delay[0]
	ATE2.Sleep(float(delay))

def IO_Set_Bias(state):
	if re.search('HIGH', str(state), re.IGNORECASE):
		ATE2.Serial_Exec_Cmd('ATS158=254', 'OK', initDelayRead=.5)
	elif re.search('LOW', str(state), re.IGNORECASE):
		ATE2.Serial_Exec_Cmd('ATS158=0', 'OK', initDelayRead=.5)
	else:
		for bval in state:
			try:
				b2val = int(bval[0])
			except:
				ATE2.Step_Fail('I/O Bias is not an integer: %s' % (bval[0]), errorCode='LMU_IO_SET_BIAS_INT')
			ATE2.Serial_Exec_Cmd('ATS158=%d' % (b2val), 'OK', initDelayRead=.5)
	
def IO_Set_Outputs(outList):
	Logger.debug('Outputs: %s' % (str(outList)))
	for setOutput in outList:
		output = setOutput[0]
		state = setOutput[1]
		if re.search('On', state, re.IGNORECASE):
			ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 8 %d' % (output), 'OK', initDelayRead=2, repeatCnt=4)
		else:
			ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 9 %d' % (output), 'OK', initDelayRead=2, repeatCnt=4)

def IO_Set_Relay(outList):
	Logger.debug('Relays: %s' % (str(outList)))
	for setOutput in outList:
		output = setOutput[0]
		state = setOutput[1]
		if re.search('On', state, re.IGNORECASE):
			ATE2.Relay_Set(output)
		elif re.search('All', state, re.IGNORECASE):
			ATE2.Relays_Clear_All()
		else:
			ATE2.Relay_Clear(output)
			
def IO_Set_vDaughter(checkList):
	state = checkList[0][0]
	# Power on Daughter board
	if re.search('ON', state, re.IGNORECASE):
		ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 102 136', 'OK')
	else:
		ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 102 128', 'OK')
		
def IO_Set_vRef(checkList):
	state = checkList[0][0]
	# Power on Aux2
	if re.search('ON', state, re.IGNORECASE):
		ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 102 34', 'OK')
	else:
		ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 102 32', 'OK')
		
def IO_Special_Setup():
	Logger.debug('Special Setup for Input 0 Test:')
	#turn on RS232PWR for 9 volts
	ATE2.Serial_Exec_Cmd('AT$APP PARAM 3985,0,2', 'OK',  cmdRepeatCnt=5, cmdRepeatDelay=2 , repeatDelay=2)
	ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 189 0', 'OK', cmdRepeatCnt=5, cmdRepeatDelay=2 , repeatDelay = 2)
	ATE2.Sleep(2)

def IO_Test():
	# test output/input loop-a-rounds
	testData_io = ATE2.Config_Get('TEST DATA', 'IO')
	for rec in testData_io.split('\n'):
		if rec == '': continue
		#print(rec)
		ATE2.Log(rec)
		if rec == '': continue
		tbl = ast.literal_eval(rec)
		action = tbl[0]
		tbl = tbl[1:]
		if action == 'I': IO_Check_Inputs(tbl)
		elif action == 'O': IO_Set_Outputs(tbl)
		elif action == 'B': IO_Set_Bias(tbl)
		elif action == 'R': IO_Set_Relay(tbl)
		elif action == 'VR': IO_Set_vRef(tbl)
		elif action == 'VD': IO_Set_vDaughter(tbl)
		elif action == 'ADC': IO_Check_ADCs(tbl)
		elif action == 'S': IO_Special_Setup()
		elif action == 'D': IO_Delay(tbl)
		elif action == 'GPIO': Linux.IO_GPIO(tbl)
		else: ATE2.Step_Fail('Invalid action [%s] in IO Test' % (action), errorCode='LMU_IO_INVALID_ACTION')

def IO_Old_Test():
	ATE2.Log_SubStep('Test Old IO Method')
	ATE2.Serial_Exec_Cmd('AT#IO?', '#IO: PASS', initDelayRead=2, cmdRepeatCnt=4, repeatCnt=3)
	
def Label_Print():
	global UUT
	ATE2.Log_SubStep('Send labels to BarTender')
	craf_btw_list = ATE2.Config_Get('CRAF', 'Label Dev BTW')
	
	# FCC, etc. Information
	radio_ID_parameters()
	
	# loop thru each label
	for thisBtw in craf_btw_list.split('\n'):  
		if thisBtw == '': continue
		craf_pn = ATE2.Config_Get('CRAF', 'Part Number')
		craf_btw = ATE2.Config_Get('CRAF', 'Label Dev BTW')
		labelType, btwLabel, cnt, fields = re.match('(\w*)\s*([\w-]*)\s*(\d*)\s*(.*)', thisBtw).group(1,2,3,4)
		Logger.debug('Type=%s Btw=%s Cnt=%s Fields=%s' % (labelType, btwLabel, cnt, fields))
		# if 'IMEI' in UUT.ATI1:
			# imei = UUT.ATI1['IMEI']
		# else:
			# imei = UUT.ATI1['MEID']
			# Logger.debug('Did not find Multimode MEID')	
		sValues = Label_Print_Fields(fields)
		# write label to scan dir
		f = open(Dir_Scan_Label + craf_pn + '-' + labelType + '.dd', 'w')
		t = '%BTW% /AF=' + ATE2.PathFiles + btwLabel + '.btw /D="%Trigger File Name%"' + ' /C=%s' % (cnt) + ' /R=2 /P\n'
		f.write(t)
		f.write('%END%\n')
		f.write(sValues)
		f.close()
		# save last label info
		f = open(Dir_Last_Label + 'LastLabel' + '.dd', 'w')
		f.write(t)
		f.write('%END%\n')
		f.write(sValues)
		f.flush()
		f.close()
		# log label info
		ATE2.Log('%s label [%s]' % (labelType, btwLabel), logToDetails=True)
		sValues = re.sub(',', ', ', sValues)
		ATE2.Log('Label values [%s]' % (sValues), logToDetails=True)
		
def Label_Via_Network_Print():
	# temporary network
	global UUT
	ATE2.Log_SubStep('Send labels to BarTender')
	craf_btw_list = ATE2.Config_Get('CRAF', 'Label Dev BTW')
	# get default printer
	f = platform.win32_ver()
	if f[0] == 'XP':
		line = os.popen('cscript C:\Windows\System32\prnmngr.vbs -g').read()
	else:
		line = os.popen('cscript C:\Windows\System32\Printing_Admin_Scripts\en-US\prnmngr.vbs -g').read()
	defPrinter = re.search('.*printer is (.*)', line, re.M).group(1)
	# FCC, etc. Information
	radio_ID_parameters()
	# loop thru each label
	for thisBtw in craf_btw_list.split('\n'):  
		if thisBtw == '': continue
		thisPrinter = defPrinter
		craf_pn = ATE2.Config_Get('CRAF', 'Part Number')
		craf_btw = ATE2.Config_Get('CRAF', 'Label Dev BTW')
		labelTypeTmp, btwLabel, cnt, fields = re.match('([\w\d:]*)\s*([\w-]*)\s*(\d*)\s*(.*)', thisBtw).group(1,2,3,4)
		# assign alternative printer if specified
		m = re.search('::(\d)', labelTypeTmp)	# printer number tacked onto labelType field for now
		if m:
			thisPrinter = ATE2.Config_Get('WORKSTATION', 'Printer_' + m.group(1))
		labelType = re.sub('::.*', '', labelTypeTmp)
		#
		cnt = int(cnt)
		Logger.debug('Type=%s Btw=%s Cnt=%s Fields=%s' % (labelType, btwLabel, cnt, fields))
		if 'IMEI' in UUT.ATI1:
			imei = UUT.ATI1['IMEI']
		else:
			imei = UUT.ATI1['MEID']
			Logger.debug('Did not find Multimode MEID')	
		sValues = Label_Print_Fields(fields)
		# write label to scan dir
		for i in range(1, cnt+1):
			c = '' if cnt == 1 else '#%d' % (i)
			f = open(Dir_Scan_Label + craf_pn + '-' + labelType + c + '.dd', 'w')
			t = '%BTW% /AF=' + ATE2.PathFiles + btwLabel + '.btw /D="%Trigger File Name%" /R=2 /P /PRN="' + thisPrinter + '"\n'
			f.write(t)
			f.write('%END%\n')
			f.write(sValues)
			f.close()
		# save last label info
		f = open(Dir_Last_Label + 'LastLabel' + '.dd', 'w')
		f.write(t)
		f.write('%END%\n')
		f.write(sValues)
		f.close()
		# log label info
		ATE2.Log('%s label [%s]' % (labelType, btwLabel), logToDetails=True)
		sValues = re.sub(',', ', ', sValues)
		ATE2.Log('Label values [%s]' % (sValues), logToDetails=True)

def Label_Print_Fields(fields):
	sValues = ''
	# get the label values
	for field in fields.split():
		# Logger.debug(field)
		if field == 'UUT_ESN':
			sValues += UUT.ESN
		elif field == 'UUT_GUID':
			sValues += UUT.Data['GUID']
		elif field == 'UUT_ICCID':
			sValues += UUT.ATI1['ICC-ID']
		elif field == 'UUT_IMEI':
			sValues += UUT.ATI1['IMEI']
		elif field == 'UUT_IMEI2':
			sValues += UUT.ATI1['IMEI2']
		elif field == 'UUT_IMEI1':
			sValues += UUT.ATI1['IMEI1']
		elif field == 'UUT_IMEI_MSND':
			sValues += re.match('(\w*),', UUT.ATI1['IMEI']).group(1)
		elif field == 'UUT_IMEI_MSNH':
			sValues += re.match('.*,(\w*)', UUT.ATI1['IMEI']).group(1)
		elif field == 'UUT_IMSI':
			sValues += UUT.ATI1['IMSI']
		elif field == 'UUT_MOBILEID':
			sValues += str(UUT.MOBILEID)
		elif field == 'UUT_MID':
			sValues += UUT.USERESN
		elif field == 'WIFI_MAC':
			sValues += UUT.Data['WIFI_MAC']
		elif field == 'LAN_MAC':
			sValues += UUT.Data['LAN_MAC']
		elif field == 'BT_MAC':
			sValues += UUT.Data['BT_MAC']
		elif field == 'UUT_SID':
			print('Reached here')
			sValues += UUT.Data['UUT_SID']
		elif field == 'UUT_SN':
			sValues += UUT.Data['UUT_SN']
		elif field == 'DateCode':
			dateCode = ATE2.Mfg_Week_Get()
			rmaFlag = ATE2.Config_Get('CRAF', 'RMA')
			if rmaFlag == 'Yes':
				dateCode = dateCode + ' W'
			sValues += dateCode
		elif field == 'RADIO_FCC_ID':
			sValues += UUT.Data['RADIO_FCC_ID']
		elif field == 'RADIO_IC':
			sValues += UUT.Data['RADIO_IC']
		elif field == 'UUT_MEID':
			sValues += re.search('(.*),.*', UUT.ATI1['MEID']).group(1)
		elif field == 'UUT_MEID_H':
			sValues += re.search('(.*),.*', UUT.ATI1['MEID']).group(0)
		elif field == 'UUT_MEID_D':
			sValues += re.search('.*,(.*)', UUT.ATI1['MEID']).group(1)	
		elif field == 'UUT_VBESN':
			#VBUS ESN
			sValues += UUT.Data['VBUS_ESN']
		elif field == 'COO':
			sValues += General.COO()
		elif field == 'EMPTY':
			sValues += ''			
		else:
			ATE2.Step_Fail('Label field not implemented [%s]' % (field), errorCode='LMU_LABEL_FLD_NOT_IMPL')
		sValues += ','
	sValues = re.sub(',$', '', sValues)
	return sValues

def LEDs_Board_Test():
	rc = int(ATE2.Operator_Instruction('Are the Orange and Green LEDs on the main board blinking?', noFail=True))
	if not rc:
		ATE2.Step_Fail('Operator failed LED Board test', errorCode='LMU_LED_OPER_FAIL_BOARD')

def LEDs_Output_Test():
	LedList = ATE2.Config_Get('ASSEMBLY', 'LEDs Output List')
	cnt = 0
	for led in LedList.split():
		ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 8 %s' % (led), 'OK')
		cnt = cnt + 1
	rc = int(ATE2.Operator_Instruction('Are the [%d] Red LEDs on the loop back harness on?' % (cnt), noFail=True))
	if not rc:
		ATE2.Step_Fail('Operator failed LED Output test', errorCode='LMU_LED_OPER_FAIL_OUTPUT')
	for led in LedList.split():
		ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 9 %s' % (led), 'OK')
		
def LEDs_Quad_Test():
	rc = int(ATE2.Operator_Instruction('Are the Green, Orange, Red and Blue LEDs on the main board blinking?', noFail=True))
	if not rc:
		ATE2.Step_Fail('Operator failed LED Board test', errorCode='LMU_LED_OPER_FAIL_QUAD')

def LEDs_Red_Green_Test():		
	rc = int(ATE2.Operator_Instruction('Are the Red and Green LEDs on the main board blinking?', noFail=True))
	if not rc:
		ATE2.Step_Fail('Operator failed LED Board test', errorCode='LMU_LED_OPER_FAIL_RED_GREEN')
		
def LEDs_Single_Green():
	rc = int(ATE2.Operator_Instruction('Is the Green LED on the main board blinking?', noFail=True))
	if not rc:
		ATE2.Step_Fail('Operator failed LED Board test', errorCode='LMU_LED_OPER_FAIL_SINGLE_GREEN')
		
def LEDs_Tri_Test():
	rc = int(ATE2.Operator_Instruction('Are the Green, Orange and Red LEDs on the main board blinking?', noFail=True))
	if not rc:
		ATE2.Step_Fail('Operator failed LED Board test', errorCode='LMU_LED_OPER_FAIL_TRI')

def LEDs_Alt_Test():
	rc = int(ATE2.Operator_Instruction('Are the Green, Orange and Blue LEDs on the main board blinking?', noFail=True))
	if not rc:
		ATE2.Step_Fail('Operator failed LED Board test', errorCode='LMU_LED_OPER_FAIL_TRI2')
		
def LMU_Hosted_App_Download():
	craf = ATE2.Config_Get('CRAF', 'LMU Hosted App Ver')
	if craf == '':
		ATE2.Log('No Hosted App', logToDetails=True)
		return (1)
	craf_appid, craf_ver, craf_file = craf.split()
	# load hosted app
	Ymodem_File_Transfer('ATHADNLD', 'Hosted_App', craf_file)
	# wait for device to reboot after download
	ATE2.Serial_Exec_Cmd('AT', 'OK', cmdRepeatCnt=60, cmdRepeatDelay=2)
	# verify loaded version
	uut_atih = ATE2.Serial_Exec_Cmd('ATIH', '=HA')
	ATE2.Serial_Exec_Cmd('ATIH', '=HA')
	for line in uut_atih:
		m = re.search('AppID:(.*)', line)
		if m: uut_appid = m.group(1); continue
		m = re.search('Version:(.*)', line)
		if m: uut_ver = m.group(1); break
	if (craf_appid == uut_appid) and (craf_ver in uut_ver):
		LMU.SQLiteDB_Post_TestResults(ts='', name='HOST APP', result='', tvalue='%s/%s' % (uut_appid, uut_ver), string='')
		LMU.PULS_Add_Entry('Hosted APP Ver: [%s/%s]' % (uut_appid, uut_ver))
		ATE2.Log('Hosted App up-to-date - [%s/%s]' % (uut_appid, uut_ver))
	else:
		ATE2.Step_Fail('Hosted App not up-to-date - CRAF [%s/%s] Board [%s/%s]' % (craf_appid, craf_ver, uut_appid, uut_ver), errorCode='LMU_HOSTED_APP_NOT_UTD')
	### run optional LMU HA configuration file
	try:
		craf_configVer, craf_configFile = ATE2.Config_Get('CRAF', 'LMU Hosted App Config').split()
	except:
		craf_configFile = False
	
	if craf_configFile:
		ATE2.Log_SubStep('Execute Hosted App Config script')
		# loop thru each cmd
		cnt = 0
		ATE2.Log('Hosted App Config file [%s]' % (craf_configFile), logToDetails=True)
		with open(ATE2.PathFiles + 'Hosted_App\\' + craf_configFile) as f:
			for cmd in f:
				if re.search('^\s*$', cmd): continue
				cnt += 1
				# macro cmds here
				m = re.search('\<(\w*)\s*(\S+)\>', cmd)
				if m:
					try:
						if m.group(1).upper() == 'DELAY':
							Logger.debug('Delay %s' % (m.group(2)))
							ATE2.Sleep(float(m.group(2)))
						else:
							ATE2.Log_Warning('No such macro cmd [%s]' % (m.group(1)))
							raise Exception()
					except:
						f.close()
						ATE2.Step_Fail('Problem with Hosted App Config macro cmd [%s] - See Detail Log' % (m.group(0)), errorCode='LMU_HOSTED_APP_MACRO_CMD')
				# non-macro cmds
				else:
					ATE2.Serial_Exec_Cmd(cmd, 'OK')
		f.close()
		ATE2.Log_Info('LMU Hosted App config cmds processed [%d]' % (cnt))
		# verify expected hosted app config loaded
		Device_Reset() # must reset device for HA version to update to include config
		craf_ha_config_ver = craf_ver + '-' + craf_configVer
		uut_ha_config_ver = re.search('Version:(.*)', ATE2.Serial_Exec_Cmd('ATIH', 'Version:', returnMatch=True)).group(1)
		if uut_ha_config_ver == craf_ha_config_ver:
			LMU.SQLiteDB_Post_TestResults(ts='', name='HOST APP', result='', tvalue=uut_ha_config_ver, string='')
			LMU.PULS_Add_Entry('Hosted APP Ver: [%s]' % (uut_ha_config_ver))
			ATE2.Log('Hosted App up-to-date - [%s]' % (uut_ha_config_ver))
		else:
			ATE2.Step_Fail('Hosted App not up-to-date - CRAF [%s] Board [%s]' % (craf_ha_config_ver, uut_ha_config_ver), errorCode='LMU_HOSTED_APP_NOT_UTD')

def Log_Level_Set():
	ATE2.Serial_Exec_Cmd('ATS125=0', 'OK')

def Mfg_Tracking_Scan():
	mfgTrackingNum = ATE2.Dialog_Input('Scan the Mfg Tracking Number', 'Mfg Tracking')
	ATE2.UI_Update('MfgTracking', mfgTrackingNum)
	return mfgTrackingNum
	
def Microphone_Test():
	ATE2.Serial_Exec_Cmd('AT$APP PARAM 292,0,50', 'OK')	
	ATE2.Serial_Exec_Cmd('ATS125=3', 'OK')	
	ATE2.Serial_Exec_Cmd('AT$APP PARAM 293,0,01', 'OK')
	Device_Reset(testcfg = False)
	ATE2.Operator_Instruction('Gently and continously tap unit; Press OK while tapping')
	ATE2.Serial_Exec_Cmd('', 'Trig=<Audio Event', repeatCnt=20, returnMatch=True)
	ATE2.Serial_Exec_Cmd('AT$APP PARAM 292,0,0', 'OK')	
	ATE2.Serial_Exec_Cmd('AT$APP PARAM 293,0,0', 'OK')
	ATE2.Serial_Exec_Cmd('ATS125=0', 'OK')
	Device_Reset()

def	Mobile_ID():
	ATE2.Log_SubStep('Get Mobile ID')
	craf_mobileId = ATE2.Config_Get('CRAF', 'Mobile ID')
	if craf_mobileId is None or craf_mobileId == '':
		ATE2.Log('Mobile ID not set')
		return
	else:
		ATE2.Log('Calculate Mobile ID using function [%s]' % (craf_mobileId))
		func = getattr(Serialize, 'QT_Mobile_ID_' + craf_mobileId)
		mobileId = func()
		UUT.MOBILEID = mobileId
		ATE2.Log('Mobile ID [%s]]' % (mobileId))

def Motion_Test():
	ATE2.Serial_Exec_Cmd('AT#MOT?', 'MOT: PASS', initDelayRead=1, repeatCnt=3)
	
def Network_Test():
	# attempt power cycle board before network or sometimes connection issues
	ATE2.Power_Turn_Off()
	ATE2.Power_Turn_On()
	LMU.Device_Reset(testcfg = False)	
	# test assumes workstation in same timezone as PULS
	LMU.Radio_Check_Connected()
	# ATE2.Log_SubStep('Send phone number to PULS')
	# line = ATE2.Serial_Exec_Cmd('ATIC','Phone\s*Number\s*: ', cmdRepeatCnt=60, cmdRepeatDelay=3 , returnMatch=True, noFail=True)
	# min = re.search(':(.*)', line).group(1)
	# PULS_Add_Entry('MIN:%s' %(min))
	ATE2.Sleep(1)
	for retry in range(3):
		ATE2.Log_SubStep('Make board send ID Report')
		ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 49 129', 'OK')
		ATE2.Sleep(5)
		ATE2.Log_SubStep('Verify ID Report received by PULS')
		pool = urllib3.PoolManager()
		url = 'https://puls.calamp.com/service/device/%s' % (UUT.ESN)
		Logger.debug(url)
		headers = {'Authorization':'Basic %s' % (base64.b64encode('mfgmdtapi:CalAmp123$'.encode())).decode()}
		resp = pool.urlopen('GET', url, headers=headers)
		status = str(resp.status)
		Logger.debug(str(resp.status))
		ATE2.Log(str(resp.status))
		respData = resp.data.decode()
		Logger.debug(respData)
		if status != '200':
			ATE2.Step_Fail('Network test failed')
		if re.search('Unable', respData):
			ATE2.Log('Network Test did not complete - retry', logToDetails=True)
			continue
		try:
			lastRptTime = re.search('lastIdReportTime...([\d\-: ]*)', resp.data.decode()).group(1)
		except:
			ATE2.Step_Fail('No Last ID Report Time', errorCode='LMU_NETWRK_TEST_NOIDREPORT')
		ATE2.Log('Last ID Report time [%s]' % (lastRptTime))
		lastRptTime = re.search('(.*)\-.*', lastRptTime).group(1)
		dt = parse(lastRptTime)
		tsRpt = time.mktime(dt.timetuple())
		tsRpt = time.mktime(dt.timetuple())
		tsNow = time.time()
		print('%s' % (time.ctime(int(tsNow))))
		print ('%s' % (time.ctime(int(tsRpt))))
		Logger.debug('Last ID Report Time %s,%s ' % ((time.ctime(int(tsRpt))), (time.ctime(int(tsNow)))))
		if tsNow - tsRpt > 120:
			ATE2.Step_Fail('Last ID Report timestamp > 120 seconds ago', errorCode='LMU_NETWRK_TEST_NOIDREPORT')
		return
	ATE2.Step_Fail('Failed to pass Network Test - see Detailed Log')
	
def NVM_Test():
	line = ATE2.Serial_Exec_Cmd('AT#NVMSIG?', '^#NVMSIG:.*(OK|Invalid)', returnMatch=True)
	if re.search('Invalid', line):
		ATE2.Log('Initializing the NVM')
		ATE2.Serial_Exec_Cmd('AT#NVMINIT', 'NVMINIT: OK')
		ATE2.Serial_Exec_Cmd('AT#NVMSIG?', '^#NVMSIG: OK')

def OneBitBus_Test():
	ATE2.Serial_Exec_Cmd('AT#1BB?', '1BB: PASS', initDelayRead=1)

def OneBitBus171_Test(setting=33):
	ATE2.Serial_Exec_Cmd('ATS171=%d' % (setting), 'OK')
	Device_Reset()
	ATE2.Serial_Exec_Cmd('AT#1BB?', '1BB: PASS', initDelayRead=1, cmdRepeatCnt=5)

def OneBitBusR_Test():
	ATE2.Log_SubStep('Test 1BB-R')
	ATE2.Serial_Exec_Cmd('AT#1BBR?', '1BBR: PASS', initDelayRead=1, cmdRepeatCnt=10)

def OneBitBusS_Test():
	rtn_158=ATE2.Serial_Exec_Cmd('ATS158?', 'OK')
	ATE2.Serial_Exec_Cmd('ATS158=0', 'OK')
	ATE2.Serial_Exec_Cmd('ATS181=64', 'OK')
	Device_Reset()
	
	ATE2.Serial_Exec_Cmd('AT#1BB?', '1BB: PASS', initDelayRead=1, cmdRepeatCnt=5)
	ATE2.Serial_Exec_Cmd('ATS181=0', 'OK')
	ATE2.Serial_Exec_Cmd('ATS158=%s' % (rtn_158[1]), 'OK')
	Device_Reset()
	
def OneBitBusT_Test():
	ATE2.Log_SubStep('Test 1BB-T')
	ATE2.Serial_Exec_Cmd('AT#1BBT?', '1BBT: PASS', initDelayRead=1, cmdRepeatCnt=10)
	
def Operator_Instruction_Connect_Battery():
	testData_rfEnclosure = ATE2.Config_Get('TEST DATA', 'RF Enclosure')
	if testData_rfEnclosure == 'Yes':
		ATE2.Operator_Instruction('Open lid; Connect the battery; Close lid')
	else:
		ATE2.Operator_Instruction('Connect the battery')

def Operator_Instruction_Disconnect_Battery():
	testData_rfEnclosure = ATE2.Config_Get('TEST DATA', 'RF Enclosure')
	if testData_rfEnclosure == 'Yes':
		ATE2.Operator_Instruction('Open lid; Disconnect battery; Close lid')
	else:
		ATE2.Operator_Instruction('Disconnect battery')
		
def Operator_Instruction_Insert_Customer_SIM():
	ATE2.Operator_Instruction('Insert the Customer SIM')
		
def Operator_Instruction_Open_Lid():
	ATE2.Operator_Instruction('Open lid for radio test')

def Operator_Manual_Instruction(instr):
	ATE2.Operator_Instruction('%s' % (instr))

def Operator_Video_Instruction(instr, mediaFile):
	ATE2.Operator_Video_Instruction(instr, '%s\Media\%s' % (ATE2.PathFiles, mediaFile))
	
def Passthru_Enter(device = None, port = None):
	ATE2.Log_SubStep('Enter PASSTHRU mode')
	if device and port:
		ATE2.Serial_Exec_Cmd('ATPASSTHRU %d %d' % (device, port), 'OK')
	else:
		ATE2.Serial_Exec_Cmd('ATPASSTHRU', 'OK')
	
def Passthru_Exit(runTestConfig=True):
	ATE2.Log_SubStep('Exit PASSTHRU mode')
	ATE2.Serial_Send_Binary(b'\x03')
	if runTestConfig:
		LMU.Test_Config_Set()

def Password_Generate():
	craf_pwdFunc = ATE2.Config_Get('CRAF', 'Password Func')
	func = getattr(Password, craf_pwdFunc)
	func() # run customer func
	ATE2.Log('Generated password [%s] using function [%s]' % (UUT.Password, craf_pwdFunc))

def Password_Set():
	ATE2.Log_SubStep('Generate password')
	Password_Generate()
	ATE2.Log_SubStep('Write password to board')
	ATE2.Serial_Exec_Cmd('AT$APP PARAM 2177,0,"%s"' % (UUT.Password), 'OK')
	ATE2.Log_SubStep('Activate password')
	# s-reg 171 bit 4
	# at$app param 2177,0," to clear pwd
	# at$app param 1024,51,16,0 to clear pwd bit
	ATE2.Serial_Exec_Cmd('AT$APP PARAM 1024,51,16,16', 'OK')
	PULS_Add_Entry('Info: Device had LMU password set')
	
def PCBA_Set():
	line = ATE2.Serial_Exec_Cmd('AT#PCBA?', 'PCBA:', repeatCnt=10, returnMatch=True)
	pcba = re.sub('.*: *', '', line)
	if pcba != '' and pcba != '0' :
		ATE2.Log('PCBA previously set [%s]' % (pcba))
		ATE2.Serial_Exec_Cmd('AT#PCBA %s' % (pcba), 'OK')
	else:
		ws_locCode, ws_wsNum = ATE2.WorkStation_Config_Get()
		epochSecs = int(time.time())
		if 'PCBA' in UUT.Data:
			if len(UUT.Data['PCBA']) > 10:
				#pcba previous set and need to use.
				pcba = UUT.Data['PCBA']
			else:
				pcba = '%02d%04d%0d' % (int(ws_locCode), int(ws_wsNum), int(epochSecs))
		else:
			# ws_locCode = re.match('^(\d*)', ATE2.Config_Get('WORKSTATION', 'Location')).group(1)
			# ws_wsNum = ATE2.Config_Get('WORKSTATION', 'Number')
			# BKMB - until INI generated use ws config file from ATE1
			pcba = '%02d%04d%0d' % (int(ws_locCode), int(ws_wsNum), int(epochSecs))
		
		Logger.debug('PCBA = %s' % (pcba))
		ATE2.Serial_Exec_Cmd('AT#PCBA %s' % (pcba), 'OK')
		ATE2.Log('PCBA saved to board [%s]' % (pcba))
	UUT.Data['PCBA'] = pcba
	ATE2.UUT.Ident2 = 'PCBA: ' + UUT.Data['PCBA']
	
def PLX_Log_File():
	usrAgent = 'CalAmpATE2/' + ATE2.TestVals.ATEver + '/' + ATE2.TestVals.ModelVersion
	pn = ATE2.Config_Get('CRAF', 'Part Number')
	assy = ATE2.Config_Get('ASSEMBLY', 'Assembly Number') + '-G0000'
	pcba = UUT.Data['PCBA']
	app = re.sub(' \(.*', '', UUT.ATI0['APP'])
	if 'IMEI' in UUT.ATI1:
		imei = UUT.ATI1['IMEI']
	else:
		imei = UUT.ATI1['MEID']
		Logger.debug('Did not find Multimode MEID')	
	if 'MEID' in UUT.ATI1:
		meid = UUT.ATI1['MEID']
	else:
		meid = ''
	testId = UUT.Data['TestID']
	body = """
ATEVer: %s
UNITPARTNUMBER: %s
BOARDPARTNUMBER: %s
PCBA: %s
#TEST_ID: %s
APP:%s
IMEI: %s
%s
	""" % (usrAgent, pn, assy, pcba, testId, app, imei, PULS_Extra_Vals)
	fplx_Log_location = ATE2.Log_Dir + 'PLX_' + testId + '.Log'
	try:
		f = open(fplx_Log_location, 'w')
		f.write(body)
		f.close()
	except:
		print(inst.args)
		ATE2.Step_Fail('Unable to write Plexus Log File', errorCode='LMU_PLEXUS_LOG_FILE_FAIL')
		return

def Power_Monitor_Test():
	ATE2.Operator_Instruction('Please Connect the Battery and Montior the Inputs')
	ATE2.Serial_Exec_Cmd('AT#INP?','OK', cmdRepeatCnt=2)
	rc = int(ATE2.Operator_Instruction('Are Values of INP5=0 and INP6=0 ?',''))
	if not rc:
		ATE2.Step_Fail('Operator failed Power Monitor test', errorCode='LMU_POWMONITOR_OPER_FAIL')
	else:
		ATE2.Operator_Instruction('Please Disconnect the Battery and Monitor the Inputs')	
		ATE2.Serial_Exec_Cmd('AT#INP?','OK', cmdRepeatCnt=2)
		rc = int(ATE2.Operator_Instruction('See if the INP5=1 and INP6=1',''))
		if not rc:
			ATE2.Step_Fail('Operator failed Power Monitor test', errorCode='LMU_POWMONITOR_OPER_FAIL')
		else:
			ATE2.Log_SubStep('Power Monitor Test Sucessful')

def Power_Read_Current():
	craf_current = ATE2.Config_Get('TEST DATA', 'Power Current')
	ATE2.Log('Craf Max Current = [%s]' % (craf_current)) 
	current = ATE2.Power_Read_Current()
	if current > craf_current:
		ATE2.Log('DUT Current = [%s]' % (current))
		ATE2.Step_Fail('Excessive Current Detected .. shutting power off ', errorCode='LMU_PWR_EXCESSIVE_CURRENT')
	else:
		ATE2.Log('DUT Current = [%s]' % (current))	
	
def Power_State_Test():
	assy_battery = ATE2.Config_Get('ASSEMBLY', 'Battery')
	if assy_battery != 'Yes':
		ATE2.Log('No battery configured', logToDetails=True)
		return
	testData_ps = ATE2.Config_Get('TEST DATA', 'POWERSTATE')
	for rec in testData_ps.split('\n'):
		if rec == '': continue
		tbl = ast.literal_eval(rec)
		action = tbl[0]
		tbl = tbl[1:]
		if action == 'ON':
			ATE2.Log_SubStep('Check Power State with power supply on')
			ATE2.Power_Turn_On()
			IO_Check_Inputs(tbl)
		elif action == 'OFF':
			ATE2.Log_SubStep('Check Power State with power supply off')
			ATE2.Power_Turn_Off()
			IO_Check_Inputs(tbl)
		else: ATE2.Step_Fail('Invalid action [%s] in Power State Test' % (action), errorCode='LMU_PWR_STATE_INVALID_ACTION')		
	ATE2.Power_Turn_On()

def PULS_Add_Entry(line):
	global PULS_Extra_Vals
	PULS_Extra_Vals += line + '\n'
	
def Radio_Check_Connected():
	ATE2.Log_SubStep('Check that radio is connected')
	for retry in range(3):
		# this routine written allow AT#TESTCONFIG mode active and radio connected
		line = ATE2.Serial_Exec_Cmd('AT#COMM?', ': ACQUIRED|: CONNECTED', cmdRepeatCnt=40, cmdRepeatDelay=3, returnMatch=True)
		if re.search('CONNECTED', line): return
		# ATE2.Serial_Exec_Cmd('AT$APP COMM CONNECT', 'OK', cmdRepeatCnt=5)
		line = ATE2.Serial_Exec_Cmd('AT#COMM?', ': CONNECTED|NONE', cmdRepeatCnt=40, cmdRepeatDelay=3, returnMatch=True)
		if re.search('CONNECTED', line): return
		ATE2.Serial_Exec_Cmd('ATIC', '')
	ATE2.Step_Fail('Radio failed to connect in %d attempts - see Detail Log' % (retryCnt), errorCode='LMU_RADIO_CHK_CONN_FAIL')

def Radio_Com_Off():
	# To be used to turn off the radio
	# Radio's to be turned back on after a reset or use the On function.
	General.Check_for_FW()
	# FW Not loaded
	if ATE2.TestVals.FwLoaded == False: return
	ATE2.Serial_Exec_Cmd('AT$APP COMM OFF','OK', repeatCnt=3)
	ATE2.Serial_Exec_Cmd('AT$APP GPS OFF','OK', repeatCnt=3)
	ATE2.Serial_Exec_Cmd('ATIC','Inactive|Data Service.*No', cmdRepeatCnt=20, cmdRepeatDelay=2, returnMatch=True)

def Radio_Com_On():
	# To be used to turn on the radio
	# A reset will also turn the radio's back on
	ATE2.Serial_Exec_Cmd('AT$APP COMM ON','OK', repeatCnt=3)
	ATE2.Serial_Exec_Cmd('AT$APP GPS ON','OK', repeatCnt=3)
	
def radio_ID_parameters():
	# get radio attributes
	uut_assynum =  ATE2.Config_Get('ASSEMBLY', 'Assembly Number')
	uut_radioVer = Comm_Get_Radio_Version()
	radio_file = 'radio_ID.txt'
	with open (Dir_ATE_DB + radio_file, 'r') as f:
		for line in f:
			#Ticket #436
			num_param_radio = len(line.split(':'));
			if num_param_radio == 4:
				partnum, radioVer, radio_fcc_id, radio_ic = line.split(':');
				radio_ic = radio_ic.rstrip('\n')
			elif num_param_radio == 6:
				partnum, radioVer, radio_fcc_id, radio_ic, radio2_fcc_id, radio2_ic = line.split(':');
				radio_ic = radio2_ic.rstrip('\n')
			elif num_param_radio == 1:
				f.close()
				ATE2.Step_Fail('Please ensure Radio ID file does not have any extra lines at end of file', errorCode='LMU_RADIO_ID_FILE')
			else:
				f.close()
				ATE2.Step_Fail('Radio ID file in DB directory does not have the correct # of items: [%d]. \n%s' % (num_param_radio,line), errorCode='LMU_RADIO_ID_DB_ERR')
			if uut_assynum != partnum: continue
			if uut_radioVer != radioVer: continue
			# Now have the correct information unless at end of file;
			break
	f.close()
	if (uut_assynum != partnum) or (uut_radioVer != radioVer):
		# Unit not in file
		craf_btw =  ATE2.Config_Get('CRAF', 'Label Dev BTW')
		if re.search('RADIO_', craf_btw):
			#Error because BTW needs Radio information
			ATE2.Step_Fail('Assembly Number [%s] and Radio Version [%s] are not in %s file' % (uut_assynum,uut_radioVer,radio_file))
		else:
			#There is no RADIO information needed in the INI BTW file.
			return
	if num_param_radio >= 4:
		UUT.Data['RADIO_FCC_ID'] = radio_fcc_id
		UUT.Data['RADIO_IC'] = radio_ic
	elif num_param_radio >= 6:
		UUT.Data['RADIO2_FCC_ID'] = radio2_fcc_id
		UUT.Data['RADIO2_IC'] = radio2_ic
	
def Real_Time_Clock_Test():
	# note - diff formats for 8/32 bit RTC output
	line = ATE2.Serial_Exec_Cmd('AT#RTC?', 'RTC [Tt]ime', returnMatch=True, cmdRepeatCnt=3)
	t1 = re.search('RTC [Tt]ime.*GMT ([\d:]*)|RTC time: (\d*)', line).group(1)
	Logger.debug(t1)
	ATE2.Sleep(1.5)
	line = ATE2.Serial_Exec_Cmd('AT#RTC?', 'RTC [Tt]ime', returnMatch=True, cmdRepeatCnt=3)
	t2 = re.search('RTC [Tt]ime.*GMT ([\d:]*)|RTC time: (\d*)', line).group(1)
	Logger.debug(t2)
	if t2 <= t1:
		ATE2.Step_Fail('Real-Time Clock time did not advance', errorCode='LMU_RTC_DID_NOT_ADVANCE')

def Reed_Current_Test():
	#Need to clear AT#TESTCONFIG
	LMU.Device_Reset(testcfg = False)
	
	#Set Current Meter into Low range  - should be part of setup.
	ATE2.Log_SubStep('Test current while in hibernate mode - Nonboost Mode')
	factory_relaybox = ATE2.Config_Get('FACTORY', 'Relay Box Present')
	relayNum = ATE2.Config_Get('ENV', 'Reed Relay')
	#Close the Reed Switch
	if factory_relaybox == 'Yes':
		ATE2.Relay_Clear(relayNum)
		ATE2.Sleep(1.5)
	else:
		ATE2.Operator_Instruction('Place the magnet near the Reed Switch')
	
	#Wait for current on Main unit to be less than 0.010 Amps
	retries = 5
	curr_spec = 0.01
	for retry in range(retries):
		uut_cur = float(ATE2.Power_Read_Current(type='P6V'))
		if uut_cur <= curr_spec:
			Logger.debug('UUT is in Hibernate mode, current is %.3f Amps' % (uut_cur))
			ATE2.Sleep(1)
			break
		else:
			Logger.debug('Current is %.3f Amps' % (uut_cur))
			ATE2.Sleep(3)
	if (retry + 1) == retries:
		ATE2.Step_Fail('UUT Failed to go into Hibernate mode. Current was [%.3f] Amps. Should be less than [%.3f] Amps.' % (uut_cur, curr_spec), errorCode='LMU_REED_CURRENT_TEST_FAIL_HIBERNATE')
	# Will have gone into hibernate mode
	
	#Remove the shorting jumper on current meter.	
	relayNum_Multimeter = ATE2.Config_Get('ENV', 'Multimeter Relay')
	if factory_relaybox == 'Yes':
		ATE2.Relay_Clear(relayNum_Multimeter)
		ATE2.Sleep(1.5)
	else:
		ATE2.Operator_Instruction('Remove the shorting jumper between the current meter inputs')
	
	#Measure Current on Multimeter
	cur_Min, cur_Max, units = ATE2.Config_Get('TEST DATA', 'Reed Nonboost').split()
	cur_Max = int(cur_Max); cur_Min = int(cur_Min)

	# Check results
	uut_cur = float(ATE2.Multimeter_Read_Current(range_auto = 'OFF', range_val = '0.05'))
	#Will return in Amps.
	uut_cur = ATE2.Current_Value_Change(unit = units, current = uut_cur)
	if uut_cur >= cur_Min and uut_cur <= cur_Max:
		ATE2.Log('REED NONBOOST value [%d] %s in range [%d, %d]' % (uut_cur, units, cur_Min, cur_Max), logToDetails=True)
		LMU.SQLiteDB_Post_TestResults(ts='', name='REED NONBOOST', result='PASS', tvalue=uut_cur, string=units)
		PULS_Add_Entry('REED NONBOOST value [%d] %s in range [%d, %d]' % (uut_cur, units, cur_Min, cur_Max))
		ATE2.Mfg_Add_Step_Detail(property='REED NONBOOST', value=uut_cur, minRange=cur_Min, maxRange=cur_Max)
	else:
		#Failed
		LMU.SQLiteDB_Post_TestResults(ts='', name='REED NONBOOST', result='FAIL', tvalue=uut_cur, string=units)
		ATE2.Mfg_Add_Step_Detail(property='REED NONBOOST', value=uut_cur, minRange=cur_Min, maxRange=cur_Max)
		ATE2.Step_Fail('REED NONBOOST value [%d] %s out of range [%d, %d]' % (uut_cur, units, cur_Min, cur_Max), errorCode='LMU_REED_CURRENT_TEST_OUT_OF_RANGE')

	#==================< BOOST >========================
	ATE2.Log_SubStep('Test current while in hibernate mode - Boost Mode')
	#Lower voltage on power supply to 3.0 volts
	ATE2.Power_Set_Voltage(pwrlvl=3.0, type='P6V')
	#Wait for boost circuit to take effect.
	curr_boost_spec = ATE2.Config_Get('TEST DATA', 'Reed Boost')
	# Get Spec
	cur_Min, cur_Max, units = ATE2.Config_Get('TEST DATA', 'Reed Boost').split()
	cur_Max = int(cur_Max); cur_Min = int(cur_Min)

	#Measure current on Multimeter
	uut_cur = float(ATE2.Multimeter_Read_Current(range_auto = 'OFF', range_val = '0.05'))
	#Will return in Amps.
	uut_cur = ATE2.Current_Value_Change(unit = units, current = uut_cur)
	#Check results
	if uut_cur >= cur_Min and uut_cur <= cur_Max:
		#Passed
		ATE2.Log('REED BOOST value [%d] %s in range [%d, %d]' % (uut_cur, units, cur_Min, cur_Max), logToDetails=True)
		LMU.SQLiteDB_Post_TestResults(ts='', name='REED BOOST', result='PASS', tvalue=uut_cur, string=units)
		PULS_Add_Entry('REED BOOST value [%d] %s in range [%d, %d]' % (uut_cur, units, cur_Min, cur_Max))
		ATE2.Mfg_Add_Step_Detail(property='REED BOOST', value=uut_cur, minRange=cur_Min, maxRange=cur_Max)
	else:
		#Failed
		LMU.SQLiteDB_Post_TestResults(ts='', name='REED BOOST', result='FAIL', tvalue=uut_cur, string=units)
		ATE2.Mfg_Add_Step_Detail(property='REED BOOST', value=uut_cur, minRange=cur_Min, maxRange=cur_Max)
		ATE2.Step_Fail('REED BOOST value [%d] %s out of range [%d, %d]' % (uut_cur, units, cur_Min, cur_Max), errorCode='LMU_REED_CURRENT_TEST_OUT_OF_RANGE')

	#Place short back on Multimeter
	if factory_relaybox == 'Yes':
		ATE2.Relay_Set(relayNum_Multimeter)
		ATE2.Sleep(1)
	else:
		ATE2.Operator_Instruction('Place the shorting jumper between the current meter inputs')
	
	#Set voltage on Power supply back to 3.6 volts
	ATE2.Power_Set_Voltage_3_6v()
	
	ATE2.Log_SubStep('Set Reed Switch open')
	if factory_relaybox == 'Yes':
		ATE2.Relay_Set(relayNum)
		ATE2.Sleep(1)
	else:
		ATE2.Operator_Instruction('Move the magnet away from the Reed Switch')
	#Reset device and apply Testconfig
	Device_Reset()

def Reed_Switch_Test():
	ATE2.Log_SubStep('Check Reed Switch open initially')
	factory_relaybox = ATE2.Config_Get('FACTORY', 'Relay Box Present')
	relayNum = ATE2.Config_Get('ENV', 'Reed Relay')
	if factory_relaybox == 'Yes':
		ATE2.Relay_Set(relayNum)
	ATE2.Serial_Exec_Cmd('AT#INP?','#INP0: 0')
	ATE2.Log_SubStep('Check Reed Switch closed')
	if factory_relaybox == 'Yes':
		ATE2.Relay_Clear(relayNum)
	else:
		ATE2.Operator_Instruction('Place the magnet near the Reed Switch')
	ATE2.Serial_Exec_Cmd('AT#INP?','#INP0: 1', cmdRepeatCnt=10)
	ATE2.Log_SubStep('Check Reed Switch open')
	if factory_relaybox == 'Yes':
		ATE2.Relay_Set(relayNum)
	else:
		ATE2.Operator_Instruction('Move the magnet away from the Reed Switch')
	ATE2.Serial_Exec_Cmd('AT#INP?','#INP0: 0')
				
def Sales_Order_Scan():
	salesOrderNum = ATE2.Dialog_Input('Scan the Sales Order number', 'Sales Order')
	ATE2.UI_Update('SalesOrder', salesOrderNum)
	return salesOrderNum
	
def Scan_Label_Test():
	# determine which barcodes to scan
	assy_barcodeRead = ATE2.Config_Get('ASSEMBLY', 'Read Barcode')
	lstValue = Label_Print_Fields(assy_barcodeRead)
	lstRemain = lstValue
	# continue to Scan while #'s are correct
	# exit scan once all the values have been completed.
	Logger.debug('Values to Scan [%s]' % lstRemain)
	while True:
		scantn = ATE2.Operator_Input('Scan a Barcode', bClear=True)
		if scantn in lstValue:
			if scantn in lstRemain:
				lstmp = ''
				for field in lstRemain.split(','):
					if field != scantn:
						lstmp += field
						lstmp += ','
				if lstmp == '': return 0
				lstRemain = re.sub(',$', '', lstmp)
				if lstRemain == '': return 0
		else:
			return 1	# unit failed
		Logger.debug('Remaining values to Scan [%s]' % lstRemain)

def Set_Peg_Action_39_0():
	ATE2.Serial_Exec_Cmd('AT$APP PEG ACTION 39 0', 'OK', returnMatch=False)
	
def SIM_Activate():
	craf_simActFunc = ATE2.Config_Get('CRAF', 'SIM Activation Func')
	if craf_simActFunc == '':
		return
	else:
		func = getattr(Activate, craf_simActFunc)
	ATE2.Log('SIM Activation function [%s]' % (craf_simActFunc), logToDetails=True)
	func() # run customer func
	UUT.IsActivated = True
	PULS_Add_Entry('#SIM_ACTIVATE Pass')
	
def SIM_Ejector_Test():
	ATE2.Operator_Instruction('Eject the SIM')
	ATE2.Log_Warning('SIM Ejector Test Not Completed')
	ATE2.Operator_Instruction('Reinsert the SIM')
	ATE2.Log_Warning('SIM Ejector Test Not Completed')
	
def SQLiteDB_Post_PF(pf):
	ts = UUT.Data['StartTime']
	testId = UUT.Data['TestID']
	pcba = UUT.Data['PCBA']
	assy = ATE2.Config_Get('ASSEMBLY', 'Assembly Number')
	pn = ATE2.Config_Get('CRAF', 'Part Number')
	ateVer = 'ATE2 ' + ATE2.TestVals.ATEver
	pf = ATE2.TestVals.ATEpass
	cur = ResultsSQLiteDbConn.cursor()
	query = "insert into TEST_PF values ('%s', '%s', '%s', '%s', '%s', '%s', %d)" % (ts, testId, pcba, assy, pn, ateVer, pf)
	cur.execute(query)
	cur.close()
	ResultsSQLiteDbConn.commit()
	
def SQLiteDB_Post_TestResults(ts, name, result, tvalue, string):
	ts = UUT.Data['StartTime']
	pcba = UUT.Data['PCBA']
	pn = ATE2.Config_Get('CRAF', 'Part Number')
	cur = ResultsSQLiteDbConn.cursor()
	query = "insert into TEST_RESULTS values ('%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (ts, pcba, pn, name, result, tvalue, string)
	cur.execute(query)
	cur.close()
	ResultsSQLiteDbConn.commit()
	
def Step_Accessory_Configure_Main_Board():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Configure Main Board for accessories')
	Accessory_Configure_Main_Board()
	
def Step_APN_Load():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Load APN list')
	APN_List_Load()

def Step_Assembly_Configure():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Configure Main Board per assembly configuration')
	Assembly_Configure()
	
def Step_Battery_Connect(pwr_off = True):
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Connect the Battery')
	assy_battery = ATE2.Config_Get('ASSEMBLY', 'Battery')
	if assy_battery != 'Yes':
		ATE2.Log('No battery configured')
		return
	if pwr_off: ATE2.Power_Turn_Off()
	LMU.Operator_Instruction_Connect_Battery()
	if pwr_off: 
		ATE2.Power_Turn_On()
		Device_Reset()

def Step_Battery_Disconnect(pwr_off = False):
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Disconnect the Battery')
	assy_battery = ATE2.Config_Get('ASSEMBLY', 'Battery')
	if assy_battery != 'Yes':
		ATE2.Log('No battery configured')
		return
	if pwr_off: ATE2.Power_Turn_Off()
	LMU.Operator_Instruction_Disconnect_Battery()
	if pwr_off: 
		ATE2.Power_Turn_On()
		Device_Reset()

def Step_Battery_Data_From_User():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Entering Battery Date Values')
	Battery_Data_From_User()

def Step_BootDelay_Set():
	ATE2.Log_Step('Setting Boot Delay')
	if not ATE2.Check_Test_Selected(): return
	craf_boot =  int(ATE2.Config_Get('CRAF', 'Boot Delay'))
	BootDelay_Set(bootDelay=craf_boot)
	
def Step_CDMA_Activation(): 
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the CDMA activation')
	assy_radio = ATE2.Config_Get('ASSEMBLY', 'Radio Type')
	CDMA_Activation()
	PULS_Add_Entry('#CDMA_Activation Pass')

def Step_Debug_Off():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Debug off')
	Debug_Off()
	
def Step_Device_Power_Cycle():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Power cycle the device')
	ATE2.Power_Turn_Off()
	ATE2.Power_Turn_On()
	Device_Reset()
	
def Step_Device_Reset():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Reset the device')
	Device_Reset()

def Step_Device_Hardware_Values():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Enter the Device Hardware Values')
	Device_Hardware_Values()
	
def Step_ESN_Pass_Within_24h():
	# Do not need for PWA or ASSY area.
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Check if ESN passed within 24 hours')
	esn = re.sub('ESN:\s*', '', ATE2.Serial_Exec_Cmd('AT#ESN?', 'ESN:', returnMatch=True))
	if int(esn) > 1000:
		conn = psycopg2.connect(host='dev-ate-01', database='MRM', user='postgres', password='P05tgR3$dev')
		curr = conn.cursor()
		curr.execute('select ident_1, start_date from mfg.uut_result where ident_1 = \'ESN: %s\' and status = \'Pass\' order by start_date desc limit 1' % (esn))
		recs = curr.fetchall()
		for rec in recs:
			ATE2.Log('Last passing ATE run [%s]' % rec[1].strftime('%A %Y-%b-%d %H:%M'))
			prev24 = datetime.datetime.now() + datetime.timedelta(days=-1)
			ATE2.Log('24h previous to now [%s]' % prev24.strftime('%A %Y-%b-%d %H:%M'))
			if rec[1] > prev24:
				ATE2.Operator_Instruction('Warning!! This ESN [%s] previously passed ATE run in last 24 hours [%s]' % (esn, rec[1].strftime('%A %Y-%b-%d %H:%M')))
		curr.close()
		conn.close()

def Step_Factory_Config_Set():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Set device to Factory configuration')
	Factory_Config_Set()
	ATE2.Sleep(1.5)
	Device_Reset()
	
def Step_HWREV_Set():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Set HW Revision')
	HWREV_Set_Step()
	
def Step_iBeacon_Config_Set():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Configure iBeacon advertising data')
	iBeacon_Config_Set()

def Step_Install_Jumper_JP1():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Install Jumper at JP1')
	ATE2.Operator_Instruction('Install Jumper to JP1')
	
def Step_Log_Level_Set():
	if not ATE2.Check_Test_Selected(): return
	Log_Level_Set()
	
def Step_Mfg_Record_Update():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Update the Mfg Record')
	fws_name = ATE2.Workstation_Name_Get()
	if ('PLX' in fws_name) or ('CLD' in fws_name):
		PLX_Log_File()
	ESN_Request_From_PULS(writeEsn=False)
	
def Step_Mfg_Tracking_Scan():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Scan the Mfg Tracking Number')
	mfgTrackingNum = Mfg_Tracking_Scan()
	ATE2.Log('Mfg Tracking Number [%s]' % (mfgTrackingNum))

def Step_Mobile_ID():
	if not LMU.ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Request Mobile ID')
	Mobile_ID()
	
def Step_Operator_Video_Instruction(title, instr, mediaFile):
	if not ATE2.Check_Test_Selected(): return
	if mediaFile == '':
		#Manual Instructions
		ATE2.Log_Step('Manual Instruction [%s]' % (title))	
		Operator_Manual_Instruction(instr)
	else:
		ATE2.Log_Step('Video Instruction [%s]' % (title))	
		Operator_Video_Instruction(instr, mediaFile)
	
def Step_Password_Set():
	if not ATE2.Check_Test_Selected():
		return
	ATE2.Log_Step('Set LMU password')
	if ATE2.Config_Get('CRAF', 'Password Func') != '':
		Password_Set()
	else:
		ATE2.Log('Password not configured', logToDetails=True)

def Step_PLX_Log_File():
	if not ATE2.Check_Test_Selected(): return
	fws_name = ATE2.Workstation_Name_Get()
	if ('PLX' in fws_name) or ('CLD' in fws_name):
		ATE2.Log_Step('Send Plexus Log File')
		PLX_Log_File()
		
def Step_Power_12v(channel=5, wait=1):
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Power-on device [12 volts]')
	ATE2.Power_Turn_Off()
	ATE2.Power_Set_Voltage_12v()
	ATE2.Power_Turn_On(channel, wait)

def Step_Power_4_5v(channel=5, wait=1):
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Power-on device [4.5 volts]')
	ATE2.Power_Turn_Off()
	ATE2.Power_Set_Voltage_4_5v()
	ATE2.Power_Turn_On(channel, wait)

def Step_Power_3_6_v(channel=5, wait=1):
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Power-on device [3.6 volts]')
	ATE2.Power_Turn_Off()
	ATE2.Power_Set_Voltage_3_6v()
	ATE2.Power_Turn_On(channel, wait)
	
def Step_Power_3_1_v(channel=5, wait=1, powerOff=True):
	# if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Power-on device [3.1 volts]')
	if powerOff:
		ATE2.Power_Turn_Off()
	ATE2.Power_Set_Voltage_3_1v()
	ATE2.Power_Turn_On(channel, wait)
	
def Step_Power_Off():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Power-off device')
	ATE2.Power_Turn_Off()
	
def Step_Radio_Com_Off():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Set Radio Off')
	Radio_Com_Off()

def Step_Radio_Com_32_Bit_Off():
	# Only need to turn on off com on 4.2m or lower on 32 bit.
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Set Radio Off')
	craf_FW = ATE2.Config_Get('CRAF', 'LMU FW Ver')
	if craf_FW > '4.2n' : return
	Radio_Com_Off()

def Step_Relay_Clear(relayNum):
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Clear relay [%d]' % (relayNum))
	ATE2.Relay_Clear(relayNum)
	
def Step_Relay_Set(relayNum):
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Set relay [%d]' % (relayNum))
	ATE2.Relay_Set(relayNum)

def Step_Relays_Clear_All():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Clear all the relays')
	ATE2.Relays_Clear_All()

def Step_Set_Peg_Action_39_0():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_SubStep('Set Peg Action 39 0')
	Set_Peg_Action_39_0()
	
def Step_Serial_Get_ATE_Port():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Get ATE Port')
	ATE2.Serial_Get_ATE_Port()
	
def Step_Serial_Get_Multimeter_Port():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Get Multimeter Port')
	ATE2.Serial_Get_Multimeter_Port()	

def Step_Test_Config_Set():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Step('Set device to Test configuration')
	Test_Config_Set()

def STM8STAT_Test():
	outBuf = ATE2.Serial_Exec_Cmd('AT#STM8STAT 1', 'OK', initDelayRead=1)
	for line in outBuf:
		if re.search(':', line):
			ATE2.Log('STM8 STAT line: %s' %(line))
			Logger.debug('STM8 STAT line: %s' %(line))
			ATE2.Mfg_Add_Step_Detail(property='STM8 STAT', value=line)
	
def Tamper_Switch_Test():
	ATE2.Operator_Instruction('Make sure Tamper Switch in Off position')
	line = ATE2.Serial_Exec_Cmd('AT#INP?', 'INP7', returnMatch=True)
	val = int(re.search('.*:\s*(\d*)', line).group(1))
	if val != 1:
		ATE2.Step_Fail('Tamper Switch not in off position (INP7 = 1)', errorCode='LMU_TAMPER_SWITCH_NOT_OFF_POS')
	ATE2.Operator_Instruction('Move the Tamper Switch to On position')
	line = ATE2.Serial_Exec_Cmd('AT#INP?', 'INP7', returnMatch=True)
	val = int(re.search('.*:\s*(\d*)', line).group(1))
	if val != 0:
		ATE2.Step_Fail('Tamper Switch did not change state (INP7 = 0)', errorCode='LMU_TAMPER_SWITCH_NO_STATE_CHANGE')
		
def Temperature_Alarm_Test():
	assy_tempAlarmInput = ATE2.Config_Get('ASSEMBLY', 'Temperature Alarm Input')
	if assy_tempAlarmInput.upper() != 'YES':
		ATE2.Log('Temperature Alarm not configured', logToDetails=True)
		return(1)
	val = int(re.search('.*:\s*(\w*)', ATE2.Serial_Exec_Cmd('AT#INP?', 'INP%s' % (assy_tempAlarmInput), returnMatch=True)).group(1))
	if val != 0:
		ATE2.Step_Fail('Temperature Alarm set [Input %d]' % (assy_tempAlarmInput), errorCode='LMU_TEMP_ALARM_SET')
	
def Test_Config_Set():
	ATE2.Serial_Exec_Cmd('AT#TESTCONFIG', 'OK', cmdRepeatCnt=5)
		
def TestID_Get():
	# ws_locCode = re.match('^(\d*)', ATE2.Config_Get('WORKSTATION', 'Location')).group(1)
	# ws_wsNum = ATE2.Config_Get('WORKSTATION', 'Number')
	# BKMB - until ini generated use ws config file from ATE1
	ws_locCode, ws_wsNum = ATE2.WorkStation_Config_Get()
	epochSecs = int(time.time())
	testId = '%02d%04d%0d' % (int(ws_locCode), int(ws_wsNum), int(epochSecs))
	Logger.debug('TestID = %s' % testId)
	ATE2.Log('Test ID: %s' % (testId))
	return testId
	
def Test_1BBs():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the 1-Bit Bus Circuits')
	assy_1bb = ATE2.Config_Get('ASSEMBLY', 'One-Bit Bus')
	craf_FW = ATE2.Config_Get('CRAF','LMU FW Ver')
	
	if assy_1bb != 'No' and (int(re.match('^(\d*)\.', craf_FW).group(1)) >= 5):
		# Need to set ATS171=1 to turn on 1BB
		ATE2.Serial_Exec_Cmd('ATS171=1', 'OK')
		Device_Reset()
	if assy_1bb == 'No':
		ATE2.Log('1-Bit Bus not configured', logToDetails=True)
		return
	if re.search('^1BB$', assy_1bb):
		OneBitBus_Test()
		PULS_Add_Entry('#1BB Pass')
	if re.search('1BBT', assy_1bb):
		OneBitBusR_Test()
		PULS_Add_Entry('#1BB-T Pass')
	if re.search('1BBR', assy_1bb):
		OneBitBusT_Test()
		PULS_Add_Entry('#1BB-R Pass')
	if re.search('1BBS', assy_1bb):
		#Shared 1 Bit Bus
		OneBitBusS_Test()
		PULS_Add_Entry('#1BB Pass')
	if re.search('1BB171', assy_1bb):
		#Shared 1 Bit Bus
		setting=33
		OneBitBus171_Test(setting)
		PULS_Add_Entry('#1BB Pass')

def Test_ADCs():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the ADCs')
	rc = ADCs_Test()
	if rc is None: PULS_Add_Entry('#ADC Pass')

def Test_Audiovox_Alarm():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the Audiovox Alarm')
	assy_audiovox = ATE2.Config_Get('ASSEMBLY', 'Audiovox')
	if assy_audiovox == 'No':
		ATE2.Log('Audivox Alarm test is not required', logToDetails=True)
		return
	rc = Audiovox_Alarm()
	if rc is None: PULS_Add_Entry('#Audiovox Alarm Pass')	
	
def Test_Aux_All_Ports():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the Aux Ports')
	assy_aux = ATE2.Config_Get('ASSEMBLY', 'Aux Port')
	if (assy_aux == 'No') or (assy_aux == ''):
		ATE2.Log('Aux Port not configured', logToDetails=True)
		return
	for aux in assy_aux.split('\n'):
		try:
			aux_text, aux_port = aux.split()
		except:
			continue
		
		ATE2.Log_SubStep('Test %s' % (aux_text))
		Aux_All_Port_Test(aux_port)
		PULS_Add_Entry('#%s Pass' % (aux_text))
	
def Test_Aux_Ports():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the Aux Ports')
	assy_aux = ATE2.Config_Get('ASSEMBLY', 'Aux Port')
	if (assy_aux == 'No') or (assy_aux == ''):
		ATE2.Log('Aux Port not configured', logToDetails=True)
		return
	craf_fwVer = ATE2.Config_Get('CRAF','LMU FW Ver')
	if craf_fwVer >= '3.6b' :
		rc = Aux_Port_Test()
	else:
		rc = Aux_Port_Test_Old()
	if rc is None: PULS_Add_Entry('#AUX Pass')
	
def Test_Battery_Charger():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the Battery Charging')
	assy_bat = ATE2.Config_Get('ASSEMBLY', 'Battery')
	if assy_bat == 'No':
		ATE2.Log('Battery not configured', logToDetails=True)
		return
	model = ATE2.Config_Get('CRAF', 'Model')
	if model == 'TTU2840':
		Battery_Charger_Ext_Test()
	else:
		Battery_Charger_Test()
	PULS_Add_Entry('#BATTCHG Pass')
	
def Test_Battery_Internal():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the Internal Battery')
	assy_bat = ATE2.Config_Get('ASSEMBLY', 'Battery')
	if assy_bat == 'No':
		ATE2.Log('Battery not configured', logToDetails=True)
		return
	Battery_Internal_Test()
	PULS_Add_Entry('#BATTINT Pass')
	
def Test_Battery_Low_Voltage():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the Battery Low Voltage Indication')
	assy_bat = ATE2.Config_Get('ASSEMBLY', 'Battery')
	if assy_bat == 'No':
		ATE2.Log('Battery not configured', logToDetails=True)
		return
	Battery_Low_Voltage_Test()
	PULS_Add_Entry('#LOWBATTV Pass')

def Test_Boost():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the Boost')
	assy_boost = ATE2.Config_Get('ASSEMBLY', 'Boost')
	if assy_boost == 'No':
		ATE2.Log('Boost not configured')
		return
	Boost_Test()
	PULS_Add_Entry('#BOOST: Pass, 1')
	
def Test_Buzzer():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the Buzzer')
	assy_buzzer = ATE2.Config_Get('ASSEMBLY', 'Buzzer')
	if assy_buzzer == 'Yes':
		rc = Buzzer_Test()
		if rc is None: PULS_Add_Entry('#Buzzer Pass')
	else:
		ATE2.Log('Buzzer not configured', logToDetails=True)	

def Test_Radio_Operating_Mode():
	ATE2.Log_Test('Test the Radio Operating Mode')
	assy_radio = ATE2.Config_Get('ASSEMBLY', 'Radio Ver List')
	if 'EHS' in assy_radio:
		if Check_Radio_Operating_Mode(): PULS_Add_Entry('#RADIO OP Pass')
		else: ATE2.Step_Fail('Failure happened in the Test Radio Operating Mode function', errorCode='LMU_TEST_RADIO_OPERATING_MODE')
	else: ATE2.Log('Non EHS Radio in List', logToDetails=True)

def Test_Comm():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the Primary Radio')
	assy_radio = ATE2.Config_Get('ASSEMBLY', 'Radio')
	if assy_radio == 'No':
		ATE2.Log('Radio not configured', logToDetails=True)
		return
	Comm_Test()
	PULS_Add_Entry('#COMM Pass')
	
def Test_Customer_App_Download():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Download the Customer App')
	rc = Customer_App_Download()
	if rc is None: PULS_Add_Entry('#CUST Pass')
	
def Test_Device_Hibernate():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Hibernate the board')
	assy_bat = ATE2.Config_Get('ASSEMBLY', 'Battery')
	if assy_bat == 'No':
		ATE2.Log('Battery not configured', logToDetails=True)
		return
	rc = Device_Hibernate()
	if rc is None: PULS_Add_Entry('#HIBER Pass')

def Test_EHS5_SETTINGS():
	if not ATE2.Check_Test_Selected(): return
	assy_radio = ATE2.Config_Get('ASSEMBLY', 'Radio Ver List')
	if 'EHS5' in assy_radio:
		ATE2.Log_Test('EHS5 Settings')
		EHS5_SETTINGS()
	else:
		ATE2.Log('Radio is non EHS5', logToDetails=True)
	
def Test_End(exitScript=True):
	# ATE2 Test_End() will call _Test_End()
	ATE2.Test_End(exitScript)
	
def _Test_End():
	global TestEndCalled
	if TestEndCalled:
		return
	TestEndCalled = True
	# print ('In Test_End callback')
	SQLiteDB_Post_PF(ATE2.TestVals.ATEpass)
	
def Test_ESN_Request_From_PULS():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Request ESN from PULS')
	ESN_Request_From_PULS()
	LMU.SQLiteDB_Post_TestResults(ts='', name='ESN:', result='', tvalue=UUT.ESN, string='')
	PULS_Add_Entry('ESN: %s' % (UUT.ESN))
	PULS_Add_Entry('#ESN Pass')
	
def Test_Firmware_Check_Version():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Check the Firmware Version')
	Firmware_Download(versionCheckOnly=True)
	PULS_Add_Entry('#FW Pass')
	
def Test_Firmware_Check_Version_Coproc(versionCheckOnly=False):
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Check the Coprocessor Firmware Version')
	Coproc.Firmware_Download(versionCheckOnly=True)
	PULS_Add_Entry('#FW-COPROC Pass')
	
def Test_Firmware_Download(specialCoprocMode=False):
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Download the Firmware')
	if specialCoprocMode:
		# Function call to multiple downloads - Specific for TTU2830 at this point.
		rc = Firmware_Download_Coproc()
	else:
		rc = Firmware_Download(specialCoprocMode=specialCoprocMode)
	if rc is None: PULS_Add_Entry('#FW Pass')
	
def Test_Firmware_Download_Coproc(craf_fwVer='', versionCheckOnly=False, specialCoprocMode=False):
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Download the Coprocessor Firmware')
	rc = Coproc.Firmware_Download(craf_fwVer=craf_fwVer, versionCheckOnly=versionCheckOnly, specialCoprocMode=specialCoprocMode)
	if rc is None: PULS_Add_Entry('#FW-COPROC Pass')

def Test_Flash():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the Flash')
	Flash_Test()
	PULS_Add_Entry('#FLASH Pass')
	
def Test_GPS():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the GPS Receiver')
	assy_gps = ATE2.Config_Get('ASSEMBLY', 'GPS')
	if assy_gps == 'No':
		ATE2.Log('GPS Receiver not configured', logToDetails=True)
		return
	RF.GPS_Test()
	PULS_Add_Entry('#GPS Pass')

def Test_HW_REV():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Obtain HW Revision')
	HWREV_Test()

def Test_Imp_Detect():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the Impact Detector')
	assy_ImpDet = ATE2.Config_Get('ASSEMBLY', 'Impact Detector')
	if assy_ImpDet != 'Yes':
		ATE2.Log('Impact Detector not configured', logToDetails=True)
		return
	Microphone_Test()
	PULS_Add_Entry('#IMPDET Pass')
	
def Test_Info():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the Board Information')
	try:
		if UUT.ATI0['Radio'] > '': 
			pass
			# ATE2.Log_Info('ATIO information Available')
	except:
		Info_ATI0()
	Info_ATI1()
	Info_ATIP()
	# if UUT.IsActivated:
		# Info_ATIC()
	PULS_Add_Entry('#INFO Pass')

def Test_IO():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the I/O')
	assy_io = ATE2.Config_Get('ASSEMBLY', 'IO')
	if assy_io == 'No':
		ATE2.Log('I/O not configured', logToDetails=True)
		return
	IO_Test()
	PULS_Add_Entry('#IO Pass')

def Test_Old_IO():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the I/O')
	assy_io = ATE2.Config_Get('ASSEMBLY', 'IO')
	if assy_io == 'No':
		ATE2.Log('I/O not configured', logToDetails=True)
		return
	IO_Old_Test()
	PULS_Add_Entry('#IO Pass')
	
def Test_Label_Print(netwrk=False):
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Print the Labels')
	craf_label = ATE2.Config_Get('CRAF', 'Label Dev BTW')
	if (craf_label == 'No') | (craf_label == ''):
		ATE2.Log('Label printing not configured', logToDetails=True)
		return
	if netwrk:
		Label_Via_Network_Print()
	else:
		Label_Print()
	PULS_Add_Entry('#LABEL Pass')
	
def Test_LEDs():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the LEDs')
	assy_leds = ATE2.Config_Get('ASSEMBLY', 'LEDs')
	if assy_leds == 'No':
		ATE2.Log('LEDs not configured', logToDetails=True)
		return
	if re.search('BOARD', assy_leds.upper()):
		LEDs_Board_Test()
		PULS_Add_Entry('#LEDS-BOARD Pass')
	if re.search('OUTPUT', assy_leds.upper()):
		LEDs_Output_Test()
		PULS_Add_Entry('#LEDS-OUTPUT Pass')
	if re.search('TRI', assy_leds.upper()):
		LEDs_Tri_Test()
		PULS_Add_Entry('#LEDS-BOARD Pass')
	if re.search('ALT', assy_leds.upper()):
		LEDs_Alt_Test()
		PULS_Add_Entry('#LEDS-BOARD Pass')		
	if re.search('GREEN', assy_leds.upper()):
		LEDs_Single_Green()
		PULS_Add_Entry('#LEDS-BOARD Pass')
	if re.search('QUAD', assy_leds.upper()):
		LEDs_Quad_Test()
		PULS_Add_Entry('#LEDS-BOARD Pass')
	if re.search('REDGRN', assy_leds.upper()):
		LEDs_Red_Green_Test()
		PULS_Add_Entry('#LEDS-BOARD Pass')

def Test_LMU_Hosted_App_Download():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Download the LMU Hosted App')
	rec = ATE2.Config_Get('CRAF', 'LMU Hosted App Ver')
	if rec == '':
		ATE2.Log('LMU Hosted App not configured')
		return
	craf_appid, craf_ver, craf_file = rec.split()
	LMU_Hosted_App_Download()
	PULS_Add_Entry('#LMU-HA Pass')

def Test_Motion():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the Motion Detector')
	assy_motion = ATE2.Config_Get('ASSEMBLY', 'Motion Detector')
	if assy_motion == 'No':
		ATE2.Log('Motion Detector not configured', logToDetails=True)
		return
	Motion_Test()
	PULS_Add_Entry('#MOT Pass')
	
def Test_NVM():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the Non-Volatile Memory')
	NVM_Test()
	PULS_Add_Entry('#NVM Pass')

def Test_Network():
	if not ATE2.Check_Test_Selected(): return
	net_tst = ATE2.Config_Get('CRAF', 'Test Network')
	if (net_tst == 'No'):
		ATE2.Log('Network Test not configured', logToDetails=True)
		return
	ATE2.Log_Test('Do Network Test With Customer SIM')
	Network_Test()
	PULS_Add_Entry('#NETWRK Pass')
	
def Test_PCBA_Set():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Set the PCBA String')
	PCBA_Set()
	PULS_Add_Entry('#PCBA Pass')

def Test_Power_Monitor():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Power State')
	Power_Monitor_Test()
	PULS_Add_Entry('#PMON: Pass')
	
def Test_Power_State():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Power State')
	Power_State_Test()
	PULS_Add_Entry('#PWRSTATE: Pass')
	
def Test_Power_Read_Current():    
    if not ATE2.Check_Test_Selected(): return
    ATE2.Log_Test('Test the DUT Current')
    Power_Read_Current()
    PULS_Add_Entry('#Power_Current Pass')	

def Test_Real_Time_Clock():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the Real-Time Clock')
	assy_rtc = ATE2.Config_Get('ASSEMBLY', 'Real-Time Clock')
	if assy_rtc == 'No':
		ATE2.Log('Real-Time Clock not configured', logToDetails=True)
		return
	Real_Time_Clock_Test()
	PULS_Add_Entry('#RTC Pass')
	#ATE2.Log_Info('#RTC Pass')
	
def Test_Reed():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Reed Switch Test')
	assy_reed = ATE2.Config_Get('ASSEMBLY', 'Reed Switch')
	if assy_reed == 'No':
		ATE2.Log('Reed Switch not configured', logToDetails=True)
		return
	Reed_Switch_Test()
	PULS_Add_Entry('#REED: Pass, 0')

def Test_Reed_Current():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Reed Switch and Hibernate Current Test')
	assy_reed = ATE2.Config_Get('ASSEMBLY', 'Reed Switch Current')
	if assy_reed == 'No':
		ATE2.Log('Reed Switch and Current not configured', logToDetails=True)
		return
	Reed_Current_Test()
	PULS_Add_Entry('#REED CURRENT: Pass, 0')

def Test_Scan_Label():
	assy_barcodeRead = ATE2.Config_Get('ASSEMBLY', 'Read Barcode')
	if not ATE2.Check_Test_Selected() or (assy_barcodeRead == ''): return
	ATE2.Log_Test('Test the Barcodes on Label')
	if assy_barcodeRead == 'No':
		ATE2.Log('Scan Label not configured', logToDetails=True)
		return
	if Scan_Label_Test():
		ATE2.Step_Fail('Label Scanning Failed', errorCode='LMU_LABEL_SCAN_FAIL')
	PULS_Add_Entry('#Scan Label Pass')

def Test_SIM_Activation():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Perform Activation Actions')
	SIM_Activate()	

def Test_SIM_Ejector():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('SIM Ejector Test')
	SIM_Ejector_Test()
	PULS_Add_Entry('#SIM-EJECT')
	
def Test_Start():
	global Callbacks, Dir_ATE_DB, Logger, Path_ATE1_Results_DB, ResultsSQLiteDbConn, TestEndCalled, UUT
	TestEndCalled = False
	ATE2.Test_Start()
	# store UUT test data
	UUT = _UUT()
	UUT.Data['StartTime'] = time.strftime("%Y-%m-%d %H:%M:%S")
	UUT.Data['TestID'] = TestID_Get()
	UUT.Data['PCBA'] = ''
	UUT.Ident2 = ''
	ATE2.UUT.TestID = UUT.Data['TestID']
	# register callback if failure
	ATE2.Callbacks.Add(_Test_End)
	# sqlite
	ResultsSQLiteDbConn = sqlite3.connect(Path_ATE1_Results_DB)
	# Reset the relay
	ATE2.Relays_Clear_All()
	# get power supply type
	ATE2.Power_Get_Power_Supply_Type()
	
def Test_STM8_STAT():
	if not ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Obtain STM8 STATS')
	STM8STAT_Test()
	
def Test_Tamper_Switch():
	if not LMU.ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the Tamper Switch')
	rc = Tamper_Switch_Test()
	if rc is None: LMU.PULS_Add_Entry('#TAMPERSW Pass')

def Test_Temperature_Alarm():
	if not LMU.ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the Temperature Alarm')
	rc = Temperature_Alarm_Test()
	if rc is None: LMU.PULS_Add_Entry('#TEMPALARM Pass')

def Test_User_ESN():
	if not LMU.ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Request User ESN')
	User_ESN_Set()

def Test_WiFi():
	if not LMU.ATE2.Check_Test_Selected(): return
	ATE2.Log_Test('Test the WiFi')
	rc = WiFi_Test()
	if rc is None: LMU.PULS_Add_Entry('#WIFI Pass')
	
def User_ESN_Set():
	ATE2.Log_SubStep('Get User ESN')
	craf_userEsnFunc = ATE2.Config_Get('CRAF', 'USER ESN Func')
	# This is related to S-Register #145 in the LMU
	craf_userMobileIDtype = ATE2.Config_Get('CRAF', 'User Mobile-id-type')
	
	if (craf_userEsnFunc is None) or (craf_userEsnFunc ==''):
		ATE2.Log('User ESN not set')
		return
	if LMU.UUT.Data['PCBA'] == '':
		ATE2.Step_Fail('User_ESN_Set failed because PCBA not set', errorCode='LMU_USER_ESN_SET_NO_PCBA')
	func = getattr(Serialize, 'QT_User_ESN_' + craf_userEsnFunc)
	userESN = func() # run customer func
	ATE2.Log('User ESN [%s] PCBA [%s]' % (userESN, UUT.Data['PCBA'])) 
	ATE2.Log_SubStep('Write User ESN to board')
	ATE2.Serial_Exec_Cmd('AT#USERESN %s OVERRIDE' % (userESN), 'OK')
	userESNw = re.search('.*:\s*(.*)', ATE2.Serial_Exec_Cmd('AT#USERESN?', 'ESN:', returnMatch=True)).group(1)
	ATE2.Log('User ESN read back [%s]' % (userESNw))
	UUT.USERESN = userESNw
	PULS_Add_Entry('MOBILE-ID: %s %s' % (craf_userMobileIDtype, UUT.USERESN))
	PULS_Add_Entry('#USERESN Pass')

def WiFi_ssid_get():
	ws = ATE2.Workstation_Name_Get()
	wsLocation = ws.split("-")
	ssid_Info = wsLocation[0] + ' WiFi SSID'
	ssid =  ATE2.Config_Get('ENV', '%s' %(ssid_Info))
	if ssid == None:
		ssid_Info = 'DEF WiFi SSID'
		ssid =  ATE2.Config_Get('ENV', '%s' %(ssid_Info))
		if ssid == None:
			ATE2.Step_Fail('No WIFI SSID available in INI file', errorCode='LMU_WIFI_SSID')
	return ssid
	
def WiFi_Test():
	# Notes: The AT#WIFI cmd cannot be issued until the board is up.
	assy_wifi = ATE2.Config_Get('ASSEMBLY', 'WiFi')
	if assy_wifi.upper() != 'YES':
		ATE2.Log('WiFi not configured', logToDetails=True)
		return
	ssid = WiFi_ssid_get()

	wifi_min, wifi_max, wifi_units = ATE2.Config_Get('TEST DATA', 'WiFi').split()
	wifi_min = int(wifi_min); wifi_max = int(wifi_max)
	ATE2.Log('WiFi RSSI range [%d, %d]' % (wifi_min, wifi_max))
	# set the SSID
	ATE2.Serial_Exec_Cmd('AT#WIFI "%s"' % (ssid), 'OK')
	# loop until RSSI in range
	for retry in range(30):
		rssi_d, mac = re.search('.*SSID,([\d-]*),(\w*)', ATE2.Serial_Exec_Cmd('AT#WIFI?', 'SSID' , cmdRepeatCnt=50, returnMatch=True)).group(1,2)
		rssi = int(rssi_d)
		if (rssi >= wifi_min and rssi <= wifi_max):
			#Note: Should be able to use IMEI2 with an ATI1 to get this info.
			UUT.Data['WIFI_MAC'] = mac
			ATE2.Log_Info('WiFi passed with value [%d] RSSI' % (rssi), logToDetails=True)
			LMU.SQLiteDB_Post_TestResults(ts='', name='WiFi RSSI:', result='', tvalue=rssi, string='')
			PULS_Add_Entry('WiFi value [%d] RSSI in range [%d, %d]' % (rssi, wifi_min, wifi_max))
			return
		ATE2.Sleep(2)
	ATE2.Step_Fail('WiFi RSSI [%d] not in range [%d, %d]' % (rssi, wifi_min, wifi_max), errorCode='LMU_WIFI_RSSI_RANGE')

def Work_Order_Scan():
	workOrderNum = ATE2.Dialog_Input('Scan the Worker Order number', 'Work Order')
	ATE2.UI_Update('WorkOrder', workOrderNum)
	return workOrderNum
	
def Ymodem_File_Transfer(ymodemCmd, dir, file):
	ATE2.Log_SubStep('Use Ymodem to load data file')
	ATE2.Log('Downloading file [%s]' % (file))
	ATE2.Serial_Exec_Cmd(ymodemCmd, 'Waiting', repeatCnt=15, exitOnMatch=True)
	if dir != '':
		dir = dir + '\\'
	filePath = ATE2.PathFiles + dir + '\\' + file
	port = re.sub('COM', '', ATE2.ATEport)
	cmd = "C:\\CalAmpAte2\\bin\\Ymodem\\YmodemAppVS6 -sy -p%s -le %s %s" % (port, filePath, '')
	Logger.debug(cmd)
	rc = os.system(cmd)
	if rc != 0:
		ATE2.Step_Fail('Ymodem failed [%d] - see Detail Log' % (rc), errorCode='LMU_YMODEM_FAILED')	

