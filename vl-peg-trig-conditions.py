import json, logging, re
import DeviceVal
import TestCase
from TestCase import *
from Serial import *

Logger = logging.getLogger('DeviceVal_Logger.Test_Cases')

########################################
### PEG Triggers
###

class PEG_Trigger_Conditions:

	Name = 'PEG Triggers'

	TestList = [
		('indx_0_trig_inactive', 'Trigger inactive event'),
		('indx_1_trig_pwrup', 'Trigger device powered on'),
		('indx_2_trig_wakeup', 'Trigger device wakeup from sleep'),
		('indx_3_trig_pwrup_wakeup', 'Trigger device powered on or wakeup from sleep'),
		('indx_4_trig_input_hi', 'Trigger input has transitioned to high'),
		('indx_5_trig_input_lo', 'Trigger input has transitioned to low'),
		('indx_6_trig_input_word', ''),
		('indx_7_trig_speed_above', 'Trigger GPS reported speed equal or above threshold'),
		('indx_8_trig_speed_below', 'Trigger GPS reported speed below threshold'),
		('indx_9_trig_zone_entry', 'Trigger when geozone entry'),
		('indx_10_trig_zone_entry', 'Trigger when geozone exit'),
		('indx_11_trig_gps_acquired', 'Trigger when GPS acquired'),
		('indx_12_trig_gps_lost', 'Trigger when GPS lost'),
		('indx_13_trig_comm_acquired', 'Trigger when comm acquired'),
		('indx_14_trig_comm_lost', 'Trigger when comm lost'),
		('indx_15_trig_ign_on', 'Trigger when ignition on'),
		('indx_16_trig_ign_off', 'Trigger when ignition off'),
		('indx_17_trig_time_distance', 'Trigger when time-distance-heading condition met'),
	]

	TestList = [
		('indx_1_trig_pwrup', 'Trigger device power-up'),
	]

	@Test_Case
	@Debug
	def indx_0_cond_none():
		pass

	@Test_Case
	@Debug
	def indx_1_cond_ign_on():
		pass
	
	@Test_Case
	@Debug
	def indx_2_cond_ign_off():
		pass

	@Test_Case
	@Debug
	def indx_3_cond_moving():
		pass

	@Test_Case
	@Debug
	def indx_4_cond_not_moving():
		pass

	@Test_Case
	@Debug
	def indx_5_cond_comm_available():
		pass

	@Test_Case
	@Debug
	def indx_6_cond_comm_not_available():
		pass

	@Test_Case
	@Debug
	def indx_7_cond_gps_acquired():
		pass

	@Test_Case
	@Debug
	def indx_8_cond_gps_not_acquired():
		pass

	@Test_Case
	@Debug
	def indx_9_cond_input_high():
		pass

	@Test_Case
	@Debug
	def indx_10_cond_input_low():
		pass

	@Test_Case
	@Debug
	def indx_11_cond_timer_active():
		pass

	@Test_Case
	@Debug
	def indx_12_cond_timer_inactive():
		pass

	@Test_Case
	@Debug
	def indx_13_cond_td_active():
		pass

	@Test_Case
	@Debug
	def indx_14_cond_td_inactive():
		pass

	@Test_Case
	@Debug
	def indx_15_cond_dayofweek():
		pass

	@Test_Case
	@Debug
	def indx_16_cond_flag_set():
		pass

	@Test_Case
	@Debug
	def indx_17_cond_flag_clear():
		pass

	@Test_Case
	@Debug
	def indx_18_cond_log_active():
		pass

	@Test_Case
	@Debug
	def indx_19_cond_comm_gps():
		pass

	@Test_Case
	@Debug
	def indx_20_cond_env():
		pass

	@Test_Case
	@Debug
	def indx_21_cond_env_equ():
		pass

	@Test_Case
	@Debug
	def indx_22_trig_host_data():
		pass

	@Test_Case
	@Debug
	def indx_23_trig_log_snd_fail():
		pass

	@Test_Case
	@Debug
	def indx_24_trig_wakeup_mon():
		pass

	@Test_Case
	@Debug
	def indx_25_trig_wakeup_tmr():
		pass

	@Test_Case
	@Debug
	def indx_26_trig_msg_send():
		pass

	@Test_Case
	@Debug
	def indx_27_trig_msg_ack():
		pass

	@Test_Case
	@Debug
	def indx_27_trig_msg_ack():
		pass

	@Test_Case
	@Debug
	def indx_28_trig_msg_sndfail():
		pass

	@Test_Case
	@Debug
	def indx_29_trig_log_rpt_complete():
		pass

	@Test_Case
	@Debug
	def indx_30_trig_comm_shutdown():
		pass

	@Test_Case
	@Debug
	def indx_31_trig_input_transition():
		pass

	@Test_Case
	@Debug
	def indx_32_trig_anyzone_entry():
		pass

	@Test_Case
	@Debug
	def indx_33_trig_anyzone_exit():
		pass

	@Test_Case
	@Debug
	def indx_34_trig_time():
		pass

	@Test_Case
	@Debug
	def indx_35_trig_distance():
		pass

	@Test_Case
	@Debug
	def indx_36_trig_log_active():
		pass

	@Test_Case
	@Debug
	def indx_37_trig_msg_rcvd():
		pass

	@Test_Case
	@Debug
	def indx_38_trig_msg_rcvd_any():
		pass

	@Test_Case
	@Debug
	def indx_39_trig_environ():
		pass

	@Test_Case
	@Debug
	def indx_40_trig_heading():
		pass

	@Test_Case
	@Debug
	def indx_41_trig_special():
		pass

	@Test_Case
	@Debug
	def indx_42_trig_moving():
		pass

	@Test_Case
	@Debug
	def indx_43_trig_not_moving():
		pass

	@Test_Case
	@Debug
	def indx_44_trig_adc_above():
		pass

	@Test_Case
	@Debug
	def indx_45_trig_adc_below():
		pass

	@Test_Case
	@Debug
	def indx_46_trig_comm_conn():
		pass

	@Test_Case
	@Debug
	def indx_47_trig_acc_below():
		pass

	@Test_Case
	@Debug
	def indx_48_trig_any_trig():
		pass

	@Test_Case
	@Debug
	def indx_49_trig_comm_state():
		pass

	@Test_Case
	@Debug
	def indx_50_trig_incoming_call():
		pass

	@Test_Case
	@Debug
	def indx_51_trig_call_state():
		pass

	@Test_Case
	@Debug
	def indx_52_trig_sms_request():
		pass

	@Test_Case
	@Debug
	def indx_53_trig_gzone_entry():
		pass

	@Test_Case
	@Debug
	def indx_54_trig_gzone_exit():
		pass

	@Test_Case
	@Debug
	def indx_55_trig_any_gzone_entry():
		pass

	@Test_Case
	@Debug
	def indx_56_trig_any_gzone_exit():
		pass

	@Test_Case
	@Debug
	def indx_57_trig_sms_null():
		pass

	@Test_Case
	@Debug
	def indx_58_trig_comm_network_id():
		pass

	@Test_Case
	@Debug
	def indx_59_trig_1bb_id():
		pass

	@Test_Case
	@Debug
	def indx_60_trig_update_begin():
		pass

	@Test_Case
	@Debug
	def indx_61_trig_update_end():
		pass

	@Test_Case
	@Debug
	def indx_62_trig_accel_detect():
		pass

	@Test_Case
	@Debug
	def indx_63_trig_msg_generate():
		pass

	@Test_Case
	@Debug
	def indx_64_trig_gps_qual_fix():
		pass

	@Test_Case
	@Debug
	def indx_65_trig_gps_qual_lost():
		pass

	@Test_Case
	@Debug
	def indx_66_trig_accelerometer():
		pass

	@Test_Case
	@Debug
	def indx_67_trig_vbus_state():
		pass

	@Test_Case
	@Debug
	def indx_68_trig_motlog_wrap():
		pass

	@Test_Case
	@Debug
	def indx_69_trig_vbus_dtc_change():
		pass

	@Test_Case
	@Debug
	def indx_70_trig_radio_jam():
		pass

	@Test_Case
	@Debug
	def indx_71_trig_accel_event():
		pass

	@Test_Case
	@Debug
	def indx_72_trig_accel_align_state():
		pass

	@Test_Case
	@Debug
	def indx_73_trig_accel_vbus_event():
		pass

	@Test_Case
	@Debug
	def indx_74_trig_acc_sched():
		pass

	@Test_Case
	@Debug
	def indx_75_trig_window_sched():
		pass

	@Test_Case
	@Debug
	def indx_76_trig_tow_alert():
		pass

	@Test_Case
	@Debug
	def indx_77_trig_ack_rcvd():
		pass

	@Test_Case
	@Debug
	def indx_78_trig_hosted_app():
		pass

	@Test_Case
	@Debug
	def indx_79_trig_tilt_on():
		pass

	@Test_Case
	@Debug
	def indx_80_trig_tilt_off():
		pass

	@Test_Case
	@Debug
	def indx_81_trig_accel_evt_start():
		pass

	@Test_Case
	@Debug
	def indx_82_trig_audio_event():
		pass

	@Test_Case
	@Debug
	def indx_83_trig_smsg_detect():
		pass

	@Test_Case
	@Debug
	def indx_84_trig_gps_jam():
		pass

	@Test_Case
	@Debug
	def indx_85_trig_mdt():
		pass

	@Test_Case
	@Debug
	def indx_86_trig_ble_fob():
		pass

	@Test_Case
	@Debug
	def indx_87_trig_dun_det():
		pass

	@Test_Case
	@Debug
	def indx_88_trig_stream():
		pass

	@Test_Case
	@Debug
	def indx_89_trig_syslog():
		pass

	@Test_Case
	@Debug
	def indx_90_trig_upload():
		pass

	@Test_Case
	@Debug
	def indx_91_trig_wakeup_mon_src():
		pass

	@Test_Case
	@Debug
	def indx_92_trig_bt_stat():
		pass

	@Test_Case
	@Debug
	def indx_93_trig_batt():
		pass

	@Test_Case
	@Debug
	def indx_94_matrix_event():
		pass

	@Test_Case
	@Debug
	def indx_95_rad_silence():
		pass


Run_Tests(PEG_Triggers)

	
# #
# ### Trigger
# ATE2.Log_Step('Peg Action Tests')
# ATE2.Serial_Exec_Cmd('ats125=7', 'OK', port='COM15')
# buf = ATE2.Serial_Exec_Cmd('at$app peg action 20 1', 'GPS Quality Fix', port='COM15', cmdRepeatCnt=1, repeatCnt=60)
# ATE2.Serial_Buffer_Match(buf, ['Trig=<GPS Lost>', 'Trig=<GPS Quality Lost>', 'Trig=<GPS Acquire>', 'Trig=<GPS Quality Fix>'])

# buf = ATE2.Serial_Exec_Cmd('at$app peg action 55 1', 'Comm Acquire', port='COM15', cmdRepeatCnt=1, repeatCnt=60)
# if buf != None:
	# ATE2.Serial_Buffer_Match(buf, ['Trig=<Comm Shutdown>', 'Trig=<Comm Lost>', 'Trig=<Comm State>', 'Trig=<Comm Connected>', 'Trig=<Comm Acquire>'])


# ATE2.Log_Step('Trigger Tests')
# for indx in (1, 4, 7):
	# ATE2.Serial_Exec_Cmd('ats158=254', 'OK', port='COM15')	# set bias high
	# ATE2.Serial_Exec_Cmd('stm_cli -o 255 255', 'Sending message opcode', port='COM61');
	# ATE2.Sleep(1)
	# ATE2.Serial_Exec_Cmd('stm_cli -o 255 0', 'Sending message opcode', port='COM61');
	# line = ATE2.Serial_Exec_Cmd('', 'Trig=<Input Transition>  Indx=%d' % (indx), port='COM15', cmdRepeatCnt=1, repeatCnt=10, initDelayRead=0, repeatDelay=.1, returnMatch=True)
	# if line: ATE2.Log_Info('[PASS] %s' % line)
	
# for indx in (1, 4, 7):
	# ATE2.Serial_Exec_Cmd('ats158=254', 'OK', port='COM15')	# set bias high
	# ATE2.Serial_Exec_Cmd('stm_cli -o 255 255', 'Sending message opcode', port='COM61');
	# ATE2.Sleep(1)
	# ATE2.Serial_Exec_Cmd('stm_cli -o 255 0', 'Sending message opcode', port='COM61');
	# line = ATE2.Serial_Exec_Cmd('', 'Trig=<Input Low>  Indx=%d' % (indx), port='COM15', cmdRepeatCnt=1, repeatCnt=10, initDelayRead=0, repeatDelay=.1, returnMatch=True)
	# if line: ATE2.Log_Info('[PASS] %s' % line)
	
# LMU.Test_End()
	
	# #
# ### Peg 

# ATE2.Log_Step('PEG Trigger Tests')

# ATE2.Log_Info('[PASS] %s' % ATE2.Serial_Exec_Cmd('at$app peg trig 1 0', 'Power-Up', port='COM15', returnMatch=True))

# ATE2.Log_Info('[PASS] %s' % ATE2.Serial_Exec_Cmd('at$app peg trig 2 0', 'Wakeup', port='COM15', returnMatch=True))

# ATE2.Log_Info('[PASS] %s' % ATE2.Serial_Exec_Cmd('at$app peg trig 3 0', 'PwrUp/WakeUp', port='COM15', returnMatch=True))

# ATE2.Log_Info('[PASS] %s' % ATE2.Serial_Exec_Cmd('at$app peg trig 4 0', 'Input High', port='COM15', returnMatch=True))

# ATE2.Log_Info('[PASS] %s' % ATE2.Serial_Exec_Cmd('at$app peg trig 5 0', 'Input Low', port='COM15', returnMatch=True))

# ATE2.Log_Info('[PASS] %s' % ATE2.Serial_Exec_Cmd('at$app peg trig 6 0', 'Input Equate', port='COM15', returnMatch=True))

# ATE2.Log_Info('[PASS] %s' % ATE2.Serial_Exec_Cmd('at$app peg trig 7 0', 'Speed Above', port='COM15', returnMatch=True))

# ATE2.Log_Info('[PASS] %s' % ATE2.Serial_Exec_Cmd('at$app peg trig 8 0', 'Speed Below', port='COM15', returnMatch=True))

# ATE2.Log_Info('[PASS] %s' % ATE2.Serial_Exec_Cmd('at$app peg trig 9 0', 'Zone Entry', port='COM15', returnMatch=True))

# ATE2.Log_Info('[PASS] %s' % ATE2.Serial_Exec_Cmd('at$app peg trig 10 0', 'Zone Exit', port='COM15', returnMatch=True))

# ATE2.Log_Info('[PASS] %s' % ATE2.Serial_Exec_Cmd('at$app peg trig 11 0', 'GPS Acquire', port='COM15', returnMatch=True));


# LMU.Test_End()





# ATE2.Serial_Exec_Cmd('at$app peg trig 11 0', 'GPS Acquire', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 12 0', 'GPS Lost', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 13 0', 'Comm Acquire', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 14 0', 'Comm Lost', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 15 0', 'Ignition On', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 16 0', 'Ignition Off', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 17 0', 'Time-Distance', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 18 0', 'Timer Timeout', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 19 0', 'Count Exceeded', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 20 0', 'Time-of-Day', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 21 0', 'Log Buf Full', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 22 0', 'Host Data', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 23 0', 'Log Send Fail', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 24 0', 'Wakeup-I/O', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 25 0', 'Wakeup-Timer', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 26 0', 'Msg Send', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 27 0', 'Msg Ack', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 28 0', 'Msg SndFail', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 29 0', 'Log Report Completed', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 30 0', 'Comm Shutdown', port='COM15');


# ATE2.Serial_Exec_Cmd('at$app peg trig 31 0', 'Input Transition', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 32 0', 'AnyZone Entry', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 33 0', 'AnyZone Exit', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 34 0', 'Time', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 35 0', 'Distance', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 36 0', 'Log Active', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 37 0', 'Msg Received', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 38 0', 'Any Msg Rcvd', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 39 0', 'Environ Chng', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 40 0', 'Heading', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 41 0', 'Special', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 42 0', 'Moving', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 43 0', 'Not Moving', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 44 0', 'A/D Above', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 45 0', 'A/D Below', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 46 0', 'Comm Connected', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 47 0', 'Accum Below', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 48 0', 'Any Trig', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 49 0', 'Comm State', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 50 0', 'Incoming Call', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 51 0', 'Call State', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 52 0', 'SMS Request', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 53 0', 'GeoZone Entry', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 54 0', 'GeoZone Exit', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 55 0', 'Any GeoZone Entry', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 56 0', 'Any GeoZone Exit', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 57 0', 'SMS Null', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 58 0', 'Comm Network ID', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 59 0', 'ID Read', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 60 0', 'Update Begin', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 61 0', 'Update End', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 62 0', 'Acceleration', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 63 0', 'Msg Generate', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 64 0', 'GPS Quality Fix', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 65 0', 'GPS Quality Lost', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 66 0', 'Accelerometer', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 67 0', 'VBus State', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 68 0', 'MotLog Wrap', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 69 0', 'VBus DTC Change', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 70 0', 'Radio Jamming', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 71 0', 'Accel Event', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 72 0', 'Accel Align State', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 73 0', 'VBus Event', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 74 0', 'Acc Schedule', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 75 0', 'Window Schedule', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 76 0', 'Tow Alert', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 77 0', 'Ack Rcvd', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 78 0', 'Hosted App', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 79 0', 'Tilt On', port='COM15');


# ATE2.Serial_Exec_Cmd('at$app peg trig 80 0', 'Tilt Off', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 81 0', 'Accel Event Start', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 82 0', 'Audio Event', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 83 0', 'Serial Msg Detect', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 84 0', 'GPS Jamming', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 85 0', 'MDT', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 86 0', 'BLE Fob', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 87 0', 'DUN Connect', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 88 0', 'Wakeup-Stream', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 89 0', 'Syslog', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 90 0', 'Upload', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 91 0', 'Wakeup-Mon-Source', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 92 0', 'BLE Status', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 93 0', 'Battery Status', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 94 0', 'Matrix Event', port='COM15');

# ATE2.Serial_Exec_Cmd('at$app peg trig 95 0', 'Radio Silence Mode', port='COM15');



# LMU.Test_End()

