import base64, configparser, datetime, json, logging
import psycopg2, os, re, sys, time, uuid, urllib3, visa
import TestCase
import pytz

from dateutil.parser import parse
from Serial import *
from TestCase import *
from ATE2 import *

# from twilio.rest import TwilioRestClient
from sinchsms import SinchSMS
 #import pytz

CfgParser = None
Logger = LoggerUI = None

DIR_Logs = '.'
DIR_Bin = '.'

def Calc_TOD_With_Offset(offset):
	hh, mm, ss = (int(x) for x in re.search('^Loc\s*([\d:]*)', Serial_Cmd('AT$APP TIME?', '^Loc')).group(1).split(':'))
	tod = (hh*3600) + (mm*60) + ss + offset
	return tod

def Check_Connected(conn=True):
	connState = 'Yes' if conn else 'No'
	Serial_Cmd('ATIC', 'Connection\s*:\s*%s' % (connState), errCode= 'INFO_ATIC_CONN', cmdRetryCnt=80, cmdRetryDelay=3, rdRetryCnt=1)

def Check_GPS_Connected(conn=True):
	connState = 'On' if conn else 'Off'
	Serial_Cmd('ATIG', 'Power\s*:\s*%s' % (connState),  errCode= 'INFO_ATIG_CONN', cmdRetryCnt=40, cmdRetryDelay=3, rdRetryCnt=1, filter='*')
	
def  Check_Device_Booted():
	Serial_Cmd('', 'VERSION', rdRetryCnt=30, filter='*')

def Check_ID_Report_Time():
	Sleep(3)
	retryCnt = 3
	for retry in range(2, retryCnt+2):
		jsonObj = DeviceVal.PULS_Device_Info()
		rptTime = re.search('(.*)\-.*', jsonObj['lastIdReportTime']).group(1)
		
		tsNow = time.time()
		dt = parse(rptTime)
		tsRpt = time.mktime(dt.timetuple())
		dur = tsNow - tsRpt
		Logger.debug('%s - ID Report time diff = %.2f' % (rptTime, dur))
		if dur < 20:
			return
		Logger.debug('Retry [%d]' % (retry))
		time.sleep(2)
	TC_Fail('ID Report time too old [%.2f]' % (dur), 'PULS_IDRPTTIME')
		
def Factory():
	radio = Serial_Cmd('ATI2', 'CDMA', retMatch=True, noFail=True, filter='.*')
	print('!!!!!!!')
	print(radio)
	print('!!!!!!!')
	Serial_Cmd('AT#FACTORY', '')
	Serial_Cmd('AT', 'OK', cmdRetryCnt=60)
	#Sleep(10)
	if not radio:
		while(True):
			buf = Serial_Cmd('ATIC', 'GPRS APN',  rdRetryCnt=15,cmdRetryDelay=5, retBuf=True, noFail=False, filter='.*')
			if re.search('broadband', buf[17],re.IGNORECASE):
				break
			else:
				Serial_Cmd('AT$app param 2306,0,"broadband"', 'OK', cmdRetryCnt=1, rdRetryCnt=60, rdRetryDelay=2 , retMatch = True)
				Serial_Cmd('AT$app param 2306,1,"broadband"', 'OK', cmdRetryCnt=1, rdRetryCnt=60, rdRetryDelay=2 , retMatch = True)
				Serial_Cmd('AT$app param 2320,0,"testpuls.calamp.com"', 'OK', cmdRetryCnt=1, rdRetryCnt=60, rdRetryDelay=2 , retMatch = True)
				Serial_Cmd('AT$app param 2319,0,"gpstrax.gieselman.com"', 'OK', cmdRetryCnt=1, rdRetryCnt=60, rdRetryDelay=2 , retMatch = True)
			Reset()
			Sleep(15)
	else:
		Serial_Cmd('AT$app param 2320,0,"testpuls.calamp.com"', 'OK', cmdRetryCnt=1, rdRetryCnt=60, rdRetryDelay=2 , retMatch = True)
		Serial_Cmd('AT$app param 2319,0,"gpstrax.gieselman.com"', 'OK', cmdRetryCnt=1, rdRetryCnt=60, rdRetryDelay=2 , retMatch = True)
		try:
			Serial_Cmd('AT#SEC 0', 'OK',noFail=True)
		except:
			pass
		Check_Connected()
		

def DT_Duration(dtStart, dtEnd):
	diff = dtEnd - dtStart
	dur = round(diff.seconds + (diff.microseconds/1000000), 2)
	return dur
	
def Get_Config_Value(section, field, defZero=False):
	try:
		val = CfgParser.get(section, field)
		return val
	except configparser.NoOptionError:
		if defZero:
			return '0'
		else:
			return ''

def Get_GMT_Time():
	dt = datetime.datetime.now(tz=pytz.utc)
	# print(dt)
	# http://www.timeapi.org/utc/now.json

def Get_Phone_Number():
	#Sleep(30)
	#Reset()
	while(True):
		radioTech = Serial_Cmd('ATI1', 'ICC-ID:',cmdRetryCnt=15)
		radioTech = re.search('.*:\d*(.*)', radioTech).group(1)
		if len(radioTech) is 0:
			Sleep(5)
			continue
		elif '8901260882212758194' in radioTech:
			phoneNum = '18122621120'
			break
		elif '8901260882212758160' in radioTech:
			phoneNum = '18122621125'
			break
		else:
			phoneNum = re.search('.*:\s*(\d*)', Serial_Cmd('ATIC', 'Phone Number.*\d+', cmdRetryCnt=30, filter='*')).group(1)
			break

	#phoneNum = '18122621120'
	return phoneNum
	
def Get_UUID():
	uid = uuid.uuid1()
	urn = re.search('^.*:(.*)', uid.urn).group(1)
	return urn  # uniform resource name

def Get_Workstation_Name():
	ws_name = os.environ['COMPUTERNAME']
	return ws_name.upper()
			
def Init():
	Init_Cfg_Parser()
	Init_Logger()
	Get_Debug_Port()

def Init_Cfg_Parser():
	global CfgParser
	cfgPath = sys.argv[1]
	CfgParser = configparser.ConfigParser()
	CfgParser.read(cfgPath)
	
def Init_Logger():
	global Logger, LoggerUI
	fileDump = 'C:\\Users\\labuser\\Desktop\\Tests-1101-2000\\Logs'#
	if not os.path.exists(fileDump):#
		os.makedirs(fileDump)#
	if(os.getcwd() != fileDump):#
		os.chdir('Logs')#
	assy = CfgParser.get('RUN', 'Assembly')
	dt = datetime.datetime.now().date()
	dtStr = re.sub('-', '', str(dt))
	# setup script logging
	ch = logging.StreamHandler()
	ch_fmt = logging.Formatter('%(asctime)s : %(message)s', '%m-%d %H:%M:%S')
	ch.setFormatter(ch_fmt)
	logging.basicConfig(filename='%s/%s-%s-script' % (DIR_Logs, assy, dtStr), level=logging.DEBUG, format='%(asctime)s : %(message)s')
	Logger = logging.getLogger('DeviceVal_Logger')
	if Logger.handlers:
		for handler in Logger.handlers:
			Logger.removeHandler(handler)
	Logger.addHandler(ch)
	# setup ui logging
	logging.basicConfig(filename='%s/%s-%s-ui' % (DIR_Logs, assy, dtStr), level=logging.DEBUG, format='%(asctime)s : %(message)s')
	ch = logging.StreamHandler()
	ch_fmt = logging.Formatter('%(asctime)s : %(message)s', '%m-%d %H:%M:%S')
	ch.setFormatter(ch_fmt)
	LoggerUI = logging.getLogger('DeviceVal_LoggerUI')
	if LoggerUI.handlers:
		for handler in LoggerUI.handlers:
			LoggerUI.removeHandler(handler)
	LoggerUI.addHandler(ch)

def Load_APN_List():
	craf_apnList = Get_Config_Value('CONFIG', 'APN List')
	Ymodem_File_Transfer('AT$APP FDNLDSER', '.', craf_apnList)
	Serial_Cmd('AT$APP FPROG 0 15', '^OK', rdRetryCnt=5) #Note - AT command at$app fprog requires LMU F/W 3.3a and higher) 

def Operator_Instruction(instr, noFail=False):
	rc = os.system('ipy %s \"%s\" \"%s\"' % (DIR_Bin + '/Dlg_General.py', 'Operator Instruction', instr))
	if not rc and not noFail:
		TC_Fail('Operator failed step [%s]' % (instr), errCode='OPER_FAIL_INSTR')
	return (rc)
	
def Pg_Connect():
		connPg = psycopg2.connect("dbname='DevVal' user='postgres' host='dev-ate-01' password='P05tgR3$dev'")
		return connPg

def Pg_Insert(query):
	return
	
	
	connPg = Pg_Connect()
	pgCur = connPg.cursor()
	pgCur.execute(query)
	pgCur.close()
	connPg.commit()
	
def Power_Reset(channel=5):
	Power_Off(channel)
	Power_On(channel)
	
def Power_Off(channel=5):
	Logger.debug('++ Power turned off')
	try:
		ins = visa.instrument('GPIB0::%d' % channel)
		ins.write('OUTPUT OFF;DISPLAY ON')
	except:
		pass
	time.sleep(1)
  
def Power_On(channel=5, wait=1):
	Logger.debug('++ Power turned on')
	try:
		ins = visa.instrument('GPIB0::%d' % channel)
		ins.write('OUTPUT ON;DISPLAY ON')
		# set local mode by clearing remote enable line
		session = ins.session
		rm = visa.ResourceManager()
		lib = rm.visalib
		lib.gpib_control_ren(session,0)
		time.sleep(wait)
	except:
		pass

def PULS_Device_Info(esn=''):
	if esn == '':
		esn = re.search('.*:\s*(.*)', Serial_Cmd('AT#ESN?', 'ESN:')).group(1)
	cfg_pulsServer = 'testpuls.calamp.com'
	url = 'https://%s/service/device/%s' % (cfg_pulsServer, esn)
	resp = PULS_Query(url)
	print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
	print(url)
	return resp
	
def PULS_Firmware_Info(appid):
	url = 'https://%s/service/firmware?appId=%s' % (TestCase.cfg_pulsServer, appid)
	resp = PULS_Query(url)
	return resp
	
def PULS_Firmware_Set(esn, fwVer):
	url = 'https://%s/service/device/%s' % (TestCase.cfg_pulsServer, esn)
	body = """
	{
		"esn":%s, "mobileId":"%s", "firmwareVersion":"%s"
	}
	""" % (esn, esn, fwVer)
	resp = PULS_Query(url, method='POST', body=body)
	return resp
	
def PULS_OBD_FW_Set(esn,fwVer,customerApiKey):
	print(esn)
	print(fwVer)
	print('pre URL set START')
	url = 'https://%s/service/vbus/%s?apikey=9G5XWBo_%s' % (TestCase.cfg_pulsServer, esn, customerApiKey)
	#"vbusFileType":"%s", "versionNumber":"%s"	
	#url = 'https://%s/service/vbus/%s' % (TestCase.cfg_pulsServer, esn) 
	print('post URL set START')
	print(url)
	body = """
	{
	 "esn":"%s",
    "mobileId":"%s",
        "vbusDeviceFiles":[{"vbusFileType":"%s",
                        "versionNumber":"%s"}]
	}
	""" % (esn, esn, 1, fwVer) 
	print(fwVer)
	print('POST fwVer')
	resp = PULS_Query(url, method='POST', body=body)  #get->post
	print('POST PULS RESP QUERY')
	return resp
	
def PULS_Query(url, method='GET', body=''):
	urllib3.disable_warnings()
	pool = urllib3.PoolManager()
	headers = { 'Authorization':'Basic %s' % (base64.b64encode('mfgmdtapi:CalAmp123$'.encode())).decode(),
		'Content-Type':'application/json'
	}
	#headers = {}
	resp = pool.urlopen(method, url, headers=headers, body=body)
	data = resp.data.decode()
	print(str(resp.status))
	print(resp.data.decode())
	jsonObj = json.loads(data)
	msg = None
	try:
		msg = jsonObj['message']
		#ATE2.Log_Warning('PULS error [%s]' % (msg))
	except:
		pass
	if msg is None:
		return jsonObj
	else:
		return None

def Reset():
	Serial_Cmd('ATRESET', '')
	Serial_Cmd('AT', 'OK', cmdRetryCnt=60)
		
def Send_SMS(msg):
	# Your new Phone Number is +16195685767
	# sinch.com $0.005/sms outbound, free inbound
	#
	client = TwilioRestClient('ACde37fa8a01e05771688149b73ce0ab63', 'f1aed1ac73d40266074dd43325cf9651')
	ret = client.messages.create(to='+17604214948', from_='+16195685767', body=msg)
	print(ret)
	
def Send_SMS_Sinch(msg, phoneNum='', mRE='', fRE=''):
	# client = SinchSMS('e50ee8c6-8ed8-4fcd-b2c1-f33f395a32f4', 'bi9tr/1h20WW2Y5gbMVtBg==') # sandbox
	if phoneNum == '':
		phoneNum = DeviceVal.Get_Phone_Number()
	if mRE == '':
		mRE = '<SMS Request>'
	filt = 'SMS:'
	if fRE != '':
		filt = filt + '|' + fRE
	client = SinchSMS('85dae588-1db7-45cd-b565-fedda7df74ae', '3a9FMFWdbEufbSft7VbJog==')
	resp = client.send_message(phoneNum, msg)
	msgId = resp['messageId']
	Logger.debug('SMS Message ID = %s' % (msgId))
	resp = Serial_Cmd('', mRE, rdRetryCnt=40, retBuf=True, filter=filt)
	return resp
	
def Sleep(delay):
	Logger.debug('-- Sleep [%s] --' % (delay))
	time.sleep(delay)
	
def Validate_Info(cmd, retryCnt):
	# retry loop
	for retry in range(2, retryCnt+2):
		buf = Serial_Cmd(cmd, '^OK')
		# loop thru buffer
		emptyFlag = False
		for line in buf:
			if re.search(':$|:-*$', line):	# line not filled in
				emptyFlag = True
				if retry <= retryCnt:
					Logger.debug('Retry [%d]' % (retry))
					time.sleep(1)
				break
		if not emptyFlag: return
	TC_Fail('Empty Info field', errCode = 'INFO_FIELDEMPTY')

def Ymodem_File_Transfer(ymodemCmd, dir, file):
	global Debug_Port
	Logger.debug('Downloading file [%s]' % (file))
	Serial_Cmd(ymodemCmd, 'Waiting', rdRetryCnt=100, exitOnMatch=True, filter='*')
	if dir != '':
		dir = dir + '\\'
	filePath = ATE2.PathFiles + dir + '\\' + file
	port = re.sub('COM', '', Serial.Debug_Port)
	cmd = "C:\\CalAmpAte2\\bin\\Ymodem\\YmodemAppVS6 -sy -p%s -le %s %s" % (port, filePath, '')
	print(cmd)
	rc = os.system(cmd)
	if rc != 0:
		TC_Fail('Ymodem failed [%d] - see Detail Log' % (rc), errCode='LMU_YMODEM_FAILED')	

def PULS_Config_Set(esn, scriptVer, cfgVer):
	# make request to PULS to update config
	Logger.debug('Config update for script [%s, %s]' % (scriptVer, cfgVer))
	url = 'https://%s/service/device/%s' % (TestCase.cfg_pulsServer, esn)
	body = """
	{
		"esn":%s, "mobileId":"%s", "scriptVersion":%s, "configVersion":%s
	}
	""" % (esn, esn, scriptVer, cfgVer)
	resp = PULS_Query(url, method='POST', body=body)
	Sleep(10)
	Serial_Cmd('ats125=7', '^OK')
	Serial_Cmd('AT$APP PEG ACTION 49 129', '^OK')

def PULS_Config_Reset(esn):
	# make request to PULS to update config
	#Logger.debug('Config update for script [%s, %s]' % (scriptVer, cfgVer))
	url = 'https://%s/service/device/%s' % (TestCase.cfg_pulsServer, esn)
	body = """
	{
		"esn":%s, "mobileId":"%s", "configStatus":"CANCEL_REQUESTED"
	}
	""" % (esn, esn)
	resp = PULS_Query(url, method='POST', body=body)
	# if resp['configStatus'] != 'Pending':
		# Logger.debug('@@ Config status not Pending')
		# raise
	# send id report to start update
	Sleep(10)
	Serial_Cmd('ats125=7', '^OK')
	Serial_Cmd('AT$APP PEG ACTION 49 129', '^OK')

def File_Signature(esn, keyFile=False,auth=False):

	PULS_Config_Set(esn, str(20), str(2))
	try:
		buf = Serial_Cmd('', 'OTA: Released', rdRetryCnt=45,retBuf=True, filter='.*')
	except:
		TC_Fail('Bad signature fail')
	
	for line in buf:
		if 'Bad signature' in line:
			ATE2.Log('Bad Signature PASS')
			break;
		if line == buf[-1] and not 'Bad signature' in line:
			TC_Fail('Bad signature not found')
	PULS_Config_Reset(esn)
	Sleep(5)
	PULS_Config_Set(esn, str(20), str(3))
	try:
		if keyFile and auth:
			buf = Serial_Cmd('', 'OTA: Released', rdRetryCnt=45,retBuf=True, filter='.*')
		else:
			buf = Serial_Cmd('', 'PRM: Updating Config...', rdRetryCnt=45,retBuf=True, filter='.*')
	except:
		TC_Fail('File not signed fail')
		
	for line in buf:
		if 'File not signed' in line:
			ATE2.Log('File not signed PASS')
			break;
		if line == buf[-1] and not 'File not signed' in line:
			TC_Fail('file not signed not found')
	
	PULS_Config_Reset(esn)

	PULS_Config_Set(esn, str(20), str(1))
	try:
		buf = Serial_Cmd('', 'PRM: Updating Config...', rdRetryCnt=45,retBuf=True, filter='.*')
	except:
		TC_Fail('Signature Ok fail')
		
	for line in buf:
		if 'Signature OK' in line:
			ATE2.Log('Signature OK PASS')
			break;
		if line == buf[-1] and not 'Signature OK' in line:
			TC_Fail('Signature OK not found')
		
	Factory()
	Serial_Cmd('ats125=7', '^OK')
	Serial_Cmd('', 'DNS: #0:0 Success, 216.177.93.236', rdRetryCnt=45, filter='.*')



		
	
		
def Check_if_Sleeping():
	currStart = float(ATE2.Power_Read_Current())
	retries_cur = 10
	for retry_cur in range(retries_cur):
		currEnd = float(ATE2.Power_Read_Current())
		if currStart - currEnd <= 0.02:
			return
		else:
			time.sleep(5)
	TC_Fail('Device failed to sleep')

def ota_firmware_download():
		fW, appid = (DeviceVal.Get_Config_Value('RUN', 'Ident')).split('/')
		
		esn = re.search('.*:\s*(.*)', Serial_Cmd('AT#ESN?', 'ESN:')).group(1)
		
		jsonObj = DeviceVal.PULS_Device_Info(esn)
	
		dnldRec = jsonObj['firmwareHistorys'][0]
		print('\n\n' + str(dnldRec)  + '\n\n')
		fwNew = dnldRec['newFirmware']['versionNumber']
		fwOld = dnldRec['oldFirmware']['versionNumber']
		ATE2.Log('Updated %s from %s' % (fwNew, fwOld))
		jsonObj = DeviceVal.PULS_Firmware_Info(appid)
		print (fW)
	
		upgradeBuildTest = DeviceVal.Get_Config_Value('RUN', 'Firmware Upgrade List')
		print (upgradeBuildTest)
		
		upgrade = upgradeBuildTest.split(',')
		print (upgrade)
		
		
		
		upgradbuildcount = 0
		while(upgradbuildcount < 1):
			
			ATE2.Log('++ Iteratation %d' % (upgradbuildcount))
			Logger.debug('++ Iteratation %d' % (upgradbuildcount))
			for build in upgrade:
				print ('Requested build to upgrade is ',build)
				DeviceVal.PULS_Firmware_Set(esn, build)
				Serial_Cmd('at$app peg action 49 129', '^OK', cmdRetryCnt=60, cmdRetryDelay=3)
				
				DeviceVal.Sleep(300)
				Serial_Cmd('at$app peg action 49 129', '^OK', cmdRetryCnt=60, cmdRetryDelay=3,filter='.*')
				ati0build = build[0] + '.' + build[1: ]
				print (build, ati0build)
				jsonObj = DeviceVal.PULS_Device_Info(esn)
				devicecompleted = False
				for retry in range(84):
					jsonObj = DeviceVal.PULS_Device_Info(esn)
					status = jsonObj['firmwareHistorys'][0]['status']
					print (status)
							
					if status == 'Completed':
						devicecompleted = True
						break
					DeviceVal.Sleep(5)	
				if not devicecompleted:
					TC_Fail('Download failed','PULS_DNLD-FAIL')
				print ('upgradedone')
				Serial_Cmd('ati0', ati0build, cmdRetryCnt=200, cmdRetryDelay=3, filter='APP:')	
			
			
			
			DeviceVal.Sleep(60)
			Serial_Cmd('at$app peg action 49 129', '^OK', cmdRetryCnt=60, cmdRetryDelay=3)
			upgradbuildcount = upgradbuildcount + 1

def ota_firmware_download_overnight(esn, build):
	
	print ('Requested build to upgrade is ',build)
	DeviceVal.PULS_Firmware_Set(esn, build)
	Serial_Cmd('at$app peg action 49 129', '^OK', cmdRetryCnt=60, cmdRetryDelay=3)
	DeviceVal.Sleep(350)
	Serial_Cmd('at$app peg action 49 129', '^OK', cmdRetryCnt=60, cmdRetryDelay=3,filter='.*')
	jsonObj = DeviceVal.PULS_Device_Info(esn)
	devicecompleted = False
	for retry in range(2):
		jsonObj = DeviceVal.PULS_Device_Info(esn)
		status = jsonObj['firmwareHistorys'][0]['status']
		print (status)
		if status == 'Completed':
			devicecompleted = True
			DeviceVal.Sleep(5)
			print ('upgradedone')		

#takes the first date and the last date and returns the amount of days between the dates	
#format YYYY-MM-DD		
def daysDifference(firstDate, lastDate):
	jan, ja = 31, 1
	feb, fe = 28, 2
	march, mar = 31, 3
	april, ap = 30, 4
	may, ma = 31, 5
	june, jun = 30, 6
	july, ju = 31, 7
	aug, au = 31, 8
	sep, se = 30, 9 
	oct, oc = 31, 10
	nov, no = 30, 11
	dec, de = 31, 12
	
	fyear,fmonth,fdate = firstDate.split('-')
	lyear,lmonth,ldate = lastDate.split('-')
	
def paramtesting():
	errorList = [] 
	mismatchList = [] 
	os.chdir('Z:\\Users\\bFan\\Master\\Tests-1101-2000')
	print(os.getcwd())
	with open("paramtesting.txt") as f:
		for line in f:
			line = line.rstrip()
			param, slot, value = re.search ('^(\d*),(\d*),(.*)', line).group(1, 2, 0)
			buf = Serial_Cmd('AT$APP PARAM? %s,%s' % (param, slot), '^%s' % (param), rdRetryCnt=1, initDelay = 0.1, noFail=True)
			if param == '2691'and fw <= '5.0c':
				print('Bug with the parameter------skipping')
				ATE2.Log('Bug with parameter 2691 before version 5.0c. SKIPPING')
				continue
			if buf == '':
				print('no param - [%s] [%s]' % (buf, value))
				ATE2.Log('no param - [%s] [%s]' % (buf, value))
				errorList.append('Error [%s]' % (value))
				continue
			if buf == value:
				continue
			else:
				print('mismatch - [%s] [%s]' % (buf, value))
				ATE2.Log('mismatch - [%s] [%s]' % (buf, value))
				mismatchList.append('Actual [%s] Expected [%s]' % (buf,value))
	return mismatchList, errorList
	
def compareLists(x,y):
	#y is error list of previous
	for item in x:
		item = re.search('\[([^]]+)\]', item).group(0).strip('[]')
		if any(item in s for s in y):
			print(item)
			print('!!!!')
			break;
	
	
	
	
	
	


	
	