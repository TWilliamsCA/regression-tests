from Serial import *
from TestCase import *

class Subtests():

	Name = 'Trigger Tests'

	SubtestList = [
		('trig_1_0', 'Power-Up'),
		('trig_2_0', 'Wakeup'),
		('trig_3_0', 'PwrUp/WakeUp'),
		('trig_4_0', 'Input High'),
		('trig_5_0', 'Input Low'),
		('trig_6_0', 'Input Equate'),
		('trig_7_0', 'Speed Above'),
		('trig_8_0', 'Speed Below'),
		('trig_9_0', 'Zone Entry'),
		('trig_10_0', 'Zone Exit'),
		('trig_11_0', 'GPS Acquire'),
		('trig_12_0', 'GPS Lost'),
		('trig_13_0', 'Comm Acquire'),
		('trig_14_0', 'Comm Lost'),
		('trig_15_0', 'Ignition On'),
		('trig_16_0', 'Ignition Off'),
		('trig_17_0', 'Time-Distance'),
		('trig_18_0', 'Timer Timeout'),
		('trig_19_0', 'Count Exceeded'),
		('trig_20_0', 'Time-of-Day'),
		('trig_21_0', 'Log Buf Full'),
		('trig_22_0', 'Host Data'),
		('trig_23_0', 'Log Send Fail'),
		('trig_24_0', 'Wakeup-I/O'),
		('trig_25_0', 'Wakeup-Timer'),
		('trig_26_0', 'Msg Send'),
		('trig_27_0', 'Msg Ack'),
		('trig_28_0', 'Msg SndFail'),
		('trig_29_0', 'Log Report Completed'),
		('trig_30_0', 'Comm Shutdown'),
		('trig_31_0', 'Input Transition'),
		('trig_32_0', 'AnyZone Entry'),
		('trig_33_0', 'AnyZone Exit'),
		('trig_34_0', 'Time'),
		('trig_35_0', 'Distance'),
		('trig_36_0', 'Log Active'),
		('trig_37_0', 'Msg Received'),
		('trig_38_0', 'Any Msg Rcvd'),
		('trig_39_0', 'Environ Chng'),
		('trig_40_0', 'Heading'),
		('trig_41_0', 'Special'),
		('trig_42_0', 'Moving'),
		('trig_43_0', 'Not Moving'),
		('trig_44_0', 'A/D Above'),
		('trig_45_0', 'A/D Below'),
		('trig_46_0', 'Comm Connected'),
		('trig_47_0', 'Accum Below'),
		('trig_48_0', 'Any Trig'),
		('trig_49_0', 'Comm State'),
		('trig_50_0', 'Incoming Call'),
		('trig_51_0', 'Call State'),
		('trig_52_0', 'SMS Request'),
		('trig_53_0', 'GeoZone Entry'),
		('trig_54_0', 'GeoZone Exit'),
		('trig_55_0', 'Any GeoZone Entry'),
		('trig_56_0', 'Any GeoZone Exit'),
		('trig_57_0', 'SMS Null'),
		('trig_58_0', 'Comm Network ID'),
		('trig_59_0', 'ID Read'),
		('trig_60_0', 'Update Begin'),
		('trig_61_0', 'Update End'),
		('trig_62_0', 'Acceleration'),
		('trig_63_0', 'Msg Generate'),
		('trig_64_0', 'GPS Quality Fix'),
		('trig_65_0', 'GPS Quality Lost'),
		('trig_66_0', 'Accelerometer'),
		('trig_67_0', 'VBus State'),
		('trig_68_0', 'MotLog Wrap'),
		('trig_69_0', 'VBus DTC Change'),
		('trig_70_0', 'Radio Jamming'),
		('trig_71_0', 'Accel Event'),
		('trig_72_0', 'Accel Align State'),
		('trig_73_0', 'VBus Event'),
		('trig_74_0', 'Acc Schedule'),
		('trig_75_0', 'Window Schedule'),
		('trig_76_0', 'Tow Alert'),
		('trig_77_0', 'Ack Rcvd'),
		('trig_78_0', 'Hosted App'),
		('trig_79_0', 'Tilt On'),
		('trig_80_0', 'Tilt Off'),
		('trig_81_0', 'Accel Event Start'),
		('trig_82_0', 'Audio Event'),
		('trig_83_0', 'Serial Msg Detect'),
		('trig_84_0', 'GPS Jamming'),
		('trig_85_0', 'MDT'),
		('trig_86_0', 'BLE Fob'),
		('trig_87_0', 'DUN Connect'),
		('trig_88_0', 'Wakeup-Stream'),
		('trig_89_0', 'Sys Log'),
		('trig_90_0', 'Upload'),
		('trig_91_0', 'Wakeup Mon Source'),
		('trig_92_0', 'BLE Status'),
		('trig_93_0', 'Battery Status'),
		('trig_94_0', 'Matrix Event'),
		('trig_95_0', 'Radio Silence Mode'),
		('trig_20_1', 'GPS Quality Fix'),
		('trig_55_1', 'Comm Acquire'),
	]
	
	def trig_1_0():
		Serial_Cmd('AT$APP PEG TRIG 1 0', 'Power-Up')

	def trig_2_0():
		Serial_Cmd('AT$APP PEG TRIG 2 0', 'Wakeup')

	def trig_3_0():
		Serial_Cmd('AT$APP PEG TRIG 3 0', 'PwrUp/WakeUp')
		
	def trig_4_0():
		Serial_Cmd('AT$APP PEG TRIG 4 0', 'Input High')

	def trig_5_0():
		Serial_Cmd('AT$APP PEG TRIG 5 0', 'Input Low')

	def trig_6_0():
		Serial_Cmd('AT$APP PEG TRIG 6 0', 'Input Equate')

	def trig_7_0():
		Serial_Cmd('AT$APP PEG TRIG 7 0', 'Speed Above')

	def trig_8_0():
		Serial_Cmd('AT$APP PEG TRIG 8 0', 'Speed Below')

	def trig_9_0():
		Serial_Cmd('AT$APP PEG TRIG 9 0', 'Zone Entry')

	def trig_10_0():
		Serial_Cmd('AT$APP PEG TRIG 10 0', 'Zone Exit')

	def trig_11_0():
		Serial_Cmd('AT$APP PEG TRIG 11 0', 'GPS Acquire')

	def trig_12_0():
		Serial_Cmd('AT$APP PEG TRIG 12 0', 'GPS Lost')

	def trig_13_0():
		Serial_Cmd('AT$APP PEG TRIG 13 0', 'Comm Acquire')

	def trig_14_0():
		Serial_Cmd('AT$APP PEG TRIG 14 0', 'Comm Lost')

	def trig_15_0():
		Serial_Cmd('AT$APP PEG TRIG 15 0', 'Ignition On')

	def trig_16_0():
		Serial_Cmd('AT$APP PEG TRIG 16 0', 'Ignition Off')

	def trig_17_0():
		Serial_Cmd('AT$APP PEG TRIG 17 0', 'Time-Distance')

	def trig_18_0():
		Serial_Cmd('AT$APP PEG TRIG 18 0', 'Timer Timeout')

	def trig_19_0():
		Serial_Cmd('AT$APP PEG TRIG 19 0', 'Count Exceeded')

	def trig_20_0():
		Serial_Cmd('AT$APP PEG TRIG 20 0', 'Time-of-Day')

	def trig_21_0():
		Serial_Cmd('AT$APP PEG TRIG 21 0', 'Log Buf Full')

	def trig_22_0():
		Serial_Cmd('AT$APP PEG TRIG 22 0', 'Host Data')

	def trig_23_0():
		Serial_Cmd('AT$APP PEG TRIG 23 0', 'Log Send Fail')

	def trig_24_0():
		Serial_Cmd('AT$APP PEG TRIG 24 0', 'Wakeup-I/O')

	def trig_25_0():
		Serial_Cmd('AT$APP PEG TRIG 25 0', 'Wakeup-Timer')

	def trig_26_0():
		Serial_Cmd('AT$APP PEG TRIG 26 0', 'Msg Send')

	def trig_27_0():
		Serial_Cmd('AT$APP PEG TRIG 27 0', 'Msg Ack')

	def trig_28_0():
		Serial_Cmd('AT$APP PEG TRIG 28 0', 'Msg SndFail')

	def trig_29_0():
		Serial_Cmd('AT$APP PEG TRIG 29 0', 'Log Report Completed')

	def trig_30_0():
		Serial_Cmd('AT$APP PEG TRIG 30 0', 'Comm Shutdown')

	def trig_31_0():
		Serial_Cmd('AT$APP PEG TRIG 31 0', 'Input Transition')

	def trig_32_0():
		Serial_Cmd('AT$APP PEG TRIG 32 0', 'AnyZone Entry')

	def trig_33_0():
		Serial_Cmd('AT$APP PEG TRIG 33 0', 'AnyZone Exit')

	def trig_34_0():
		Serial_Cmd('AT$APP PEG TRIG 34 0', 'Time')

	def trig_35_0():
		Serial_Cmd('AT$APP PEG TRIG 35 0', 'Distance')

	def trig_36_0():
		Serial_Cmd('AT$APP PEG TRIG 36 0', 'Log Active')

	def trig_37_0():
		Serial_Cmd('AT$APP PEG TRIG 37 0', 'Msg Received')

	def trig_38_0():
		Serial_Cmd('AT$APP PEG TRIG 38 0', 'Any Msg Rcvd')

	def trig_39_0():
		Serial_Cmd('AT$APP PEG TRIG 39 0', 'Environ Chng')

	def trig_40_0():
		Serial_Cmd('AT$APP PEG TRIG 40 0', 'Heading')

	def trig_41_0():
		Serial_Cmd('AT$APP PEG TRIG 41 0', 'Special')

	def trig_42_0():
		Serial_Cmd('AT$APP PEG TRIG 42 0', 'Moving')

	def trig_43_0():
		Serial_Cmd('AT$APP PEG TRIG 43 0', 'Not Moving')

	def trig_44_0():
		Serial_Cmd('AT$APP PEG TRIG 44 0', 'A/D Above')

	def trig_45_0():
		Serial_Cmd('AT$APP PEG TRIG 45 0', 'A/D Below')

	def trig_46_0():
		Serial_Cmd('AT$APP PEG TRIG 46 0', 'Comm Connected')

	def trig_47_0():
		Serial_Cmd('AT$APP PEG TRIG 47 0', 'Accum Below')

	def trig_48_0():
		Serial_Cmd('AT$APP PEG TRIG 48 0', 'Any Trig')

	def trig_49_0():
		Serial_Cmd('AT$APP PEG TRIG 49 0', 'Comm State')

	def trig_50_0():
		Serial_Cmd('AT$APP PEG TRIG 50 0', 'Incoming Call')

	def trig_51_0():
		Serial_Cmd('AT$APP PEG TRIG 51 0', 'Call State')

	def trig_52_0():
		Serial_Cmd('AT$APP PEG TRIG 52 0', 'SMS Request')

	def trig_53_0():
		Serial_Cmd('AT$APP PEG TRIG 53 0', 'GeoZone Entry')

	def trig_54_0():
		Serial_Cmd('AT$APP PEG TRIG 54 0', 'GeoZone Exit')

	def trig_55_0():
		Serial_Cmd('AT$APP PEG TRIG 55 0', 'Any GeoZone Entry')

	def trig_56_0():
		Serial_Cmd('AT$APP PEG TRIG 56 0', 'Any GeoZone Exit')

	def trig_57_0():
		Serial_Cmd('AT$APP PEG TRIG 57 0', 'SMS Null')

	def trig_58_0():
		Serial_Cmd('AT$APP PEG TRIG 58 0', 'Comm Network ID')

	def trig_59_0():
		Serial_Cmd('AT$APP PEG TRIG 59 0', 'ID Read')

	def trig_60_0():
		Serial_Cmd('AT$APP PEG TRIG 60 0', 'Update Begin')

	def trig_61_0():
		Serial_Cmd('AT$APP PEG TRIG 61 0', 'Update End')

	def trig_62_0():
		Serial_Cmd('AT$APP PEG TRIG 62 0', 'Acceleration')

	def trig_63_0():
		Serial_Cmd('AT$APP PEG TRIG 63 0', 'Msg Generate')

	def trig_64_0():
		Serial_Cmd('AT$APP PEG TRIG 64 0', 'GPS Quality Fix')

	def trig_65_0():
		Serial_Cmd('AT$APP PEG TRIG 65 0', 'GPS Quality Lost')

	def trig_66_0():
		Serial_Cmd('AT$APP PEG TRIG 66 0', 'Accelerometer')

	def trig_67_0():
		Serial_Cmd('AT$APP PEG TRIG 67 0', 'VBus State')

	def trig_68_0():
		Serial_Cmd('AT$APP PEG TRIG 68 0', 'MotLog Wrap')

	def trig_69_0():
		Serial_Cmd('AT$APP PEG TRIG 69 0', 'VBus DTC Change')

	def trig_70_0():
		Serial_Cmd('AT$APP PEG TRIG 70 0', 'Radio Jamming')

	def trig_71_0():
		Serial_Cmd('AT$APP PEG TRIG 71 0', 'Accel Event')

	def trig_72_0():
		Serial_Cmd('AT$APP PEG TRIG 72 0', 'Accel Align State')

	def trig_73_0():
		Serial_Cmd('AT$APP PEG TRIG 73 0', 'VBus Event')

	def trig_74_0():
		Serial_Cmd('AT$APP PEG TRIG 74 0', 'Acc Schedule')

	def trig_75_0():
		Serial_Cmd('AT$APP PEG TRIG 75 0', 'Window Schedule')

	def trig_76_0():
		Serial_Cmd('AT$APP PEG TRIG 76 0', 'Tow Alert')

	def trig_77_0():
		Serial_Cmd('AT$APP PEG TRIG 77 0', 'Ack Rcvd')

	def trig_78_0():
		Serial_Cmd('AT$APP PEG TRIG 78 0', 'Hosted App')

	def trig_79_0():
		Serial_Cmd('AT$APP PEG TRIG 79 0', 'Tilt On')

	def trig_80_0():
		Serial_Cmd('AT$APP PEG TRIG 80 0', 'Tilt Off')

	def trig_81_0():
		Serial_Cmd('AT$APP PEG TRIG 81 0', 'Accel Event Start')

	def trig_82_0():
		Serial_Cmd('AT$APP PEG TRIG 82 0', 'Audio Event')

	def trig_83_0():
		Serial_Cmd('AT$APP PEG TRIG 83 0', 'Serial Msg Detect')

	def trig_84_0():
		Serial_Cmd('AT$APP PEG TRIG 84 0', 'GPS Jamming')

	def trig_85_0():
		Serial_Cmd('AT$APP PEG TRIG 85 0', 'MDT')

	def trig_86_0():
		Serial_Cmd('AT$APP PEG TRIG 86 0', 'BLE Fob')

	def trig_87_0():
		Serial_Cmd('AT$APP PEG TRIG 87 0', 'DUN Connect')

	def trig_88_0():
		Serial_Cmd('AT$APP PEG TRIG 88 0', 'Wakeup-Stream')

	def trig_89_0():
		Serial_Cmd('AT$APP PEG TRIG 89 0', 'Sys log')

	def trig_90_0():
		Serial_Cmd('AT$APP PEG TRIG 90 0', 'Upload')

	def trig_91_0():
		Serial_Cmd('AT$APP PEG TRIG 91 0', 'WAKEUP MON SRC')

	def trig_92_0():
		Serial_Cmd('AT$APP PEG TRIG 92 0', 'BLE Status')

	def trig_93_0():
		Serial_Cmd('AT$APP PEG TRIG 93 0', 'Batt Status')

	def trig_94_0():
		Serial_Cmd('AT$APP PEG TRIG 94 0', 'Matrix Event')

	def trig_95_0():
		Serial_Cmd('AT$APP PEG TRIG 95 0', 'Radio Silence Mode')

	def trig_20_1():
		buf = Serial_Cmd('AT$APP PEG TRIG 20 1', 'GPS Quality Fix', returnMatch=False)

	def trig_55_1():
		buf = Serial_Cmd('AT$APP PEG TRIG 20 1', 'Comm Acquire', returnMatch=False)
		
Run_Subtests(Subtests)
