import json, logging, re, colorama, os, random, signal, sys, traceback, time,datetime
import DeviceVal
import TestCase
import os
import time
import LMU
from TestCase import *
from Serial import *
from time import gmtime, strftime

Logger = logging.getLogger('DeviceVal_Logger.Test_Cases')

### Test Cases
###
#global APN,Maint,Inbound,APPID,FW,ESN

##############################################
############## Starting Tests ################
DeviceVal.Init()
global APN,MAINTENANCE,INBOUND,APPID,FW,ESN,RADIO
ESN = re.search('.*:\s*(.*)', Serial_Cmd('AT#ESN?', 'ESN:')).group(1)
APPID = re.search('.*[,](\S{3})', Serial_Cmd('ATI0', 'APP', filter='.*')).group(1)
FW = re.search('.*[V](\S{4})', Serial_Cmd('ATI0', 'APP', filter='.*')).group(1)
APN = 'broadband'
MAINTENANCE = 'testpuls.calamp.com'
INBOUND = 'gpstrax.gieselman.com'



class BasicTests:

	Name = 'Basic Tests'

	TestList = [
		('DNSLookup','DNSLookup'),
		('accum_gps_off', 'Accumulate time when GPS off'),
		('accum_gps_on', 'Accumulate time when GPS on'),
		('accumulator_restore_on_wakeup', 'Restore accumulator on wakeup'),
		('autoconnect_on_powerup', 'Auto-connect on power-up'),
		('fill_log_reset', 'Fill log reset'),
		('log_batchmode', 'Log Batchmode'),
		('info_cmds', 'Information cmds'),
		('ota_config_update', 'OTA config update'),
		('ota_firmware_download', 'OTA firmware download'),
		('peg_event_report', 'PEG - Event report'),
		('peg_id_report', 'PEG - ID report'),
		('recover_system_time_after_wakeup', 'Recover system time'), #need to finish
		('peg_timeouts','Peg timer timeouts'), #need to finish
		('peg_sleep_wakeup_timer', 'PEG - Sleep wakeup on timer'),
		('wakeup_on_time_of_day', 'wakeup_on_time_of_day'),
		('sms_param_msg', 'Send SMS param msg'),
		('comm_on_off', 'COMM ON/OFF'),
		('gps_on_off', 'GPS ON/OFF'),
		('sms_unit_request_msgs', 'Send SMS unit request msg'),
		('sms_vehicle_bus_msgs', 'Send SMS vehicle bus msg'),

		("security", "Level 1 Security"),
		('system_time_src_3D_GPS', 'System time does not sync to none-3D GPS fix time, S174 bit 4'),
		('system_time_src_reject_CellNetwork','System time does not sync to network time'),
		('system_time_src_reject_Server','System time does not sync to Server time'),
		('system_time_src_reject_None_3D_GPS','System time does not sync to none-3D GPS time'),
		('system_time_src_reject_RTC','System time does not sync to RTC time'),
		('system_time_src_reject_3D_GPS','System time does not sync to 3D-GPS time'),
		('system_time_src_Server','System time syncs to Server time'),
		('Param_migration','Param_migration'),
		
		('kevins_test','Kevins Test'),
		('testing_func','testing'),
		('wakeup_on_serial_linebreak', 'checks if the device wakes up on serial line break'),
		('new_comm_architecture', 'new comm architecture test list'),
		('simlock', 'simlock feature test list'),
		('long_motion_log','tests long motion log type 0,1'),
		('TCP','TCP'),

	]

	TestList = [
		# ('DNSLookup','DNSLookup'),
		# ('logActivityRestartOTA', 'logActivityRestartOTA'),
		# ('accum_gps_off', 'Accumulate time when GPS off'),
		# ('accum_gps_on', 'Accumulate time when GPS on'),
		('accumulator_restore_on_wakeup', 'Restore accumulator on wakeup'),
		# ('autoconnect_on_powerup', 'Auto-connect on power-up'),
		# ('fill_log_reset', 'Fill log reset'),
		# ('log_batchmode', 'Log Batchmode'),
		# ('info_cmds', 'Information cmds'),
		# #('ota_config_update', 'OTA config update'),
		# #('ota_firmware_download', 'OTA firmware download'),
		# ('peg_event_report', 'PEG - Event report'),
		# ('peg_id_report', 'PEG - ID report'),
		# ('recover_system_time_after_wakeup', 'Recover system time'), #need to finish
		# ('peg_timeouts','Peg timer timeouts'), #need to finish
		# ('peg_sleep_wakeup_timer', 'PEG - Sleep wakeup on timer'),
		# ('wakeup_on_time_of_day', 'wakeup_on_time_of_day'),
		# #('sms_param_msg', 'Send SMS param msg'),
		# ('comm_on_off', 'COMM ON/OFF'),
		# ('gps_on_off', 'GPS ON/OFF'),
		# #('sms_unit_request_msgs', 'Send SMS unit request msg'),
		# #('sms_vehicle_bus_msgs', 'Send SMS vehicle bus msg'),
		# #("security", "Level 1 Security"),
		# ('system_time_src_3D_GPS', 'System time does not sync to none-3D GPS fix time, S174 bit 4'),
		# ('system_time_src_reject_CellNetwork','System time does not sync to network time'),
		# ('system_time_src_reject_Server','System time does not sync to Server time'),
		# ('system_time_src_reject_None_3D_GPS','System time does not sync to none-3D GPS time'),
		# ('system_time_src_reject_RTC','System time does not sync to RTC time'),
		# ('system_time_src_reject_3D_GPS','System time does not sync to 3D-GPS time'),
		# ('system_time_src_Server','System time syncs to Server time'),
		# #('kevins_test','Kevins Test'),
		# ('wakeup_on_serial_linebreak', 'checks if the device wakes up on serial line break'),
		# ('new_comm_architecture', 'new comm architecture test list'),
		# #('simlock', 'simlock feature test list'),
		# ('param300','Time-Distance Profile Timer Suppress'),
		# ('long_motion_log','tests long motion log type 0,1'),
		# ('TCP','TCP'),
	]
	@Test_Case
	def Test_Start():
		global APN,MAINTENANCE,INBOUND,APPID,FW,ESN,RADIO
		ESN = re.search('.*:\s*(.*)', Serial_Cmd('AT#ESN?', 'ESN:')).group(1)
		APPID = re.search('.*[,](\S{3})', Serial_Cmd('ATI0', 'APP', filter='.*')).group(1)
		FW = re.search('.*[V](\S{4})', Serial_Cmd('ATI0', 'APP', filter='.*')).group(1)
		APN = 'broadband'
		MAINTENANCE = 'testpuls.calamp.com'
		INBOUND = 'gpstrax.gieselman.com'
		#RADIO = re.search('.*:\s*(.*)', Serial_Cmd('ATIC', 'Radio Access')).group(1)

	
	@Test_Case
	def param300():
		ATE2.Log('TEST APPLICABLE FOR FW 6.2H+')
		actionCodes = [1,2,3,40,44,57,81,82,83,122]
		DeviceVal.Factory()
		ATE2.Log('***** Setting up Time-Distance Profile [Index 0] ********')	
		Serial_Cmd('ATS125=7', '^OK', rdRetryCnt=15)
		Serial_Cmd('at$app param 300,0,255,0', 'OK') #bitmapped control reset of time-distance profile
		Serial_Cmd('at$app param 262,0,30', 'OK') #set up time threshold for 30sec
		Serial_Cmd('at$app peg action 7 0', 'OK') #starts time distance profile (this behavior loops the time)
		DeviceVal.Sleep(15)
		for code in actionCodes:
			Serial_Cmd('at$app peg action %d 0' % (code), '^OK')
			DeviceVal.Sleep(2)
			TmDstD = Serial_Cmd('at$app peg action 7 0', 'TmDst Reset:', cmdRetryCnt=5, cmdRetryDelay=1,filter='.*')
			resetval = (re.search('Time=\s*(\d+)',TmDstD).groups(3))[0]
			resetval = int(resetval)
			if resetval > 25:
				print('Action %d worked' %(code))
				ATE2.Log('Device Correctly Reset Timer Using Action Code - %d' % (code))
			else:
				TC_Fail('Unexpected value at action code - %d' % (code))
				
		ATE2.Log('***** ENABLING PARAM 300 FEATURE ********')	
		Serial_Cmd('at$app param 300,0,255,1', 'OK') #ENABLES FEATURE - suppresses the reset of Time-Distance timer when reports get sent out
		Serial_Cmd('','TmDst Timeout: set for 30 sec',cmdRetryCnt=30,cmdRetryDelay=1,filter='.*')
		ATE2.Log('Timer Initial Time Set')
		DeviceVal.Sleep(5)
		for code in actionCodes:
			Serial_Cmd('at$app peg action %d 0' % (code), '^OK')
			DeviceVal.Sleep(1)
			TmDstD = Serial_Cmd('at$app peg action 7 0', 'TmDst Reset:', cmdRetryCnt=5, cmdRetryDelay=1,filter='.*')
			resetval = (re.search('Time=\s*(\d+)',TmDstD).groups(3))[0]
			resetval = int(resetval)
			if resetval < 25:
				print('Action %d worked' %(code))
				ATE2.Log('Device Correctly Suppressed Timer Using Action Code - %d' % (code))
			else:
				TC_Fail('Unexpected value at action code - %d' % (code))
		ATE2.Log('Test Complete')
		
		
	@Test_Case
	@Debug
	def logActivityRestartOTA():
		if FW < '6.2c':
			ATE2.Log('Fix is not supported in firmware. Supports 6.2c and above')
			return
		Serial_Cmd('AT#FACTORY', '')
		Serial_Cmd('AT', 'OK', cmdRetryCnt=60)
		Serial_Cmd('ats125=7', 'OK', cmdRetryCnt=60)
		Serial_Cmd('AT$app param 2306,0,"%s"' %APN, 'OK', cmdRetryCnt=1, rdRetryCnt=60, rdRetryDelay=2 , retMatch = True)
		Serial_Cmd('AT$app param 2306,1,"%s"' %APN, 'OK', cmdRetryCnt=1, rdRetryCnt=60, rdRetryDelay=2 , retMatch = True)
		Serial_Cmd('AT$app param 2320,0,"%s"' %MAINTENANCE, 'OK',filter='.*')
		Serial_Cmd('ats157=2', '^OK')
		#Serial_Cmd('at$app log clear', '^OK')
		DeviceVal.Reset()
		DeviceVal.Check_Connected()
		Serial_Cmd('at$app log clear', '^OK', filter='.*')
		Serial_Cmd('at$app peg action 1 1', '<Log Active>', filter='Trig=')
		#Serial_Cmd('', '<Log Active>', filter='Trig=')
		DeviceVal.Sleep(90)
		fw = FW.replace(".", "")
		#Serial_Cmd('', 'LogActive Timeout', filter='.*', rdRetryCnt= 60)
		DeviceVal.PULS_Firmware_Set(ESN,fw)
		Serial_Cmd('at$app peg action 49 129', 'OK', filter='.*')
		try:
			Serial_Cmd('', 'DNLD: CRC Pass, File Integrity Validated', filter='.*',rdRetryCnt=120)
			DeviceVal.Sleep(30)
		except: 
			TC_Fail('Log active Timeout triggered in the middle of OTA')
	
	@Test_Case
	@Debug
	def wakeup_on_serial_linebreak():
		#setting up sleep parameters
		Serial_Cmd('at$app param 1064,0,4294967295,8', 'OK')
		Serial_Cmd('at$app param 265,0,150', 'OK')
		Serial_Cmd('at$app peg action 22 0', 'OK')
		DeviceVal.Sleep(40)
		ser = serial.Serial(0) 
		ser.sendBreak(0.25)
		ser.close()
		#listening on comm1 for debug message stating reason for wakeup
		try:
			Serial_Cmd('', 'Boot Reason: Wakeup, Trigger', rdRetryCnt=80, filter='.*')
		except:
			TC_Fail('Device did not wake up on trigger')
	#may not work on 2830
	
	@Test_Case
	@Debug
	def new_comm_architecture():
		ati0 = Serial_Cmd('ATI0', 'APP', filter='.*')
		fw = re.search('.*[V](\S{4})',ati0).group(1)
		if int(fw[0]) < 6 :
			TC_Fail('File signature only supported on firmwares 6.2a and above')
		elif int(fw[0]) < 6 and int(fw[1]) < 2:
			TC_Fail('File signature only supported on firmwares 6.2a and above')
			
		DeviceVal.Sleep(5)
	
		class Subtests:
			
			Name = 'new comm architecture'
			
			SubtestList = [
				('new_at_commands', 'checks if new at commands are working'),
				('new_service_setup', 'checks if new services are setup correctly'),
				('wrong_index', 'checks if the wrong index is accepted'),
				('private_service', 'tests the private service'),
			]
			
			SubtestList = [
				('new_at_commands', 'checks if new at commands are working'),
				('new_service_setup', 'checks if new services are setup correctly'),
				('wrong_index', 'checks if the wrong index is accepted'),
				('private_service', 'tests the private service'),
			]
	
			def new_at_commands():
				Serial_Cmd('ATIS', 'OK')
				Serial_Cmd('ATIR', 'OK')
				Serial_Cmd('ATIK', 'OK')
				Serial_Cmd('ATILL', 'OK')
			
			
			def new_service_setup():
				#setting up the inbound server and clearing the logs to match the string exactly
				Serial_Cmd('AT#FACTORY', '')
				Serial_Cmd('AT', 'OK', cmdRetryCnt=60)
				
				Serial_Cmd('at$app log clear', 'OK', rdRetryCnt=10)
				DeviceVal.Sleep(4)
				
				Serial_Cmd('at$app param 789,0,"M2L0Q0R0Nconn1"', 'OK')
				Serial_Cmd('at$app param 789,1,"M1L1Q0R0Nconn2"', 'OK')
				DeviceVal.Reset()
				DeviceVal.Sleep(3)
				
				query = Serial_Cmd('ATIS', '^OK', retBuf=True, filter='.*')
				list_1 = query[2:4]
				if list_1 != ['conn1(0)  log(0:0)  radio(0) mode(0:1) inb(0)  0.0.0.0:20500', 'conn2(1)  log(1:0)  radio(0) mode(0:1) inb(1)  0.0.0.0:20500']:
					TC_Fail('Power reset unlock the device')
			
			
			def wrong_index():
				Serial_Cmd('at$app param 789,8,"M2L0Q0R0Nconn1"', 'ERROR')
				Serial_Cmd('at$app param 789,7,"M2L0Q0R0Nconn1"', 'ERROR')
				
				
		Run_Subtests(Subtests)
	
	
	################################################################################################
	#         This test is currently applicable for a specific device marked for simlock           #
	#                       This will always fail on other devices                                 #
	################################################################################################
	@Test_Case
	@Debug
	def simlock():
		ati0 = Serial_Cmd('ATI0', 'APP', filter='.*')
		fw = re.search('.*[V](\S{4})',ati0).group(1)
		if int(fw[0]) < 6 :
			TC_Fail('File signature only supported on firmwares 6.2a and above')
		elif int(fw[0]) < 6 and int(fw[1]) < 2:
			TC_Fail('File signature only supported on firmwares 6.2a and above')
			
		DeviceVal.Sleep(5)
		
		class Subtests:
			
			Name = 'sim lock'
			
			SubtestList = [
				('initial_query', 'checks for any sim lock upon bootup'),
				('power_reset_verification', 'verifies that power reset does not unlock unit'),
				('outside_range', 'checks if outside range numbers are accepted'),
				('more_than_5_args', 'checks the response when more than 5 argumets are entered'),
				('starting_with_0', 'checks that numbers starting with 0 are not accepted'),
				('wrong_unlock_key', 'checks if wrong key unlocks device'),
			]
			
			SubtestList = [
				('initial_query', 'checks for any sim lock upon bootup'),
				('power_reset_verification', 'verifies that factory reset does not unlock unit'),
				('outside_range', 'checks if outside range numbers are accepted'),
				('more_than_5_args', 'checks the response when more than 5 argumets are entered'),
				('starting_with_0', 'checks that numbers starting with 0 are not accepted'),
				('wrong_unlock_key', 'checks if wrong key unlocks device'),
			]
			
			def simlockcheckquery():
				query = Serial_Cmd('AT#SIMLOCK?', '^OK', retBuf=True, filter='.*')
				list_1 = query [2:7]
				return list_1
			
			def simunlock():
				Serial_Cmd('at#simunlock 2176573329', 'OK')
								
	
			def initial_query():
				list = Subtests.simlockcheckquery()
				if list != ['#1:', '#2:', '#3:', '#4:', '#5:']:
					DeviceVal.Sleep(3)
					Subtests.simunlock()
				
				
			def power_reset_verification():
				Serial_Cmd('at#simlock 999999 99999 999999 999999 0', 'OK')			
				DeviceVal.Power_Reset()	
				DeviceVal.Sleep(3)
				list = Subtests.simlockcheckquery()
				if list != ['#1: 999999', '#2: 99999', '#3: 999999', '#4: 999999' ,'#5: 0']:
					TC_Fail('Power reset unlock the device')
					DeviceVal.Sleep(2)
					Subtests.simunlock()
					
				DeviceVal.Sleep(3)
				Subtests.simunlock()
			
			def outside_range():
				Serial_Cmd('at#simlock 1 23456789', 'ERROR')
				list = Subtests.simlockcheckquery()
				if list != ['#1:', '#2:', '#3:', '#4:', '#5:']:
					TC_Fail('outside range numbers were accepted')
					DeviceVal.Sleep(2)
					Subtests.simunlock()
					
				DeviceVal.Sleep(3)
				Subtests.simunlock()
				
			def more_than_5_args():
				Serial_Cmd('at#simlock 310410 311411 325678 599699 0 999999 567890', 'OK')
				list = Subtests.simlockcheckquery()
				if list != ['#1: 310410', '#2: 311411', '#3: 325678', '#4: 599699' ,'#5: 0']:
					TC_Fail('First five entried do not match with the list entered')
					DeviceVal.Sleep(2)
					Subtests.simunlock()
				
				DeviceVal.Sleep(3)
				Subtests.simunlock()
			
			def starting_with_0():
				Serial_Cmd('at#simlock 000012', 'ERROR')
				list = Subtests.simlockcheckquery()
				if list != ['#1:', '#2:', '#3:', '#4:', '#5:']:
					TC_Fail('number starting with 0 was accepted')
					DeviceVal.Sleep(2)
					Subtests.simunlock()
					
				DeviceVal.Sleep(3)
				Subtests.simunlock()
				
			def wrong_unlock_key():
				Serial_Cmd('at#simlock 310410 311411 325678 599699 0', 'OK')
				DeviceVal.Sleep(2)
				Serial_Cmd('AT#SIMUNLOCK 1111111111', 'Invld Key 1111111111')
				DeviceVal.Sleep(2)
				list = Subtests.simlockcheckquery()
				if list != ['#1: 310410', '#2: 311411', '#3: 325678', '#4: 599699' ,'#5: 0']:
					TC_Fail('wrong key unlocked device')
					DeviceVal.Sleep(2)
					Subtests.simunlock()
				
				DeviceVal.Sleep(3)
				Subtests.simunlock()
			
		Run_Subtests(Subtests)
			
	@Test_Case
	@Debug
	def DNSLookup():
		print('DNS lookup')
		Serial_Cmd('AT#FACTORY', '')
		Serial_Cmd('AT', 'OK', cmdRetryCnt=60)
		DeviceVal.Sleep(5)
		Serial_Cmd('AT$app param 2306,0,"%s"' %APN, 'OK', cmdRetryCnt=1, rdRetryCnt=60, rdRetryDelay=2 , retMatch = True)
		Serial_Cmd('AT$app param 2306,1,"%s"' %APN, 'OK', cmdRetryCnt=1, rdRetryCnt=60, rdRetryDelay=2 , retMatch = True)
		DeviceVal.Check_Connected()
		Serial_Cmd('ATS125=7', '^OK', rdRetryCnt=15)
		Serial_Cmd('AT$app param 2320,0,"%s"' %MAINTENANCE, 'OK',filter='.*')
		w = Serial_Cmd(' ', 'Success, 216.177.93.236',filter='.*',cmdRetryCnt=30, noFail = True,retBuf=True)
		DeviceVal.Sleep(5)
		atic = Serial_Cmd('ATIC', 'Maint. Server   :', retBuf=True)
		ip = re.search('\((.+?)\)',atic[0]).group(1)
		for i in range(0,5):
			if i == 5:
				TC_Fail('DNSLOOKUPFAILED, Failed to look up testPULS')
			if ip == '216.177.93.236':
				break
			DeviceVal.Sleep(10)
		if not w:
			if ip == '216.177.93.246':
				TC_Fail('DNSLOOKUPFAILED, Failed to look up testPULS')
		
		Serial_Cmd('AT$app param 2319,0,"%s"' %INBOUND, 'OK',filter='.*')
		w = Serial_Cmd(' ', 'Success, 40.78.56.22',filter='.*',cmdRetryCnt=30, noFail = True,retBuf=True)
		DeviceVal.Slee(5)
		atic = Serial_Cmd('ATIC', 'Inbound Server  :', retBuf=True)
		ip = re.search('\((.+?)\)',atic[0]).group(1)
		for i in range(0,5):
			if i == 5:
				TC_Fail('DNSLOOKUPFAILED, Failed to look up GPSTRAX')
			if ip == '40.78.56.22':
				break
			DeviceVal.Sleep(10)
		if not w:
			if ip == '0.0.0.0':
				TC_Fail('DNSLOOKUPFAILED, Failed to look up GPSTRAX')

		
	@Test_Case
	@Debug
	def comArch():
		print('comm')
		class Subtests:
				
			Name = 'Comm Architecture'

			SubtestList = [
				('comCommands', 'verify comm commands work'),
				('verify4services', 'verify 4 distinct services')
			]
			
			SubtestList = [
				
				('verify4services', 'verify 4 distinct services')
				
			]
			
			def comCommands():
				Serial_Cmd('ATIS', '^OK', initDelay = 1,filter='.*')
				Serial_Cmd('ATIR', '^OK', initDelay = 1,filter='.*')
				Serial_Cmd('ATIK', '^OK', initDelay = 1,filter='.*')
				Serial_Cmd('ATILL', '^OK', initDelay = 1,filter='.*')
			
			def verify4services():
				Serial_Cmd('AT$APP PARAM 789,0,"M0L0Q0R0Nconn1', '^OK', initDelay = 1,filter='.*')#modem index 0 Log 0 Qdeletes oldest msg R0 fixed
				Serial_Cmd('AT$APP PARAM 789,1,"M0L1Q0R0Nconn2', '^OK', initDelay = 1,filter='.*')#modem index 0 Log 1 Qdeletes oldest msg R0 fixed
				Serial_Cmd('AT$APP PARAM 789,2,"M0L0Q0R1Nconn3', '^OK', initDelay = 1,filter='.*')#modem index 0 Log 0 Qdeletes oldest msg R1 LCR
				Serial_Cmd('AT$APP PARAM 789,3,"M0L1Q0R1Nconn4', '^OK', initDelay = 1,filter='.*')#modem index 0 Log 01 Qdeletes oldest msg R1 LCR
				DeviceVal.Reset()
				buf = Serial_Cmd('ATIS', '^OK', retBuf = True, initDelay = 1,filter='.*')

				for line in buf:
					if 'conn1' in line:
						result = re.findall(r'\(([^()]+)\)', line)
						if result[0] != '0':
							TC_Fail('Service 1 Failed', result[0])
						if result[1] != '0:0':
							TC_Fail('Service 1 Failed', result[1])
					if 'conn2' in line:
						result = re.findall(r'\(([^()]+)\)', line)
						if result[0] != '1':
							TC_Fail('Service 2 Failed', result[0])
						if result[1] != '1:0':
							TC_Fail('Service 2 Failed', result[1])
					if 'conn3' in line:
						result = re.findall(r'\(([^()]+)\)', line)
						if result[0] != '2':
							TC_Fail('Service 3 Failed', result[0])
						if result[1] != 'NA':
							TC_Fail('Service 3 Failed', result[1])
					if 'conn4' in line:
						result = re.findall(r'\(([^()]+)\)', line)
						if result[0] != '3':
							TC_Fail('Service 4 Failed', result[0])
						if result[1] != 'NA':
							TC_Fail('Service 4 Failed', result[1])

		Run_Subtests(Subtests)

	@Test_Case
	@Debug
	def filesignature():
		#DeviceVal.Sleep(15)
		ati0 = Serial_Cmd('ATI0', 'APP', filter='.*')
		fw = re.search('.*[V](\S{4})',ati0).group(1)
		print(fw)
		print('!!!!!!!!!!!!!')
		if int(fw[0]) < 6:
			TC_Fail('File signature only supported on firmwares 6 and above')
		esn = re.search('.*:\s*(.*)', Serial_Cmd('AT#ESN?', 'ESN:')).group(1)
		path = os.getcwd() + '\\KeyFile'
		DeviceVal.PULS_Config_Reset(esn)
		DeviceVal.Sleep(5)
		class Subtests:
	
			Name = 'file signature'

			SubtestList = [
				('fileSigNoKeyFile', 'File signature without keyfile'),
				('fileSignwKeyFileNoAuth', 'File signature with keyfile but file Auth diasbled'),
				('fileSignwKeyFileAuth', 'File signature with keyfile and Auth enabled'),
			]
			
			SubtestList = [
				('fileSigNoKeyFile', 'File signature without keyfile'),
				('fileSignwKeyFileNoAuth', 'File signature with keyfile but file Auth diasbled'),
				('fileSignwKeyFileAuth', 'File signature with keyfile and Auth enabled'),
			]

			def fileSigNoKeyFile():
				DeviceVal.File_Signature(esn)
				
			def fileSignwKeyFileNoAuth():
				DeviceVal.Sleep(15)
				Serial_Cmd('ATS125=7', '^OK', rdRetryCnt=15)
				DeviceVal.Ymodem_File_Transfer('atdsf ', path, 'KeyFile_Souvik_Good_CaseD.ckf')
				Serial_Cmd('atpf 0 18', 'OK')
				DeviceVal.Sleep(2)
				try:
					Serial_Cmd('atreset', 'KeyFile:', rdRetryCnt=25, filter='.*')
					DeviceVal.Sleep(10)
				except:
					TC_Fail('KeyFile not loaded')
				DeviceVal.File_Signature(esn)
				
			def fileSignwKeyFileAuth():
				DeviceVal.Sleep(15)
				Serial_Cmd('ATS125=7', '^OK', rdRetryCnt=15)
				DeviceVal.Ymodem_File_Transfer('atdsf ', path, 'KeyFile_Souvik_Good_CaseE.ckf')
				Serial_Cmd('atpf 0 18', 'OK')
				DeviceVal.Sleep(2)
				try:
					Serial_Cmd('atreset', 'KeyFile:', rdRetryCnt=25, filter='.*')
					DeviceVal.Sleep(10)
				except:
					TC_Fail('KeyFile not loaded')
				DeviceVal.File_Signature(esn, keyFile=True, auth=True)

				
				
				
				
		Run_Subtests(Subtests)
	
	
	
	@Test_Case
	@Debug
	def ota_download_stress_test():
		
		while True:
			
			DeviceVal.ota_firmware_download_overnight('5571000142', '31d')
			DeviceVal.Sleep(20)
			
			ver = Serial_Cmd('ATI0', '^STM:\d.\d.\d').split()
			print('********************')
			print(ver[0])
			print('********************')
			
			if (ver[0] == 'STM:0.0.0' or ver[0] == 'STM:2.4.0'):
				TC_Fail('STM VERSION IS 0.0.0')
				break
				
			DeviceVal.ota_firmware_download_overnight('5571000142', '32a')
			DeviceVal.Sleep(20)
			
			ver = Serial_Cmd('ATI0', '^STM:\d.\d.\d').split()
			print('********************')
			print(ver[0])
			print('********************')
			
			if (ver[0] == 'STM:0.0.0' or ver[0] == 'STM:2.3.0'):
				TC_Fail('STM VERSION IS 0.0.0')
				break
	@Test_Case
	@Debug
	def long_motion_log():
	
		Serial_Cmd('ATS127=255', 'OK')  #to save accumulators
		Serial_Cmd('ATS124=16', 'OK')   #to turn on hex debug messages
		Serial_Cmd('at$app param 1061,1,816', 'OK')  #to set the log size 
		Serial_Cmd('at$app param 1061,3,816', 'OK')  #to set the log size
		Serial_Cmd('at$app param 1048,0,8', 'OK')    #to set type of log message 8 refers to accumulator
		Serial_Cmd('at$app param 1048,1,8', 'OK')    #to set type of log message 8 refers to accumulator
		Serial_Cmd('at$app param 1043,0,1', 'OK')    #to set the log interval to 1 sec
		Serial_Cmd('at$app param 1043,1,1', 'OK')    #to set the log interval to 1 sec
		Serial_Cmd('at$app param 2319,0,"gpstrax.gieselman.com"', 'OK') #set inbound server to gpstrax
		Serial_Cmd('at$app param 2319,1,"gpstrax.gieselman.com"', 'OK')
		
		#populating accumulator values
		for n in range(0,32):
			Serial_Cmd('AT$APP Param 2560,%d,%d' % (n,n), 'OK', rdRetryCnt=5)
		
		#choosing which accumulators to send
		for n in range(0,4):
			Serial_Cmd('AT$APP Param 1075,%d,255,255' % (n), 'OK', rdRetryCnt=5)
			
		for n in range(32,36):
			Serial_Cmd('AT$APP Param 1075,%d,255,255' % (n), 'OK', rdRetryCnt=5)
			
		#save the environment
		Serial_Cmd('at$app peg action 39 0', 'OK')
		#Serial_Cmd('at$app clear log')
		
		#reset the device and check for connection
		DeviceVal.Reset()
		DeviceVal.Check_Connected()
		
		DeviceVal.Sleep(6)
		
		#send log report type 0
		try: 
			Serial_Cmd('at$app peg action 132 1', '1C 00 00 00 1D 00 00 00 1E 00 00 00 1F', retBuf=True, cmdRetryCnt=3, cmdRetryDelay=2, filter='.*')
		except:
			TC_Fail('Long Motion 1 log not created')
		
		DeviceVal.Sleep(6)
		
		#send log report type 1
		try: 
			Serial_Cmd('at$app peg action 112 1', '1C 00 00 00 1D 00 00 00 1E 00 00 00 1F', retBuf=True, cmdRetryCnt=3, cmdRetryDelay=2, filter='.*')
		except:
			TC_Fail('Long Motion 0 log not created')
	
	@Test_Case
	@Debug
	def accumulator_above_below_trig():
		Serial_Cmd('at$app peg action 28 0',' ') #stop time accum 
		Serial_Cmd('at$app peg action 19 0',' ') #clear accum 0
		Serial_Cmd('at$app param 266,0,20','OK') #acc threshold to 20 
		Serial_Cmd('at$app param 280,0,0','OK') #acc index 
		Serial_Cmd('at$app peg action 26 0',' ') #starts time accumulation 
		Serial_Cmd('', '<Accum Below> ', rdRetryCnt=10, filter='Trig=')
		DeviceVal.Sleep(10)
		Serial_Cmd('at$app acc?', ' ')
		Serial_Cmd('', '<Count Exceeded> ', rdRetryCnt=20, filter='Trig=')
		##works
		print('works')
		('at$app acc?', ' ')
		
		# DeviceVal.Validate_Info('ATIC', 1)
				# atic = Serial_Cmd('ATIC', '^OK', retBuf=True, filter='.*')
				# cnx = Serial_Match('^Connection.*:\s*(.*)', atic)
				# print('Connection = %s' % (cnx))
				# if cnx != 'Yes':
					# TC_Fail('No connection', 'INFO_ATIC_NOCONN')
		

	@Test_Case
	@Debug
	def parameter_acceptance():
		DeviceVal.Factory()
		Serial_Cmd('ats125=1', '^OK')
		Logger.debug('+++ load the customer csv')
		cwd = os.getcwd()
		CSVLOC = cwd 
		print(CSVLOC)
		#PLACE CUSTOMER SCRIPT 
		working = DeviceVal.Load_BadCustomer_Script(CSVLOC, '111.222-paramTEST.csv', '111')
		if working == '1':
			print('1 Rec has been passed correctly.')
			#1024,1,6F
		else:
			TC_Fail('CustomerScript Recieved Incorrect Number of Records: [%s]' % (working), 'REC_VALUE')
		
		
		
		
		
		
		#DeviceVal.Factory()
		#Serial_Cmd('', ' 0 Recs', rdRetryCnt=5, filter='PRM: Config Updated')
		#256
		for n in range(0,4):   #number of index's 
			Serial_Cmd('AT$APP Param? 256,%d' % (n), '256,%d,0' % (n), rdRetryCnt=5)  
		for n in range(0,4):   #number of index's 
			Serial_Cmd('AT$APP Param? 264,%d' % (n), '264,%d,0' % (n), rdRetryCnt=5)  
		for n in range(0,4):   #number of index's 
			Serial_Cmd('AT$APP Param? 269,%d' % (n), '269,%d,0' % (n), rdRetryCnt=5)  	
		for n in range(0,64):   #number of index's 
			Serial_Cmd('AT$APP Param? 280,%d' % (n), '280,%d,0' % (n), rdRetryCnt=5) 
		for n in range(0,2):   #number of index's 
			Serial_Cmd('AT$APP Param? 2319,%d' % (n), '2319,%d,""' % (n), rdRetryCnt=5) 
		Serial_Cmd('AT$APP Param? 2320,0' , '2320,0,"maint.vehicle-location.com"', rdRetryCnt=5) 
		
		
				
	@Test_Case
	def accum_gps_off():
		DeviceVal.Sleep(20)
		atig = Serial_Cmd('atig', 'OK', retBuf=True, filter='.*')
		for line in atig:
			if 'No Fix' in line:
				TC_Fail('No GPS fix')
				
		Serial_Cmd('AT$APP PARAM 512,0,12,0,0,0,26,0,0,0', 'OK')  # 11=gps acquired trigger, 26,0=time into acc 0
		Serial_Cmd('AT$APP PARAM 512,1,11,0,0,0,28,0,0,0', 'OK')  # 12=gps lost trigger, 28,0 time off in acc 0
		Serial_Cmd('AT$APP PEG ACTION 19 0', 'OK') # clear acc 0
		Serial_Cmd('AT$APP GPS OFF', 'OK')
		DeviceVal.Sleep(10) # acc should have at least this value
		Serial_Cmd('AT$APP GPS ON', 'OK')
		acc, value, limit, type =(Serial_Cmd('AT$APP ACC?', '^\s*0\s*').split())
		if int(value) < int(3):
			TC_Fail('Accumulator Value less than expected [%s]' % (value), 'ACC_VALUE')
		Logger.debug('Acc [%s] Value [%s]' % (acc, value))
		
	@Test_Case	
	@Debug
	def accum_gps_on():
		DeviceVal.Sleep(50)
		atig = Serial_Cmd('atig', 'OK', retBuf=True, filter='.*')
		for line in atig:
			if 'No Fix' in line:
				TC_Fail('No GPS fix')
		Serial_Cmd('AT$APP PARAM 512,0,11,0,0,0,26,0,0,0', 'OK')
		Serial_Cmd('AT$APP GPS ON', 'OK')#restart gps
		DeviceVal.Sleep(10)
		# Serial_Cmd('AT$APP PARAM 512,0,12,0,0,0,26,0,0,0', 'OK')  # 12=gps on trigger, 26,0=time into acc 0
		# Serial_Cmd('AT$APP PARAM 512,1,11,0,0,0,28,0,0,0', 'OK')  # 12=gps on on trigger, 28,0 time off in acc 0
		# #DeviceVal.Reset()
		# #DeviceVal.Sleep(180)
		# Serial_Cmd('AT$APP PEG ACTION 19 0', 'OK',filter='.*') # clear acc 0
		# Serial_Cmd('AT$APP GPS ON', 'OK',filter='.*')
		# #DeviceVal.Sleep(120) # acc should have this approx value
		# #Serial_Cmd('AT$APP GPS OFF', 'OK',filter='.*')
		# DeviceVal.Sleep(30)
		# acc, value, limitType =(Serial_Cmd('AT$APP ACC?', '^\s*0\s*').split())
		# print (value)
		# if int(value) < int(5):
			# TC_Fail('Accumulator Value less than expected [%s]' % (value), 'ACC_VALUE')
		# Logger.debug('Acc [%s] Value [%s]' % (acc, value))
		
	@Test_Case
	@Debug
	def accumulator_restore_on_wakeup():
		Serial_Cmd('AT$APP PEG ACTION 26 1', '^OK')  # put time in acc 1
		Serial_Cmd('AT$APP PARAM 1024,7,1,1', '^OK')  # save accs to nvm on sleep
		Serial_Cmd('AT$APP PARAM 265,0,10', '^OK')  # set time to sleep in acc 0
		Serial_Cmd('AT$APP PEG ACTION 29 2', '^OK') # reset acc 2
		Serial_Cmd('AT$APP PEG ACTION 42 16', '^OK') # reset acc 2
		
		for n in range(3,63):
			Serial_Cmd('AT$APP PEG ACTION 18 %d' % (n), '^OK', cmdRetryDelay=2)  # incr acc n
		Serial_Cmd('AT$APP PEG ACTION 22 0', '^OK')  # sleep on acc 0
		#Serial_Cmd('', 'Sleep.*sec', rdRetryCnt=15)
		#Serial_Cmd('', 'VERSION', rdRetryCnt=15)
		#Serial_Cmd('ATS125=0', '^OK', rdRetryCnt=15)
		Serial_Cmd('AT$APP ACC?', '^OK', rdRetryCnt=10, filter='^\d*\s*')
		
	@Test_Case
	def info_cmds():
	
		class Subtests:
		
			Name = 'Info Tests'

			SubtestList = [
				('ati0', 'Info ATI0'),
				('ati1', 'Info ATI1'),
				('ati2', 'Info ATI2'),
				('ati3', 'Info ATI3'),
				('atic', 'Info ATIC'),
				('atig', 'Info ATIG'),
				('atip', 'Info ATIP'),
			]
			
			SubtestList = [
				('ati0', 'Info ATI0'),
				('ati1', 'Info ATI1'),
				('ati2', 'Info ATI2'),
				('ati3', 'Info ATI3'),
				('atic', 'Info ATIC'),
				('atig', 'Info ATIG'),
				('atip', 'Info ATIP'),
			]

			def ati0():
				DeviceVal.Validate_Info('ATI0', 30)
			
			def ati1():
				DeviceVal.Validate_Info('ATI1', 30)

			def ati2():
				DeviceVal.Validate_Info('ATI2', 1)
				radioTech = re.search('.*:\s*(.*)', Serial_Cmd('ATIC', 'Radio Access')).group(1)
				Serial_Cmd('ATI2', '%s' %RADIO, errCode='INFO_ATI2_RADIOTECH')
				
			def ati3():
				DeviceVal.Validate_Info('ATI3', 1)
				inpBits = re.search('INP:\s*(.*) ', Serial_Cmd('ATI3', 'INP:')).group(1)[::-1] # inp bits reversed
				Note('Reversed bits = %s' % (inpBits))
				# verify each INP bit matches line in Input? cmd
				input = Serial_Cmd('AT$APP INPUT?', 'OK', retBuf=True, filter='.*')
				print ("vinod")
				print (input)
				cnt = 0
				for bit in inpBits:
					found = False
					for line in input:
						m = re.search('INPUT-%s:.*State=(.*)' % (cnt), line)
						if m:
							print('Bit [%d] Value [%s] State [%s]' % (cnt, bit, m.group(1)))
							if bit != m.group(1):
								TC_Fail('ATI3 INP bit mismatch with Input?', 'INFO_ATI3_INPBIT')
							else:
								found = True
								break
					if not found:
						TC_Fail('Input not found in Input?', 'INFO_ATI3_INPBIT')
					cnt = cnt + 1
			
			
			
			def atic():
				DeviceVal.Validate_Info('ATIC', 1)
				atic = Serial_Cmd('ATIC', '^OK', retBuf=True, filter='.*')
				cnx = Serial_Match('^Connection.*:\s*(.*)', atic)
				print('Connection = %s' % (cnx))
				if cnx != 'Yes':
					TC_Fail('No connection', 'INFO_ATIC_NOCONN')
				rssi = int(Serial_Match('^RSSI.*:\s*([-\d]*)', atic))
				print('RSSI = %d' % (rssi))
				if rssi < -100 or rssi > -50: 	
					TC_Fail('RSSI out of range', 'INFO_ATIC_RSSI')

			def atig():
				DeviceVal.Check_GPS_Connected()
				DeviceVal.Validate_Info('ATIG', 1)
				atig = Serial_Cmd('ATIG', '^OK', retBuf=True, filter='.*')
				sats = int(Serial_Match('^Sats.*:\s*(.*)', atig))
				print('Sats = %d' % (sats))
				DeviceVal.Sleep(10)
				if sats < 4:
					TC_Fail('Not enough satellites acquired', 'INFO_ATIG_SATS')
				Add_TC_Detail('SATS', '%d' % (sats), '4', '99')
				hdop = float(Serial_Match('^HDOP.*:\s*(.*)', atig))
				print('HDOP = %d' % (hdop))
				if hdop > 5:
					TC_Fail('HDOP value [%d] too high' % (hdop), 'INFO_ATIG_HDOP')
				gpsTime = Serial_Match('^GPS Time.*:\s*(.*)', atig)
				# validate gps time within 5 min of gmt network service time
				print('GPS Time = %s' % (gpsTime))
				
			def atip():
				DeviceVal.Validate_Info('ATIP', 1)
				
		Run_Subtests(Subtests)
		
	@Test_Case
	def auto_apn():     #APN LIST TEST 
		# use apn list file
		DeviceVal.Load_APN_List()
		Serial_Cmd('ATS155=0', 'OK')
		Serial_Cmd('AT$APP PARAM 2306,0,"', 'OK')
		Serial_Cmd('AT$APP PARAM 2306,1,"', 'OK')
		Serial_Cmd('AT$APP PARAM? 2306,*', 'OK')
		DeviceVal.Reset()
		DeviceVal.Check_Connected()
		Serial_Cmd('AT$APP PARAM? 2306,0', 'BROADBAND')
		# Serial_Cmd('AT$APP PARAM? 2306,1', 'BROADBAND')
		return
		# auto provision on, 2306 empty, connects and populates blank 2306 slots
		Serial_Cmd('ATS155=0', 'OK')
		Serial_Cmd('AT$APP PARAM 2306,0,"', 'OK')
		Serial_Cmd('AT$APP PARAM 2306,1,"', 'OK')
		Serial_Cmd('AT$APP PARAM? 2306,*', 'OK')
		DeviceVal.Reset()
		DeviceVal.Check_Connected()
		Serial_Cmd('AT$APP PARAM? 2306,0', 'ISP.CINGULAR')
		Serial_Cmd('AT$APP PARAM? 2306,1', 'ISP.CINGULAR')
		# auto provision on, 2306 populated, connects and populates 2306
		Serial_Cmd('ATS155=0', 'OK')
		Serial_Cmd('AT$APP PARAM 2306,0,"', 'OK')
		Serial_Cmd('AT$APP PARAM 2306,1,"ISP.CINGULAR', 'OK')
		Serial_Cmd('AT$APP PARAM? 2306,*', 'OK')
		DeviceVal.Reset()
		DeviceVal.Check_Connected()
		Serial_Cmd('AT$APP PARAM? 2306,0', 'ISP.CINGULAR')
		Serial_Cmd('AT$APP PARAM? 2306,1', 'ISP.CINGULAR')
		# auto provision on, 2306 empty, connects but no populate 2306
		Serial_Cmd('ATS155=1', 'OK')
		Serial_Cmd('AT$APP PARAM 2306,0,"', 'OK')
		Serial_Cmd('AT$APP PARAM 2306,1,"', 'OK')
		Serial_Cmd('AT$APP PARAM? 2306,*', 'OK')
		DeviceVal.Reset()
		DeviceVal.Check_Connected()
		Serial_Cmd('AT$APP PARAM? 2306,0', '0,""')
		Serial_Cmd('AT$APP PARAM? 2306,1', '1,""')
		# use apn list file
		Serial_Cmd('ATS155=0', 'OK')
		Serial_Cmd('AT$APP PARAM 2306,0,"', 'OK')
		Serial_Cmd('AT$APP PARAM 2306,1,"', 'OK')
		Serial_Cmd('AT$APP PARAM? 2306,*', 'OK')
		DeviceVal.Reset()
		DeviceVal.Check_Connected()
		Serial_Cmd('AT$APP PARAM? 2306,0', 'BROADBAND')
		
		
		
		# if ats155=1 and 2306 populated then will use 2306
		
		# for non-cdma devices
		# ats155=0	auto provision on
		# should auto populate from apn list file
		# 
		# ats155=1	auto provision off
		# set 2306
		# should connect using value in 2306
		
		# test that both slots used?
		# test slot 1 with slot 0 wrong value
		
		# cdma/lte
		# leave ats155=0 ...
		# 2306 doesn't get populated, but still get connection

	@Test_Case
	def autoconnect_on_powerup():
		Serial_Cmd('AT$APP PEG ACTION 70 1', 'OK')  # restart app
		DeviceVal.Check_Connected(False)  #needs  to makesure device already is powered off
		DeviceVal.Check_Connected()
		
	@Test_Case
	def ota_config_update():
		#esn = DeviceVal.Get_ESN()
		scriptVer, configVer = (DeviceVal.Get_Config_Value('RUN', 'Script Update')).split('.')
		esn = re.search('.*:\s*(.*)', Serial_Cmd('AT#ESN?', 'ESN:')).group(1)
		DeviceVal.PULS_Config_Set(esn, scriptVer, configVer)
		#sets device to pending
		Serial_Cmd('AT$APP PEG ACTION 49 129', 'OK')  # send ID Report now
		#DeviceVal.Check_ID_Report_Time()
		DeviceVal.Sleep(30)
		Serial_Cmd('at$app peg action 49 129', '^OK', cmdRetryCnt=60, cmdRetryDelay=3,filter='.*')
		jsonObj = DeviceVal.PULS_Device_Info(esn)
		devicecompleted = False
		for retry in range(84):
			jsonObj = DeviceVal.PULS_Device_Info(esn)
			scriptVerPuls = str(jsonObj['scriptVersion']) #receive script version from PULS
			configVerPuls = str(jsonObj['configVersion']) #receive script version from PULS
			paramSV = re.search('.*,(.+)$',Serial_Cmd('AT$APP PARAM? 1024,1','1024,1,',cmdRetryCnt=5, cmdRetryDelay=1)).group(1) #receive script version from param
			paramCV = re.search('.*,(.+)$',Serial_Cmd('AT$APP PARAM? 1024,23','1024,23,',cmdRetryCnt=5, cmdRetryDelay=1)).group(1) # #receive script version from param
			print (scriptVerPuls, paramSV, scriptVer)
			print (configVerPuls, configVer, paramCV)
			if (scriptVerPuls == scriptVer == paramSV) and (configVerPuls == configVer == paramCV):
				devicecompleted = True
				break
				DeviceVal.Sleep(5)	
			if not devicecompleted:
				TC_Fail('Download failed','PULS_DNLD-FAIL')
				print ('upgradedone')				
		# file w bad values
		# empty file
		# large file??
		
	@Test_Case
	def ota_firmware_download():
		
		fW, appid = (DeviceVal.Get_Config_Value('RUN', 'Ident')).split('/')
		
		esn = re.search('.*:\s*(.*)', Serial_Cmd('AT#ESN?', 'ESN:')).group(1)
		
		
		jsonObj = DeviceVal.PULS_Device_Info(esn)
	
		dnldRec = jsonObj['firmwareHistorys'][0]
		print('\n\n' + str(dnldRec)  + '\n\n')
		fwNew = dnldRec['newFirmware']['versionNumber']
		fwOld = dnldRec['oldFirmware']['versionNumber']
		ATE2.Log('Updated %s from %s' % (fwNew, fwOld))
		jsonObj = DeviceVal.PULS_Firmware_Info(appid)
		print (fW)
		
		
		
		upgradeBuildTest = DeviceVal.Get_Config_Value('RUN', 'Firmware Upgrade List')
		print (upgradeBuildTest)
		
		upgrade = upgradeBuildTest.split(',')
		print (upgrade)
		
		upgradbuildcount = 0
		while(upgradbuildcount < 1):
			
			ATE2.Log('++ Iteratation %d' % (upgradbuildcount))
			Logger.debug('++ Iteratation %d' % (upgradbuildcount))
			for build in upgrade:
				print ('Requested build to upgrade is ',build)
				DeviceVal.PULS_Firmware_Set(esn, build)
				Serial_Cmd('at$app peg action 49 129', '^OK', cmdRetryCnt=60, cmdRetryDelay=3)
				#time.sleep(10)
				DeviceVal.Sleep(300)
				Serial_Cmd('at$app peg action 49 129', '^OK', cmdRetryCnt=60, cmdRetryDelay=3,filter='.*')
				ati0build = build[0] + '.' + build[1: ]
				print (build, ati0build)
				jsonObj = DeviceVal.PULS_Device_Info(esn)
				devicecompleted = False
				for retry in range(84):
					jsonObj = DeviceVal.PULS_Device_Info(esn)
					status = jsonObj['firmwareHistorys'][0]['status']
					print (status)
							
					if status == 'Completed':
						devicecompleted = True
						break
					DeviceVal.Sleep(5)	
				if not devicecompleted:
					TC_Fail('Download failed','PULS_DNLD-FAIL')
				print ('upgradedone')
				Serial_Cmd('ati0', ati0build, cmdRetryCnt=200, cmdRetryDelay=3, filter='APP:')	
			
			
			
			DeviceVal.Sleep(60)
			Serial_Cmd('at$app peg action 49 129', '^OK', cmdRetryCnt=60, cmdRetryDelay=3)
			upgradbuildcount = upgradbuildcount + 1
	
	
	@Test_Case
	@Debug

	def udp_param_write():
		########################################
		#512,3 -->  0 to non 0			       #
		#512,4 --> non 0 to 0                  #
		#512,5 --> non 0 to different non 0    #
		#512,6 --> remains same                #
		########################################
		
		#setting s reg 181 to 1 for UDP download
		Serial_Cmd('ats181=1', 'OK')
		
		#setting parameters to required values
		Serial_Cmd('at$app param 512,3,0,0,0,0,0,0,0,0', 'OK')
		Serial_Cmd('at$app param 512,4,1,0,16,0,32,0,0,0', 'OK')
		Serial_Cmd('at$app param 512,5,15,4,9,0,13,0,0,0', 'OK')
		Serial_Cmd('at$app param 512,6,0,16,64,6,5,0,112,32', 'OK')
		
		#download the config file with necessary changes
		esn = re.search('.*:\s*(.*)', Serial_Cmd('AT#ESN?', 'ESN:')).group(1)
		DeviceVal.PULS_Config_Set(esn, '16', '16')
		Serial_Cmd('AT$APP PEG ACTION 49 129', 'OK')  # send ID Report now
		DeviceVal.Sleep(20)
		
		#querying each param and storing in a var
		buf1 = Serial_Cmd('AT$APP PARAM? 512,3', '^512', cmdRetryCnt=5, cmdRetryDelay=10)
		buf2 = Serial_Cmd('AT$APP PARAM? 512,4', '^512', cmdRetryCnt=5, cmdRetryDelay=10)
		buf3 = Serial_Cmd('AT$APP PARAM? 512,5', '^512', cmdRetryCnt=5, cmdRetryDelay=10)
		buf4 = Serial_Cmd('AT$APP PARAM? 512,6', '^512', cmdRetryCnt=5, cmdRetryDelay=10)
		
		#expected parameter values
		var1 = '512,3,15,0,17,5,8,0,54,0'
		var2 = '512,4,0,0,0,0,0,0,0,0'
		var3 = '512,5,3,0,17,0,8,0,0,0'
		var4 = '512,6,0,16,64,6,5,0,112,32'
		
		#comparison
		if buf1 != var1:
			TC_Fail('Parameter Mismatch','PARAM_MISMATCH')
		else:
			print('param 512,3 matches')
			
		if buf2 != var2:
			TC_Fail('Parameter Mismatch','PARAM_MISMATCH')
		else:
			print('param 512,4 matches')
			
		if buf3 != var3:
			TC_Fail('Parameter Mismatch','PARAM_MISMATCH')
		else:
			print('param 512,5 matches')
			
		if buf4 != var4:
			TC_Fail('Parameter Mismatch','PARAM_MISMATCH')
		else:
			print('param 512,6 matches')
		
	@Test_Case
	@Debug
	def peg_comm_shutdown():
		Serial_Cmd('AT$APP PEG ACTION 55 0', '<Comm Shutdown>', rdRetryCnt=5, filter='Trig=')
		DeviceVal.Check_Connected(False)
	
	@Test_Case
	@Debug
	def peg_sleep_wakeup_timer():
		Serial_Cmd('AT$APP PARAM 265,0,10', 'OK')  # set timer 0
		Serial_Cmd('AT$APP PEG ACTION 22 0', 'OK', rdRetryCnt=15)  # sleep, wakeup on 0
		dtStart = datetime.datetime.now()
		Serial_Cmd('', 'VERSION', rdRetryCnt=15)
		dtEnd = datetime.datetime.now()
		dur = DeviceVal.DT_Duration(dtStart, dtEnd)
		Logger.debug('Sleep duration = %s' % (dur))
		if dur < 10 or dur > 25:
			TC_Fail('Sleep duration out-of-range', errCode='PEG_SLEEPDUR')
			
	@Test_Case
	def peg_id_report():
		Serial_Cmd('AT$APP PEG ACTION 49 129', 'OK')  # send ID Report now
		DeviceVal.Check_ID_Report_Time()
	
	@Test_Case
	@Debug
	def peg_event_report():
		for offset in range(3):
			code = 101 + offset
			#Serial_Cmd('AT$APP PEG ACTION 1 %d' % (code), 'Ack0x', rdRetryCnt=10, filter='LMD: Send|LMD: Rcv|Inbnd:')
			Serial_Cmd('AT$APP PEG ACTION 1 %d' % (code), 'Ack', rdRetryCnt=10, filter='.*')

	@Test_Case
	def sms_param_msg():
		DeviceVal.Send_SMS('!S0<payload')	

	@Test_Case
	def sms_serial_msg():
		# just payload
		DeviceVal.Send_SMS('!S0<payload>')	
		# payload with cr/lf
		DeviceVal.Send_SMS('!S1<payload>')	
		
	@Test_Case
	def comm_on_off():
		# comm off
		Serial_Cmd('AT$APP COMM OFF', 'OK')
		DeviceVal.Check_Connected(False)
		# comm on
		Serial_Cmd('AT$APP COMM ON', 'OK')
		DeviceVal.Check_Connected()
		
	@Test_Case
	def gps_on_off():
		# gps off
		Serial_Cmd('AT$APP GPS OFF', 'OK')
		DeviceVal.Check_GPS_Connected(False)
		#Serial_Cmd('ATIG', 'Power\s*:\s*Off', errCode='INFO_ATIG_POWER')
		# gps on
		Serial_Cmd('AT$APP GPS ON', 'OK', errCode='INFO_ATIG_POWER')
		DeviceVal.Check_GPS_Connected()
		#Serial_Cmd('ATIG', 'Power\s*:\s*On')
		
	@Test_Case
	def recover_system_time_after_wakeup():
		ATE2.Log('GPS Off')
		Serial_Cmd('AT$APP GPS OFF', 'OFF')
		Serial_Cmd('AT$APP PARAM 265,0,5', 'OK')  # set timer
		Serial_Cmd('AT$APP PEG ACTION 22 0', 'OK')  # sleep on timer
		DeviceVal.Get_GMT_Time()

		# at$app gps off
		# peg action 22 0    sleep 265,0,<cnt> 
		#	pull time from app[time] debug and compare to compare gmt remote time
		
		# turn off comm, on gps
		# sleep, wakup
		# verify gmt time
		
		# turn off both comm, gps
		# sleep wakup
		# check time
		
	@Test_Case
	def peg_timeouts():
		Serial_Cmd('AT$APP PARAM 512,0,12,0,0,0,13,0,0,0', 'OK')  # start timer when gps off
		Serial_Cmd('AT$APP PARAM 512,1,11,0,0,0,14,0,0,0', 'OK')  # stop timer when gps off
		Serial_Cmd('AT$APP GPS OFF', 'OK')
		Serial_Cmd('AT$APP GPS ON', 'OK')
		
	@Test_Case
	@Debug
	def peg_time_of_day_timer():
		DeviceVal.Get_GMT_Time()
		Serial_Cmd('AT$APP PARAM 267,0,<GMT TIME OFFSET>', '')  # calculate second past gmt midnight to time of test + 2 min
		Serial_Cmd('AT$APP PARAM 265,0,5', 'OK')  # timer 0 to 5 seconds
		Serial_Cmd('AT$APP PARAM 512,0,20,0,0,0,22,0,0,0', 'OK') # (22,0) sleep using timer 0, 20 trigger the peg tmo day trigger
		
		# at$app param 267,0,<calculate gmt offset>    # 
			# calculate second past gmt midnight to time of test + 2 min
			# 265,0,15   # timer 0 to 15 seconds
			# at$app param 512,0,20,0,0,0,22,0,0,0    # (22,0) sleep using timer 0, 20 trigger the peg tmo day trigger
			# check sleep event occured.  checked wakeup event occurred
			
	@Test_Case
	def sleep_on_ignition_off():
		Serial_Cmd('AT$APP PARAM 265,0,10', 'OK') # timer 0 10 seconds
		Serial_Cmd('AT$APP PARAM 512,0,16,0,0,0,22,0,0,0') # (16=ignition off, 22,0 = sleep using val in timer 0)
		
		# at$app 265,0,10    # timer 0 to 10 seconds
		# at$app param 512,0,16,0,0,0,22,0,0,0    (16=ignition off, 22,0 = sleep using val in timer 0)
		# force ignition on/off  ?? how to do this w/o a test fixture
		# verify sleep/wakeup
	
	@Test_Case
	def sleep_on_timer():
		Serial_Cmd('AT$APP PARAM 265,0,10', 'OK') # timer 0 - 10 seconds
		Serial_Cmd('AT$APP PARAM 265,1,5', 'OK') # timer 1 - 5 seconds
		Serial_Cmd('AT$APP PARAM 512,0,18,0,0,0,22,0,0,0', 'OK') # 18=timer tmo trigger, 22,0 = sleep using timer 0
		Serial_Cmd('AT$APP PARAM 512,1,3,0,0,0,13,1,0,0', 'OK') # 3=on powerup, 13=start timer 1
		
		# vinod to redo this 

	@Test_Case
	@Debug
	def sms_param_msg():
		# write request - set timer 0 = 120, accum 0 thresh = 300
		Serial_Cmd('AT$APP PARAM 265,0,0', 'OK') # timer 0
		Serial_Cmd('AT$APP PARAM 266,0,0', 'OK') # accum 0 thresh
		resp = DeviceVal.Send_SMS_Sinch('!P0000000101010900050000000078010A0005000000012C00000000', mRE='!P000000010300000000')
		Serial_Cmd('AT$APP PARAM? 265,0', ',120$') # timer 0
		Serial_Cmd('AT$APP PARAM? 266,0', ',300$')
		
	@Test_Case
	@Debug
	def sms_unit_request_msgs():
		class Subtests:
		
			Name = "SMS MASTER"
	
			SubtestList = [
				('r0_lmu_status', '!R0 - LMU status'),
				('r1_write_param', '!R1 - write param'),
				('r3_49_129_id_report', '!R3,49,129 - ID Report'),
				('r3_70_0_app_reset', '!R3,70,0 - app reset'),
				('r5_gps_status', '!R5 - GPS status'),
				('r8_firmware_update', '!R8 - firmware update'),
				('ra_adc_values', '!RA - ADC values'),
				('rb_battery_status', '!RB - Battery fuel guage status'),
				('rc_comm_status', '!RC - comm status'),
				('rj_google_maps', '!RJ - Google maps location'),
				('rn_nuvi_status', '!RN - NUVI status'),
				('rp_param', '!RP - read/write a param'),
				('rv_audio_settings', '!RV - audio settings'),
				('rq_accelerometer_output', '!RQ - accelerometer output'),
				('sms_wakeup_radio'),('!wakeup - wakeup_sleep'),
			]
	
			SubtestList = [
				 ('r0_lmu_status', '!R0 - LMU status'),
				 ('r1_write_param', '!R1 - write param'),
				 ('r3_49_129_id_report', '!R3,49,129 - ID Report'),
				 ('r3_70_0_app_reset', '!R3,70,0 - app reset'),
				 ('r5_gps_status', '!R5 - GPS status'),
				 ('r8_firmware_update', '!R8 - firmware update'),
				 ('ra_adc_values', '!RA - ADC values'),
				 ('rb_battery_status', '!RB - Battery fuel guage status'),
				 ('rc_comm_status', '!RC - comm status'),
				 ('rj_google_maps', '!RJ - Google maps location'),
				 ('rn_nuvi_status', '!RN - NUVI status'),
				 ('rp_param', '!RP - read/write a param'),
				 ('rv_audio_settings', '!RV - audio settings'),
				 ('rq_accelerometer_output', '!RQ - accelerometer output'),		
				 ('sms_wakeup_radio'),('!wakeup - wakeup_sleep'),
			]

			
			def r0_lmu_status():
				esn = DeviceVal.Send_SMS_SEND('!r0')
				DeviceVal.Sleep(10) #delayed wait becasue first time smsing
				DeviceVal.Send_SMS_REC_ati0(esn)
				DeviceVal.Sleep(10)
			
			def r1_write_param():
				DeviceVal.Send_SMS_SEND('!r1,2306,1,badparam')
				DeviceVal.Sleep(20)
				Serial_Cmd('AT$APP PARAM? 2306,1','badparam',cmdRetryCnt=3,cmdRetryDelay=2,filter='.*')
			
			def r3_49_129_id_report():
				esn = DeviceVal.Send_SMS_SEND('!r3,49,129')
				Serial_Cmd('','MAINT: Send ID Report',cmdRetryCnt=20,cmdRetryDelay=2,filter=',*')
				DeviceVal.Check_ID_Report_Time()
					
				
			def r3_70_0_app_reset():
				esn = DeviceVal.Send_SMS_SEND('!r3,70,0')
				Serial_Cmd('','Rebooting...',cmdRetryCnt=20,cmdRetryDelay=2)
				DeviceVal.Check_Device_Booted()
				DeviceVal.Check_Connected()
				DeviceVal.Sleep(10)
			
			def r5_gps_status():
				DeviceVal.Sleep(20)
				esn = DeviceVal.Send_SMS_SEND('!r5')
				DeviceVal.Send_SMS_REC(esn) 
				DeviceVal.Sleep(10)
			
			def r8_firmware_update():
				ATE2.Log('This request only applies to the older LMU-1000™. This message will cause the LMU-1000™ to initiate a software update from a server via HTTP.')
				esn = DeviceVal.Send_SMS_SEND('!r8')
				print('--Checking If we recieved a response.')
				Serial_Cmd('','!r8',cmdRetryCnt=20,cmdRetryDelay=2)
				
			def ra_adc_values():
				ATE2.Log('***** Test Configured with Known Bug ********')	
				esn = DeviceVal.Send_SMS_SEND('!ra')
				Serial_Cmd('','FAILED',cmdRetryCnt=15,cmdRetryDelay=2,filter='.*')
			
			def rb_battery_status():
				esn = DeviceVal.Send_SMS_SEND('!rb')
				Serial_Cmd('','!rb',cmdRetryCnt=20,cmdRetryDelay=2)
				
			def rc_comm_status():
				esn = DeviceVal.Send_SMS_SEND('!rc')
				DeviceVal.Send_SMS_REC(esn)
				DeviceVal.Sleep(10)
			
			def rj_google_maps():
				esn = DeviceVal.Send_SMS_SEND('!rj')
				DeviceVal.Send_SMS_REC(esn)
				DeviceVal.Sleep(10)
			
			def rn_nuvi_status():
				esn = DeviceVal.Send_SMS_SEND('!rn')	 
				DeviceVal.Send_SMS_REC(esn)
				DeviceVal.Sleep(10)
				
			def rp_param(): 
				esn = DeviceVal.Send_SMS_SEND('!rp?2306,*')
				DeviceVal.Send_SMS_REC(esn)
				DeviceVal.Sleep(10)
				
			def rv_audio_settings():
				#Sets Audio Volume from wiki
				esn = DeviceVal.Send_SMS_SEND('!rv')	
				Serial_Cmd('','!rv',cmdRetryCnt=20,cmdRetryDelay=2)
				
			def rq_accelerometer_output():
				esn = DeviceVal.Send_SMS_SEND('!rq')
				DeviceVal.Send_SMS_REC(esn)
				
			def sms_wakeup_radio():
				ATE2.Log('	!!!This feature does not work on 3640s as of 2/20/2018')
				esn = re.search('.*:\s*(.*)', Serial_Cmd('AT#ESN?', 'ESN:')).group(1)
				Serial_Cmd('at$app param 265,1,0','OK')
				Serial_Cmd('ats171=4','OK')
				DeviceVal.Reset()
				DeviceVal.Check_Connected()
				Serial_Cmd('at$app peg action 22 1','OK')
				print('---now sleeping')
				DeviceVal.Sleep(15)
				SMSDeviceVal.Send_SMS_DuringSleep('!r0',esn)
				SMSDeviceVal.Send_SMS_REC(esn)
				Serial_Cmd('','Boot Reason: Wakeup, Comm',cmdRetryCnt=20,cmdRetryDelay=2)
			
		Run_Subtests(Subtests)
		
	@Test_Case
	@Debug
	def sms_vehicle_bus_msgs():
		class Subtests:
				
			Name = 'Vehicle Bus Msgs'

			SubtestList = [
				('v0_vbus_status', '!V0 - vehicle bus status'),
				('vd_dtc_status', '!VD - DTC status'),
				('vv_vehicle_detection_status', '!VV - vehicle detection status'),
			]
			
			SubtestList = [
				('v0_vbus_status', '!V0 - vehicle bus status'),
				('vd_dtc_status', '!VD - DTC status'),
				('vv_vehicle_detection_status', '!VV - vehicle detection status'),
			]		
			
			def v0_vbus_status():
				#resp = DeviceVal.Send_SMS_Sinch('!v0', mRE='=VBUS=')
				DeviceVal.Sleep(20)
				esn = DeviceVal.Send_SMS_SEND('!v0')
				DeviceVal.Send_SMS_REC(esn) 
				DeviceVal.Sleep(10)
			
			def vd_dtc_status():
				#resp = DeviceVal.Send_SMS_Sinch('!vd', mRE='=VBUS DTC=')
				DeviceVal.Sleep(20)
				esn = DeviceVal.Send_SMS_SEND('!vd')
				DeviceVal.Send_SMS_REC(esn) 
				DeviceVal.Sleep(10)
				
			def vv_vehicle_detection_status():
				#resp = DeviceVal.Send_SMS_Sinch('!vv', mRE='-Veh Disc-')
				DeviceVal.Sleep(20)
				esn = DeviceVal.Send_SMS_SEND('!vv')
				DeviceVal.Send_SMS_REC(esn) 
				DeviceVal.Sleep(10)

		Run_Subtests(Subtests)			
			
	@Test_Case
	def wakeup_on_ignition():
		Serial_Cmd('AT$APP PARAM 265,0,0', 'OK') # timer 0 - 0 seconds
		Serial_Cmd('AT$APP PARAM 1029,0,255,1', 'OK') # wakeup from sleep on ignition on
		Serial_Cmd('AT$APP PEG ACTION 22 0', '') # force sleep indefinitely until trigger

		# trigger ignition on
		# check device running
		
	@Test_Case
	def wakeup_on_motion():
		Serial_Cmd('AT$APP PARAM 265,0,0', 'OK') # timer 0 - 0 seconds
		Serial_Cmd('AT$APP PARAM 1029,0,255,1', 'OK') # wakeup from sleep on motion
		Serial_Cmd('AT$APP PEG ACTION 22 0', '') # force sleep indefinitely until trigger
		
		#if(motion != 'No'):						#checks ini 
		#	Serial_Cmd('AT$APP PARAM 265,0,0', 'OK') # timer 0 - 0 seconds
		#	Serial_Cmd('AT$APP PARAM 1029,0,255,1', 'OK') # wakeup from sleep on motion  1029 0-1
		#	Serial_Cmd('AT$APP PEG ACTION 22 0', '') # force sleep indefinitely until trigger
		#else:
		#	ATE2.Log('Motion Detector not enabled.')
		# trigger motion
		# check device running
		
	@Test_Case
	@Debug
	def wakeup_on_time_of_day():
		tod = DeviceVal.Calc_TOD_With_Offset(35)
		print(tod)
		Serial_Cmd('AT$APP PARAM 267,0,%s' % (tod), 'OK')  # current time + delta
		Serial_Cmd('AT$APP PEG ACTION 24 0', 'OK')  # device sleep and wakeup on timer 0
		Serial_Cmd('', 'Sleep.*sec', rdRetryCnt=45)
		Serial_Cmd('', 'VERSION', rdRetryCnt=60)
		
		# 267,0,<gps current time + delta>
		# peg 24 0   # cause device to sleep and wakeup on timer 0 
		# check that wakeup occurred
	
		# 1030 - local time zone
		# at$app time?
		
	@Test_Case
	def fill_log_reset():
		Serial_Cmd('at$app comm off', '^OK')
		Serial_Cmd('at$app clear log', '^OK', rdRetryCnt=10)
		try:
			Serial_Cmd('at$app log fill 50', '^OK')  
		except:
			Serial_Cmd('at$app log qfill 50', '^OK')
		DeviceVal.Sleep(20)
		#Serial_Cmd('at$app log?', '50 Records', filter='Log Status:', cmdRetryCnt=10)
		Serial_Cmd('at$app comm on', '^OK')
		DeviceVal.Check_Connected()
		DeviceVal.Reset()
		Serial_Cmd('at$app log?', '0 Records', filter='Log Status', cmdRetryCnt=15)
	
	@Test_Case
	def log_batchmode():
		Serial_Cmd('at$app comm off', '^OK')
		Serial_Cmd('at$app clear log', '^OK', rdRetryCnt=10)
		Serial_Cmd('at$app peg action 1 100', '^OK')
		Serial_Cmd('at$app peg action 2 200', '^OK')
		Serial_Cmd('at$app peg action 3 300', '^OK')
		Serial_Cmd('at$app log?','3 [Records | Recs]',filter='Log Status',cmdRetryCnt=10) #8bits require Recs 
		Serial_Cmd('at$app param 1024,20,32,32', '^OK') #LMU will use Batch mode on next power up
		#Serial_Cmd('atreset','')
		DeviceVal.Power_Reset()
		Serial_Cmd('at$app comm on', '^OK',rdRetryCnt=20,rdRetryDelay=5)
		DeviceVal.Check_Connected()
		Serial_Cmd('at$app log?','3 Records',filter='Log Status',cmdRetryCnt=10)
		

	@Test_Case
	def sync_time():
		pass
# Added Test Cases:
# 1. Prevent system time being sync'd to GPS time until GPS has a 3D fix ( 1-78, V5.0c Release Notes)
# Procedure:
# - Set s178=16
# - Power cycle the LMU
# - Watch for LMU's system time

# The result should be: device system time syncs to 3D GPS time.  
 
	@Test_Case
	def comm_mode_4():
		pass
# 2. COMM mode 4 added ( 2 radio and 3 logs ) ( 3 logs not supported yet) ( Item 1-80 V5.0c Release Notes)
# 14:55:47.432> at$app param? 1068,*
# 14:55:47.432> 1068,0,4
# 14:55:47.432> 
# 14:55:47.432> OK
# 14:55:48.712> ati0 
# 14:55:48.712> APP:LMU,184 V5.0c (Aug 19 2016 17:55:45) 
# 14:55:48.712> PIC:STM S/N 4832001478 
# 14:55:48.712> GPS:UBLOX-00040007 -7.03 (45969) 
# 14:55:48.712> Radio:u-blox LEON-G100-07.60.00+ 
# 14:55:48.712> Radio1:u-blox LEON-G100-07.60.00+
# 14:55:48.712> 
# 14:55:48.712> OK

	@Test_Case
	@Debug
	def peg_triggers():
		import Triggers

	@Test_Case
	@Debug
	def security():
		# Grabing Info
		ati0 = Serial_Cmd('ati0', 'APP', filter='.*')
		fwBuild = re.search('.*[V](\S{4})',ati0).group(1)
		secSum = Serial_Cmd('at#sec?', '#SEC: ',rdRetryCnt=15, initDelay = 1, filter='\d{2}')
		print(secSum)
		secSum = 7
		# Using DeviceVal.Get_ESN() cause subtest not to run
		# If functions aren't defined no error is displayed
		ESN = re.search('.*:\s*(.*)', Serial_Cmd('AT#ESN?', 'ESN:')).group(1)
		secSum = int(secSum)
		ESN = int(ESN)

		class SubtestList:
			Name = 'Security Tests'

			# Full Test List Header
			SubtestList = [
				#('sec_firmware_check','Security: Firmware Check'),
				#('sec_dwnld_key_file','Security: Downloading Key File'),
				# Security: File Authentication is part of Boyang's Function
				('sec_message_auth','Security: Message Aithentication'),
				('sec_SMS_auth','Security: SMS Authentication'),
				('sec_disable_firmware_dngrde','Security: Disable Firmware Downgrade'),
				('sec_disable_SMS','Security: Disable SMS'),
			]
			# Run List
			SubtestList = [
				#('sec_firmware_check','Security: Firmware Check'),
				#('sec_dwnld_key_file','Security: Downloading Key File'),
				# Security: File Authentication is part of Boyang's Function
				('sec_message_auth','Security: Message Aithentication'),
				#('sec_SMS_auth','Security: SMS Authentication'),
				#('sec_disable_firmware_dngrde','Security: Disable Firmware Downgrade'),
				#('sec_disable_SMS','Security: Disable SMS'),
			]

			def sec_firmware_check():
				if int(fwBuild[0]) < 6:
					ATE2.Log('Security tests are only for builds 6.0a and above')
					TC_Fail('Invalid Firmware Build: ', fwBuild)
				ATE2.Log('\tPASS: Firmware Supports Level 1 Security!')
			def sec_dwnld_key_file():
				ATE2.Log('\tClearing Possible Key Files (Factory Restore)')
				Serial_Cmd('at#sec 0','OK')
				DeviceVal.Factory()
				#NOTE: All enable checks (first three) are set on no matter what till there is a at#factory and at#sec 0
				if DeviceVal.Get_Config_Value('RUN', 'KeyFile'):
					keyFileType = DeviceVal.Get_Config_Value('RUN', 'KeyFile')
					print(keyFileType)
					fileDirectory = 'C:\\Users\\labuser\\Desktop\\Tests-1101-2000\\KeyFiles'
					if not os.path.exists(fileDirectory):
						os.makedirs(fileDirectory)
					if (os.getcwd() != fileDirectory):
						os.chdir('KeyFiles')
						# LMU.Ymodem (Not Config Correctly For This Operation)
					DeviceVal.Ymodem_File_Transfer('atdsf',fileDirectory,keyFileType)
					Serial_Cmd('ats125=7','OK')
					Serial_Cmd('atpf 0 18','OK')
					DeviceVal.Sleep(10)
					try:
						Serial_Cmd('atreset', 'KeyFile:',cmdRetryCnt=5,filter='.*')
						secSum = Serial_Cmd('at#sec?', '#SEC: ',rdRetryCnt=15, initDelay = 1, filter='\d{2}')
						secSum = int(re.search(':(.*)', secSum).group(1))
						print(secSum)
					except:
						print('KeyFile not loaded')
						TC_Fail('No KeyFile Found')
				else:
					ATE2.Log('\tKeyFile Attribute Not Found Within .ini and/or value is not assigned.')
					TC_Fail('Check ini file and KeyFiles directory for keyfile.')
			# File Authentication is on Boyang's function
			def sec_message_auth():
				print('Entering Message Auth')
				if secSum >= 2:
					ATE2.Log('\tMessage Authentication Enabled')
					try:
						DeviceVal.Check_Connected()
						print('Device Connnected!')
					except:
						TC_Fail('Device Must Have a Connection to Perform This Test!\nCommon Errors:\nConnection interrupting script loaded on the device.\nThis device needs an antenna setup.')
					Serial_Cmd('at$app param 2320,0,"testpuls.calamp.com"', 'OK')
					Serial_Cmd('at$app param 2319,0,"gpstrax.gieselman.com"', 'OK')
					# Add Device to gpstrax (With Incorrect Auth Config) If not already added.
					# We are going to add 'golden' devices that will be used for testing.
					ATE2.Operator_Instruction('Please modify the Message Authentication Seed so it is INVALID. GPSTrax>Devices>Edit> Change it to 0180C0E171B9DD190D87C3E0F02A140B')
					# Disabling firewall temp and sending an event report
					Serial_Cmd('at$app peg action 1 1', 'OK')
					DeviceVal.Sleep(10)
					print('Sending an ID Report')
					debugBuffer = DeviceVal.GPSTRAX_RESTful_API_SendIDReport(ESN)
					indexNum = 0
					done = False
					error = False
					print(debugBuffer[0])
					while not done and debugBuffer.__len__() > indexNum:
							try:
								m = re.search('Unit Request\s\((\d*)\)', debugBuffer[indexNum])
								print('Found: %s' % str(m))
								if str(m.group(0)) != 'None':
									print(re.search('Unit Request\s\((\d)\)', debugBuffer[indexNum]).group(0))
									try:
										if str(re.search('MsgAuth Invalid', debugBuffer[indexNum + 1])) =='None':
											error = True
											print(debugBuffer[indexNum + 1])
											done = True
									except:
										error = True
										break
									break
							except:
								indexNum += 1
					if error:
						TC_Fail('\tUnauthenticated message was NOT blocked!')
					ATE2.Log('\tUnauthenticated message was blocked!')
					ATE2.Operator_Instruction('Please modify the Message Authentication Seed so it is VALID. GPSTrax>Devices>Edit> Change it to 0180C0E171B9DD190D87C3E0F02A140A')
					# Disabling firewall temp and sending an event report
					Serial_Cmd('at$app peg action 1 1', 'OK')
					DeviceVal.Sleep(10)
					print('Sending an ID Report')
					debugBuffer = DeviceVal.GPSTRAX_RESTful_API_SendIDReport(ESN)
					indexNum = 0
					done = False
					print(debugBuffer[0])
					while not done and debugBuffer.__len__() > indexNum:
							try:
								m = re.search('Unit Request\s\((\d*)\)', debugBuffer[indexNum])
								print('Found: %s' % str(m))
								if str(m.group(0)) != 'None':
									print(re.search('Unit Request\s\((\d)\)', debugBuffer[indexNum]).group(0))
									try:
										if str(re.search('MsgAuth Invalid', debugBuffer[indexNum + 1])) =='None':
											print(debugBuffer[indexNum + 1])
											done = True
											break
									except:
										error = False
										break
									break
							except:
								indexNum += 1
					if error:
						TC_Fail('\tAuthenticated message was blocked!')
				else:
					ATE2.Log('\tMessage Authentication Disabled')
					ATE2.Operator_Instruction('Please modify the Message Authentication Seed so it is INVALID. GPSTrax>Devices>Edit> Change it to 0180C0E171B9DD190D87C3E0F02A140B')
					# Disabling firewall temp and sending an event report
					Serial_Cmd('at$app peg action 1 1', 'OK')
					# Disabling firewall temp and sending an event report
					Serial_Cmd('at$app peg action 1 1', 'OK')
					DeviceVal.Sleep(10)
					print('Sending an ID Report')
					debugBuffer = DeviceVal.GPSTRAX_RESTful_API_SendIDReport(ESN)
					indexNum = 0
					done = False
					print(debugBuffer[0])
					while not done and debugBuffer.__len__() > indexNum:
							try:
								m = re.search('Unit Request\s\((\d*)\)', debugBuffer[indexNum])
								print('Found: %s' % str(m))
								if str(m.group(0)) != 'None':
									print(re.search('Unit Request\s\((\d)\)', debugBuffer[indexNum]).group(0))
									try:
										if str(re.search('MsgAuth Invalid', debugBuffer[indexNum + 1])) =='None':
											print(debugBuffer[indexNum + 1])
											done = True
											break
										else:
											error = True
											break
									except:
										error = False
										break
									break
							except:
								indexNum += 1
					if error:
						TC_Fail('\tUnauthenticated message blocked!')
			def sec_SMS_auth():
				if secSum >= 16:
					ATE2.Log('\tAll SMS Communications are Disabled: Ignoring Test')
					pass
				else:
					if secSum >= 4:
						ATE2.Log('\tSMS Authentication Enabled')
						try:
							DeviceVal.Send_SMS_Sinch('!r0', mRE='SMS: MsgAuth Failed', fRE='SMS: MsgAuth Failed')
							ATE2.Log('\tAn Unauthenticated Message Failed to Be Recieved!')
							ATE2.Operator_Instruction('Please modify the SMS Message Authentication Seed so it is VALID. GPSTrax>Devices>Edit> Change it to 1F8EC7633099CC67B2D8EDF67B3D1F06')
							try:
								# Must send a message to recieve a 32 bit auth
								# CANNOT just send one message and SMS sinch might not cut it for the first message
								ATE2.Log('\tAn Auth SMS Message was sent and recieved as expected!')
								if DeviceVal.GPSTRAX_RESTful_API_SendSMS(ESN) == 200:
									pass
								else:
									TC_Fail('GPSTrax RESTful IPA Failed to send an SMS')
							except:
								TC_Fail('Auth Messsage Failed to send and be recieved')
						except:
							TC_Fail('Unauthenticated Message either failed to send or was successfully recieved even though SMS Auth is Enabled')
					else:
						ATE2.Log('\tSMS Authentication Disabled')
						# Try sending any sms request (!r0) It should go through
						try:
							DeviceVal.Send_SMS_Sinch('!r0', mRE='Resp.*LMD', fRE='Resp: APP:|Resp: INB:')
							pass
						except:
							TC_Fail('SMS Message Failed To Be Sent And/Or Recieved (Maybe check the connnection?)')
			# Trying the at#dngrd command?
			def sec_disable_firmware_dngrde():
				if secSum >= 8:
					ATE2.Log('\tFirmare Downgrade is Disabled')
					if fwBuild <= '6.0a':
						TC_Fail('Firmware downgrade fail. Need firmware above 60a to test feature')
					DeviceVal.Check_Connected()
					DeviceVal.PULS_Firmware_Set(ESN, '60a')
					Serial_Cmd('AT$APP PEG ACTION 49 129', 'OK')
					Serial_Cmd('ats125=7', 'OK')
					Serial_Cmd('','DNLD: Version 60a is not compatible!',cmdRetryCnt=15,filter='.*')
					ATE2.Log('\tPass:Firmware unable to downgrade with bit enabled')
				else:
					ATE2.Log('\tFirmare Downgrade is Enabled')
					DeviceVal.PULS_Firmware_Set(ESN, '60a')
					Serial_Cmd('AT$APP PEG ACTION 49 129', 'OK')
					Serial_Cmd('ats125=7', 'OK')
					try:
						Serial_Cmd('','DNLD: Version 60a is not compatible!',cmdRetryCnt=15,filter='.*')
						TC_Fail('Firmware downgrade should be Enabled')
					except:
						try:
							Serial_Cmd('','DNLD:Srvr:216.177.93.236,Port:80',cmdRetryCnt=20,filter='.*')
						except:
							TC_Fail('unable to downgrade firmware')
						Serial_Cmd('','Copyright (c) 1999-2017 CalAmp Corp.',cmdRetryCnt=1000,filter='.*')
			def sec_disable_SMS():
				if secSum >= 16:
					ATE2.Log('\tSMS Disabled')
					# Send any message you should recieve an error message -> SMS: Disabled, Security
					DeviceVal.Send_SMS_Sinch('!r0', mRE='SMS: Disabled, Security', fRE='SMS: Disabled, Security')
					ATE2.Log('\tDebug Correct Error Found!: SMS: Disabled, Security')
					pass
				else:
					ATE2.Log('\tSMS Enabled')
					try:
						# Need to have an auth sms message for working test for all cases (Edit Send after message if necessary)
						DeviceVal.Send_SMS_Sinch('!r0:1F8EC7633099CC67B2D8EDF67B3D1F06', mRE='Resp.*LMD', fRE='Resp: APP:|Resp: INB:')
						ATE2.Log('\tAn Auth SMS Message was sent and recieved as expected!')
						pass
					except:
						TC_Fail('SMS Message Failed To Be Sent And/Or Recieved (Maybe check the connnection and/or Auth Seed)')
				# OTA is slow, no OTA unless directed)
		Run_Subtests(SubtestList)
	# end of securtity tests

	@Test_Case
	def system_time_src_Server():
		Serial_Cmd('AT$APP PARAM 1069,0,23', '^OK') # disable all time sources except server
		DeviceVal.Power_Reset()
		DeviceVal.Check_Connected()
		Serial_Cmd('AT$APP PEG ACTION 92 0', '^OK')
		ATE2.Log('If this test failed for whatever reason make sure the battery is removed (b/c Power_Reset) \n')
		ATE2.Log('and also reset the time sync config (AT#RTCST)')
		Serial_Cmd('atit', '^Source 3', cmdRetryCnt=5, cmdRetryDelay=30, filter='^Source')
		Serial_Cmd('AT$APP PARAM 1069,0,0', '^OK')
		# Adding this to clear any time sync config - Teddy
		Serial_Cmd('at#rtcst 0', '.*OK')
		
	@Test_Case
	def system_time_src_reject_3D_GPS():
		Serial_Cmd('AT$APP PARAM 1069,0,1', '^OK') # enable 3D-GPS time rejection, param 1069 bit 0 
		DeviceVal.Power_Reset()
		DeviceVal.Check_Connected()
		m = Serial_Cmd('atit', '^Source [0|1|2|4]', cmdRetryCnt=5, cmdRetryDelay=30, filter='^Source')
		if re.search('Source 0', m):
			Serial_Cmd('AT$APP PARAM 1069,0,0', '^OK')
			# Adding this to clear any time sync config - Teddy
			Serial_Cmd('at#rtcst 0', '.*OK')
			TC_Fail('Bad time source', 'TIME_BADSOURCE')
		Serial_Cmd('AT$APP PARAM 1069,0,0', '^OK')
		# Adding this to clear any time sync config - Teddy
		Serial_Cmd('at#rtcst 0', '.*OK')
		
	@Test_Case
	def system_time_src_reject_RTC():
		Serial_Cmd('AT$APP PARAM 1069,0,2', '^OK') # enable RTC time rejection, param 1069 bit 1 
		DeviceVal.Power_Reset()
		DeviceVal.Check_Connected()
		m = Serial_Cmd('atit', '^Source [0|1|2|4]', cmdRetryCnt=5, cmdRetryDelay=30, filter='^Source')
		#DeviceVal.Sleep(10)
		if re.search('Source 1', m):
			Serial_Cmd('AT$APP PARAM 1069,0,0', '^OK')
			# Adding this to clear any time sync config - Teddy
			Serial_Cmd('at#rtcst 0', '.*OK')
			TC_Fail('Bad time source', 'TIME_BADSOURCE')		
		Serial_Cmd('AT$APP PARAM 1069,0,0', '^OK')	
		# Adding this to clear any time sync config - Teddy
		Serial_Cmd('at#rtcst 0', '.*OK')
									
	@Test_Case
	def system_time_src_reject_None_3D_GPS():
		Serial_Cmd('AT$APP PARAM 1069,0,4', '^OK') # enable RTC time rejection, param 1069 bit 2 
		DeviceVal.Power_Reset()
		DeviceVal.Check_Connected()
		m = Serial_Cmd('atit', '^Source [0|1|2|4]', cmdRetryCnt=5, cmdRetryDelay=30, filter='.*')
		if re.search('Source 2', m):
			Serial_Cmd('AT$APP PARAM 1069,0,0', '^OK')
			# Adding this to clear any time sync config - Teddy
			Serial_Cmd('at#rtcst 0', '.*OK')
			TC_Fail('Bad time source', 'TIME_BADSOURCE')
		Serial_Cmd('AT$APP PARAM 1069,0,0', '^OK')
		# Adding this to clear any time sync config - Teddy
		Serial_Cmd('at#rtcst 0', '.*OK')
		
	@Test_Case
	def system_time_src_reject_Server():
		# Adding this to clear any time sync config - Teddy
		Serial_Cmd('at#rtcst 0', '.*OK')
		Serial_Cmd('AT$APP PARAM 1069,0,8', '^OK') # enable RTC time rejection, param 1069 bit 3 
		DeviceVal.Power_Reset()
		DeviceVal.Check_Connected()
		Serial_Cmd('AT$APP PEG ACTION 92 0', '^OK')
		DeviceVal.Sleep(10)
		m = Serial_Cmd('atit', '^Source [0|1|2|3|4|5]', cmdRetryCnt=5, cmdRetryDelay=10, filter='^Source')
		if re.search('Source 3', m):
			Serial_Cmd('AT$APP PARAM 1069,0,0', '^OK')
			# Adding this to clear any time sync config - Teddy
			Serial_Cmd('at#rtcst 0', '.*OK')
			TC_Fail('Bad time source', 'TIME_BADSOURCE')
		Serial_Cmd('AT$APP PARAM 1069,0,0', '^OK')
		# Adding this to clear any time sync config - Teddy
		Serial_Cmd('at#rtcst 0', '.*OK')
		
	@Test_Case
	def system_time_src_reject_CellNetwork():
		# Adding this to clear any time sync config - Teddy
		Serial_Cmd('at#rtcst 0', '.*OK')
		Serial_Cmd('AT$APP PARAM 1069,0,16', '^OK') # enable network time rejection, param 1069 bit 4 
		DeviceVal.Power_Reset()
		DeviceVal.Check_Connected()
		m = Serial_Cmd('atit', '^Source [0|2|4|5]', cmdRetryCnt=5, cmdRetryDelay=10, filter='^Source')
		if re.search('Source 4', m):
			Serial_Cmd('AT$APP PARAM 1069,0,0', '^OK')
			# Adding this to clear any time sync config - Teddy
			Serial_Cmd('at#rtcst 0', '.*OK')
			TC_Fail('Bad time source', 'TIME_BADSOURCE')
		Serial_Cmd('AT$APP PARAM 1069,0,0', '^OK')
		# Adding this to clear any time sync config - Teddy
		Serial_Cmd('at#rtcst 0', '.*OK')
		
	@Test_Case
	def system_time_src_3D_GPS():
		# Adding this to clear any time sync config - Teddy
		Serial_Cmd('at#rtcst 0', '.*OK')
		Serial_Cmd('ats174=18', '^OK') # enable 3d gps system time, s174 bit 4,s174 default is 2
		DeviceVal.Reset()
		DeviceVal.Check_Connected()
		m = Serial_Cmd('atit', '^Source [0|2|4]', cmdRetryCnt=10, cmdRetryDelay=30, filter='^Source')
		if re.search('Source 2', m):
			Serial_Cmd('AT$APP PARAM 1069,0,0', '^OK')
			# Adding this to clear any time sync config - Teddy
			Serial_Cmd('at#rtcst 0', '.*OK')
			TC_Fail('Bad time source', 'TIME_BADSOURCE')
		# Adding this to clear any time sync config - Teddy
		Serial_Cmd('at#rtcst 0', '.*OK')

	@Test_Case
	def send_sms_with_encrypt_digest():
		resp = DeviceVal.Send_SMS_Sinch('!r0:C9000CCE7F35EAD986E2AF47D016D903')
		print(resp)
		
	@Test_Case	
	@Debug
	def TCP(): #view LC-2371 for details. Written by Boyang
		ATE2.Log("Test Applicaple to 7.1a and higher")
		print('Test Applicaple to 7.1a and higher')
		if FW < '7.1a':
			ATE2.Log("Firmware not 7.1a and greater")
			return 

		##########Pre-test settings#################
		Serial_Cmd('AT#FACTORY', '')
		Serial_Cmd('AT', 'OK', cmdRetryCnt=60)
		Serial_Cmd('at$app param 2306,0,%s' %(APN), 'OK')
		Serial_Cmd('at$app param 2306,1,%s' %(APN), 'OK')
		Serial_Cmd('at$app param 2319,0,%s' %(INBOUND), 'OK')
		Serial_Cmd('at$app param 2319,1,%s' %(INBOUND), 'OK')
		Serial_Cmd('at$app param 2320,0,%s' %(MAINTENANCE), 'OK')
		Serial_Cmd('at$app param 789,0,"M0L0Q0R0"', 'OK') #configure service 0
		Serial_Cmd('at$app param 789,1,"M0L1Q0R0"', 'OK') #configure service 1
		Serial_Cmd('at$app param 787,0,1', 'OK') #inbound TCP
		Serial_Cmd('at$app param 787,1,1', 'OK') #inbound TCP
		Serial_Cmd('at$app param 2330,0,1', 'OK') #maintenance TCP
		Serial_Cmd('ats181=1', 'OK') #enable TCP param read/write
		
		
		class Subtests:
		
			Name = 'TCP'
			
			SubtestList = [
				('TCP_Establish_Connection', 'Establish_TCP_Connection'),
				('TCP_Preemption', 'Preemption for TCP resource'),
				('TCP_Inbound_Preempt_500_Packets', 'Inbound_Preempt_500_Packets'),
				('TCP_Invalid_Connection', 'TCP_Reasource_Release'),
				('TCP_Reasource_Release', 'TCP_Reasource_Release'),
				('TCP_Four_Resources', 'TCP_Four_Resources'),
			]
				
			SubtestList = [
				('TCP_Establish_Connection', 'Establish_TCP_Connection'),
				('TCP_Preemption', 'Preemption for TCP resource'),
				('TCP_Reasource_Release', 'TCP_Reasource_Release'),
				('TCP_Inbound_Preempt_500_Packets', 'Inbound_Preempt_500_Packets'),
				('TCP_Invalid_Connection', 'TCP_Reasource_Release'),
				#('TCP_Four_Resources', 'TCP_Four_Resources'),
			]
			@Debug
			def TCP_Establish_Connection(): #Verify TCP connection can be established
				#return
				DeviceVal.Reset()
				DeviceVal.Check_Connected()
				Serial_Cmd('', 'SKT: Connection established',cmdRetryCnt=60, filter='.*')
				
			@Debug
			def TCP_Preemption(): #Priority: OTA>GPS ASSIST>Maintenance > Inbound. This Test case will verify maintenance > inbound
				pass
			
			@Debug			
			def TCP_Inbound_Preempt_500_Packets(): #Verify inbound service can be preempt once existing service has 500 packets sent
				#return
				DeviceVal.Reset()
				DeviceVal.Check_Connected()
				Serial_Cmd('at$app peg action 49 129', 'SKT: Connection established',cmdRetryCnt=60, filter='.*')
				Serial_Cmd('', 'SKT: Connection Closed',cmdRetryCnt=70, filter='.*')
				Serial_Cmd('at$app peg action 1 1', 'SKT: Connection established',filter='.*')
				Serial_Cmd('at$app peg route 1', '^OK')
				for i in range(0,10):
					Serial_Cmd('at$app peg action 1 1', '^OK')
				Serial_Cmd('at$app peg route 0', '^OK')
				buf = Serial_Cmd('atis', 'OK', retBuf= True, filter='.*')
				if 'log(1:10)' not in buf[3]: TC_Fail('Logs were not properly initialize')	
				Serial_Cmd('at$app log fill 250', '^OK')
				DeviceVal.Sleep(120)
				Serial_Cmd('', 'SKT: Connection Closed', rdRetryCnt = 200, initDelay = 0, cmdRetryDelay=0, filter = '.*')
				DeviceVal.Sleep(25)
				buf = Serial_Cmd('atis', 'OK', retBuf= True, filter='.*')
				if 'log(1:0)' not in buf[3]: TC_Fail('Failed to send all logs')	

				
			@Debug	
			def TCP_Invalid_Connection(): #Verify TCP resource is not used up for invalid TCP connections NOTE: ONLY APPLICABLE TO DEVICES WITH 1 TCP RESOURCE(3030)
				#return
				DeviceVal.Reset()
				DeviceVal.Check_Connected()
				Serial_Cmd('', 'SKT: Connection established',cmdRetryCnt=60, filter='.*')
				Serial_Cmd('at$app param 769,0,20520', '^OK', filter='.*')
				Serial_Cmd('at$app peg route 1', '^OK', filter='.*')
				for i in range(0,10):
					Serial_Cmd('at$app peg action 1 1', '^OK', filter='.*')
				Serial_Cmd('at$app peg route 0', '^OK', filter='.*')
				Serial_Cmd('at$app peg action 1 1', '^OK', filter='.*')
				Serial_Cmd('', 'SKT: Connection Closed',cmdRetryCnt=70, filter='.*')
				#Serial_Cmd('', 'TCP: preempt resource',cmdRetryCnt=70, filter='.*')
				DeviceVal.Sleep(20)
				buf = Serial_Cmd('atis', 'OK', retBuf= True, filter='.*')
				if 'log(1:0)' not in buf[3]: TC_Fail('Failed to send all logs')	
				Serial_Cmd('at$app param 769,0,20500', '^OK')
				
			@Debug
			def TCP_Reasource_Release(): #Verify upon release of TCP resource, other services can use resource if it has pending items to be sent
				#return
				DeviceVal.Reset()
				DeviceVal.Check_Connected()
				Serial_Cmd('', 'SKT: Connection established',cmdRetryCnt=60, filter='.*')
				Serial_Cmd('', 'SKT: Connection Closed',cmdRetryCnt=70, filter='.*')
				Serial_Cmd('at$app peg action 1 1', 'SKT: Connection established',filter='.*')
				#Serial_Cmd('', 'SKT: Connection established',filter='.*',cmdRetryCnt=10)
				Serial_Cmd('at$app peg route 1', '^OK')
				for i in range(0,10):
					Serial_Cmd('at$app peg action 1 1', '^OK')
				Serial_Cmd('', 'SKT: Connection Closed',cmdRetryCnt=70, filter='.*')
				DeviceVal.Sleep(20)
				buf = Serial_Cmd('atis', 'OK', retBuf= True, filter='.*')
				if 'log(1:0)' not in buf[3]: TC_Fail('Failed to send all logs')	
				
				
		Run_Subtests(Subtests)
		
	@Test_Case
	def script_loading():
		esn = re.search('.*:\s*(.*)', Serial_Cmd('AT#ESN?', 'ESN:')).group(1)
		#pcba = re.search('.*:\s*(.*)', Serial_Cmd('AT#PCBA?', 'PCBA:')).group(1)
		param1, param2 = (DeviceVal.Get_Config_Value('RUN', 'Parameter Migration')).split('/')
		module = Serial_Cmd('ATI2', 'GENERIC', filter='.*')
		module = re.search('[^\s]+',module).group(0)
		#module = 'HSPA'#######################################################################################################################
		ati0 = Serial_Cmd('ATI0', 'APP', filter='.*')
		unit = re.search('.*[:](\w{3})',ati0).group(1)
		appID = re.search('.*[,](\S{3})',ati0).group(1)
		fw = re.search('.*[V](\S{4})',ati0).group(1)

		bldFiles = {
		'fw1': 'LMU-%s-%s-%s.bin' %(module,appID,param1), #firmware to upgrade from
		'fw2': 'LMU-%s-%s-%s.bin' %(module,appID,param2), #firmware to upgrade to
		'script':'32bit_Init_50j.csv',
		'script2':'250.253-Hino Jasper APN Transition script.csv',
		'script3':'3.85-LMU30C5V3-CON01.csv'
		}
		bldFiles1 = {
		'fw1': 'TTU-%s-%s-%s.bin' %(module,appID,param1), #firmware to upgrade from
		'fw2': 'TTU-%s-%s-%s.bin' %(module,appID,param2), #firmware to upgrade to
		}
		if unit == 'LMU':
			print('Using LMU')
			mode = bldFiles
		else:
			print('Using TTU')
			mode = bldFiles1
		firmwarePath = os.getcwd() + '\\fw'
		fw = fw.replace(".", "")

		if fw[0] > param1[0]: #proper way to downgrade firmwares ex: 5 > 4
			print('MUST DOWNGRADE PROPERLY\n')
			ATE2.Log('MUST DOWNGRADE PROPERLY')			
			if fw < '50j':
				Serial_Cmd('at#flash-eraseall', 'OK', rdRetryCnt=60, initDelay = 1)
				DeviceVal.Ymodem_File_Transfer('atdnld', firmwarePath, mode['fw1'])
				DeviceVal.Sleep(45)
				Serial_Cmd('AT', 'OK', cmdRetryCnt=60)
				DeviceVal.Factory()
				DeviceVal.Reset()
				print('IF if')
			else:
				DeviceVal.Ymodem_File_Transfer('at#dngrd', firmwarePath, mode['fw1'])
				DeviceVal.Sleep(45)
				Serial_Cmd('AT', 'OK', cmdRetryCnt=60)
				DeviceVal.Factory()
				DeviceVal.Reset()
				print('IF else')
			
			Serial_Cmd('AT', 'OK', cmdRetryCnt=60)
			Serial_Cmd('AT#ESN %s OVERRIDE' % (esn), 'OK', cmdRetryCnt=3)
			Serial_Cmd('at#nvminit', 'OK', rdRetryCnt=60, initDelay = 1)
		
		elif fw == param1: # the case when nothing needs to be done ex: firmwares are the same
			print('elif')
			pass
		else: # the case when upgrade needs to be done or within the same firmware number ex 50c and 50j
			print('Executing else')
			print(fw)
			print(param1)
			param1 = param1.replace(".", "")
			print(param1)
			DeviceVal.Ymodem_File_Transfer('atdnld', firmwarePath, mode['fw1'])
			DeviceVal.Sleep(45)
			Serial_Cmd('AT', 'OK', cmdRetryCnt=60)
			DeviceVal.Factory()
			print('Else Statement executed')
			
		#Serial_Cmd('AT#PCBA %s' % (pcba), 'OK', cmdRetryCnt=3)
		DeviceVal.Ymodem_File_Transfer('atdscfg', firmwarePath, bldFiles['script']) #comment
		DeviceVal.Sleep(5)		
		Serial_Cmd('AT$APP PEG ACTION 39 0', 'OK', cmdRetryCnt=5) #comment
		kevins_test()  #comment
		DeviceVal.Ymodem_File_Transfer('atdnld', firmwarePath, mode['fw2'])
		DeviceVal.Sleep(45)
		Serial_Cmd('AT', 'OK', cmdRetryCnt=60)
		kevins_test()

	
		
		
		
	@Test_Case
	def kevins_test():
		ati0 = Serial_Cmd('ATI0', 'APP',rdRetryCnt=60, initDelay = 1, filter='.*')
		ati0 = re.search('.*[,](.*)([\s]+)',ati0).group(1)
		appID = ati0[:3]
		fw = ati0[5:9]
		DeviceVal.Sleep(5)
		Serial_Cmd('AT$APP GPS OFF', 'OK')
		fileDump = 'Z:\\Users\\bFan\\Master\\Tests-1101-2000\\Parameter Migration'
		os.chdir(fileDump)
		print('!!!!!!!!!!!!!')
		print(os.getcwd())
		print('!!!!!!!')
		if not os.path.exists(fileDump):
			os.makedirs(fileDump)
		if(os.getcwd() != fileDump):
			os.chdir('Parameter Migration')
						
		target = open('[%s]param-mismatch[%s]2.txt' % (appID, fw), 'w+') #file to be written to if parameters are not matching
		target2 = open('[%s]param-error[%s]2.txt' % (appID, fw), 'w+') # file to be written to if the return is "ERROR"
		with open("32bit_Init_50j.txt") as f:
		#with open("32bit_Init_50j.txt") as f:
			for line in f:
				line = line.rstrip()
				param, slot, value = re.search ('^(\d*),(\d*),(.*)', line).group(1, 2, 0)
				buf = Serial_Cmd('AT$APP PARAM? %s,%s' % (param, slot), '^%s' % (param), rdRetryCnt=1, initDelay = 0.1, noFail=True)
				if param == '2691':
					if fw <= '5.0c':
						print('Bug with the parameter------skipping')
						ATE2.Log('Bug with parameter 2691 before version 5.0c. SKIPPING')
						continue
				if buf == '':
					print('no param - [%s] [%s]' % (buf, value))
					ATE2.Log('no param - [%s] [%s]' % (buf, value))
					target2.write('exp = [%s] act = [%s]\n' % (value, buf))
					continue
				if buf == value:
					continue
				else:
					print('mismatch - [%s] [%s]' % (buf, value))
					ATE2.Log('mismatch - [%s] [%s]' % (buf, value))
					target.write('exp = [%s] act = [%s]\n' % (value, buf))
		target.close()
		target2.close()
		
	@Test_Case
	def Multiprofile_Wifitest():
		wifi = DeviceVal.Get_Config_Value('RUN','Wifi')
		if wifi == 'odin':
			ATE2.Log('Enabling ODIN board to test wifi')
			Serial_Cmd('ATS150=49', 'OK')
			Serial_Cmd('ATS171=1', 'OK')
			Serial_Cmd('ATS177=0', 'OK')
			Serial_Cmd('AT$APP PARAM 3072,7,10', 'OK')
			Serial_Cmd('AT$APP PARAM 3072,9,3', 'OK')
			Serial_Cmd('AT$APP PARAM 3072,15,13', 'OK')
			Serial_Cmd('AT$APP PARAM 3072,17,8', 'OK')
			Serial_Cmd('AT$APP PARAM 1068,0,2', 'OK')
		elif wifi == 'connectone':
			ATE2.Log('Enabling Wifi setting for connectone')
			Serial_Cmd('ATS150=20', 'OK')
			Serial_Cmd('ATS177=184', 'OK')
			Serial_Cmd('AT$APP PARAM 3072,9,3', 'OK')
			Serial_Cmd('AT$APP PARAM 3073,9,12', 'OK')
			Serial_Cmd('AT$APP PARAM 3074,9,3', 'OK')
			Serial_Cmd('AT$APP PARAM 1052,1,131072,131072', 'OK')
		else:
			TC_Fail('WIFI information not specified', 'WIFI_INFO_RUN')
		
		Serial_Cmd('AT$APP PARAM 3200,0,255', 'OK') #Multi-profile mode
		#Serial_Cmd('AT$APP PARAM 3200,0,0', 'OK') # if set will only use profile 0 and ignore other profiles
		
		Serial_Cmd('AT$APP PARAM 3201,0,1', 'OK') #STATION_MODE (1=MODE_INFRASTRUCTURE)
		Serial_Cmd('AT$APP PARAM 3202,0,3', 'OK') #ENCRYPTION (0=open,1=WEP,2=WPA,3=WPA2,4=MIX,5=WPA2ENTR)
		Serial_Cmd('AT$APP PARAM 3203,0,1', 'OK') #IP_MODE (0=manual,1=DHCP)
		Serial_Cmd('AT$APP PARAM 3204,0,1', 'OK') #KEYINDEX
		Serial_Cmd('AT$APP PARAM 3205,0,"LmuWiFiNet"', 'OK') #SSID-0
		Serial_Cmd('AT$APP PARAM 3206,0,"wifiuser"', 'OK') #KEY "password"-0
		
	@Test_Case
	@Debug
	def ota_obd_fw_db():
		fW, appid = (DeviceVal.Get_Config_Value('RUN', 'Ident')).split('/')
		esn = re.search('.*:\s*(.*)', Serial_Cmd('AT#ESN?', 'ESN:')).group(1)
		upgradeDB = DeviceVal.Get_Config_Value('RUN', 'OBD Database Upgrade List')
		upgradeOFW = DeviceVal.Get_Config_Value('RUN', 'OBD Firmware Upgrade List')
		devicecompleted = False
		
		##@@@@@@@@@@@@@@@@@@@FIRMWARE@@@@@@@@@@@@@@@@@
		upgradeBuildTest = DeviceVal.Get_Config_Value('RUN', 'OBD Firmware Upgrade List')
		print (upgradeBuildTest)
		upgrade = upgradeBuildTest.split(',')
		for build in upgrade:
			print ('Requested build to upgrade is ',build)
		
			customerApiKey = "Qehfd-gNO8nBZuRgsG_8cb4DPWEtS-5AEwzqNCrv9dqoGr0iGXNM2l3mklqi0aN2" 
			print('Upgrading Firmware to -> ' + upgradeOFW )
			print('************')
			#
			resp = DeviceVal.PULS_OBD_FW_Set(esn,build,customerApiKey)  #set obd firmware
			print('POST DEVICEVAL OBD FW SET')
			Serial_Cmd('at$app peg action 49 129','^OK', cmdRetryCnt=30, cmdRetryDelay=3)
			print('sleeping')
			DeviceVal.Sleep(180)
			a = Serial_Cmd('ati0','APP',filter ='.*')
			b = Serial_Cmd('ativ','FWVer',filter ='.*')
			
			print('aaaaaaaaaaa')
			print(a)
			print(b)
			print('aaaaaaaaaaaa')
			print(resp)
			print('looping')
	
		#Serial_Cmd('at$app peg action 49 129','^OK', cmdRetryCnt=30, cmdRetryDelay=3)
		##DeviceVal.Sleep(300)
		##Serial_Cmd('at$app peg action 49 129','^OK', cmdRetryCnt=30, cmdRetryDelay=3)
		
		print('1^^^^^^^^^^^^')
		jsonObj = DeviceVal.PULS_Device_Info(esn)
		print('2^^^^^^^^^^^^')
		#status = jsonObj['vbusDeviceFiles'][1]['status']
		#status = jsonObj['vbusFileHistories'][0]['status']

		print('3^^^^^^^^^^^^')

		# print('********')
		# print (status)
		# print('********')
							
		##if status == 'Completed':
		##	devicecompleted = True
			
			
		##if not devicecompleted:
		##	TC_Fail('Download of OBD Failed.','PULS_DNLD-FAIL')
		print('END OF TEST')
		print('$$$$$$$$$$$$$$$')
					
					#maybe check for post "sending obd firmware data" 
	
Run_Tests(BasicTests)
