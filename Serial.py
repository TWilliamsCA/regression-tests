import logging, re, serial, time
import DeviceVal, TestCase
from serial.tools import list_ports


Logger = logging.getLogger('DeviceVal_Logger.Serial')

Debug_Port = None

def Get_Debug_Port():
	ports = Get_Port_List()
	global Debug_Port 
	for portRec in ports:
		if re.search('Prolific', portRec[1]):
			Debug_Port = portRec[0]
			Logger.debug('Debug port [%s]' % (Debug_Port))
	if Debug_Port is None:
		Debug_Port = 'COM1'
		Logger.debug('Default Debug port [%s]' % (Debug_Port))
			
def Get_Port_List():
	portList = list_ports.comports()
	return portList

def Serial_Match(regex, buf):
	matches = map(re.compile(regex).match, buf)
	return [m.group(1) for m in matches if m][0]

def Serial_Cmd(cmd, expect, port='', rdRetryCnt=3, initDelay=0.2, rdRetryDelay=1, retMatch=True, retBuf=False,
	exitOnMatch=False, cmdRetryCnt=1, cmdRetryDelay=1, noFail=False, errorRE=None, filter='', baudRate=115200, errText='',
	errCode='SERIAL_CMDFAIL'):
	global Debug_Port
	if retBuf: retMatch = False
	if filter == '':
		filter = expect
	elif filter == '*':
			filter = expect
	else:
		if filter is not None:
			filter = expect + '|' + filter
	try:
		if port == '':
			port = Debug_Port
		rdRetryCntSave = rdRetryCnt
		cmdRetryCntSave = cmdRetryCnt
		ser = serial.Serial(port=port, baudrate=baudRate, timeout=1)
		if cmd != '':
			ser.flushInput()
			ser.flushOutput()
		# loop thru cmd retries
		while True:
			cmdRetryCnt -= 1
			rdRetryCnt = rdRetryCntSave
			outBuf = []
			Logger.debug('%%%% [%s] [%s] [%s]' % (cmd, expect, port))
			if cmd != '':
				ser.write(bytes(cmd + '\r', 'utf-8'))
			time.sleep(initDelay)
			# loop thru read retries
			while True:
				cnt = 0
				while True:
					cnt += 1
					line = ser.readline().decode('latin-1', errors='ignore').strip()
					if len(line) > 0:
						filterLine = False
						if filter is not None:
							if not re.search(filter, line):
									filterLine = True
						if not filterLine:
							outBuf.append(line)
							try:
								Logger.debug('%d %s' % (cnt, line.encode('latin-1').decode()))
							except UnicodeDecodeError:
								Logger.debug('%d %s' % (cnt, line.encode('latin-1')))
					if not ser.inWaiting() or exitOnMatch:
						break
				for line in outBuf:
					if errorRE is not None:
						for expr in errorRE:
							if re.search(expr, line):
								Logger.debug('>>>> fail match: %s' % (line))
								TestCase.TC_Fail('Matched fail expression [%s] - see Detail Log' % (expr), errorCode='SERIAL_MATCHEDFAILEXPR')
					if re.search(expect, line):
						ser.close()
						try:
							Logger.debug('>>>> match: %s' % (line.encode('latin-1').decode()))
						except:
							Logger.debug('>>>> match: %s' % (line.encode('latin-1')))
						if retMatch:
							return line
						else:
							return outBuf
				rdRetryCnt -= 1
				if rdRetryCnt == 0 and cmdRetryCnt == 0:
					ser.close()
					if noFail:
						return ''
					else:
						t = 'Serial cmd failed [%s] [%s] [%dc%ds:%dr%ds]' % (cmd, expect, cmdRetryCntSave, cmdRetryDelay, rdRetryCntSave, rdRetryDelay)
						TestCase.TC_Fail(t, errText=errText, errCode=errCode)
				elif rdRetryCnt > 0:
					Logger.debug('-- %d' % (rdRetryDelay))
					time.sleep(rdRetryDelay)
				else:
					Logger.debug('== %d' % (cmdRetryDelay))
					time.sleep(cmdRetryDelay)
					break
	except:
		# Logger.debug('** Caught serial exception')
		try:
			ser.flushInput()
			ser.flushOutput()
			ser.close()
		except:
			pass
		raise
		