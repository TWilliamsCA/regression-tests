import datetime, inspect, logging, sys, traceback
import DeviceVal
import Serial
import ATE2
import colorama
from colorama import Fore, Back, Style

### Initializations
###

colorama.init()
Logger = logging.getLogger('DeviceVal_Logger.Serial')

tcResult = uuidRun = tcSuite = uuidTC = None
tcPass = tcFail = tcSubPass = tcSubFail = 0
tcErrCode = tcErrText = None
tcRecs = []
tcDetails = []

### Exception classes
###

class Error(Exception):
	pass
	
class TestCaseError(Error):
	pass

### Decorators
###

def Debug(tcFunc):
	def wrapper():
		Serial.Serial_Cmd('ATS125=7', '^OK', cmdRetryCnt=3, filter='*')
		Logger.debug('!! Debug on')
		try:
			tcFunc()
		except TestCaseError:
			raise TestCaseError
		Serial.Serial_Cmd('ATS125=0', '^OK', cmdRetryCnt=3, filter='*')
		Logger.debug('!! Debug off')
	return wrapper

def Test_Case(tcFunc):

	def wrapper(tcCnt, tcName, tcDesc):
		global tcResult, tcPass, tcFail, tcRec, uuidTc, tcErrCode, tcErrText
		Logger.debug('')
		Logger.debug('')
		Logger.debug('%s!! Test Case [#%d] [%s] [%s]%s' % (Fore.BLACK+Back.WHITE, tcCnt, tcName, tcDesc, Style.RESET_ALL))
		ATE2.Log_Test('Test Case [#%d] [%s] [%s]' % (tcCnt, tcName, tcDesc))
		Logger.debug('')
		tcStartTime = datetime.datetime.now()
		tcResult = 'PASS'
		tcErrCode = tcErrText = ''
		try:
			tcFunc()
		except TestCaseError:
			tcResult = 'FAIL'
		except Exception as error:
			# ATE2.Log("%s" % error)
			Logger.debug(traceback.format_exc())
			TC_Fail('General script error - see log', 'GEN_SCRIPTERR', raiseError=False)
			tcResult = 'FAIL'

		if tcResult == 'PASS':
			tcPass += 1
		else:
			tcFail += 1
		tcEndTime = datetime.datetime.now()
		elapsedTime = tcEndTime - tcStartTime
		dur = round(elapsedTime.seconds + (elapsedTime.microseconds/1000000), 2)
		uuidTc = DeviceVal.Get_UUID()
		tcThis = (uuidTc, tcSuite, tcName, tcCnt, tcResult, tcErrCode, tcErrText, tcStartTime, tcEndTime, dur, tcSubCnt, tcSubPass, tcSubFail)
		tcRecs.append(tcThis)
		Logger.debug('Test Case Duration = %.2f sec' % (dur))
		if tcResult is 'PASS':
			Logger.debug('%s Test Case [%s] %s ' % (Fore.BLACK+Back.GREEN, tcResult, Style.RESET_ALL))
		else:
			Logger.debug('%s Test Case [%s] %s ' % (Fore.BLACK+Back.RED, tcResult, Style.RESET_ALL))
	return wrapper
	
def Subtest(tcFunc, tcCnt, tcName, tcDesc):
	global tcResult, tcSubPass, tcSubFail, tcSubSuite
	Logger.debug('')
	Logger.debug('%s!! Subtest [#%d] [%s] [%s]%s' % (Fore.BLACK+Back.WHITE, tcCnt, tcName, tcDesc,  Style.RESET_ALL))
	ATE2.Log_SubStep('!! Subtest [#%d] [%s] [%s]' % (tcCnt, tcName, tcDesc))
	tcStartTime = datetime.datetime.now()
	tcSubResult = 'PASS'
	try:
		tcFunc()
	except TestCaseError:
		tcSubResult = 'FAIL'
	except Exception as error:
		Logger.debug(traceback.format_exc())
		TC_Fail('General script error - see log', 'GEN_SCRIPTERR', raiseError=False)
		tcResult = 'FAIL'
	if tcSubResult == 'PASS':
		tcSubPass += 1
	else:
		tcSubFail += 1
		tcResult = 'FAIL'
	tcEndTime = datetime.datetime.now()
	elapsedTime = tcEndTime - tcStartTime
	dur = round(elapsedTime.seconds + (elapsedTime.microseconds/1000000), 2)
	uuidTc = DeviceVal.Get_UUID()
	print(uuidTc)
	tcCnt = float('%d.' % (TCcnt) + '{0:0>3}'.format(tcCnt))
	tcThis = (uuidTc, tcSubSuite, tcName, tcCnt, tcResult, tcErrCode, tcErrText, tcStartTime, tcEndTime, dur, 0, 0, 0)
	tcRecs.append(tcThis)
	Logger.debug('Subtest Duration = %.2f sec' % (dur))
	if tcSubResult is 'PASS':
		Logger.debug('%s Test Case [%s] %s ' % (Fore.BLACK+Back.GREEN, tcSubResult, Style.RESET_ALL))
	else:
		Logger.debug('%s Test Case [%s] %s ' % (Fore.BLACK+Back.RED, tcSubResult, Style.RESET_ALL))
	Logger.debug('Subtest [%s]' % (tcSubResult))

### Methods
###

def Run_Tests(testClass):
	global tcPass, tcFail, tcSubCnt, tcSubPass, tcSubFail, uuidRun, tcSuite, TCcnt, tcFailFlag
	global cfg_pulsServer
	# read config values
	#DeviceVal.Init()
	tcSuite = testClass.Name
	cfg_model = DeviceVal.Get_Config_Value('RUN', 'Model')
	cfg_assy = DeviceVal.Get_Config_Value('RUN', 'Assembly')
	cfg_ident = DeviceVal.Get_Config_Value('RUN', 'Ident')
	cfg_tester = DeviceVal.Get_Config_Value('TESTER', 'Tester')
	cfg_email = DeviceVal.Get_Config_Value('TESTER', 'Email')
	cfg_pulsServer = DeviceVal.Get_Config_Value('ENV', 'PULS Server')

	# loop thru tests
	ATE2.Log('')
	uuidRun = DeviceVal.Get_UUID()
	runStartTime = datetime.datetime.now()
	tcCnt = 0
	tcFailFlag = False
	for tc in testClass.TestList:
		tcName, tcDesc = tc
		fn = getattr(testClass, tcName)
		tcCnt += 1
		TCcnt = tcCnt
		tcSubCnt = tcSubPass = tcSubFail = 0
		fn(tcCnt, tcName, tcDesc)

	# end of test run
	runEndTime = datetime.datetime.now()
	elapsedTime = runEndTime - runStartTime
	dur = round(elapsedTime.seconds + (elapsedTime.microseconds/1000000), 2)
	Logger.debug('')
	Logger.debug('')
	Logger.debug('Test Run Complete [Total=%d PASS=%d FAIL=%d Pass Rate=%.1f%%]' % (tcPass+tcFail, tcPass, tcFail, tcPass/(tcPass+tcFail)*100))
	Logger.debug('')
	Logger.debug('Test Run Duration = %.2f sec' % (dur))
	ATE2.Log('Test Run Complete [Total=%d PASS=%d FAIL=%d Pass Rate=%.1f%%]' % (tcPass+tcFail, tcPass, tcFail, tcPass/(tcPass+tcFail)*100))
	ATE2.Log('Test Run Duration = %.2f sec' % (dur))
	
	# create test run record
	query = """
INSERT INTO run_result VALUES (nextval('%s'), '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%.2f', '%s', %d, %d, %d);
""" % ('run_result_run_result_id_seq', uuidRun, cfg_model, cfg_assy, cfg_ident, DeviceVal.Get_Workstation_Name(), runStartTime, runEndTime, dur, cfg_tester, tcPass+tcFail, tcPass, tcFail)
	DeviceVal.Pg_Insert(query)
	# create test case records
	for tcRec in tcRecs:
		uuidTc, tcSuite, tcName, tcCnt, tcResult, tcErrCode, tcErrText, tcStartTime, tcEndTime, dur, tcSubCnt, tcSubPass, tcSubFail = tcRec
		query = """
INSERT INTO tc_result VALUES (nextval('%s'), '%s', '%s', '%s', '%s', %f, '%s', '%s', '%s', '%s', '%s', %.2f, %d, %d, %d)
""" % ('tc_result_tc_result_id_seq', uuidTc, uuidRun, tcSuite, tcName, tcCnt, tcResult, tcErrCode, tcErrText, tcStartTime, tcEndTime, dur, tcSubCnt, tcSubPass, tcSubFail)
		DeviceVal.Pg_Insert(query)
	# create tc detail records
	for tcDetail in tcDetails:
		uuidTcDetail, uuidTc, prop, value, minRng, maxRng = tcDetail
		query = """
INSERT INTO tc_detail VALUES (nextval('%s'), '%s', '%s', '%s', '%s', '%s', '%s')
""" % ('tc_detail_tc_detail_id_seq', uuidTcDetail, uuidTc, prop, value, minRng, maxRng)
		DeviceVal.Pg_Insert(query)

def Run_Subtests(subtestClass):
	global tcSubCnt, tcSubPass, tcSubFail, tcSubSuite
	tcSubCnt = tcSubPass = tcSubFail = 0
	tcSubSuite = subtestClass.Name
	for tc in subtestClass.SubtestList:
		tcName, tcDesc = tc
		fn = getattr(subtestClass, tcName)
		tcSubCnt += 1
		Subtest(fn, tcSubCnt, tcName, tcDesc)
	Logger.debug('Subtests Complete [Total=%d PASS=%d FAIL=%d Pass Rate=%.1f%%]' % (tcSubPass+tcSubFail, tcSubPass, tcSubFail, tcSubPass/(tcSubPass+tcSubFail)*100))
	
def Add_TC_Detail(prop, value, minRng='', maxRng=''):
	global uuidTc
	uuidTc = DeviceVal.Get_UUID()
	detailThis = (DeviceVal.Get_UUID(), uuidTc, prop, value, minRng, maxRng)
	tcDetails.append(detailThis)
		
def TC_Fail(msg, errCode='', errText='', raiseError=True):
	global tcFailFlag, tcErrCode, tcErrText
	tcFailFlag = True
	tcErrCode = errCode
	tcErrText = msg
	Logger.debug('%s !! FAIL - %s [%s] %s' % (Fore.BLACK+Back.RED, msg, errCode, Style.RESET_ALL))
	ATE2.SendMsgToATE('LogError', '     FAIL - %s [%s]' % (msg, errCode))
	if errText != '':
		ATE2.SendMsgToATE('LogError', '%s' % (errText))
	if raiseError:
		raise TestCaseError


	
def Note(msg):
	Logger.debug('Note: %s' % (msg))
	


	
	
