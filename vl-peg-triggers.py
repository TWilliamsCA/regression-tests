import json, logging, re
import DeviceVal
import TestCase
from TestCase import *
from Serial import *

Logger = logging.getLogger('DeviceVal_Logger.Test_Cases')

########################################
### PEG Triggers
###

class PEG_Triggers:

	Name = 'PEG Triggers'

	TestList = [
		('indx_0_trig_inactive', 'Trigger inactive event'),
		('indx_1_trig_pwrup', 'Trigger device powered on'),
		('indx_2_trig_wakeup', 'Trigger device wakeup from sleep'),
		('indx_3_trig_pwrup_wakeup', 'Trigger device powered on or wakeup from sleep'),
		('indx_4_trig_input_hi', 'Trigger input has transitioned to high'),
		('indx_5_trig_input_lo', 'Trigger input has transitioned to low'),
		('indx_6_trig_input_word', ''),
		('indx_7_trig_speed_above', 'Trigger GPS reported speed equal or above threshold'),
		('indx_8_trig_speed_below', 'Trigger GPS reported speed below threshold'),
		('indx_9_trig_zone_entry', 'Trigger when geozone entry'),
		('indx_10_trig_zone_entry', 'Trigger when geozone exit'),
		('indx_11_trig_gps_acquired', 'Trigger when GPS acquired'),
		('indx_12_trig_gps_lost', 'Trigger when GPS lost'),
		('indx_13_trig_comm_acquired', 'Trigger when comm acquired'),
		('indx_14_trig_comm_lost', 'Trigger when comm lost'),
		('indx_15_trig_ign_on', 'Trigger when ignition on'),
		('indx_16_trig_ign_off', 'Trigger when ignition off'),
		('indx_20_trig_tod', 'Time of day'),
		('indx_30_trig_comm_shutdown', 'comm shutdown'),
		('indx_53_trig_gzone_entry', 'Trigger on specific geozone entry'),
		('indx_54_trig_gzone_exit', 'Trigger on specific geozone entry'),
		('indx_55_trig_any_gzone_entry', 'Trigger on any geozone entry'),
		('indx_56_trig_any_gzone_exit', 'Trigger on any geozone exit'),
		('twoGeozoneTest', 'two geozone test')
	]

	TestList = [
		('indx_20_trig_tod', 'Time of day'),
		
	]

	@Test_Case
	@Debug
	def indx_0_trig_inactive():
		pass

	@Test_Case
	@Debug
	def indx_1_trig_pwrup():
		DeviceVal.Power_Reset()
		Serial_Cmd('', '<Power-Up>', rdRetryCnt=5, filter='Trig=')

	@Test_Case
	@Debug
	def indx_2_trig_wakeup():
		pass

	@Test_Case
	@Debug
	def indx_3_trig_pwrup_wakeup():
		pass

	@Test_Case
	@Debug
	def indx_4_trig_input_hi():
		pass

	@Test_Case
	@Debug
	def indx_5_trig_input_lo():
		pass

	@Test_Case
	@Debug
	def indx_6_trig_input_word():
		pass

	@Test_Case
	@Debug
	def indx_7_trig_speed_above():
		pass

	@Test_Case
	@Debug
	def indx_8_trig_speed_below():
		pass

	@Test_Case
	@Debug
	def indx_9_trig_zone_entry():
		Serial_Cmd('at$app param 261,1,331379758,3122191546,1610,1610,1,402', '^OK') #Square peg zone with center at carlsbad lab and length and width of 1 mile and hysterisis of 1/4 mile
		#test cases:
		#unit in peg zone but not in hysterisis
		#unit in peg zone and in hysterisis
		#unit not in peg zone but in hysterisis
		#unit not in either
		
		
	@Test_Case
	@Debug
	def indx_10_trig_zone_exit():
		pass

	@Test_Case
	@Debug
	def indx_11_trig_gps_acquired():
		pass

	@Test_Case
	@Debug
	def indx_12_trig_gps_lost():
		pass

	@Test_Case
	@Debug
	def indx_13_trig_comm_acquired():
		pass

	@Test_Case
	@Debug
	def indx_14_trig_comm_lost():
		pass

	@Test_Case
	@Debug
	def indx_15_trig_ign_on():
		pass

	@Test_Case
	@Debug
	def indx_16_trig_comm_ign_off():
		pass

	@Test_Case
	@Debug
	def indx_17_trig_time_distance():
		pass

	@Test_Case
	@Debug
	def indx_18_trig_timer_timeout():
		pass

	@Test_Case
	@Debug
	def indx_19_trig_count_exceeded():
		pass

	@Test_Case
	@Debug
	def indx_20_trig_tod():
		time = DeviceVal.Calc_TOD_With_Offset(10)
		print('!!!!!!')
		print(time)
		print('!!!!!!!!')
		Serial_Cmd('AT$APP PARAM 267,0,%s' %(str(time)), 'OK')
		Serial_Cmd('', '<Time-of-Day>', rdRetryCnt=15)
		#buf = Serial_Cmd('', 'CRC',  rdRetryCnt=15,cmdRetryDelay=5, retBuf=True, noFail=True, filter='.*')
		#Serial_Cmd('AT$APP PARAM 267,0,<GMT TIME OFFSET>', '')  # calculate second past gmt midnight to time of test + 2 min
		#Serial_Cmd('AT$APP PARAM 265,0,5', 'OK')  # timer 0 to 5 seconds
		#Serial_Cmd('AT$APP PARAM 512,0,20,0,0,0,22,0,0,0', 'OK') # (22,0) sleep using timer 0, 20 trigger the peg tmo day trigger
		#need test while unit is asleep

	@Test_Case
	@Debug
	def indx_21_trig_buff_full():
		pass

	@Test_Case
	@Debug
	def indx_22_trig_host_data():
		pass

	@Test_Case
	@Debug
	def indx_23_trig_log_snd_fail():
		pass

	@Test_Case
	@Debug
	def indx_24_trig_wakeup_mon():
		pass

	@Test_Case
	@Debug
	def indx_25_trig_wakeup_tmr():
		pass

	@Test_Case
	@Debug
	def indx_26_trig_msg_send():
		pass

	@Test_Case
	@Debug
	def indx_27_trig_msg_ack():
		pass

	@Test_Case
	@Debug
	def indx_27_trig_msg_ack():
		pass

	@Test_Case
	@Debug
	def indx_28_trig_msg_sndfail():
		pass

	@Test_Case
	@Debug
	def indx_29_trig_log_rpt_complete():
		pass

	@Test_Case
	@Debug
	def indx_30_trig_comm_shutdown():
		Serial_Cmd('AT$APP comm off', '<Comm Lost>')
		Serial_Cmd('AT$APP comm on', '^OK')

	@Test_Case
	@Debug
	def indx_31_trig_input_transition():
		pass

	@Test_Case
	@Debug
	def indx_32_trig_anyzone_entry():
		pass

	@Test_Case
	@Debug
	def indx_33_trig_anyzone_exit():
		pass

	@Test_Case
	@Debug
	def indx_34_trig_time():
		pass

	@Test_Case
	@Debug
	def indx_35_trig_distance():
		pass

	@Test_Case
	@Debug
	def indx_36_trig_log_active():
		pass

	@Test_Case
	@Debug
	def indx_37_trig_msg_rcvd():
		pass

	@Test_Case
	@Debug
	def indx_38_trig_msg_rcvd_any():
		pass

	@Test_Case
	@Debug
	def indx_39_trig_environ():
		pass

	@Test_Case
	@Debug
	def indx_40_trig_heading():
		pass

	@Test_Case
	@Debug
	def indx_41_trig_special():
		pass

	@Test_Case
	@Debug
	def indx_42_trig_moving():
		pass

	@Test_Case
	@Debug
	def indx_43_trig_not_moving():
		pass

	@Test_Case
	@Debug
	def indx_44_trig_adc_above():
		pass

	@Test_Case
	@Debug
	def indx_45_trig_adc_below():
		pass

	@Test_Case
	@Debug
	def indx_46_trig_comm_conn():
		pass

	@Test_Case
	@Debug
	def indx_47_trig_acc_below():
		pass

	@Test_Case
	@Debug
	def indx_48_trig_any_trig():
		pass

	@Test_Case
	@Debug
	def indx_49_trig_comm_state():
		pass

	@Test_Case
	@Debug
	def indx_50_trig_incoming_call():
		pass

	@Test_Case
	@Debug
	def indx_51_trig_call_state():
		pass

	@Test_Case
	@Debug
	def indx_52_trig_sms_request():
		pass

	@Test_Case
	@Debug
	def indx_53_trig_gzone_entry():
		Serial_Cmd('at$app gz erase', '^OK')
		Serial_Cmd('AT$APP GPS POS 33.1379763 -117.2775823', '^OK') # spoof the gps position to be at carlsbad lab
		Serial_Cmd('AT$APP GZ WRITE 0 1 12 5 33.119582 -117.210251', '^OK') # set geozone location in San Marcos with a radius of 5m
		Serial_Cmd('AT$APP GPS POS 33.119582 -117.210251', '^OK') # spoof the gps position to be at the center of the geozone
		Serial_Cmd('', '<GeoZone Entry>  Indx=12', rdRetryCnt=15)

	@Test_Case
	@Debug
	def indx_54_trig_gzone_exit():
		Serial_Cmd('at$app gz erase', '^OK')
		Serial_Cmd('AT$APP GZ WRITE 0 1 12 5 33.119582 -117.210251', '^OK') # set geozone location in San Marcos with a radius of 5m
		Serial_Cmd('AT$APP GPS POS 33.119582 -117.210251', '^OK') # spoof the gps position to be at the center of the geozone
		Serial_Cmd('', '<Any GeoZone Entry>', rdRetryCnt=15) #verify unit in geozone
		
		
		Serial_Cmd('AT$APP GPS POS 33.1379763 -117.2775823', '^OK') # spoof the gps position to be at carlsbad lab
		Serial_Cmd('', '<GeoZone Exit>  Indx=12', rdRetryCnt=15) #verify unit out geozone

		

	@Test_Case
	@Debug
	def indx_55_trig_any_gzone_entry():
		Serial_Cmd('at$app gz erase', '^OK')
		Serial_Cmd('AT$APP GPS POS 33.1379763 -117.2775823', '^OK') # spoof the gps position to be at carlsbad lab
		Serial_Cmd('AT$APP GZ WRITE 0 1 12 5 33.119582 -117.210251', '^OK') # set geozone location in San Marcos with a radius of 5m
		Serial_Cmd('AT$APP GPS POS 33.119582 -117.210251', '^OK') # spoof the gps position to be at the center of the geozone
		Serial_Cmd('', '<Any GeoZone Entry>', rdRetryCnt=15)
		
	@Test_Case
	@Debug
	def indx_56_trig_any_gzone_exit():
		Serial_Cmd('at$app gz erase', '^OK')
		Serial_Cmd('AT$APP GZ WRITE 0 1 12 5 33.119582 -117.210251', '^OK') # set geozone location in San Marcos with a radius of 5m
		Serial_Cmd('AT$APP GPS POS 33.119582 -117.210251', '^OK') # spoof the gps position to be at the center of the geozone
		Serial_Cmd('', '<Any GeoZone Entry>', rdRetryCnt=15) #verify unit in geozone
		
		
		Serial_Cmd('AT$APP GPS POS 33.1379763 -117.2775823', '^OK') # spoof the gps position to be at carlsbad lab
		Serial_Cmd('', '<Any GeoZone Exit>', rdRetryCnt=15) #verify unit in geozone
		
	@Test_Case
	@Debug
	def twoGeozoneTest():
		geozone1 = False
		geozone2 = False
		Serial_Cmd('at$app gz erase', '^OK')
		Serial_Cmd('AT$APP GZ WRITE 0 1 1 1619 33.1379763 -117.2775823', '^OK') # set circular geozone location in carlsbad lab with a radius of 1 mile
		Serial_Cmd('AT$APP GZ WRITE 1 2 2 0 33.139819 -117.279207', '^OK') #vertices 1 triangular geozone
		Serial_Cmd('AT$APP GZ WRITE 2 2 2 0 33.135199 -117.279323', '^OK') #vertices 2
		Serial_Cmd('AT$APP GZ WRITE 3 2 2 0 33.136223 -117.273444', '^OK') #vertices 3
		Serial_Cmd('AT$APP GPS POS 33.119582 -117.210251', '^OK') #change gps location to san marcos
		buf = Serial_Cmd('AT$APP GPS POS 33.1379763 -117.2775823', '<GeoZone Entry>  Indx=2', rdRetryCnt=15,initDelay=0.05, retBuf=True, filter='.*') # change gps location to be carlsbad

		for line in buf:
			if '<GeoZone Entry>  Indx=1' in line: geozone1 = True
			if '<GeoZone Entry>  Indx=2' in line: geozone2 = True
		if geozone1 == False or geozone2 == False:
			TC_Fail('Failed to enter geozone')
		
		geozone1 = False
		geozone2 = False
		buf = Serial_Cmd('AT$APP GPS POS 33.119582 -117.210251', '<GeoZone Exit>  Indx=2', rdRetryCnt=15,initDelay=0.05, retBuf=True, filter='.*') # change gps location to be san marcos
		for line in buf:
			if '<GeoZone Exit>  Indx=1' in line: geozone1 = True
			if '<GeoZone Exit>  Indx=2' in line: geozone2 = True
		if geozone1 == False or geozone2 == False:
			TC_Fail('Failed to exit geozone')
		
	@Test_Case
	@Debug
	def indx_57_trig_sms_null():
		pass

	@Test_Case
	@Debug
	def indx_58_trig_comm_network_id():
		pass

	@Test_Case
	@Debug
	def indx_59_trig_1bb_id():
		pass

	@Test_Case
	@Debug
	def indx_60_trig_update_begin():
		pass

	@Test_Case
	@Debug
	def indx_61_trig_update_end():
		pass

	@Test_Case
	@Debug
	def indx_62_trig_accel_detect():
		pass

	@Test_Case
	@Debug
	def indx_63_trig_msg_generate():
		pass

	@Test_Case
	@Debug
	def indx_64_trig_gps_qual_fix():
		pass

	@Test_Case
	@Debug
	def indx_65_trig_gps_qual_lost():
		pass

	@Test_Case
	@Debug
	def indx_66_trig_accelerometer():
		pass

	@Test_Case
	@Debug
	def indx_67_trig_vbus_state():
		pass

	@Test_Case
	@Debug
	def indx_68_trig_motlog_wrap():
		pass

	@Test_Case
	@Debug
	def indx_69_trig_vbus_dtc_change():
		pass

	@Test_Case
	@Debug
	def indx_70_trig_radio_jam():
		pass

	@Test_Case
	@Debug
	def indx_71_trig_accel_event():
		pass

	@Test_Case
	@Debug
	def indx_72_trig_accel_align_state():
		pass

	@Test_Case
	@Debug
	def indx_73_trig_accel_vbus_event():
		pass

	@Test_Case
	@Debug
	def indx_74_trig_acc_sched():
		pass

	@Test_Case
	@Debug
	def indx_75_trig_window_sched():
		pass

	@Test_Case
	@Debug
	def indx_76_trig_tow_alert():
		pass

	@Test_Case
	@Debug
	def indx_77_trig_ack_rcvd():
		pass

	@Test_Case
	@Debug
	def indx_78_trig_hosted_app():
		pass

	@Test_Case
	@Debug
	def indx_79_trig_tilt_on():
		pass

	@Test_Case
	@Debug
	def indx_80_trig_tilt_off():
		pass

	@Test_Case
	@Debug
	def indx_81_trig_accel_evt_start():
		pass

	@Test_Case
	@Debug
	def indx_82_trig_audio_event():
		pass

	@Test_Case
	@Debug
	def indx_83_trig_smsg_detect():
		Serial_Cmd('AT$APP PARAM 2178 1,""', 'OK')  # msg detection using slot 1
		Serial_Cmd('AT$APP PARAM 2176 1,""', 'OK')  # msg generation using slot 1
		Serial_Cmd('AT$APP PEG ACTION 129 1', 'OK')  # send serial msg using slot 1

	@Test_Case
	@Debug
	def indx_84_trig_gps_jam():
		pass

	@Test_Case
	@Debug
	def indx_85_trig_mdt():
		pass

	@Test_Case
	@Debug
	def indx_86_trig_ble_fob():
		pass

	@Test_Case
	@Debug
	def indx_87_trig_dun_det():
		pass

	@Test_Case
	@Debug
	def indx_88_trig_stream():
		pass

	@Test_Case
	@Debug
	def indx_89_trig_syslog():
		pass

	@Test_Case
	@Debug
	def indx_90_trig_upload():
		pass

	@Test_Case
	@Debug
	def indx_91_trig_wakeup_mon_src():
		pass

	@Test_Case
	@Debug
	def indx_92_trig_bt_stat():
		pass

	@Test_Case
	@Debug
	def indx_93_trig_batt():
		pass

	@Test_Case
	@Debug
	def indx_94_matrix_event():
		pass

	@Test_Case
	@Debug
	def indx_95_rad_silence():
		pass


Run_Tests(PEG_Triggers)

	
# #
# ### Trigger
# ATE2.Log_Step('Peg Action Tests')
# ATE2.Serial_Exec_Cmd('ats125=7', 'OK', port='COM15')
# buf = ATE2.Serial_Exec_Cmd('at$app peg action 20 1', 'GPS Quality Fix', port='COM15', cmdRepeatCnt=1, repeatCnt=60)
# ATE2.Serial_Buffer_Match(buf, ['Trig=<GPS Lost>', 'Trig=<GPS Quality Lost>', 'Trig=<GPS Acquire>', 'Trig=<GPS Quality Fix>'])

# buf = ATE2.Serial_Exec_Cmd('at$app peg action 55 1', 'Comm Acquire', port='COM15', cmdRepeatCnt=1, repeatCnt=60)
# if buf != None:
	# ATE2.Serial_Buffer_Match(buf, ['Trig=<Comm Shutdown>', 'Trig=<Comm Lost>', 'Trig=<Comm State>', 'Trig=<Comm Connected>', 'Trig=<Comm Acquire>'])


# ATE2.Log_Step('Trigger Tests')
# for indx in (1, 4, 7):
	# ATE2.Serial_Exec_Cmd('ats158=254', 'OK', port='COM15')	# set bias high
	# ATE2.Serial_Exec_Cmd('stm_cli -o 255 255', 'Sending message opcode', port='COM61');
	# ATE2.Sleep(1)
	# ATE2.Serial_Exec_Cmd('stm_cli -o 255 0', 'Sending message opcode', port='COM61');
	# line = ATE2.Serial_Exec_Cmd('', 'Trig=<Input Transition>  Indx=%d' % (indx), port='COM15', cmdRepeatCnt=1, repeatCnt=10, initDelayRead=0, repeatDelay=.1, returnMatch=True)
	# if line: ATE2.Log_Info('[PASS] %s' % line)
	
# for indx in (1, 4, 7):
	# ATE2.Serial_Exec_Cmd('ats158=254', 'OK', port='COM15')	# set bias high
	# ATE2.Serial_Exec_Cmd('stm_cli -o 255 255', 'Sending message opcode', port='COM61');
	# ATE2.Sleep(1)
	# ATE2.Serial_Exec_Cmd('stm_cli -o 255 0', 'Sending message opcode', port='COM61');
	# line = ATE2.Serial_Exec_Cmd('', 'Trig=<Input Low>  Indx=%d' % (indx), port='COM15', cmdRepeatCnt=1, repeatCnt=10, initDelayRead=0, repeatDelay=.1, returnMatch=True)
	# if line: ATE2.Log_Info('[PASS] %s' % line)
	
# LMU.Test_End()
	
	# #




