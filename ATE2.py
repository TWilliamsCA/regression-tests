###############################################
## Imports
###############################################
import atexit, configparser, ctypes, datetime, fileinput, inspect, json, logging, os, re
import serial, serial.tools, signal, socket, subprocess, sys, threading, time, traceback, urllib3, uuid, visa, pickle
from subprocess import call
from xml.dom import minidom
from serial.tools import list_ports
from tkinter import *
import tkinter.simpledialog as tks
import tkinter.messagebox as tkm
import easygui as eg
from threading import Thread

Logger = logging.getLogger('ATE2_Logger.ATE2')

###############################################
## Module Initializations
###############################################
class _TestVals(object):
	def __init__(self):
		self.Model = ''
		self.Part = ''
		self.Assembly = ''
		self.Cust = ''
		self.Script = ''
		self.bNoPower = False
		self.bJP1Log = False
		self.ATEver = ''
		self.ATEpass = True
		self.PowerSupplyModel = ''
		self.PowerSupplyType = ''
		self.RC = ''
		self.IsRMA = False
		self.WorkOrder = ''
		self.SalesOrder = ''
		self.CmTrackingNum = ''
		self.UserPkid = 0
		self.LibVersion = ''
		self.ModelVersion = ''
		self.MfgServer = ''
		self.FwLoaded = False
		
class _UUT(object):
	def __init__(self):
		self.TestID = ''
		self.Ident1 = ''
		self.Ident2 = ''
		self.StartDate = ''
		self.EndDate = ''
		self.TestResult = 'Fail'
		self.ErrorCode = ''
		self.ErrorText = ''
		self.UutResultPkid = 0
		self.StepResultPkid = 0
		self.StepIndex = 0
		self.StepType = ''
		self.StepName = ''
		self.StepStartDate = ''
		self.StepEndDate = ''
		self.StepElapsedTime = 0
		self.Test_Selected = {}
		self.Step_Details = []
		
class _Callbacks(object):
	def __init__(self):
		self.CallbackList = []
		
	def Add(self, func):
		self.CallbackList.append(func)

## Globals		
ATEport = 'COM1'
LINUXport = ''
MultimeterPort = ''
ConfParser = None
Dir_ATE_DB = 'c:/CalAmpAte2/db/'
Dir_Bin = 'C:\\CalAmpAte2\\bin\\'
Dir_Bin_APG = Dir_Bin + 'APG\\'
Dir_Models = 'C:\\CalAmpAte2\\models\\'
Log_Dir = 'C:\\New folder'
#Logger = None
PathFiles = ''
RelayTool = 'C:\\CalAmpATE2\\bin\\Relays\\Relays'

gparms_file = 'c:/CalAmpAte2/DB/parms.txt'
gcookie_file = 'c:/CalAmpAte2/DB/cookie.txt'

###############################################
## ATE2
###############################################

def Check_Test_Selected():
	# from stack get script function from parent frame, field 4
	frameList = inspect.stack()
	scriptFunc = frameList[1][3]
	del frameList
	scriptFunc = re.sub('^Local_', '', scriptFunc)
	# if test not found assume selected to allow tests to use ### skip ### feature
	try:
		Logger.debug('>>> %s [%s]' % (scriptFunc, UUT.Test_Selected[scriptFunc]))
	except:
		return True
	if UUT.Test_Selected[scriptFunc] == 'True':
		return True
	else:
		Log_Warning('Test/Step not selected [%s]' % (scriptFunc))
		return False

def Config_Get(section, field, defZero=False):
	try:
		val = ConfParser.get(section, field)
		return val
	except configparser.NoOptionError:
		if defZero:
			return '0'
		else:
			return ''

def Cookie_Set(msg):
	global gcookie_file
	# msg is what needs to be placed out to the file.
	# Need to empty the cookie file
	#firstRun = ["NONE","","",""]
	
	f = open(gcookie_file, 'wb')
	pickle.dump(msg,f)
	f.close()

def Cookie_Read(i_slot):
	global gcookie_file
	#i_slot is the location of the data to retrieve
	
	f = open(gcookie_file, 'rb')
	cookiereturn = pickle.load(f)
	f.close()
	return(cookiereturn[i_slot])

def Current_Value_Change(unit='AMPS', current = 0.0):
	# Will change the value from AMPS to units specified.
	if re.search('AMPS', unit.upper()):
		n = 0
	elif re.search('MA', unit.upper()):
		n = 3
	elif re.search('UA', unit.upper()):
		n = 6
	for retry in range(n): current = current*10
	return float('%.1f' % (current))
	
def Dialog_Input(msg, title):
	input = eg.enterbox(msg=msg, title=title)
	return input

def Exceptions_Log_Uncaught(ex_cls, ex, tb):
	if  re.search('UnboundLocalError', str(ex_cls)):
		Log_Info_Big('Verify serial port is working and not locked')
	elif re.search('VisaIOError', str(ex_cls)):
		Log_Info_Big('Verify GPIB cable is plugged in')
	Logger.debug(str(ex_cls))
	Logger.debug(str(ex))
	Logger.debug(traceback.print_tb(tb))
	Log_Warning('Script Exception follows:')
	Log(str(ex_cls))
	Log(str(ex))
	Step_Fail('Script Error - %s' % (str(ex_cls)), errorCode = 'SCRIPT_ERROR')
	
def Log(desc, logToDetails=False):
	SendMsgToATE('Log', desc)
	if logToDetails == True:
		Logger.debug(desc)

def Logger_Start():
	global Logger
	# setup logging
	Logger = logging.getLogger('ATE2_Logger')
	if Logger.handlers:
		for handler in Logger.handlers:
			Logger.removeHandler(handler)
	pn = ConfParser.get('CRAF', 'Part Number')
	loc, ws = WorkStation_Config_Get()
	dt = datetime.datetime.now().date()
	dtStr = re.sub('-', '', str(dt))
	logging.basicConfig(filename='%sWS%s_%s_%s-%s-script' % (Log_Dir, loc, ws, pn, dtStr), level=logging.DEBUG, format='%(asctime)s : %(message)s')
	ch = logging.StreamHandler()
	ch_fmt = logging.Formatter('%(asctime)s : %(message)s', '%m-%d %H:%M:%S')
	ch.setFormatter(ch_fmt)
	Logger.addHandler(ch)

def Log_Info(desc, logToDetails=False):
	SendMsgToATE('LogInfo', desc)
	if logToDetails == True:
		Logger.debug(desc)

def Log_Info_Big(desc, logToDetails=False):
	SendMsgToATE('LogTip', desc)
	if logToDetails == True:
		Logger.debug(desc)
		
def Log_Step(desc):
	# Logger.debug(desc)
	SendMsgToATE('LogStep', desc)
		
def Log_SubStep(desc):
	# Logger.debug(desc)
	SendMsgToATE('LogSubStep', desc)

def Log_Test(desc):
	# Logger.debug(desc)
	SendMsgToATE('LogTest', desc)

def Log_Warning(desc, logToDetails=False):
	SendMsgToATE('LogWarning', desc)
	if logToDetails == True:
		logger.Debug(desc)
		
def Mfg_Add_Step_Detail(property='', value='', minRange='', maxRange=''):
	detailRec = (property, value, minRange, maxRange)
	UUT.Step_Details.append(detailRec)

def Mfg_Post_Ident(type='', ident=''):
	body = """
{
	"uutResultPkid" : %s,
	"type" : "%s",
	"ident" : "%s"
}
""" % (UUT.TestResultPkid, type, ident)
	# print(body)
	http = urllib3.PoolManager()
	url = 'localhost:8080/reporting/mfg/ident'
	headers = {'Content-Type' : 'application/json'}
	resp = http.urlopen('POST', url, headers=headers, body=body)
	status = str(resp.status)
	respTxt = resp.data.decode().rstrip()
	Logger.debug('HTTP status [%s]' % (status))
	Logger.debug(respTxt)

def Mfg_Post_Step_Details():
	if UUT.UutResultPkid <= 0:
		Logger.debug('Error in Mfg_Post_Step_Details function' )
		pass
	try:
		for rec in UUT.Step_Details:
			property, value, minRange, maxRange = rec
			body = """
		{
			"uutResultPkid" : %s,
			"stepResultPkid" : %s,
			"property" : "%s",
			"value" : "%s",
			"minRange" : "%s",
			"maxRange" : "%s"
		}
		""" % (UUT.UutResultPkid, UUT.StepResultPkid, property, value, minRange, maxRange)
			# print(body)
			http = urllib3.PoolManager()
			url = '%s/reporting/mfg/stepdetail'  % (TestVals.MfgServer)
			headers = {'Content-Type' : 'application/json'}
			resp = http.urlopen('POST', url, headers=headers, body=body)
			status = str(resp.status)
			respTxt = resp.data.decode().rstrip()
			# Logger.debug('HTTP status [%s]' % (status))
			# Logger.debug(respTxt)
	except:
		Logger.debug('Error in Mfg_Post_Step_Details function' )
		pass
	
def Mfg_Post_Step_Result(descr=''):
	TestVals.StepIndex += 1
	body = """
	"uutResultPkid" : %s,
	"stepIndex" : %s,
	"name" : "%s",
	"stepType" : "%s",
	"startDate" : "%s",
	"endDate" : "%s",
	"elapsedTime" : "%f",
	"stepResult" : "%s",
	"errorCode" : "%s",
	"errorText" : "%s"
""" % (UUT.UutResultPkid, UUT.StepIndex, descr, UUT.StepName, UUT.StepStartDate, UUT.StepEndDate, UUT.StepElapsedTime, UUT.ErrorCode, UUT.ErrorText)

def Mfg_Post_UUT_Result():
	wsName = Workstation_Name_Get()
	wsMacAddr = Config_Get('WORKSTATION', 'MAC Address')
	cell = Config_Get('WORKSTATION', 'Cell')
	location = Config_Get('WORKSTATION', 'Location')
	testType = Config_Get('TEST', 'Test Type')
	product = Config_Get('ASSEMBLY', 'Product')
	platform = Config_Get('ASSEMBLY', 'Platform')
	model = Config_Get('CRAF', 'Model')
	assemblyNum = Config_Get('ASSEMBLY', 'Assembly Number')
	partNum = Config_Get('CRAF', 'Part Number')
	isRMA = 'true' if TestVals.IsRMA else 'false'
	customerId = Config_Get('CRAF', 'CUSTOMER ID')
	diff = UUT.StartDate 
	t1 = UUT.StartDate
	t2 = datetime.datetime.now()
	UUT.StartDate = UUT.StartDate.strftime('%a %Y-%m-%d %H:%M:%S')
	UUT.EndDate = t2.strftime('%a %Y-%m-%d %H:%M:%S')
	diff = t2 - t1
	dur = round(diff.seconds + (diff.microseconds/1000000), 2)
	Logger.debug('Test Duration = %.2f sec' % (dur))
	body = """
{
	"wsName" : "%s",
	"wsMacAddr" : "%s",
	"cell" : "%s",
	"location" : "%s",
	"testType" : "%s",
	"product" : "%s",
	"platform" : "%s",
	"model" : "%s",
	"assemblyNum" : "%s",
	"partNum" : "%s",
	"testId" : "%s",
	"ident1" : "%s",
	"ident2" : "%s",
	"serverStartDate" : "%s",
	"startDate" : "%s",
	"endDate" : "%s",
	"elapsedTime" : "%.2f",
	"status" : "%s",
	"errorCode" : "%s",
	"errorMsg" : "%s",
	"isRMA" : "%s",
	"customerId" : "%s",
	"workOrder" : "%s",
	"salesOrder" : "%s",
	"cmTrackingNum" : "%s",
	"userPkid" : "%s",
	"libVersion" : "%s",
	"modelVersion" : "%s"
}
""" % (wsName, wsMacAddr, cell, location, testType, product, platform, model, assemblyNum, partNum,
	UUT.TestID, UUT.Ident1, UUT.Ident2, '', UUT.StartDate, UUT.EndDate, dur, UUT.TestResult, UUT.ErrorCode, UUT.ErrorText, isRMA, customerId,
	TestVals.WorkOrder, TestVals.SalesOrder, TestVals.CmTrackingNum, TestVals.UserPkid, TestVals.LibVersion, TestVals.ModelVersion)
	try:
		http = urllib3.PoolManager()
		url = '%s/reporting/mfg/uutresult'  % (TestVals.MfgServer)
		headers = {'Content-Type' : 'application/json'}
	
		resp = http.urlopen('POST', url, headers=headers, body=body)
		status = str(resp.status)
		respTxt = resp.data.decode().rstrip()
		#Logger.debug('HTTP status [%s]' % (status))
		Logger.debug(respTxt)
		jsonObj = json.loads(respTxt)
		UUT.UutResultPkid =(jsonObj['uutResultPkid'])
		# print('uutResultPkid = %s' % (UUT.UutResultPkid)) 
	except:
		Logger.debug('Error in Mfg_Post_UUT_Result function' )
		pass
	
def Mfg_Week_Get():
	dt = datetime.date.today()
	mfgWeek = dt.isocalendar()[1]
	yr = int(re.match('..(..)', str(dt.year)).group(1))
	if dt.month == 12 and mfgWeek == 1:
		yr += 1
	mfgWeek = '%02d' % (mfgWeek)
	yr = '%02d' % (yr)
	return mfgWeek + yr

def Multimeter_Read_Current(range_auto = 'ON', range_val = None):
	if MultimeterPort == '':
		#Multimeter not available, do this manually
		if range_val == '0.05':
			curr = Operator_Input(instr = 'Enter Current Meter Value in uA (MicroAmps)', charCnt=0, noFail=False, bClear=True, defVal='')
			curr = curr + 'e-6'
		else:
			curr = Operator_Input(instr = 'Enter Current Meter Value in AMPS', charCnt=0, noFail=False, bClear=True, defVal='')
	else:
		Serial_Exec_Cmd('FUNC CURR:DC', '', port=MultimeterPort, baudRate=9600)
		#Serial_Exec_Cmd('CURR:DC:RANG:AUTO ON', '', port=MultimeterPort, baudRate=9600)
		if range_auto.upper() == 'ON':
			Serial_Exec_Cmd('CURR:DC:RANG:AUTO ON', '', port=MultimeterPort, baudRate=9600)
		elif range_auto.upper() == 'OFF':
			Serial_Exec_Cmd('CURR:DC:RANG:AUTO OFF', '', port=MultimeterPort, baudRate=9600)
		if range_val:
			Serial_Exec_Cmd('CURR:DC:RANG %s' % (range_val), '', port=MultimeterPort, baudRate=9600)
		mmOut = Serial_Exec_Cmd('FETCH?', '', port=MultimeterPort, baudRate=9600)
		curr = re.search('.*\r(.*)', mmOut[0]).group(1)
	Logger.debug('Current [%s]' % (curr))
	return curr
	
def Operator_Input(instr='', charCnt=0, noFail=False, bClear=False, defVal=''):
	outbuf = subprocess.check_output('ipy %s \"%s\" \"%s\" \"%s\" \"%s\" \"%s\"' % (Dir_Bin + '\\Dlg_Input.py', 'Operator Input', instr, charCnt, str(bClear), defVal))
	outbuf = outbuf.decode('latin-1')
	try:
		rval, input = outbuf.strip().split(':')
	except:
		rval = 'FAIL'
	if  rval == 'FAIL' and not noFail:
		Step_Fail('Operator failed step [%s]' % (instr), errorCode='OPER_FAIL_INPUT')
	return (input)
	
def Operator_Instruction(instr, noFail=False):
	rc = os.system('ipy %s \"%s\" \"%s\"' % (Dir_Bin + '\\Dlg_General.py', 'Operator Instruction', instr))
	if not rc and not noFail:
		Step_Fail('Operator failed step [%s]' % (instr), errorCode='OPER_FAIL_INSTR')
	return (rc)
	
def Operator_Video_Instruction(instr, mediaFilePath, noFail=False):
	print(mediaFilePath)
	rc = os.system('ipy %s \"%s\" \"%s\" \"%s\"' % (Dir_Bin + '\\Dlg_Video.py', 'Operator Instruction', instr, mediaFilePath))
	if not rc and not noFail:
		Step_Fail('Operator failed step [%s]' % (instr), errorCode='OPER_FAIL_VIDEO_INSTR')
	return (rc)

def Power_Get_Power_Supply_Type(channel=5):
	try:
		ins = visa.instrument('GPIB0::%d' % channel)
		ins.write('*IDN?')
		idn = ins.read()
		model = re.search(',(\w*)', idn).group(1)
		TestVals.PowerSupplyModel = model
		if re.search('E3631A', model):
			TestVals.PowerSupplyType = 'DUAL'
		elif re.search('6613C', model):
			TestVals.PowerSupplyType = '66XX'
		else:
			TestVals.PowerSupplyType = 'SINGLE'
		Logger.debug('Power supply model = %s is %s channel' % (TestVals.PowerSupplyModel, TestVals.PowerSupplyType))
	except:
		pass

def Power_Ramp_Voltage_13_5v(channel=5):
	if TestVals.bNoPower == 'True': return
	ins = visa.instrument('GPIB0::%d' % channel)
	for v in range(121, 136, 1):
		volts = v/10.0
		Power_SCPI_Apply(ins, volts, 1.000)
		time.sleep(.1)
		
def Power_Read_Current(channel=5, type='P25V'):
	ins = visa.instrument('GPIB0::%d' % channel)
	# if TestVals.PowerSupplyType == 'DUAL':
	#	ins.write('MEAS:CURR:DC? %s' %(type))
	# else:
	ins.write('MEAS:CURR?') 
	current = ins.read()
	return current

def Power_SCPI_Apply(ins, volts, current):
	if TestVals.PowerSupplyType == 'DUAL':
		ins.write('APPLY P25V,%f,%f' % (volts, current))
	elif TestVals.PowerSupplyType == '66XX':
		ins.write('VOLT %f' % (volts))
	else:
		ins.write('APPLY %f,%f' % (volts, current))
	Logger.debug('Volts: %.4f, current: %.4f' % (volts,current))

def Power_Set_N25V_OFF():
	if TestVals.bNoPower == 'True': return
	if TestVals.PowerSupplyType == 'DUAL':
		Power_Set_Voltage(channel=5, pwrlvl=0.0, type='N25V')
	else:
		Step_Fail('This test requires a DUAL power supply', errorCode='ATE_PWR_N25V_DUAL_SUPPLY_REQUIRED')
		
def Power_Set_P6V_OFF():
	if TestVals.PowerSupplyType == 'DUAL':
		Power_Set_Voltage(channel=5, pwrlvl=0.0, type='P6V')
	else:
		Step_Fail('This test requires a DUAL power supply', errorCode='LMU_PWR_P6V_DUAL_SUPPLY_REQUIRED')

def Power_Set_Voltage(channel=5, pwrlvl=0, type=''):
	if TestVals.bNoPower == 'True': return
	Logger.debug('Type: [%s]  pwrlvl: [%.4f] ' % (type, pwrlvl))
	ins = visa.instrument('GPIB0::%d' % channel)
	if type == 'P6V' or type == 'P25V' or type == 'N25V':
		ins.write('APPLY %s,%f,1.000' % (type, pwrlvl))	
	else:
		Step_Fail('Power Supply type does not exist: %s' % (type))
		
def Power_Set_P25V_Voltage(pwrlvl=0, channel=5, type='P25v', logUI=False):
	offset = float(Config_Get('WORKSTATION', 'P25V Offset', defZero=True))
	pwrlvl = float(pwrlvl) + offset
	Logger.debug('Type: [%s]  pwrlvl: [%.4f] ' % (type, pwrlvl))
	ins = visa.instrument('GPIB0::%d' % channel)
	Power_SCPI_Apply(ins, pwrlvl, 1.0)
	if logUI:
		Log('P25v set to [%.1f] volts' % (pwrlvl))
	
def Power_Set_Voltage_2_5v():
	if TestVals.bNoPower == 'True': return
	try:
		OffSet = float(ConfParser.get('WORKSTATION', '2.5V Offset'))
	except:
		OffSet = 0
	pwrlvlset = 2.5 + OffSet
	if TestVals.PowerSupplyType == 'DUAL':
		Power_Set_Voltage(channel=5, pwrlvl=pwrlvlset, type='P6V')
	else:
		Step_Fail('This test requires a DUAL power supply', errorCode='LMU_PWR_2.5V_DUAL_SUPPLY_REQUIRED')

def Power_Set_Voltage_3_6v(channel=5):
	if TestVals.bNoPower == 'True': return
	try:
		OffSet = float(ConfParser.get('WORKSTATION', '3.6V Offset'))
	except:
		OffSet = 0
	pwrlvl = 3.6 + OffSet
	Logger.debug('pwrlvl: %.4f, offset: %.4f' % (pwrlvl,OffSet))
	ins = visa.instrument('GPIB0::%d' % channel)
	ins.write('APPLY P6V,%f,1.000' % (pwrlvl))

def Power_Set_Voltage_3_1v(channel=5):
	if TestVals.bNoPower == 'True': return
	try:
		OffSet = float(ConfParser.get('WORKSTATION', '3.1V Offset'))
	except:
		OffSet = 0
	pwrlvl = 3.1 + OffSet
	Logger.debug('pwrlvl: %.4f, offset: %.4f' % (pwrlvl,OffSet))
	ins = visa.instrument('GPIB0::%d' % channel)
	ins.write('APPLY P6V,%f,1.000' % (pwrlvl))

def Power_Set_Voltage_4_5v(channel=5):
	if TestVals.bNoPower == 'True': return
	try:
		OffSet = float(ConfParser.get('WORKSTATION', '4.5V Offset'))
	except:
		OffSet = 0
	pwrlvl = 4.5 + OffSet
	Logger.debug('pwrlvl: %.4f, offset: %.4f' % (pwrlvl,OffSet))
	ins = visa.instrument('GPIB0::%d' % channel)
	ins.write('APPLY P6V,%f,1.000' % (pwrlvl))
	
def Power_Set_Voltage_5v(channel=5):
	if TestVals.bNoPower == 'True': return
	try:
		OffSet = float(ConfParser.get('WORKSTATION', '5V Offset'))
	except:
		OffSet = 0
	pwrlvl = 5 + OffSet
	Logger.debug('pwrlvl: %.4f, offset: %.4f' % (pwrlvl,OffSet))
	ins = visa.instrument('GPIB0::%d' % channel)
	ins.write('APPLY P6V,%f,1.000' % (pwrlvl))

def Power_Set_Voltage_12v(channel=5):
	if TestVals.bNoPower == 'True': return
	try:
		OffSet = float(ConfParser.get('WORKSTATION', '12V Offset'))
	except:
		OffSet = 0
	pwrlvl = 12 + OffSet
	Logger.debug('pwrlvl: %.4f, offset: %.4f' % (pwrlvl,OffSet))
	ins = visa.instrument('GPIB0::%d' % channel)
	Power_SCPI_Apply(ins, pwrlvl, 1.0)

def Power_Turn_Off(channel=5):
	if TestVals.bNoPower == 'True': return
	Logger.debug('!! Power turned off')
	try:
		ins = visa.instrument('GPIB0::%d' % channel)
		ins.write('OUTPUT OFF;DISPLAY ON')
	except:
		pass
	time.sleep(1)
  
def Power_Turn_On(channel=5, wait=1):
	if TestVals.bNoPower == 'True': return
	Logger.debug('!! Power turned on')
	try:
		ins = visa.instrument('GPIB0::%d' % channel)
		ins.write('OUTPUT ON;DISPLAY ON')
		# set local mode by clearing remote enable line
		session = ins.session
		rm = visa.ResourceManager()
		lib = rm.visalib
		lib.gpib_control_ren(session,0)
		time.sleep(wait)
	except:
		pass

def Relay_Clear(relayNum):
	# clearing bit turns relay off
	currMask = Relays_Read()
	relayMask = (0xffff & ~(0x1 << int(relayNum)))
	mask = currMask & relayMask
	Logger.debug('Relay masks: Curr=0x%x Relay=0x%x New=0x%x' % (currMask, relayMask, mask))
	cmd = RelayTool + ' W ' + '%x > NUL' % (mask)
	currMask = Relays_Read()
	Logger.debug('relays read after clear = 0x%x' % currMask)
	rc = os.system(cmd)
	if rc != 0:
		Step_Fail('Failed to clear relay [%d]' % (rc), errorCode='ATE_RELAY_CLR_FAIL')

def Relay_Set(relayNum):
	# setting bit turns relay on
	currMask = Relays_Read()
	relayMask = (0xffff & (0x1 << int(relayNum)))
	mask = currMask | relayMask
	Logger.debug('Relay masks: Curr=0x%x Relay=0x%x New=0x%x' % (currMask, relayMask, mask))
	cmd = RelayTool + ' W ' + '%x > NUL' % (mask)
	rc = os.system(cmd)
	if rc != 0:
		Step_Fail('Failed to set relay [%d]' % (rc), errorCode='LMU_RELAY_SET_FAIL')				

def Relays_Clear_All():
	if Relays_Read() != -1:
		cmd = RelayTool + ' W ffff'
		rc = os.system(cmd)
		if rc != 0:
			Logger.debug('Failed to clear relays [%d]' % (rc))

def Relays_Read():
	cmd = RelayTool + ' R > NUL'
	rc = os.system(cmd)
	return rc

def SendMsgToATE(msgType, msgData):
	HOST, PORT = "localhost", 9999
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.connect((HOST, PORT))
	data = msgType + ': ' + msgData
	sock.send(bytes(data + '\n', 'utf-8'))
	sock.close()
	time.sleep(.1)
	
def Serial_Exec_Cmd(cmd, expect, port='', repeatCnt=1, initDelayRead=0.25, repeatDelay=1, returnMatch=False, exitOnMatch=False, cmdRepeatCnt=3,
	cmdRepeatDelay=1, noFail=False, errorRE=None, filterRE=None, baudRate=115200):
	global ATEport
	try:
		if port == '':
			port = ATEport
		repeatCntSave = repeatCnt
		cmdRepeatCntSave = cmdRepeatCnt
		ser = serial.Serial(port=port, baudrate=baudRate, timeout=1)
		if cmd != '':
			ser.flushInput()
			ser.flushOutput()
		# loop thru cmd retries
		while True:
			cmdRepeatCnt -= 1
			repeatCnt = repeatCntSave
			outBuf = []
			Logger.debug('%%%% %s [%s]' % (cmd, port))
			if cmd != '':
				ser.write(bytes(cmd + '\r', 'utf-8'))
			time.sleep(initDelayRead)
			# loop thru read retries
			while True:
				cnt = 0
				while True:
					cnt += 1
					line = ser.readline().decode('latin-1', errors='ignore').strip()
					if len(line) > 0:
						filterLine = False
						if filterRE is not None:
							if not re.search(filterRE, line):
									filterLine = True
						if not filterLine:
							outBuf.append(line)
							try:
								Logger.debug('%d %s' % (cnt, line.encode('latin-1').decode()))
							except UnicodeDecodeError:
								Logger.debug('%d %s' % (cnt, line.encode('latin-1')))
					if not ser.inWaiting() or exitOnMatch:
						break
				for line in outBuf:
					if errorRE is not None:
						for expr in errorRE:
							if re.search(expr, line):
								Logger.debug('>>>> fail match: %s' % (line))
								Step_Fail('Matched fail expression [%s] - see Detail Log' % (expr), errorCode='ATE_SERIAL_MATCHED_FAIL_EXPR')
					if re.search(expect, line):
						ser.close()
						try:
							Logger.debug('>>>> match: %s' % (line.encode('latin-1').decode()))
						except:
							Logger.debug('>>>> match: %s' % (line.encode('latin-1')))
						if returnMatch:
							return line
						else:
							return outBuf
				repeatCnt -= 1
				if repeatCnt == 0 and cmdRepeatCnt == 0:
					ser.close()
					if noFail:
						return ''
					else:
						t = 'Command failed [%s] [%s] [%dc%ds:%dr%ds]' % (cmd, expect, cmdRepeatCntSave, cmdRepeatDelay, repeatCntSave, repeatDelay)
						Logger.debug(t)
						Step_Fail(t, errorCode='ATE_SERIAL_CMD_FAIL')
				elif repeatCnt > 0:
					Logger.debug('-- %d' % (repeatDelay))
					time.sleep(repeatDelay)
				else:
					Logger.debug('== %d' % (cmdRepeatDelay))
					time.sleep(cmdRepeatDelay)
					break
	except:
		Logger.debug('** Caught serial exception')
		try:
			ser.flushInput()
			ser.flushOutput()
			ser.close()
		except:
			pass
		raise
		
def Serial_Send_Binary(binStr, port=''):
	global ATEport
	try:
		if port == '':
			port = ATEport
		ser = serial.Serial(port=port, baudrate=115200, timeout=1)
		ser.flushInput()
		ser.flushOutput()
		ser.write(binStr)
		ser.close()
	except:
		Logger.debug('**Caught serial binary exception')
		ser.flushInput()
		ser.flushOutput()
		raise
		
def Serial_Exec_Binary_Cmd(cmd, expect, port='', repeatCnt=1, initDelayRead=0.25, repeatDelay=1, returnMatch=False, exitOnMatch=False, cmdRepeatCnt=1, cmdRepeatDelay=1, noFail=False):
	global ATEport
	try:
		if port == '':
			port = ATEport
		repeatCntSave = repeatCnt
		cmdRepeatCntSave = cmdRepeatCnt
		ser = serial.Serial(port=port, baudrate=115200, timeout=3)
		ser.flushInput()
		ser.flushOutput()
		# loop thru cmd retries
		while True:
			cmdRepeatCnt -= 1
			repeatCnt = repeatCntSave
			outBuf = []
			t = ('\\x%02X' % (ord(c)) for c in cmd)
			tCmd = ''.join(t)
			Logger.debug('$$ %s [%s]' % (tCmd, port))
			ser.write(cmd.encode())
			ser.write(bytes(chr(13),'utf-8'))
			time.sleep(initDelayRead)
			if expect == '':
				return None
			# loop thru read retries
			while True:
				cnt = 0
				while True:
					cnt += 1
					line = ser.readline().strip()
					if len(line) > 0:
						s = line.decode('latin-1')
						outBuf.append(s)
						Logger.debug('%d %s' % (cnt, s.encode('latin-1')))
						t = ('\\x%02X' % (ord(c)) for c in line.decode('latin-1'))
						Logger.debug('%s' % (''.join(t)))
					if not ser.inWaiting() or exitOnMatch:
						break
				for line in outBuf:
					if re.search(expect, line):
						ser.close()
						Logger.debug('>>>> match: %s' % (line.encode('latin-1')))
						if returnMatch:
							return line
						else:
							return outBuf
				repeatCnt -= 1
				if repeatCnt == 0 and cmdRepeatCnt == 0:
					ser.close()
					if noFail:
						return ''
					else:
						tExpect = ('\\x%02X' % (ord(c)) for c in expect)
						tExpect = ''.join(tExpect)
						t = 'Binary Command failed [%s] [%s] [%dc%ds:%dr%ds]' % (tCmd, tExpect, cmdRepeatCntSave, cmdRepeatDelay, repeatCntSave, repeatDelay)
						Logger.debug(t)
						Step_Fail(t, errorCode='ATE_SERIAL_BIN_CMD_FAIL')
				elif repeatCnt > 0:
					Logger.debug('-- %d' % (repeatDelay))
					time.sleep(repeatDelay)
				else:
					Logger.debug('== %d' % (cmdRepeatDelay))
					time.sleep(cmdRepeatDelay)
					break
	except:
		Logger.debug('** Caught serial exception')
		ser.close()
		raise
				
def Serial2_Exec_Cmd(cmd, expect, port='', repeatCnt=1, initDelayRead=0.25, repeatDelay=1, returnMatch=False, exitOnMatch=False, cmdRepeatCnt=1, cmdRepeatDelay=1, buf=100):
	global ATEport
	if port == '':
		port = ATEport
	repeatCntSave = repeatCnt
	cmdRepeatCntSave = cmdRepeatCnt
	ser = serial.Serial(port=port, baudrate=115200, timeout=1)
	ser.flushInput()
	ser.flushOutput()
	# loop thru cmd retries
	while True:
		cmdRepeatCnt -= 1
		repeatCnt = repeatCntSave
		outBuf = []
		Logger.debug('%%%% %s [%s]' % (cmd, port))
		ser.write(bytes(cmd + '\r', 'utf-8'))
		time.sleep(initDelayRead)
		# loop thru read retries
		while True:
			cnt = 0
			while True:
				cnt += 1
				line = ser.read(buf).decode()
				line = re.sub('\r', '\r\n', line)
				# line = ser.readline().decode().strip()
				if len(line) > 0:
					outBuf.append(line)
					Logger.debug('%d %s' % (cnt, line))
				if not ser.inWaiting() or exitOnMatch:
					break
			for line in outBuf:
				if re.search(expect, line):
					ser.close()
					Logger.debug('>>>> match: %s' % (line))
					if returnMatch:
						return line
					else:
						return outBuf
			repeatCnt -= 1
			if repeatCnt == 0 and cmdRepeatCnt == 0:
				ser.close()
				t = 'Command failed [%s] [%s] [%d cmds x %d sec] [%d reads x %d sec]' % (cmd, expect, cmdRepeatCntSave, cmdRepeatDelay, repeatCntSave, repeatDelay)
				Logger.debug(t)
				Step_Fail(t, errorCode='LMU_SERIAL2_CMD_FAIL')
			elif repeatCnt > 0:
				Logger.debug('-- %d' % (repeatDelay))
				time.sleep(repeatDelay)
			else:
				Logger.debug('== %d' % (cmdRepeatDelay))
				time.sleep(cmdRepeatDelay)
				break

def Serial_Get_ATE_Port():
	ports = Serial_List_Ports()
	global ATEport
	ATEport = ''
	for portRec in ports:
		if re.search('Prolific', portRec[1]):
			ATEport = portRec[0]
			Log('ATE port [%s]' % (ATEport))
	if ATEport == '':
		ATEport = 'COM1'
		Log('Default ATE port [%s]' % (ATEport))

def Serial_Get_Multimeter_Port():
	ports = Serial_List_Ports()
	global MultimeterPort
	MultimeterPort = ''
	for portRec in ports:
		if re.search('CP210x', portRec[1]):
			MultimeterPort = portRec[0]
	Log('Multimeter port [%s]' % (MultimeterPort))

def Serial_Get_FTDI_Port(descStr):
	port = None
	outbuf = subprocess.check_output('c:/CalAmpAte2/bin/FTDI/FTDI_Read.exe')
	for line in outbuf.decode().split('\r\n'):
		m = re.search('(\d*):%s' % (descStr), line)
		if m:
			port = 'COM' + m.group(1)
			Logger.debug('FTDI port found [%s]' % (line))
			return port
	return port

def Serial_Get_Port(idString):
	ports = Serial_List_Ports()
	for portRec in ports:
		if re.search(idString, portRec[1]):
			Log('Port [%s]' % (portRec[0]))
			return portRec[0]
	Step_Fail('Port not found', errorCode='ATE_SERIAL_PORT_NOT_FOUND')
		
def Serial_List_Ports():
	portList = list_ports.comports()
	return portList

def Signal_Handler(signal, frame):
	Power_Turn_Off()
	dt = datetime.datetime.now()
	Logger.debug('#### Cancel ATE script - %s ####' % (dt))
	sys.exit(0)
	
def Sleep(delay):
	Logger.debug('-- %d' % (delay))
	time.sleep(delay)
	
def Step_Fail(desc, errorCode='ATE_STEP_FAIL_GENERAL'):
	Logger.debug('Step Fail: %s' % (desc))
	Logger.debug('#### End ATE script - %s ####' % (datetime.datetime.now()))
	SendMsgToATE('LogError', desc)
	#Clear the relay box
	Relays_Clear_All()
	Power_Turn_Off()
	TestVals.ATEpass = False
	UUT.ErrorCode = errorCode
	UUT.ErrorText = re.sub('\\\\', '\\\\\\\\', desc)
	Test_End()

def Test_End(exitScript=True):
	# call back top level script Test_End()
	Callbacks.CallbackList[0]()
	Logger.debug('#### End ATE test - %s ####' % (datetime.datetime.now()))
	result = 'Pass' if TestVals.ATEpass else 'Fail'
	UUT.TestResult = result
	Mfg_Post_UUT_Result()
	Mfg_Post_Step_Details()
	SendMsgToATE('Result', result)
	if exitScript:
		sys.exit(0)
	
def Test_Start():
	global ATEport, Callbacks, ConfParser, PathModel, PathFiles, TestVals, UUT
	# uut vals
	UUT = _UUT()
	UUT.StartDate = datetime.datetime.now()
	# test vals
	TestVals = _TestVals()
	# Read parms exported from ATE
	f = open(gparms_file, 'r')
	rec = f.readline().strip()
	f.close()
	(TestVals.Model, TestVals.Part, TestVals.Cust, TestVals.Script, TestVals.bNoPower, TestVals.ATEver, TestVals.bJP1Log) = re.split(':', rec)
	# set paths
	PathModel = Dir_Models + TestVals.Model + '\\'
	PathFiles = PathModel + 'Files\\'
	# initialize config parser
	ConfParser = configparser.ConfigParser()
	ConfParser.read(PathModel + 'configs\\' + TestVals.Part + '.ini')
	# mfg server
	TestVals.MfgServer = Config_Get('ENV', 'Mfg Server')
	# visa logging at debug level by default?!
	visa.logger.setLevel(logging.CRITICAL)
	# start the logger
	Logger_Start()
	dt = datetime.datetime.now()
	Logger.debug('#### Start ATE script - %s ####' % (dt))
	# get version
	f = open('c:/CalAmpAte2/bin/version.py', 'r')
	rec = f.readline().strip()
	f.close()
	tmp = re.search('.*\'(.*)\'', rec).group(1)
	Logger.debug('ATE2 Version %s' % (tmp))
	TestVals.LibVersion = tmp
	# get model version
	f = open(PathModel + 'version.py', 'r')
	rec = f.readline().strip()
	f.close()
	tmp = re.search('.*\'(.*)\'', rec).group(1)
	Logger.debug('Model Version %s' % (tmp))
	TestVals.ModelVersion = tmp
	# set default ATE comm port
	ATEport = 'COM1'
	# Check if Relay I/O is available
	#BKMB
	#Note: Line 942 is showing an error please see email sent by Everardo PN: LMU42H20W-UTA01 
	#if Relays_Read() != -1:
		#Relays are present
	# read tests from UI
	i=0
	with open (Dir_ATE_DB + 'tests.txt', 'r') as f:
		for line in f:
			name, selected = line.split()
			UUT.Test_Selected[name] = selected
			if selected == 'False':
				Mfg_Add_Step_Detail(property='TEST%03d' % i, value=name, minRange='', maxRange='')
				i += 1
	Mfg_Add_Step_Detail(property='TEST DESELECT', value=i, minRange='', maxRange='')
	f.close()

	# setup callback structure
	Callbacks = _Callbacks()
	
	# following call causes exceptions to be caught by script
	sys.excepthook = Exceptions_Log_Uncaught
	# catch signals
	signal.signal(signal.SIGBREAK, Signal_Handler)

def UI_Update(field, val):
	SendMsgToATE('UiUpdate', '%s;%s' % (field, val))
	
def Username_Get():
	user = os.environ['USERNAME']
	return user

def UUID_Generate():
	UUID = uuid.uuid4()
	return UUID
	
def WorkStation_Config_Get():
	#BKMB - New function for Workstation information
	# location = 2 chars, ws num = 4 chars
	wsCfg = 'c:\\CalAmpATE\\Workstation\wsGroupID.cfg'
	# get workstation info
	f = open(wsCfg, 'r')
	line = f.readline()
	f.close()
	ws, loc = re.search('ID:(\d*).(\d*)', line).group(1,2)
	ws = ws.rjust(4, '0') 
	loc = loc.rjust(2, '0')
	return loc, ws
	
def Workstation_MAC_Addr_Get():
	outbuf = subprocess.check_output('ipconfig /all')
	for line in outbuf.decode().split('\r\n'):
		m = re.search('(\d*)\s*.*Tcpip' % (descStr), line)
		if m:
			port = 'COM' + m.group(1)
			Logger.debug('FTDI port found [%s]' % (line))
			return port
	return macAddr
	
def Workstation_Name_Get():
	ws_name = os.environ['COMPUTERNAME']
	return ws_name.upper()

def Workstation_Num_Get():
	ws_name = os.environ['COMPUTERNAME']
	ws_num_str = re.sub('.*-', '', ws_name)
	try:
		ws_num = int(ws_num_str)
	except:
		ws_num = 9999
	return ws_num

