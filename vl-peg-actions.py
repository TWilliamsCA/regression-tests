import json, logging, re
import DeviceVal
import TestCase
from TestCase import *
from Serial import *

Logger = logging.getLogger('DeviceVal_Logger.Test_Cases')

########################################
### PEG Triggers
###

class PEG_Actions:

	Name = 'PEG Actions'

	TestList = [
		('action_0_none', 'None'),
		('action_1_send_report', 'Send event report'),
		('action_2_log_report', 'Log event report'),
		('action_3_send_log_report', 'Send/log event report'),
		('action_4_send_log', 'Send log event report and clear buffer'),
		('action_5_clear_log', 'Delete all logged records'),
		('action_20_power_up_gps', 'Power-up GPS'),
		('action_8_set_output', 'set output'),
		('action_9_clear_output', 'clear output'),
		('action_18_Increment_accumulator', 'increment accumulator'),
		('action_24_sleep_time_of_day', 'sleep till time of day'),
		('action_26_start_time_accumulator', 'time accumulator'),
		('action_28_stop_accumulator', 'stop acummulator'),
		('action_29_stop_clear_accumulator', 'stop and clear acummulator'),
		('action_86_start_RSSI_accumulator', 'start rssi accumulator')

	]

	TestList = [
		('action_5_clear_log', 'Delete all logged records'),
		('action_8_set_output', 'set output'),
		('action_9_clear_output', 'clear output'),
		('action_18_Increment_accumulator', 'increment accumulator'),
		('action_24_sleep_time_of_day', 'sleep till time of day'),
		('action_26_start_time_accumulator', 'time accumulator'),
		('action_28_stop_accumulator', 'stop acummulator'),
		('action_29_stop_clear_accumulator', 'stop and clear acummulator'),
		('action_86_start_RSSI_accumulator', 'start rssi accumulator')
	]

	
	@Test_Case
	@Debug
	def action_0_none():
		Serial_Cmd('AT$APP PEG ACTION 0 0', 'Action.*Not Active')
	
	@Test_Case
	@Debug
	def action_1_send_report():
		Serial_Cmd('AT$APP PEG ACTION 1 11', 'Code=11.*Dispo=Send')
		# check report at server
	
	@Test_Case
	@Debug
	def action_2_log_report():
		Serial_Cmd('AT$APP PEG ACTION 2 22', 'Code=22.*Dispo=Log')
		# check report at server
	
	@Test_Case
	@Debug
	def action_3_send_log_report():
		Serial_Cmd('AT$APP PEG ACTION 3 33', 'Code=33.*Dispo=SndLog')
		# check report at server
	
	@Test_Case
	@Debug
	def action_4_send_log():
		Serial_Cmd('AT$APP PEG ACTION 4 1', 'Initiate Log Send')
		# check report at server.  action 3 = two separate logs
		# check buffer cleared
	
	@Test_Case
	@Debug
	def action_5_clear_log():
		try:
			Serial_Cmd('at$app log fill 22', '^OK')  
		except:
			Serial_Cmd('at$app log qfill 22', '^OK')
		Serial_Cmd('at$app log?', '22 Records', filter='Log Status', cmdRetryCnt=15)
		Serial_Cmd('AT$APP PEG ACTION 5 0', '^OK')
		Serial_Cmd('at$app log?', '0 Records', filter='Log Status', cmdRetryCnt=15)
		
		# check buffer cleared
		
		
	@Test_Case
	#@Debug
	def action_8_set_output():
		output = Serial_Cmd('AT$APP output?', 'OK', retBuf=True, filter='.*')
		numOutput = len(output) - 2		
		for i in range(numOutput):
			if i is 4:
				continue
			Serial_Cmd('AT$APP PEG ACTION 8 %d' % (i), '^OK', cmdRetryDelay=3)
			
		output = Serial_Cmd('AT$APP output?', 'OK', retBuf=True, filter='.*')
		w = 0	
		print('!!!!!!!!!!!!!!!!!!!!!!!!!')
		for a in output:
			w = w + 1
			if 'OUTPUT-4' in a:
				print('OUTPUT 4- Power switch. Not Testing')
				ATE2.Log('OUTPUT 4 - Power Switch. Not Testing')
				continue
			if 'OUTPUT' in a:
				if 'State=1' in a:
					ATE2.Log('Output [%s] pass'% str(w-2))
					print('Output [%s] pass'% str(w-2))
				else:
					TC_Fail('Output [%s] failed to set'% str(w-2))

		output = Serial_Cmd('AT$APP output?', 'OK', retBuf=True, filter='.*')
		print(output)
					
	@Test_Case
	#@Debug
	def action_9_clear_output():
		output = Serial_Cmd('AT$APP output?', 'OK', retBuf=True, filter='.*')
		numOutput = len(output) - 2		
		for i in range(numOutput):
			if i is 4:
				continue
			Serial_Cmd('AT$APP PEG ACTION 9 %d' % (i), '^OK', cmdRetryDelay=3)
			
		output = Serial_Cmd('AT$APP output?', 'OK', retBuf=True, filter='.*')
		w = 0	
		print('!!!!!!!!!!!!!!!!!!!!!!!!!')
		for a in output:
			w = w + 1
			if 'OUTPUT-4' in a:
				print('OUTPUT 4- Power switch. Not Testing')
				ATE2.Log('OUTPUT 4 - Power Switch. Not Testing')
				continue
			if 'OUTPUT' in a:
				if 'State=0' in a:
					ATE2.Log('Output [%s] pass'% str(w-2))
					print('Output [%s] pass'% str(w-2))
				else:
					TC_Fail('Output [%s] failed to clear'% str(w-2))
			
		output = Serial_Cmd('AT$APP output?', 'OK', retBuf=True, filter='.*')
		print(output)

	@Test_Case
	@Debug
	def action_18_Increment_accumulator():	
		for i in range(32):
			Serial_Cmd('AT$APP PEG ACTION 29 %s' % (str(i)), 'OK')
		accumulator = Serial_Cmd('AT$APP acc?', 'OK', retBuf=True, filter='.*')
		
		for i in range(32):
			Serial_Cmd('AT$APP PEG ACTION 18 %s' % (str(i)), 'OK')
		accumulator = Serial_Cmd('AT$APP acc?', 'OK', retBuf=True, filter='.*')

		a = 0
		for line in accumulator:
			if a == 0 or a == 1 or a == 34: 
				a = a +1 
				continue
			acc, value, limit = line.split()
			if(int(value) != 1):
				TC_Fail('Accumulator[%s] failed to increment' % (acc))
			a = a +1
			
	@Test_Case
	@Debug
	def action_20_power_up_gps():
		buf = Serial_Cmd('AT$APP PEG ACTION 20 1', '^OK' , retBuf=True)
		Serial_Match('<GPS Lost>')
		Serial_Match('<GPS Quality Lost>')
		Serial_Match('<GPS Acquire>')
		Serial_Match('<GPS Quality Fix>')
		
	@Test_Case
	@Debug
	def action_24_sleep_time_of_day():
		for i in range(4):
			tod = DeviceVal.Calc_TOD_With_Offset(10)
			print(tod)
			Serial_Cmd('AT$APP PARAM 267,%s,%s' % (str(i),tod), 'OK')  # current time + delta
			Serial_Cmd('AT$APP PEG ACTION 24 %s' % (str(i)), 'OK')  # device sleep and wakeup on timer 0
			Serial_Cmd('', 'Sleep.*sec', rdRetryCnt=45)
			Serial_Cmd('', 'VERSION', rdRetryCnt=60)
			print('index %s passed' % (str(i)))
			DeviceVal.Sleep(15)
			
	@Test_Case
	@Debug
	def action_26_start_time_accumulator():
		Serial_Cmd('AT$APP PEG ACTION 26 0', '^OK')
		DeviceVal.Sleep(10)
		acc, value, limit, type =(Serial_Cmd('AT$APP ACC?', '^\s*0\s*').split())
		if int(value) < 5:
			TC_Fail('accumulator value is less than expected')
		else:
			ATE2.Log('Accumulator is as expected')
			
	@Test_Case
	@Debug
	def action_28_stop_accumulator():
		Serial_Cmd('AT$APP PEG ACTION 26 0', '^OK')
		DeviceVal.Sleep(10)
		Serial_Cmd('AT$APP PEG ACTION 28 0', '^OK')
		acc, value, limit =(Serial_Cmd('AT$APP ACC?', '^\s*0\s*').split())
		print(value)
		DeviceVal.Sleep(5)
		acc1, value1, limit1 =(Serial_Cmd('AT$APP ACC?', '^\s*0\s*').split())
		print(value1)
		if int(value) == int(value1):
			ATE2.Log('Accumulator stopped')
		else:
			TC_Fail('Accumulator failed to stopped')
			
	@Test_Case
	@Debug
	def action_29_stop_clear_accumulator():
		Serial_Cmd('AT$APP PEG ACTION 26 0', '^OK')
		DeviceVal.Sleep(10)
		Serial_Cmd('AT$APP PEG ACTION 28 0', '^OK')
		acc, value, limit =(Serial_Cmd('AT$APP ACC?', '^\s*0\s*').split())
		print(value)
		
	@Test_Case
	@Debug
	def action_86_start_RSSI_accumulator():
		atic = Serial_Cmd('ATIC', '^OK', retBuf=True, filter='.*')
		rssi = int(Serial_Match('^RSSI.*:\s*([-\d]*)', atic))
		Serial_Cmd('AT$APP PEG ACTION 86 0', '^OK')
		newRssi = rssi + 200;
		acc, value, limit, type =(Serial_Cmd('AT$APP ACC?', '^\s*0\s*').split())
		print(value)
		print(newRssi)
		if int(value) != int(newRssi):
			TC_Fail('RSSI values in accumulator different than expected')
		else:
			ATE2.Log('RSSI value same as accumulator value')
		

Run_Tests(PEG_Actions)
