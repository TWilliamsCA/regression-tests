import clr, sys, wpf
clr.AddReference('PresentationFramework')
clr.AddReference('PresentationCore')
clr.AddReference('WindowsBase')
clr.AddReference('System.Windows.Presentation')

from System.Windows import Window
from System.Windows import Application, MessageBox, Window, Media

class DialogWindow(Window):
	def __init__(self): 
		self.Title = sys.argv[0]
		wpf.LoadComponent(self, 'Dlg_General.xaml')
		self.DlgText.Text = sys.argv[0]
		self.DlgBtnOK.Focus()

	def OK_OnClick(self, sender, e):
		sender.Content="This has been clicked"
		
		#exit(1)

	def Fail_OnClick(self, sender, e):
		exit(0)

if __name__ == '__main__':
	w = DialogWindow()
	Application().Run(w)
